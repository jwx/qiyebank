﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forget.aspx.cs" Inherits="web.forget" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <style type="text/css">
        body, html { margin: 0px; font-family: 'Microsoft YaHei'; }
        .top { margin: 0 auto; background-color: #F0F0F0; height: 100px; }
        .topc { width: 1000px; margin: 0px auto; }
        .toplogo { padding: 20px 0px; line-height: 50px; font-size: 20px; }
        .fgx { height: 1px; background-color: #c3c3c3; }
        .btn { margin-top: 18px; width: 120px; background-color: #FF4400; border: 0px; height: 36px; line-height: 32px; font-size: 14px; color: #FFFFFF; border-radius: 3px; padding: 0px; font-weight: bold; cursor: pointer; }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    var username = $.trim($("#txtUserName").val());
                    if (username == "") {
                        layer.msg("请输入登录名！", { icon: 5, time: 1000 });
                    }
                    var res = ajax.SendMail(username).value;
                    if (res.indexOf("@") != -1) {
                        layer.msg("邮件已成功发送至邮箱" + res, { icon: 6, time: 2500 });
                    } else {
                        layer.msg("" + res + "", { icon: 5, time: 2500 });
                    }
                    return false;
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="top">
            <div class="topc">
                <div class="toplogo">
                    <img style="height: 50px; vertical-align: middle;" src="<%=string.IsNullOrEmpty(modelSettings.logo)?"/images/logo_jingyi.png":modelSettings.logo %>" />
                    <span>找回密码 </span>
                </div>
            </div>
        </div>
        <div class="fgx"></div>
        <div style="width: 1000px; margin: 0 auto;">
            <div style="width: 630px; margin: 50px auto; background: url(images/attention.png) no-repeat left center; padding: 35px;">
                请输入你需要找回登录密码的账户名
            </div>
            <div style="width: 700px; margin: 0 auto;">
                <span style="margin-left: 40px;">登录名:</span>
                <asp:TextBox runat="server" ID="txtUserName" placeholder="请输入您的登录名" datatype="*" Style="border: 1px solid #D7D7D7; border-radius: 2px; height: 18px; padding: 6px 0; line-height: 14px; padding-left: 5px; font-size: 13px; width: 240px;" />
                <span class="Validform_checktip">请输入您的登录名</span>
            </div>
            <div style="width: 508px; margin: 35px auto;">
                <asp:Button runat="server" ID="btnSave" CssClass="btn" Text="确定" />
            </div>
        </div>
    </form>
</body>
</html>
