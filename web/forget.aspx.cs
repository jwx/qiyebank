﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{
    public partial class forget : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(this.GetType());

            modelSettings = new BLL.Z_Settings().GetModel_Top();
            modelSettings = modelSettings == null ? new Model.Z_Settings() { name = "精益软件管理系统", logo = "/images/jingyi_logo.png" } : modelSettings;
        }
        protected Model.Z_Settings modelSettings;

        [AjaxPro.AjaxMethod]
        public string SendMail(string username)
        {
            Model.Z_Users modelUser = new BLL.Z_Users().Login(username);
            if (modelUser != null && modelUser.id != 0)
            {
                if (string.IsNullOrEmpty(modelUser.email))
                {
                    return "系统检测到您尚未维护邮箱，请联系管理员重置密码！";
                }
                else
                {
                    Model.Z_Settings modelSettings = new BLL.Z_Settings().GetModel_Top();
                    if (!string.IsNullOrEmpty(modelSettings.smtp) && !string.IsNullOrEmpty(modelSettings.email) && !string.IsNullOrEmpty(modelSettings.pswd))
                    {
                        Tool.MailHelper mail = new Tool.MailHelper(modelSettings.smtp, modelSettings.email, modelSettings.pswd);
                        string mailbody = "<html><body><div> 您好，" + modelUser.realname + "，您正在使用密码取回功能，原有系统密码为：" + Falcon.Function.Decrypt(modelUser.userpswd) + "，如需修改，请登录系统后操作。请勿回复此邮件！</div></body></html>";
                        if (mail.SendMail(modelUser.email, null, "密码找回", mailbody, null))
                        {
                            //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('邮件已发送，请登录您在本系统的预留邮箱查看！',{icon:6,time:5000});", true);
                            return modelUser.email;
                        }
                        else
                        {
                            return "邮件发送失败，请重新操作或联系管理员重置密码！";
                        }
                    }
                    else
                    {
                        return "系统检测到本系统未维护邮箱信息，请联系管理员重置密码！";
                    }
                }
            }
            else
            {
                return "未查询到此用户信息！";
            }
        }
    }
}