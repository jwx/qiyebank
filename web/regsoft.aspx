﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="regsoft.aspx.cs" Inherits="web.regsoft" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .content { width: 400px; margin: 50px auto; text-align: center; border: 1px solid #cccccc; }
            .content .info { font-size: 24px; }
            .content div { margin: 20px auto; }
                .content div input[type='text'] { border: 1px solid #cccccc; padding: 5px 3px; width: 300px; }
    </style>
    <script src="Scripts/jquery-1.11.3.min.js"></script>
    <script src="Plug/layer/layer.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <div class="info">
                您的软件还没有注册
            </div>
            <div class="divinput">
                机器码：<asp:TextBox ID="txtJiqi" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="divinput">
                注册码：<asp:TextBox ID="txtZhuce" runat="server" CssClass="form-control" placeholder="请输入注册码" nullmsg="请输入注册码！"></asp:TextBox>
            </div>
            <div class="divbtn" style="text-align: center; padding-bottom: 30px;">
                <asp:Button ID="btnLogin" runat="server" Text="点击注册" CssClass="btnlogin" OnClick="btnLogin_Click" Style="width: auto; height: 36px; line-height: 36px; padding: 0 20px; font-size: 14px; border: none; background-image: none; background-color: #36B7F9; color: #fff; cursor: pointer; overflow: hidden; border-radius: 2px; font-family: 微软雅黑; cursor: pointer;" />
            </div>
            <div class="copyright" style="margin-top: 10px;">
                请联系：<a href="http://www.jingyisoft.com" target="_blank">临沂精益软件有限公司</a>
            </div>
        </div>
    </form>
</body>
</html>
