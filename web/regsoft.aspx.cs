﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{
    public partial class regsoft : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtJiqi.Text = Tool.CpuHelper.GetCpuInfo();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (new BLL.Z_Settings().Update_Cpu(txtZhuce.Text.Trim()))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('注册成功！',{icon:6,time:1500},function(){window.location.href='login.aspx'})", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('注册失败！',{icon:5,time:1500})", true);
            }
        }
    }
}