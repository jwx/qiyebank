﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="web.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit" />
    <title><%=modelSettings.name %></title>
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <link href="/Css/jingyi.css" rel="stylesheet" />
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="Scripts/jquery.cookie.js"></script>
    <script type="text/javascript">
        $(function () {
            $.post("/verify.aspx", function () {
                $(".verifycode").attr("src", "/verify.aspx?t=" + new Date());
            });

            $("#changeVerify,.verifycode").click(function () {
                $(".verifycode").attr("src", "/verify.aspx?t=" + new Date());
            });

            $("form").Validform({
                tiptype: 3,
                showAllError: false,
                beforeCheck: function (curform) {
                    //在表单提交执行验证之前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话将不会继续执行验证操作;	
                    var username = $.trim($("#txtUserName").val());
                    if (username == "") {
                        layer.msg("请输入登录账号！", { icon: 5, time: 1500 });
                        $("#txtUserName").focus();
                        return false;
                    }
                    var password = $.trim($("#txtPassword").val());
                    if (password == "") {
                        layer.msg("请输入登录密码！", { icon: 5, time: 1500 });
                        $("#txtPassword").focus();
                        return false;
                    }
                    var verify = $.trim($("#txtVerifyCode").val());
                    if (verify == "") {
                        layer.msg("请输入验证码！", { icon: 5, time: 1500 });
                        $("#txtVerifyCode").focus();
                        return false;
                    }

                    var v = $.cookie("Verify").toString().toLowerCase();
                    var txtVerify = verify.toLowerCase();
                    if (v != txtVerify) {
                        layer.msg("验证码不正确！", { icon: 5, time: 1500 }, function () {
                            $("#changeVerify").click();
                        });
                        $("#txtVerifyCode").focus();
                        return false;
                    }
                }, beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;	

                    var username = $.trim($("#txtUserName").val());
                    var password = $.trim($("#txtPassword").val());

                    var res = ajax.Login(username, password).value;
                    if (res == "1") {
                        layer.msg("登录成功！", { icon: 6, time: 1000 }, function () {
                            window.location.href = ajax.LoginSuccess().value;
                        });
                    } else {
                        layer.msg("" + res + "", { icon: 5, time: 1500 });
                    }
                    return false;
                }
            });

            $("#QQ_left").click(function () {
                $("#QQ_right").toggle(200);
            });

        });
    </script>
    <style type="text/css">
        #QQ { top: 30%; height: 200px; right: 0px; position: fixed; z-index: 1000; _position: absolute; _right: 0; }
            #QQ #QQ_left { background: url("/images/guding_03.png") no-repeat; width: 24px; height: 66px; margin-top: 30px; float: left; cursor: pointer; }
            #QQ #QQ_right { background: url("/images/tg_hei.jpg") no-repeat; width: 108px; height: 199px; float: left; }
        .android { position: absolute; left: 5px; top: 200px; width: 120px; height: 120px; }
            .android img { width: 120px; height: 120px; display: block; }
            .android div { font-size: 12px; text-align: center; }
        .ios { position: absolute; left: 5px; top: 380px; width: 120px; height: 120px; }
            .ios img { width: 120px; height: 120px; display: block; }
            .ios div { font-size: 12px; text-align: center; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="android">
            <img src="/images/a.png" />
            <div>安卓客户端下载地址</div>
        </div>
        <div class="ios">
            <img src="/images/p.png" />
            <div>苹果客户端下载地址</div>
        </div>
        <div id="QQ">
            <div id="QQ_left"></div>
            <div id="QQ_right">
                <div style="width: 108px; height: 38px;"></div>
                <div style="width: 108px; height: 88px;">
                    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=706994177&site=qq&menu=yes">
                        <img src="/images/77.jpg" /></a>
                </div>
                <div style="width: 108px; height: 77px;">
                    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=263852110&site=qq&menu=yes">
                        <img src="/images/71.jpg" />
                    </a>
                </div>
            </div>
        </div>
        <div class="login">
            <div class="login_title">
                <div class="login_title_banner">
                    <img src="<%=string.IsNullOrEmpty(modelSettings.logo)?"/images/logo_jingyi.png":modelSettings.logo %>" />
                </div>
            </div>
            <div class="login_area">
                <img src="<%=string.IsNullOrEmpty(modelSettings.image)?"/images/logo_login.jpg":modelSettings.image %>" class="login_image" />
                <div class="login_module">
                    <div class="login_wrap">
                        <div class="login_name">
                            <%=modelSettings.name %>
                        </div>
                        <div class="login_data">
                            <div class="item">
                                <div class="block">
                                    登录账号： 
                                </div>
                                <asp:TextBox ID="txtUserName" runat="server" placeholder="请输入登录账号" datatype="*" TabIndex="1"></asp:TextBox>
                            </div>
                            <div class="item">
                                <div class="block">
                                    <span>登录密码：</span>
                                    <a href="javascript:void(0);">忘记密码？</a>
                                    <div class="clear"></div>
                                </div>
                                <asp:TextBox ID="txtPassword" runat="server" placeholder="请输入登录密码" datatype="*" TextMode="Password" TabIndex="2"></asp:TextBox>
                            </div>
                            <div class="item">
                                <div class="block clearfix">
                                    <span>验证码：</span>
                                    <a href="javascript:void(0);" id="changeVerify">看不清，换一张</a>
                                </div>
                                <div class="clear"></div>
                                <div class="div_verify">
                                    <asp:TextBox ID="txtVerifyCode" runat="server" CssClass="verify" placeholder="请输入验证码" datatype="*" TabIndex="3"></asp:TextBox>
                                    <img src="/images/loading.gif" alt="验证码" class="verifycode" />
                                </div>
                            </div>
                            <div>
                                <asp:Button ID="btnLogin" runat="server" CssClass="btnlogin" Text="登 录" TabIndex="4" OnClick="btnLogin_Click" />
                            </div>
                            <div class="support">
                                技术支持：<a href="http://www.jingyisoft.com" target="_blank">临沂精益软件有限公司</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
