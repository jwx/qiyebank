﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;

namespace web
{
    /// <summary>
    /// apicloud 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class apicloud : System.Web.Services.WebService
    {
        #region 基础
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        [WebMethod]
        public string Login(string username, string userpswd)
        {
            Model.Service<Model.Users_Mobile> jsonModel = new Model.Service<Model.Users_Mobile>();
            Model.Users_Mobile data = new Model.Users_Mobile();

            Model.UserInfo model = new BLL.UserInfo().Login(username);
            if (model == null || model.id == 0)
            {
                jsonModel.error = "用户不存在！";
            }
            else
            {
                if (model.isDel == 1)
                {
                    jsonModel.error = "账号已销户！";
                }
                else if (model.loginpswd != Falcon.Function.Encrypt(userpswd))
                {
                    jsonModel.error = "登录失败！";
                }
                else
                {
                    data.id = model.id;
                    data.avatar = string.IsNullOrEmpty(model.avatar) ? (Falcon.UrlPath.GetRootPath() + "images/logo.jpg") : model.avatar;
                    data.realname = model.realname;
                    data.username = model.username;
                    jsonModel.success = "登录成功！";
                }
            }
            jsonModel.data = data;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Users_Mobile>>(jsonModel);
        }

        private int GetPageCount(int recordCount, int pageSize)
        {
            return recordCount % pageSize == 0 ? recordCount / pageSize : (recordCount / pageSize + 1);
        }

        [WebMethod]
        public string GetSystemName()
        {
            Model.Z_Settings modelSettings = new BLL.Z_Settings().GetModel_Top();
            if (modelSettings != null && !string.IsNullOrEmpty(modelSettings.name))
            {
                return modelSettings.name;
            }
            return "企业内部银行账户登录";
        }

        //[WebMethod]
        //public string GetListUsers(string pageIndex, string pageSize, string where)
        //{
        //    Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

        //    int pageindex = Falcon.Function.ToInt(pageIndex);
        //    pageindex = pageindex <= 0 ? 1 : pageindex;//分页最小为1页
        //    int pagesize = Falcon.Function.ToInt(pageSize);
        //    pagesize = pagesize <= 0 ? 10 : pagesize;//单页数据量最小为10条
        //    int recordCount = 0, pageCount = 0;
        //    string filter = "1=1";
        //    if (!string.IsNullOrEmpty(where.Trim()))
        //    {
        //        filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
        //    }
        //    List<Model.Z_Users> list = Tool.Pager.Query<Model.Z_Users>(pageindex, pagesize, out recordCount, out pageCount, "Z_Users", "id,username,realname", filter, "id desc", 3);

        //    if (list == null || list.Count == 0)
        //    {
        //        jsonList.error = "暂无数据！";
        //    }
        //    else
        //    {
        //        List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
        //        foreach (Model.Z_Users one in list)
        //        {
        //            listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.username, subTitle = one.realname });
        //        }
        //        jsonList.data = listView_Mobile;
        //        jsonList.pc = pageCount;
        //        jsonList.rc = recordCount;
        //    }
        //    return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        //}

        //[WebMethod]
        //public string Update(string id)
        //{
        //    return id;
        //}
        #endregion

        #region 首页
        /// <summary>
        /// 未完待续
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetMoney(int uid)
        {
            Model.Service<Model.Mobile.Money> jsonModel = new Model.Service<Model.Mobile.Money>();
            jsonModel.data = new Model.Mobile.Money();

            Model.Setup modelSetup = new BLL.Setup().Get_Setup();
            if (modelSetup == null || modelSetup.isSaved == 0)
            {
                if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))
                {
                    jsonModel.error = "系统未设置活期利率！";
                }
                else if (string.IsNullOrEmpty(modelSetup.dq_lilv))
                {
                    jsonModel.error = "系统未设置定期利率！";
                }
                else
                {
                    jsonModel.error = "系统未保存设置！";
                }
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                string strDtNow = dtNow.ToString("yyyy-MM-dd");

                decimal huoqi = 0;
                decimal huoqi_lixi = 0;
                List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(uid, modelSetup.hq_houcunxianqu);

                if (listHuoqi != null && listHuoqi.Count != 0)
                {
                    huoqi = (from m in listHuoqi select m.jine).Sum();

                    if (modelSetup.hq_autoCalc == 1 && modelSetup.hq_byDay == 2)//每日结算利息
                    {
                        huoqi_lixi = new BLL.Huoqi().GetLixiByOrder_Yesterday(uid, dtNow);
                    }
                    else
                    {
                        //获取非今日存款数据
                        listHuoqi = (from m in listHuoqi where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") != strDtNow select m).ToList();
                        foreach (Model.Huoqi one in listHuoqi)
                        {
                            huoqi_lixi += new BLL.Huoqi().GetLixiByOrder_Yesterday(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        }
                    }
                }
                jsonModel.data.huoqi = huoqi.ToString("0.00");
                jsonModel.data.huoqi_shouyi = huoqi_lixi.ToString("0.00");


                decimal dingqi = 0;
                decimal dingqi_lixi = 0;
                List<Model.Dingqi> listDingqi = new BLL.Dingqi().GetList_Qukuan(uid);
                if (listDingqi != null && listDingqi.Count != 0)
                {
                    dingqi = (from m in listDingqi select m.jine).Sum();
                    listDingqi = (from m in listDingqi where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") != strDtNow select m).ToList();
                    foreach (Model.Dingqi one in listDingqi)
                    {
                        dingqi_lixi += new BLL.Dingqi().GetLixi_Yesterday(modelSetup, one, dtNow);
                    }
                }

                jsonModel.data.dingqi = dingqi.ToString("0.00");
                jsonModel.data.dingqi_shouyi = dingqi_lixi.ToString("0.00");

                jsonModel.data.all = (huoqi + dingqi).ToString("0.00");
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Mobile.Money>>(jsonModel);
        }

        /// <summary>
        /// 获取用户账户信息，包含活期和定期余额及系统设置信息
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetAccountInfo(int uid)
        {
            Model.Service<Model.Mobile.Money> jsonModel = new Model.Service<Model.Mobile.Money>();
            jsonModel.data = new Model.Mobile.Money();

            Model.Setup modelSetup = new BLL.Setup().Get_Setup();
            if (modelSetup == null || modelSetup.isSaved == 0)
            {
                if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))
                {
                    jsonModel.error = "系统未设置活期利率！";
                }
                else if (string.IsNullOrEmpty(modelSetup.dq_lilv))
                {
                    jsonModel.error = "系统未设置定期利率！";
                }
                else
                {
                    jsonModel.error = "系统未保存设置！";
                }
            }
            else
            {
                decimal huoqi = new BLL.Huoqi().GetYue(uid);
                decimal dingqi = new BLL.Dingqi().GetYue(uid);
                jsonModel.data.huoqi = huoqi.ToString("0.00");
                jsonModel.data.dingqi = dingqi.ToString("0.00");
                jsonModel.data.all = (huoqi + dingqi).ToString("0.00");

                jsonModel.data.modelSetup = modelSetup;
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Mobile.Money>>(jsonModel);
        }
        #endregion

        #region 活期
        /// <summary>
        /// 获取活期列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetListHuoqi(int pageIndex, int pageSize, string where, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "delid=0 and uid=" + uid;
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.Mobile.ListHuoqi> list = Tool.Pager_Sqlite.Query<Model.Mobile.ListHuoqi>(pageIndex, pageSize, out recordCount, "Huoqi", "id,danhao,yuanjine,jine,cunkuanTime", filter, "id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.Mobile.ListHuoqi one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.danhao, subTitle = one.cunkuanTime, remark = one.yuanjine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        /// <summary>
        /// 获取活期明细
        /// </summary>
        /// <param name="id"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetHuoqiInfo(int id, int uid)
        {
            Model.Service<Model.Huoqi> jsonModel = new Model.Service<Model.Huoqi>();
            Model.Huoqi model = new BLL.Huoqi().GetModel(id, uid);
            if (model != null && model.id != 0)
            {
                model.chs_money = model.yuanjine.ToString("0.00");
                model.str_jine = model.jine.ToString("0.00");
                model.type = ((Config.Enums.Huoqi_Laiyuan)model.laiyuan).ToString();//交易类型
                model.lixi = model.zonglixi.ToString("0.00");
                //return Tool.JsonHelper.JsonSerializer<Model.Huoqi>(model);
            }
            else
            {
                jsonModel.error = "没有数据！";
            }
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Huoqi>>(jsonModel);
        }
        ///// <summary>
        ///// 获取活期明细
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="uid"></param>
        ///// <returns></returns>
        //[WebMethod]
        //public string GetHuoqiInfo(int id, int uid)
        //{
        //    Model.Huoqi model = new BLL.Huoqi().GetModel(id, uid);
        //    if (model != null && model.id != 0)
        //    {
        //        model.chs_money = model.yuanjine.ToString("0.00");
        //        model.str_jine = model.jine.ToString("0.00");
        //        model.type = ((Config.Enums.Huoqi_Laiyuan)model.laiyuan).ToString();//交易类型
        //        model.lixi = model.zonglixi.ToString("0.00");
        //        return Tool.JsonHelper.JsonSerializer<Model.Huoqi>(model);
        //    }
        //    return " ";
        //}

        /// <summary>
        /// 获取用户活期余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetHuoqi(int uid)
        {
            return new BLL.Huoqi().GetYue(uid).ToString("0.00");
        }

        /// <summary>
        /// 获取活期利率
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetHuoqiLilv()
        {
            //hq_byDay,hq_lilv,hq_daylilv,days
            Model.Setup modelSetup = new BLL.Setup().Get_HuoqiLilv();
            return modelSetup == null ? " " : Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
        }

        /// <summary>
        /// 获取用户取款金额产生的利息，返回null或包含利息和取款单据的json数据
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="qukuan"></param>
        /// <param name="strSetup"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetHuoqiLixi(int uid, decimal qukuan, string strSetup)
        {
            return new BLL.Huoqi().GetLixi(uid, qukuan, strSetup);
        }

        [WebMethod]
        public string GetHuoqiZhuanRuInfo(string uid)
        {
            Model.Service<Model.HuoqiInInfo> model = new Model.Service<Model.HuoqiInInfo>();
            model.data = new Model.HuoqiInInfo();
            Model.Z_Settings modelSettings = new BLL.Z_Settings().GetModel_Top();
            if (modelSettings != null)
            {
                model.data.zhifubao = modelSettings.keywords;
                model.data.zhifubao_uname = modelSettings.description;
            }
            else
            {
                model.error = "读取收款支付宝信息失败！";
            }
            Model.UserInfo modelUserInfo = new BLL.UserInfo().GetModel(Falcon.Function.ToInt(uid.Replace("uid_", "")));
            if (modelUserInfo != null && modelUserInfo.id != 0)
            {
                model.data.zhifubao_pay = modelUserInfo.zhifubao;
                model.data.realname = modelUserInfo.realname;
                model.data.mobile = modelUserInfo.mobile;
            }
            Model.Setup modelSetup = new BLL.Setup().Get_Setup();
            if (modelSetup != null)
            {
                model.data.modelSetup = modelSetup;
            }
            else
            {
                model.error = "读取系统设置失败！";
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.HuoqiInInfo>>(model);
        }

        [WebMethod]
        public string AddHuoqi(int uid, decimal jine, string zhifubao, string realname, string mobile, string remark, string setup)
        {
            DateTime dtNow = DateTime.Now;
            return new BLL.HuoqiIn().Add(uid, jine, zhifubao, realname, mobile, setup, dtNow.ToString("yyyy-MM-dd HH:mm:ss"), dtNow.ToString("yyyy-MM-dd"), remark).ToString();
        }

        /// <summary>
        /// 获取提现申请列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetList_HuoqiIn(int pageIndex, int pageSize, string where, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "uid=" + uid;
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.HuoqiIn> list = Tool.Pager_Sqlite.Query<Model.HuoqiIn>(pageIndex, pageSize, out recordCount, "V_HuoqiIn_Info", "id,jine,addTime,oid", filter, "id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.HuoqiIn one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.addTime, subTitle = one.oid == 0 ? "审核中" : (one.oid == -1 ? "审核未通过" : "审核通过"), remark = one.jine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        [WebMethod]
        public string GetHuoqiInInfo(int id, int uid)
        {
            Model.Service<Model.HuoqiIn> jsonModel = new Model.Service<Model.HuoqiIn>();

            Model.HuoqiIn model = new BLL.HuoqiIn().GetModel(id, uid);
            if (model == null || model.id == 0)
            {
                jsonModel.error = "转入记录不存在！";
            }
            else
            {
                model.str_jine = model.jine.ToString("0.00");
                model.str_oid = model.oid == 0 ? "待审核" : (model.oid == -1 ? "审核未通过" : "审核通过");
            }
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.HuoqiIn>>(jsonModel);
        }

        /// <summary>
        /// 活期转账给好友
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="to_uid"></param>
        /// <param name="to_uname"></param>
        /// <param name="remark"></param>
        /// <param name="zhifuma"></param>
        /// <param name="strJsonLixi"></param>
        /// <param name="strSetup"></param>
        /// <returns></returns>
        [WebMethod]
        public string HuoqiZhuanzhang(int uid, int to_uid, string to_uname, string remark, string zhifuma, string strJsonLixi, string strSetup)
        {
            Model.Service<Model.ServiceResult> jsonModel = new Model.Service<Model.ServiceResult>();
            Model.ServiceResult data = new Model.ServiceResult();
            string msg = string.Empty;
            Model.UserInfo modelUser = new BLL.UserInfo().GetModel(uid);
            if (modelUser != null && modelUser.id != 0)
            {
                if (Falcon.Function.Decrypt(modelUser.userpswd) == zhifuma)
                {
                    int oid_qk = new BLL.Huoqi().Qukuan_Benjin_Zhanzhang(uid, modelUser.realname, to_uid, to_uname, strJsonLixi, Tool.JsonHelper.JsonDeserialize<Model.Setup>(strSetup), 0, remark, DateTime.Now, ref msg);
                    if (oid_qk != 0)
                    {
                        data.res = oid_qk.ToString();
                        jsonModel.success = "操作成功！";
                    }
                    else
                    {
                        data.res = "0";
                        jsonModel.error = msg;
                    }
                }
                else
                {
                    data.res = "0";
                    jsonModel.error = "支付密码错误！";
                }
            }
            else
            {
                data.res = "0";
                jsonModel.error = "当前用户不存在！";
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.ServiceResult>>(jsonModel);
        }

        /// <summary>
        /// 活期转账列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetList_Zhuanzhang(int pageIndex, int pageSize, string where, int fromMe, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "1=1";
            if (fromMe == 1)
            {
                filter += " and uid=" + uid;
            }
            else if (fromMe == 2)
            {
                filter += " and to_uid=" + uid;
            }
            else
            {
                filter += " and (uid=" + uid + " or to_uid=" + uid + ")";
            }

            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.HuoqiZhuanzhang> list = Tool.Pager_Sqlite.Query<Model.HuoqiZhuanzhang>(pageIndex, pageSize, out recordCount, "V_HuoqiZhuanzhang_Info", "id,realname_fukuan,realname_shoukuan,addTime,jine", filter, "id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.HuoqiZhuanzhang one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = "转账人:" + one.realname_fukuan + ",收款人:" + one.realname_shoukuan, subTitle = one.addTime, remark = one.jine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        /// <summary>
        /// 获取转账数据，点击付款人可以链接到付款单数据，点击收款人可以链接到收款单数据，调用GetHuoqiInfo接口
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetZhuanzhangInfo(int id)
        {
            Model.Service<Model.HuoqiZhuanzhang> modelJson = new Model.Service<Model.HuoqiZhuanzhang>();
            modelJson = new Model.Service<Model.HuoqiZhuanzhang>();
            Model.HuoqiZhuanzhang model = new BLL.HuoqiZhuanzhang().GetModel(id);
            if (model != null && model.id != 0)
            {
                modelJson.data = model;
            }
            else
            {
                modelJson.error = "没有数据！";
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.HuoqiZhuanzhang>>(modelJson);
        }
        #endregion

        #region 定期
        /// <summary>
        /// 获取定期列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetListDingqi(int pageIndex, int pageSize, string where, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "delid=0 and uid=" + uid;
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.Mobile.ListDingqi> list = Tool.Pager_Sqlite.Query<Model.Mobile.ListDingqi>(pageIndex, pageSize, out recordCount, "Dingqi", "id,danhao,jine,cunqukuan,cunkuanTime,daoqiTime", filter, "id desc");
            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.Mobile.ListDingqi one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.danhao, subTitle = one.cunqukuan == 1 ? (one.cunkuanTime + "至" + one.daoqiTime) : one.cunkuanTime, remark = one.jine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        /// <summary>
        /// 获取定期明细
        /// </summary>
        /// <param name="id"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetDingqiInfo(int id, int uid)
        {
            Model.Service<Model.Dingqi> jsonModel = new Model.Service<Model.Dingqi>();
            Model.Dingqi model = new BLL.Dingqi().GetModel(id, uid);
            if (model != null && model.id != 0)
            {
                model.modelSetup = new Model.Setup();
                model.chs_money = model.jine.ToString("0.00");
                model.str_hasQukuan = model.hasQukuan == 1 ? "已取款" : "未取款";
                model.qukuanfangshi = model.qukuanType == 0 ? "未取款" : ((Config.Enums.Dingqi_Qukuan)model.qukuanType).ToString();
                if (model.cunqukuan == 1 && model.hasQukuan == 0)
                {
                    Model.Setup modelSetup = new BLL.Setup().Get_Setup();
                    if (modelSetup != null)
                    {
                        model.modelSetup = modelSetup;
                        model.str_lixi = new BLL.Dingqi().GetLixi(modelSetup, model, DateTime.Today).ToString("0.00");
                    }
                }
                else
                {
                    model.str_lixi = model.lixi.ToString("0.00");
                }
                model.str_daoqi_handler = ((Config.Enums.Dingqi_DaoqiHandler)model.daoqi_handler).ToString();
            }
            else
            {
                jsonModel.error = "没有数据！";
            }
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Dingqi>>(jsonModel);
        }

        /// <summary>
        /// 获取用户定期余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetDingqi(int uid)
        {
            return new BLL.Dingqi().GetYue(uid).ToString("0.00");
        }

        /// <summary>
        /// 获取定期利率
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetDingqiLilv()
        {
            //dq_byDay,dq_lilv,dq_zc,dq_zclilv
            Model.Setup modelSetup = new BLL.Setup().Get_DingqiLilv();
            return modelSetup == null ? " " : Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
        }

        /// <summary>
        /// 获取定期存款周期
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetZhouqi_Dingqi()
        {
            //dq_byDay,dq_lilv,dq_zc,dq_zclilv
            Model.Setup modelSetup = new BLL.Setup().Get_DingqiLilv();
            return modelSetup == null ? " " : modelSetup.dq_lilv;
        }

        /// <summary>
        /// 活期单据转定期
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="qukuan"></param>
        /// <param name="setup"></param>
        /// <param name="dqjs"></param>
        /// <param name="ddlZhouqiText"></param>
        /// <param name="ddlZhouqiValue"></param>
        /// <param name="lilv"></param>
        /// <param name="budaoqi"></param>
        /// <returns></returns>
        [WebMethod]
        public string DingqiZhuanRu_Option(int uid, string qukuan, string setup, int dqjs, string ddlZhouqiText, string ddlZhouqiValue, decimal lilv, decimal budaoqi, string zhifuma)
        {
            try
            {
                string msg = string.Empty;
                Model.UserInfo modelUser = new BLL.UserInfo().GetModel(uid);
                if (modelUser != null && modelUser.id != 0)
                {
                    if (Falcon.Function.Decrypt(modelUser.userpswd) == zhifuma)
                    {
                        int zhouqi = Falcon.Function.ToInt(ddlZhouqiValue.Split('|')[0]);//存款周期|利率
                        DateTime cunkuanTime = DateTime.Now;
                        string daoqiTime = ddlZhouqiText.EndsWith("天") ? cunkuanTime.AddDays(zhouqi + 1).ToString("yyyy-MM-dd") : cunkuanTime.AddMonths(zhouqi).AddDays(1).ToString("yyyy-MM-dd");

                        if (new BLL.Huoqi().Qukuan_Zhuancun(uid, qukuan, Tool.JsonHelper.JsonDeserialize<Model.Setup>(setup), -1, "用户手机端操作", dqjs, ddlZhouqiText, lilv, budaoqi, daoqiTime, "用户手机端转入", ref msg) != 0)
                        {
                            return "1";
                        }
                        else
                        {
                            return msg;
                        }
                    }
                    else
                    {
                        return "支付密码错误！";
                    }
                }
                else
                {
                    return "没有用户信息！";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// 定期单据转活期
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="uid"></param>
        /// <param name="lixi"></param>
        /// <param name="strSetup"></param>
        /// <returns></returns>
        [WebMethod]
        public string Dingqi_ToHuoqi(int oid, int uid, decimal lixi, string strSetup)
        {
            Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(strSetup);
            if (modelSetup != null)
            {
                string msg = string.Empty;
                if (new BLL.Dingqi().Qukuan_ToHuoqi(oid, uid, lixi, modelSetup, -1, "", ref msg) != 0)
                {
                    return "1";
                }
            }
            return "0";
        }

        #endregion

        #region 贷款

        [WebMethod]
        public string GetListDaikuan(int pageIndex, int pageSize, string where, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "delid=0 and uid=" + uid;
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.Mobile.ListDaikuan> list = Tool.Pager_Sqlite.Query<Model.Mobile.ListDaikuan>(pageIndex, pageSize, out recordCount, "Daikuan", "id,danhao,jine,addTime", filter, "id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.Mobile.ListDaikuan one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.danhao, subTitle = one.daikuanTime, remark = one.jine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        [WebMethod]
        public string GetListPayback(int dkid)
        {
            Model.Service<List<Model.DaikuanPayback>> jsonModel = new Model.Service<List<Model.DaikuanPayback>>();
            List<Model.DaikuanPayback> list = new BLL.DaikuanPayback().GetList(dkid);
            if (list != null && list.Count != 0)
            {
                jsonModel.data = list;
                //return Tool.JsonHelper.JsonSerializer(list);
            }
            else
            {
                jsonModel.error = "未找到还款信息！";
            }
            //return " ";
            return Tool.JsonHelper.JsonSerializer(jsonModel);
        }

        ///// <summary>
        ///// 获取定期明细
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="uid"></param>
        ///// <returns></returns>
        //[WebMethod]
        //public string GetDingqiInfo(int id, int uid)
        //{
        //    Model.Service<Model.Dingqi> jsonModel = new Model.Service<Model.Dingqi>();
        //    Model.Dingqi model = new BLL.Dingqi().GetModel(id, uid);
        //    if (model != null && model.id != 0)
        //    {
        //        model.modelSetup = new Model.Setup();
        //        model.chs_money = model.jine.ToString("0.00");
        //        model.str_hasQukuan = model.hasQukuan == 1 ? "已取款" : "未取款";
        //        model.qukuanfangshi = model.qukuanType == 0 ? "未取款" : ((Config.Enums.Dingqi_Qukuan)model.qukuanType).ToString();
        //        if (model.cunqukuan == 1 && model.hasQukuan == 0)
        //        {
        //            Model.Setup modelSetup = new BLL.Setup().Get_Setup();
        //            if (modelSetup != null)
        //            {
        //                model.modelSetup = modelSetup;
        //                model.str_lixi = new BLL.Dingqi().GetLixi(modelSetup, model, DateTime.Today).ToString("0.00");
        //            }
        //        }
        //        else
        //        {
        //            model.str_lixi = model.lixi.ToString("0.00");
        //        }
        //        model.str_daoqi_handler = ((Config.Enums.Dingqi_DaoqiHandler)model.daoqi_handler).ToString();
        //    }
        //    else
        //    {
        //        jsonModel.error = "没有数据！";
        //    }
        //    jsonModel.data = model;
        //    return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Dingqi>>(jsonModel);
        //}

        /// <summary>
        /// 获取用户定期余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetDaikuan(int uid)
        {
            Model.Service<Model.Daikuan_Mobile> jsonModel = new Model.Service<Model.Daikuan_Mobile>();
            Model.Daikuan_Mobile model = new BLL.Daikuan().GetDaikuan(uid);
            if (model != null)
            {
                model.listPayback = new BLL.DaikuanPayback().GetList_Yuqi(uid);
                jsonModel.data = model;
            }
            else
            {
                jsonModel.error = "未查询到数据！";
            }
            return Tool.JsonHelper.JsonSerializer(jsonModel);
        }

        ///// <summary>
        ///// 获取定期利率
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod]
        //public string GetDingqiLilv()
        //{
        //    //dq_byDay,dq_lilv,dq_zc,dq_zclilv
        //    Model.Setup modelSetup = new BLL.Setup().Get_DingqiLilv();
        //    return modelSetup == null ? " " : Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
        //}

        ///// <summary>
        ///// 获取定期存款周期
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod]
        //public string GetZhouqi_Dingqi()
        //{
        //    //dq_byDay,dq_lilv,dq_zc,dq_zclilv
        //    Model.Setup modelSetup = new BLL.Setup().Get_DingqiLilv();
        //    return modelSetup == null ? " " : modelSetup.dq_lilv;
        //}

        ///// <summary>
        ///// 活期单据转定期
        ///// </summary>
        ///// <param name="uid"></param>
        ///// <param name="qukuan"></param>
        ///// <param name="setup"></param>
        ///// <param name="dqjs"></param>
        ///// <param name="ddlZhouqiText"></param>
        ///// <param name="ddlZhouqiValue"></param>
        ///// <param name="lilv"></param>
        ///// <param name="budaoqi"></param>
        ///// <returns></returns>
        //[WebMethod]
        //public string DingqiZhuanRu_Option(int uid, string qukuan, string setup, int dqjs, string ddlZhouqiText, string ddlZhouqiValue, decimal lilv, decimal budaoqi, string zhifuma)
        //{
        //    try
        //    {
        //        string msg = string.Empty;
        //        Model.UserInfo modelUser = new BLL.UserInfo().GetModel(uid);
        //        if (modelUser != null && modelUser.id != 0)
        //        {
        //            if (Falcon.Function.Decrypt(modelUser.userpswd) == zhifuma)
        //            {
        //                int zhouqi = Falcon.Function.ToInt(ddlZhouqiValue.Split('|')[0]);//存款周期|利率
        //                DateTime cunkuanTime = DateTime.Now;
        //                string daoqiTime = ddlZhouqiText.EndsWith("天") ? cunkuanTime.AddDays(zhouqi + 1).ToString("yyyy-MM-dd") : cunkuanTime.AddMonths(zhouqi).AddDays(1).ToString("yyyy-MM-dd");

        //                if (new BLL.Huoqi().Qukuan_Zhuancun(uid, qukuan, Tool.JsonHelper.JsonDeserialize<Model.Setup>(setup), -1, "用户手机端操作", dqjs, ddlZhouqiText, lilv, budaoqi, daoqiTime, "用户手机端转入", ref msg) != 0)
        //                {
        //                    return "1";
        //                }
        //                else
        //                {
        //                    return msg;
        //                }
        //            }
        //            else
        //            {
        //                return "支付密码错误！";
        //            }
        //        }
        //        else
        //        {
        //            return "没有用户信息！";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}

        ///// <summary>
        ///// 定期单据转活期
        ///// </summary>
        ///// <param name="oid"></param>
        ///// <param name="uid"></param>
        ///// <param name="lixi"></param>
        ///// <param name="strSetup"></param>
        ///// <returns></returns>
        //[WebMethod]
        //public string Dingqi_ToHuoqi(int oid, int uid, decimal lixi, string strSetup)
        //{
        //    Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(strSetup);
        //    if (modelSetup != null)
        //    {
        //        string msg = string.Empty;
        //        if (new BLL.Dingqi().Qukuan_ToHuoqi(oid, uid, lixi, modelSetup, -1, "", ref msg) != 0)
        //        {
        //            return "1";
        //        }
        //    }
        //    return "0";
        //}


        #endregion

        #region 提现
        /// <summary>
        /// 活期提现申请
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="qukuan"></param>
        /// <param name="setup"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        [WebMethod]
        public string HuoqiZhuanChu_Option(int uid, string qukuan, string setup, string remark, string zhifuma)
        {
            try
            {
                string msg = string.Empty;
                Model.UserInfo modelUser = new BLL.UserInfo().GetModel(uid);
                if (modelUser != null && modelUser.id != 0)
                {
                    if (Falcon.Function.Decrypt(modelUser.userpswd) == zhifuma)
                    {
                        int oid = new BLL.Huoqi().Qukuan_BenxiTixian(uid, qukuan, Tool.JsonHelper.JsonDeserialize<Model.Setup>(setup), -1, remark, ref msg);
                        if (oid != 0)
                        {
                            return "1";
                        }
                        else
                        {
                            return msg;
                        }
                    }
                    else
                    {
                        return "支付密码错误！";
                    }
                }
                else
                {
                    return "没有用户信息！";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// 获取提现申请列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetListApply(int pageIndex, int pageSize, string where, int uid)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "uid=" + uid;
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.Mobile.ListApply> list = Tool.Pager_Sqlite.Query<Model.Mobile.ListApply>(pageIndex, pageSize, out recordCount, "V_Apply_Info", "id,jine,addTime,isFukuan", filter, "id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.Mobile.ListApply one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.addTime, subTitle = one.isFukuan == 0 ? "未付款" : "已付款", remark = one.jine.ToString("C2") });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        /// <summary>
        /// 获取提现详细信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetApplyInfo(int id, int uid)
        {
            Model.Service<Model.Apply> jsonModel = new Model.Service<Model.Apply>();

            Model.Apply model = new BLL.Apply().GetModel(id, uid);
            if (model == null || model.id == 0)
            {
                jsonModel.error = "提现记录不存在！";
            }
            else
            {
                model.str_isFukuan = model.isFukuan == 0 ? "未付款" : "已付款";
            }
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Apply>>(jsonModel);
        }
        #endregion

        #region 通知

        /// <summary>
        ///  通知公告
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetListTongzhi(int pageIndex, int pageSize, string where)
        {
            Model.Service<List<Model.ListView_Mobile>> jsonList = new Model.Service<List<Model.ListView_Mobile>>();

            pageIndex = pageIndex <= 0 ? 1 : pageIndex;//分页最小为1页 
            pageSize = pageSize <= 0 ? 10 : pageSize;//单页数据量最小为10条
            int recordCount = 0;
            string filter = "isEnable=1";
            if (!string.IsNullOrEmpty(where.Trim()))
            {
                filter += (" and " + Falcon.Function.DeleteAnyHtmlTag(where));
            }
            List<Model.Tongzhi> list = Tool.Pager_Sqlite.Query<Model.Tongzhi>(pageIndex, pageSize, out recordCount, "Tongzhi", "id,title,addTime", filter, "sort asc,id desc");

            if (list == null || list.Count == 0)
            {
                jsonList.error = "暂无数据！";
            }
            else
            {
                List<Model.ListView_Mobile> listView_Mobile = new List<Model.ListView_Mobile>();
                foreach (Model.Tongzhi one in list)
                {
                    listView_Mobile.Add(new Model.ListView_Mobile() { id = one.id, title = one.title, subTitle = one.addTime, remark = "" });
                }
                jsonList.data = listView_Mobile;
                jsonList.rc = recordCount;
                jsonList.pc = GetPageCount(recordCount, pageSize);
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<List<Model.ListView_Mobile>>>(jsonList);
        }

        [WebMethod]
        public string GetTongzhiInfo(int id)
        {
            Model.Service<Model.Tongzhi> jsonModel = new Model.Service<Model.Tongzhi>();
            Model.Tongzhi data = new Model.Tongzhi();

            Model.Tongzhi model = new BLL.Tongzhi().GetModel(id);
            if (model == null || model.id == 0)
            {
                jsonModel.error = "通知公告不存在！";
            }
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.Tongzhi>>(jsonModel);
        }

        #endregion

        #region 我的

        [WebMethod]
        public string GetUserInfo(string uid)
        {
            Model.Service<Model.UserInfo> jsonModel = new Model.Service<Model.UserInfo>();
            Model.UserInfo data = new Model.UserInfo();

            Model.UserInfo model = new BLL.UserInfo().GetModel("id,username,realname,idcard,email,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime", Falcon.Function.ToInt(uid.Replace("uid_", "")));
            if (model == null || model.id == 0)
            {
                jsonModel.error = "用户不存在！";
            }
            //List<Model.Bank> listBank = new BLL.Bank().GetList();
            //if (listBank != null && listBank.Count != 0)
            //{
            //    List<Model.Select> listSelect = new List<Model.Select>();
            //    foreach (Model.Bank one in listBank)
            //    {
            //        listSelect.Add(new Model.Select() { txt = one.name, val = one.name });
            //    }
            //    model.list_bankname = listSelect;
            //}
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.UserInfo>>(jsonModel);
        }

        [WebMethod]
        public string GetUserInfoByUserName(string username)
        {
            Model.Service<Model.UserInfo> jsonModel = new Model.Service<Model.UserInfo>();
            Model.UserInfo data = new Model.UserInfo();

            Model.UserInfo model = new BLL.UserInfo().GetModel("id,username,realname,idcard,email,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime", username);
            if (model == null || model.id == 0)
            {
                jsonModel.error = "用户不存在！";
            }
            //List<Model.Bank> listBank = new BLL.Bank().GetList();
            //if (listBank != null && listBank.Count != 0)
            //{
            //    List<Model.Select> listSelect = new List<Model.Select>();
            //    foreach (Model.Bank one in listBank)
            //    {
            //        listSelect.Add(new Model.Select() { txt = one.name, val = one.name });
            //    }
            //    model.list_bankname = listSelect;
            //}
            jsonModel.data = model;
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.UserInfo>>(jsonModel);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="oldpswd"></param>
        /// <param name="newpswd"></param>
        /// <returns></returns>
        [WebMethod]
        public string UpdatePswd(int uid, string oldpswd, string newpswd)
        {
            return new BLL.UserInfo().UpdatePswd_Login(uid, oldpswd, newpswd) ? "1" : "0";
        }

        /// <summary>
        /// 修改支付密码
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="oldpswd"></param>
        /// <param name="newpswd"></param>
        /// <returns></returns>
        [WebMethod]
        public string UpdatePayPswd(int uid, string oldpswd, string newpswd)
        {
            Model.Service<Model.ServiceResult> jsonModel = new Model.Service<Model.ServiceResult>();
            jsonModel.data = new Model.ServiceResult();
            Regex pswd = new Regex(@"^\d{6}$");
            if (pswd.IsMatch(newpswd))
            {
                string msg = string.Empty;
                if (new BLL.UserInfo().UpdatePswd_Pay(uid, oldpswd, newpswd, ref msg))
                {
                    jsonModel.success = "操作成功！";
                    jsonModel.data.res = "1";
                }
                else
                {
                    jsonModel.error = msg;
                }
            }
            else
            {
                jsonModel.error = "请输入6位数字的支付密码！";
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.ServiceResult>>(jsonModel);
        }

        [WebMethod]
        public string UpdateBaseInfo(string uid, string email, string mobile, string address)
        {
            return new BLL.UserInfo().UpdateBaseInfo(Falcon.Function.ToInt(uid.Replace("uid_", "")), email, mobile, address).ToString();
        }

        [WebMethod]
        public string UpdateAvatar(int uid, string jsonPhotos)
        {
            Model.Service<Model.ServiceResult> jsonModel = new Model.Service<Model.ServiceResult>();
            jsonModel.data = new Model.ServiceResult();
            if (!string.IsNullOrEmpty(jsonPhotos))
            {
                try
                {
                    List<Model.PicInfo> listPic = Tool.JsonHelper.JsonDeserialize<List<Model.PicInfo>>(jsonPhotos);
                    if (listPic != null && listPic.Count != 0)
                    {
                        foreach (Model.PicInfo one in listPic)
                        {
                            if (!string.IsNullOrEmpty(one.pic))
                            {
                                if (!one.pic.EndsWith(".jpg"))
                                {
                                    one.pic = GetPicture(Convert.FromBase64String(one.pic));

                                    if (!new BLL.UserInfo().Update_Avatar(uid, one.pic))
                                    {
                                        jsonModel.error = "操作失败！";
                                    }
                                }
                                string rootPath = Falcon.UrlPath.GetRootPath();
                                jsonModel.data.res = one.pic.StartsWith(rootPath) ? one.pic : (rootPath + one.pic);
                                break;
                            }
                        }
                    }
                    else
                    {
                        jsonModel.error = "请上传头像！";
                    }
                }
                catch
                {
                    jsonModel.error = "json数据异常！";
                }
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.ServiceResult>>(jsonModel);
        }

        [WebMethod]
        public string CheckPayPswd(int uid, string zhifuma)
        {
            Model.Service<Model.ServiceResult> model = new Model.Service<Model.ServiceResult>();
            Model.UserInfo modelUser = new BLL.UserInfo().GetModel(uid);
            if (modelUser != null && modelUser.id != 0)
            {
                if (Falcon.Function.Decrypt(modelUser.userpswd) == zhifuma)
                {
                    model.data = new Model.ServiceResult() { res = "1" };
                }
                else
                {
                    model.error = "支付密码错误！";
                }
            }
            else
            {
                model.error = "没有用户信息！";
            }
            return Tool.JsonHelper.JsonSerializer<Model.Service<Model.ServiceResult>>(model);
        }

        #endregion

        #region 公共
        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private string GetPicture(byte[] buffer)
        {
            try
            {
                if (!System.IO.Directory.Exists(Server.MapPath("/uploadFile/images")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("/UploadFile/images"));
                }
                string fileName = "/uploadFile/images/" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + new Random().Next(0, 1000).ToString("000") + ".jpg";
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer, 0, buffer.Length))
                {
                    ms.Seek(0, System.IO.SeekOrigin.Begin);

                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms);
                    image.Save(Server.MapPath(fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return fileName;
            }
            catch
            {
                return "";
            }
        }
        #endregion
    }
}
