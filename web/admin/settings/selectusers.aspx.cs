﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class selectusers : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                //listDepart = new BLL.Z_Depart().GetListByWhere("1=1");
                //listDepart = new BLL.Z_Depart().GetSortedList(listDepart, 0, 0);
                listDepart = new BLL.Z_Depart().GetSortedList(0, false);

                listUsers = new BLL.Z_Users().GetListByWhere("isDel=0");

                listDepart = listDepart == null ? new List<Model.Z_Depart>() : listDepart;
                listUsers = listUsers == null ? new List<Model.Z_Users>() : listUsers;
            }
        }

        protected List<Model.Z_Depart> listDepart;

        protected List<Model.Z_Users> listUsers;
    }
}