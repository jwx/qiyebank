﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class initSystem : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSave.CssClass += getPower("保存");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtShouquanma.Text.Trim() == SessionShouquan)
            {
                Model.Z_Users model = new BLL.Z_Users().GetModel(SessionUid);

                new BLL.Z_LoginLogs().Add(model.id, model.username, model.realname, Context.Request.UserHostAddress, "初始化系统");
                //string dbPath = Tool.DatabaseHelper.dbPath;
                //    System.IO.File.Copy(dbPath, dbPath.Replace("jingyi.dll", "\\database\\Bank_" + DateTime.Now.ToString("yyyyMMddHHmmff") + "_系统初始化前备份.db"));

                new BLL.Setup().InitSystem_Table(new string[] { "Apply", "Bank", "Dingqi", "Huoqi", "HuoqiIn", "HuoqiLixi","HuoqiZhuanzhang", "Notification", "ServiceLog", "SubNotification", "Tongzhi", "UserInfo", "UserLogs" });
                //new BLL.Setup().InitSystem_Table("Apply");
                //new BLL.Setup().InitSystem_Table("Dingqi");
                //new BLL.Setup().InitSystem_Table("Huoqi");
                //new BLL.Setup().InitSystem_Table("HuoqiLixi");
                //new BLL.Setup().InitSystem_Table("Notification");
                //new BLL.Setup().InitSystem_Table("ServiceLog");
                //new BLL.Setup().InitSystem_Table("SubNotification");
                //new BLL.Setup().InitSystem_Table("UserInfo");
                //new BLL.Setup().InitSystem_Table("UserLogs");


                if (new BLL.Setup().InitSystem_Setup())
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('初始化成功！',{icon:6,time:1500});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('授权码错误！',{icon:5,time:1500});", true);
            }
        }
    }
}