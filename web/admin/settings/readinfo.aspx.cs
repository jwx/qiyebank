﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class readinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                flag = false;
                Query();
            }
        }

        protected List<Model.SubNotification> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "nid=" + Falcon.Function.GetQueryInt("id");
            //if (flag)
            //{
            //    if (txtBase.Text.Trim() != "")
            //    {
            //        where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
            //    }
            //    else
            //    {
            //        if (txtTitle.Text.Trim() != "") where += " and title like '%" + txtTitle.Text.Trim() + "%'";
            //        if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text.Trim() + "%'";
            //        if (txtStart.Text.Trim() != "") where += " and datediff(day,'" + txtStart.Text.Trim() + "',addTime)>=0";
            //        if (txtEnd.Text.Trim() != "") where += " and datediff(day,'" + txtEnd.Text.Trim() + "',addTime)<=0";
            //    }
            //}
            listRecord = Tool.Pager_Sqlite.Query<Model.SubNotification>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_SubNotification_Info", "*", where, "isRead asc,id asc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(int id)
        {
            return new BLL.SubNotification().Delete(id);
        }
    }
}