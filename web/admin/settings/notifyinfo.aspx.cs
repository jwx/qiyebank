﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class notifyinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            int id = Falcon.Function.GetQueryInt("id", 0);
            if (id != 0)
            {
                model = new BLL.Notification().GetModel(id);
                if (model != null) new BLL.SubNotification().Update_Read(id, SessionUid);
            }
            model = model == null ? new Model.Notification() : model;
        }

        protected Model.Notification model;

    }
}