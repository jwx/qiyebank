﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="selectusers.aspx.cs" Inherits="web.admin.settings.selectusers" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            $("#ckAll").click(function () {
                $("input[type='checkbox']").prop("checked", $(this).prop("checked"));
            });

            $(".ckdpt").click(function () {
                $(this).parent().find(".ckuser").prop("checked", $(this).prop("checked"));
            });

            $("#btnSave").click(function () {
                var array_uid = [];
                var array_name = [];
                $(".ckuser:checked").each(function () {
                    array_uid.push($(this).val());
                    array_name.push($(this).attr("name"));
                });
                $("#hideUids", parent.document).val(array_uid.toString());
                $("#receiver", parent.document).html(array_name.toString());
                parent.layer.closeAll();
                return false;
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server" style="margin-left: 200px;">
        <div class="main" style="padding-bottom: 20px;">
            <asp:CheckBox ID="ckAll" runat="server" Text="全选" Font-Bold="true" />
            <% foreach (Model.Z_Depart depart in listDepart)
               { %>
            <div style="margin-left: 25px;">
                <input type="checkbox" id="dpt<%=depart.id %>" class="ckdpt" />
                <label for="dpt<%=depart.id %>"><%=depart._name %></label>
                <% List<Model.Z_Users> listDptUsers = (from m in listUsers where m.depart == depart.id select m).ToList(); %>
                <% if (listDptUsers != null && listDepart.Count != 0)
                   {%>
                <div style="margin-left: 25px;">
                    <% foreach (Model.Z_Users users in listDptUsers)
                       { %>
                    <input type="checkbox" id="u<%=users.id %>" class="ckuser" value="<%=users.id %>" name="<%=users.realname %>" />
                    <label for="u<%=users.id %>"><%=users.realname %></label>
                    <%}%>
                </div>
                <%  } %>
            </div>
            <%} %>
        </div>
        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" />
        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
    </form>
</body>
</html>
