﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print.aspx.cs" Inherits="web.admin.settings.print" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <style type="text/css">
        .area { background-color: white; margin: 10px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="area">
                <a href="/Plug/lodop/CLodop_Setup_for_Win32NT.exe">打印安装程序下载</a>
            </div>
        </div>
    </form>
</body>
</html>--%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <script type="text/javascript">
      
    </script>
    <style type="text/css">
        .contentDiv div { margin-top: 10px; vertical-align: top; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <a href="/Plug/lodop/CLodop_Setup_for_Win32NT.exe">打印安装程序下载</a>
            </div>
            <div class="contentDiv shadow">
                零钱存款打印模板
                <div>
                    <input type="button" value="打印设计" class="layer" href="../print/hqc_design.aspx" title="零钱存款打印模板" />
                </div>
            </div>
            <div class="contentDiv shadow">
                零钱取款打印模板   
                <div>
                    <input type="button" value="打印设计" class="layer" href="../print/hqq_design.aspx" title="零钱取款打印模板" />
                </div>
            </div>
            <div class="contentDiv shadow">
                定期存款打印模板   
                <div>
                    <input type="button" value="打印设计" class="layer" href="../print/dqc_design.aspx" title="定期存款打印模板" />
                </div>
            </div>
            <div class="contentDiv shadow">
                定期取款打印模板   
                <div>
                    <input type="button" value="打印设计" class="layer" href="../print/dqq_design.aspx" title="定期取款打印模板" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
