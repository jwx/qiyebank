﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="initSystem.aspx.cs" Inherits="web.admin.settings.initSystem" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main white">
            <div class="box">
                <table class="tableInfo">
                    <tr>
                        <td class="tdl" style="width: 200px;"><span class="required">*</span>授权码： 
                        </td>
                        <td>
                            <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" CssClass="tbox" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                            <span class="Validform_checktip">请输入授权码</span>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="初始化" CssClass="btn btn-sm btnRed" OnClick="btnSave_Click" OnClientClick="return confirm('初始化系统会清空所有用户和存款数据，确定要执行初始化操作吗？')" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
