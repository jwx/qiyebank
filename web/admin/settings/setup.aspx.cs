﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class setup : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                for (int i = 0; i < 24; i++)
                {
                    ddlBackupTime.Items.Add(new ListItem() { Text = i.ToString(), Value = i.ToString() });
                }

                Model.Setup model = new BLL.Setup().Get_Setup();
                if (model != null)
                {
                    ckUseShouquanma.Checked = model.useShouquanma == 1;
                    ddlBackupTime.SelectedValue = model.backupTime.ToString();

                    rdHqjxByMonth.Checked = model.hq_byDay == 0;
                    rdHqjxByDay.Checked = model.hq_byDay != 0;

                    rdHqjsManual.Checked = model.hq_autoCalc == 0;
                    rdHqjsAuto.Checked = model.hq_autoCalc == 1 && model.hq_byDay != 2;
                    rdHqjsEveryDay.Checked = model.hq_autoCalc == 1 && model.hq_byDay == 2;

                    txtDayLilv.Text = model.hq_daylilv == 0 ? "" : model.hq_daylilv.ToString();
                    if (model.hq_autoCalc != 1) txtDays.Attributes.Add("ignore", "ignore");//每月XX号自动结算
                    txtDays.Text = model.days == 0 ? "" : model.days.ToString();
                    rdHqqkXcxq.Checked = model.hq_houcunxianqu == 0;
                    rdHqqkHcxq.Checked = model.hq_houcunxianqu == 1;
                    rdHqjxByMonth.Enabled = rdHqjxByDay.Enabled = rdHqjsManual.Enabled = rdHqjsAuto.Enabled = rdHqjsEveryDay.Enabled = model.isSaved == 0;
                    //rdHqckCkManual.Checked = model.hqck_autoCk == 0;
                    //rdHqckCkAuto.Checked = model.hqck_autoCk == 1;
                    //rdHqqkCkManual.Checked = model.hqqk_autoCk == 0;
                    //rdHqqkCkAuto.Checked = model.hqqk_autoCk == 1;
                    rdDqjs0.Checked = model.dq_daoqi == 0;
                    rdDqjs1.Checked = model.dq_daoqi == 1;
                    rdDqjs2.Checked = model.dq_daoqi == 2;
                    rdDqjs3.Checked = model.dq_daoqi == 3;

                    rdDqjxByMonth.Checked = model.dq_byDay == 0;
                    rdDqjxByDay.Checked = model.dq_byDay == 1;
                    //rdDqjsManual.Checked = model.dq_autoCalc == 0;
                    //rdDqjsAuto.Checked = model.dq_autoCalc == 1;
                    rdDqjxByMonth.Enabled = rdDqjxByDay.Enabled = model.isSaved == 0;
                    //rdDqckCkManual.Checked = model.dqck_autoCk == 0;
                    //rdDqckCkAuto.Checked = model.dqck_autoCk == 1;
                    //rdDqqkCkManual.Checked = model.dqqk_autoCk == 0;
                    //rdDqqkCkAuto.Checked = model.dqqk_autoCk == 1;
                    //rdDkjxByMonth.Checked = model.dk_byDay == 0;
                    //rdDkjxByDay.Checked = model.dk_byDay == 1;
                    //rdDkjxByMonth.Enabled = rdDkjxByDay.Enabled = model.isSaved == 0;
                }
            }
        }

        [AjaxPro.AjaxMethod]
        public void clear_HuoqiLilv()
        {
            new BLL.Setup().Update_HuoqiLilv("");
        }

        [AjaxPro.AjaxMethod]
        public void clear_DingqiLilv()
        {
            new BLL.Setup().Update_HuoqiLilv("");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int dqjs = 0;
            if (rdDqjs1.Checked) dqjs = 1;
            else if (rdDqjs2.Checked) dqjs = 2;
            else if (rdDqjs3.Checked) dqjs = 3;

            //if (new BLL.Setup().Update_Setup(ckUseShouquanma.Checked ? 1 : 0, rdHqjxByDay.Checked ? 1 : (rdHqjxByEveryDay.Checked ? 2 : 0), (rdHqjxByEveryDay.Checked ? 1 : (rdHqjsAuto.Checked ? 1 : 0)), Falcon.Function.ToDecimal(txtDayLilv.Text.Trim(), 0), Falcon.Function.ToInt(txtDays.Text.Trim()), rdHqqkHcxq.Checked ? 1 : 0, rdDqjxByDay.Checked ? 1 : 0, dqjs))
            if (new BLL.Setup().Update_Setup(ckUseShouquanma.Checked ? 1 : 0, Falcon.Function.ToInt(ddlBackupTime.SelectedValue), rdHqjxByDay.Checked ? (rdHqjsEveryDay.Checked ? 2 : 1) : 0, rdHqjsManual.Checked ? 0 : 1, Falcon.Function.ToDecimal(txtDayLilv.Text.Trim(), 0), Falcon.Function.ToInt(txtDays.Text.Trim()), rdHqqkHcxq.Checked ? 1 : 0, rdDqjxByDay.Checked ? 1 : 0, dqjs))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1500},function(){window.location.href=window.location;});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1500});", true);
            }
        }
    }
}