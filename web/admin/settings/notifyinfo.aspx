﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="notifyinfo.aspx.cs" Inherits="web.admin.settings.notifyinfo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <script type="text/javascript">
        $(function () {
            var mark = $(".listTable tbody #" + $(".main").attr("id"), parent.document).find(".readmark");
            if (mark.length == 1) {
                mark.remove();
                var tar = $(".topbar .read", top.document);
                var number = parseInt(tar.text());
                tar.text(number - 1);
            }
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main" id="<%=model.id %>">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>标题： 
                    </td>
                    <td>
                        <%=model.title %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>内容： 
                    </td>
                    <td>
                        <%=model.content %>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
