﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="setup.aspx.cs" Inherits="web.admin.settings.setup" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/Validform/Validform_Datatype.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            if ($("#rdHqjxByDay").is(":checked") && $("#rdHqjsEveryDay").is(":checked")) {
                $("#rililv").show().find("input").removeAttr("ignore");
            } else {
                if ($("#rdHqjxByMonth").is(":checked")) {
                    $("#rdHqjsEveryDay").parent().hide();
                }
                $("#rililv").hide().find("input").attr("ignore", "ignore");
                $("#btnLqll").show();
            }

            //设置零钱利率对应的页面
            $("#btnLqll").attr("href", $("input[name='rdHqjx']:checked").parent().attr("href"));
            $("input[name='rdHqjx']").click(function () {
                //设置零钱利率对应的页面
                $("#btnLqll").attr("href", $(this).parent().attr("href"));

                if ($("#rdHqjxByMonth").is(":checked")) {//选中了按月计算利息
                    $("#rdHqjsEveryDay").parent().hide();
                    if ($("#rdHqjsEveryDay").is(":checked")) {
                        $("#rdHqjsManual").click();//默认设置为利息取款时结算
                    }
                } else {
                    $("#rdHqjsEveryDay").parent().show();
                }
                ajax.clear_HuoqiLilv().value;
            });

            //设置定期利率对应的页面
            $("#btnDqll").attr("href", $("input[name='rdDqjx']:checked").parent().attr("href"));
            $("input[name='rdDqjx']").click(function () {
                //设置定期利率对应的页面
                $("#btnDqll").attr("href", $(this).parent().attr("href"));
                ajax.clear_DingqiLilv().value;
            });
            ////设置贷款利率对应的页面
            //$("#btnDkll").attr("href", $("input[name='rdDkjx']:checked").parent().attr("href"));
            //$("input[name='rdDkjx']").click(function () {
            //    //设置定期利率对应的页面
            //    $("#btnDkll").attr("href", $(this).parent().attr("href"));
            //    ajax.clear_DingqiLilv().value;
            //});

            $("input[name='rdHqjs']").click(function () {
                var obj = $(this).parents("p").find(".autoSetup");
                if ($(this).attr("id") == "rdHqjsEveryDay") {
                    obj.hide();
                    obj.find("input").attr("ignore", "ignore");
                    $("#btnLqll").hide();//屏蔽零钱利率
                    $("#rililv").show().find("input").removeAttr("ignore");//显示每日年利率
                } else {
                    $("#rililv").hide();
                    $("#btnLqll").show();
                    if ($(this).attr("id") == "rdHqjsAuto") {//自动结算，必须填写
                        obj.show();
                        obj.find("input").removeAttr("ignore");
                    } else {
                        obj.hide();
                        obj.find("input").attr("ignore", "ignore");
                    }
                }
            });
        });
    </script>
    <style type="text/css">
        .item { }

            .item .title { font-size: 14px; margin-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dedede; }

            .item .content { padding-top: 15px; padding-bottom: 10px; }

                .item .content p { height: 36px; margin: 0px; }

                    .item .content p input[type='radio'] { vertical-align: top; }

                    .item .content p input[type='button'] { margin-left: 8px; }

                    .item .content p input[type='text'] { margin: 0px 3px; }

                    .item .content p .tip { padding-left: 10px; color: #bbbbbb; }

                    .item .content p .warn { padding-left: 10px; color: #DD4E42; }
        /*.item .content p .autoSetup { display: none; }*/
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="area">
                <div class="item">
                    <div class="title">
                        通用设置
                    </div>
                    <div class="content">
                        <p>
                            <asp:CheckBox ID="ckUseShouquanma" runat="server" Text="是否使用授权码" Checked="true" /><span class="tip">使用授权码后，存取款需要输入授权码才可进行保存操作</span>
                        </p>
                        <p>
                            每日
                            <asp:DropDownList ID="ddlBackupTime" runat="server">
                            </asp:DropDownList>
                            时自动备份数据库
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        零钱设置
                    </div>
                    <div class="content">
                        <p>
                            利息计算方式： 
                            <asp:RadioButton ID="rdHqjxByMonth" runat="server" Text="按月计算" GroupName="rdHqjx" Checked="true" href="../huoqi/lilvbymonth.aspx" />
                            <asp:RadioButton ID="rdHqjxByDay" runat="server" Text="按天计算" GroupName="rdHqjx" href="../huoqi/lilvbyday.aspx" />
                            <span class="warn">一经设置，不可修改</span>
                        </p>
                        <p class="hqjs">
                            利息结算方式：
                            <asp:RadioButton ID="rdHqjsManual" runat="server" Text="取款时结算" GroupName="rdHqjs" Checked="true" />
                            <asp:RadioButton ID="rdHqjsAuto" runat="server" Text="自动结算" GroupName="rdHqjs" />
                            <span class="autoSetup" style="<%=rdHqjsAuto.Checked?"": "display:none;" %>">（每月<asp:TextBox ID="txtDays" runat="server" datatype="n,numrange" min="1" max="30" Style="width: 25px;"></asp:TextBox>号，系统自动进行利息计算） 
                            </span>
                            <span>
                                <asp:RadioButton ID="rdHqjsEveryDay" runat="server" Text="每日结算" GroupName="rdHqjs" />
                            </span>
                            <span class="warn">结算方式一经设置，不可修改</span>
                        </p>
                        <p>
                            零钱利率设置：<input type="button" id="btnLqll" value="设置" class="layer hidden" title="设置零钱利率" />
                            <span id="rililv" class="hidden">年利率：<asp:TextBox ID="txtDayLilv" runat="server" Style="width: 50px;" datatype="*" nullmsg="请填写年利率！"></asp:TextBox>%
                            </span>
                        </p>
                        <%-- <p>
                        存款审核方式：
                        <asp:RadioButton ID="rdHqckCkManual" runat="server" Text="人工审核" GroupName="rdHqckCk" Checked="true" />
                        <asp:RadioButton ID="rdHqckCkAuto" runat="server" Text="自动审核" GroupName="rdHqckCk" />
                    </p>--%>
                        <p>
                            零钱取款顺序：
                            <asp:RadioButton ID="rdHqqkXcxq" runat="server" Text="先存先取" GroupName="rdHqqk" Checked="true" />
                            <asp:RadioButton ID="rdHqqkHcxq" runat="server" Text="后存先取" GroupName="rdHqqk" />
                        </p>
                        <%--<p>
                        取款审核方式：
                        <asp:RadioButton ID="rdHqqkCkManual" runat="server" Text="人工审核" GroupName="rdHqqkCk" Checked="true" />
                        <asp:RadioButton ID="rdHqqkCkAuto" runat="server" Text="自动审核" GroupName="rdHqqkCk" />
                    </p>--%>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        定期设置
                    </div>
                    <div class="content">
                        <p>
                            利息计算方式： 
                        <asp:RadioButton ID="rdDqjxByMonth" runat="server" Text="按月计算" GroupName="rdDqjx" Checked="true" href="../dingqi/lilvbymonth.aspx" />
                            <asp:RadioButton ID="rdDqjxByDay" runat="server" Text="按天计算" GroupName="rdDqjx" href="../dingqi/lilvbyday.aspx" />
                            <span class="warn">一经设置，不可修改</span>
                        </p>
                        <p>
                            到期结算方式：
                            <asp:RadioButton ID="rdDqjs0" runat="server" Text="取款时结算本金和利息" GroupName="rdDqjs" Checked="true" />
                            <asp:RadioButton ID="rdDqjs1" runat="server" Text="本金转定期_利息转零钱" GroupName="rdDqjs" />
                            <asp:RadioButton ID="rdDqjs2" runat="server" Text="本金和利息转定期账户" GroupName="rdDqjs" />
                            <asp:RadioButton ID="rdDqjs3" runat="server" Text="本金和利息转零钱账户" GroupName="rdDqjs" />
                        </p>
                        <p>
                            定期利率设置：<input type="button" id="btnDqll" value="设置" class="layer" title="设置定期利率" />
                        </p>
                        <%--<p>
                        存款审核方式：
                        <asp:RadioButton ID="rdDqckCkManual" runat="server" Text="人工审核" GroupName="rdDqckCk" Checked="true" />
                        <asp:RadioButton ID="rdDqckCkAuto" runat="server" Text="自动审核" GroupName="rdDqckCk" />
                    </p>
                    <p>
                        取款审核方式：
                        <asp:RadioButton ID="rdDqqkCkManual" runat="server" Text="人工审核" GroupName="rdDqqkCk" Checked="true" />
                        <asp:RadioButton ID="rdDqqkCkAuto" runat="server" Text="自动审核" GroupName="rdDqqkCk" />
                    </p>--%>
                    </div>
                </div>

                <div class="item">
                    <div class="title">
                        贷款设置
                    </div>
                    <div class="content">
                        <%-- <p>
                            利息计算方式： 
                        <asp:RadioButton ID="rdDkjxByMonth" runat="server" Text="按月计算" GroupName="rdDkjx" Checked="true" href="../daikuan/lilvbymonth.aspx" />
                            <asp:RadioButton ID="rdDkjxByDay" runat="server" Text="按天计算" GroupName="rdDkjx" href="../daikuan/lilvbyday.aspx" />
                            <span class="warn">一经设置，不可修改</span>
                        </p>--%>
                        <%--<p>
                            结算方式：
                            <asp:RadioButton ID="RadioButton3" runat="server" Text="取款时结算本金和利息" GroupName="rdDqjs" Checked="true" />
                            <asp:RadioButton ID="RadioButton4" runat="server" Text="本金转定期_利息转零钱" GroupName="rdDqjs" />
                            <asp:RadioButton ID="RadioButton5" runat="server" Text="本金和利息转定期账户" GroupName="rdDqjs" />
                            <asp:RadioButton ID="RadioButton6" runat="server" Text="本金和利息转零钱账户" GroupName="rdDqjs" />
                        </p>--%>
                        <p>
                            定期利率设置：<input type="button" id="btnDkll" value="设置" class="layer" title="设置贷款利率" href="../daikuan/lilvbymonth.aspx" />
                        </p>
                    </div>
                </div>

                <div>
                    <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="layui-btn layui-btn-normal noloading" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
