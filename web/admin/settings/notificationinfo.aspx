﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="notificationinfo.aspx.cs" Inherits="web.admin.settings.notificationinfo" ValidateRequest="false" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor = K.create("#txtContent", {
                allowFileManager: false,
                uploadJson: '/Ajax/upload_json.ashx',
                afterBlur: function () {
                    editor.sync();// 将编辑器的HTML数据同步到textarea
                }
            });
        });

        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                //beforeSubmit: function (curform) {
                //    var id = $("#hideId").val();
                //    var title = $("#txtTitle").val();
                //    var content = $("#txtContent").val();
                //    var disabled = $("#ckDisable").prop("checked") ? 0 : 1;
                //    var sort = $("#txtSort").val();
                //    var hideUids = $("#hideUids").val();
                //    alert(id.toString() + "," + title + "," + content + "," + disabled + "," + sort + "," + hideUids);
                //    if (ajax.Save(id, title, content, disabled, sort, receiver).value) {
                //        layer.msg('操作成功！', { icon: 6, time: 1000 }, function () {
                //            parent.location.reload();
                //            parent.layer.closeAll();
                //        });
                //    } else {
                //        layer.msg('操作失败！', { icon: 5, time: 1000 });
                //    }
                //    return false;
                //}
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <asp:HiddenField ID="hideId" runat="server" />
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>标题： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" datatype="*" placeholder="请输入标题"></asp:TextBox>
                        <span class="Validform_checktip">请输入标题</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>内容： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtContent" runat="server" datatype="*" placeholder="请输入内容" TextMode="MultiLine" Style="width: 80%; height: 300px;"></asp:TextBox>
                        <span class="Validform_checktip">请输入内容</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">接收人： 
                    </td>
                    <td>
                        <asp:HiddenField ID="hideUids" runat="server" />
                        <%--<div id="receiver" style="float: left; margin-right: 5px;">
                        </div>--%>
                        <asp:Label ID="receiver" runat="server"></asp:Label>
                        <a class="label label-success layer" href="selectusers.aspx" title="请选择接收人">选择接收人</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">状态： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckDisable" runat="server" Text="隐藏当前通知公告" /><span class="Validform_checktip">隐藏后用户无法查看</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">排序： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSort" runat="server" datatype="n1-4" placeholder="请输入排序级别" Text="100" errormsg="请填写0-9999之间的整数数字"></asp:TextBox>
                        <span class="Validform_checktip">请输入0-9999之间的整数数字，数字越小，排序越靠前</span>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
