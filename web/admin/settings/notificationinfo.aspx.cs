﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class notificationinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.Notification model = new BLL.Notification().GetModel(id);
                    txtTitle.Text = model.title;
                    txtContent.Text = model.content;
                    ckDisable.Checked = model.isEnable == 0;
                    txtSort.Text = model.sort.ToString();
                    List<Model.SubNotification> listSubNotification = new BLL.SubNotification().GetListByWhere("nid=" + id);
                    listSubNotification = listSubNotification == null ? new List<Model.SubNotification>() : listSubNotification;
                    hideUids.Value = string.Join(",", (from m in listSubNotification select m.uid).ToList());
                    receiver.Text = string.Join(",", (from m in listSubNotification select m.realname).ToList());
                }
            }
            hideId.Value = id.ToString();
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Notification().Add(txtTitle.Text, txtContent.Text, ckDisable.Checked ? 0 : 1, Falcon.Function.ToInt(txtSort.Text, 100), SessionUid, hideUids.Value))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Notification().Update(id, txtTitle.Text, txtContent.Text, ckDisable.Checked ? 0 : 1, Falcon.Function.ToInt(txtSort.Text, 100), SessionUid, hideUids.Value))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }

        //[AjaxPro.AjaxMethod]
        //public bool Save(int id, string title, string content, int disable, int sort, string receiver)
        //{
        //    if (id == 0)
        //    {
        //        return new BLL.Notification().Add(title, content, disable, sort, SessionUid, receiver);
        //        //if ()
        //        //{
        //        //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
        //        //}
        //        //else
        //        //{
        //        //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
        //        //}
        //    }
        //    else
        //    {
        //        return new BLL.Notification().Update(id, title, content, disable, sort, SessionUid, receiver);
        //        //if (new BLL.Notification().Update(id, title, content, disable, sort, SessionUid, receiver))
        //        //{
        //        //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
        //        //}
        //        //else
        //        //{
        //        //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
        //        //}
        //    }
        //}
    }
}