﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class settings : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSave.CssClass += getPower("保存");

                Model.Z_Settings model = new BLL.Z_Settings().GetModel_Top();
                txtName.Text = model.name;
                imgLogo.ImageUrl = string.IsNullOrEmpty(model.logo) ? "/images/logo_jingyi.png" : model.logo;
                imgImage.ImageUrl = string.IsNullOrEmpty(model.image) ? "/images/logo_login.jpg" : model.image;
                imgLogo1.ImageUrl = string.IsNullOrEmpty(model.mlogo) ? "/images/logo_jingyi.png" : model.mlogo;

                if (model.email != Config.Email.email)
                {
                    txtSmtp.Text = model.smtp;
                    txtEmail.Text = model.email;
                    txtPswd.Text = model.pswd;
                }
                txtKeywords.Text = model.keywords;
                txtDescription.Text = model.description;
            }
        }

        [AjaxPro.AjaxMethod]
        public int Save(string name, string logo, string image, string mlogo, string smtp, string email, string pswd, string keywords, string description)
        {
            bool flag = string.IsNullOrEmpty(email.Trim());
            return new BLL.Z_Settings().Update(name, logo, image, mlogo, flag ? Config.Email.smtp : smtp, flag ? Config.Email.email : email.Trim(), flag ? Config.Email.pswd : pswd, keywords, description);
        }

        [AjaxPro.AjaxMethod]
        public void DeleteFile(string filePath)
        {
            if (!filePath.Contains("logo_jingyi.png") && !filePath.Contains("logo_login.jpg"))
            {
                Falcon.FileOP.DeleteFile(Server.MapPath(filePath));
            }
        }
    }
}