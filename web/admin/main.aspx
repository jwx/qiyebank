﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main.aspx.cs" Inherits="web.admin.main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit" />
    <title><%=modelSettings.name %></title>
    <link href="/Css/jingyi.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <%--<script src="/Scripts/jingyi.js"></script>--%>
    <script src="/Plug/layer/layer.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script src="/Plug/layui/layui.js"></script>
    <%--<script src="/Scripts/tab.js"></script>--%>
    <script src="/Scripts/jquery.cookie.js"></script>
    <script type="text/javascript">
        $(function () {
            //layui.use(['layout', 'layer'], function () {
            //    var layout = layui.layout, layer = layui.layer;
            //    layer.config({
            //        extend: 'layer.ext.js'
            //    })
            //    layout.tab();
            //});

            //initSize();
            //$(window).resize(function () {
            //    initSize();
            //}); 

            $(".iframe").height($(window).height() - $(".top").height() - $("#tabDiv").height() - 10);

            $(".menu>li").hover(function () {
                $(this).find(".submenu").show();
            }, function () {
                $(this).find(".submenu").hide();
            });


            $(".submenu").width($(".menu").width() - 30);
            $(".submenu:even").css("background", "url(/images/menu_bg1.jpg) no-repeat #FFFFFF right bottom");

            $(".logout").click(function () {
                layer.confirm("确定要退出系统吗？", function () {
                    window.location.href = "/login.aspx";
                });
            });

            $(".openTab").click(function () {
                $(this).parents(".submenu").hide();
                var t = $(this);
                var url = t.attr("href");
                if (url != "") {
                    var layuiTab = $(".layui-tab .layui-tab-title");
                    var index = layer.open({
                        type: 2,
                        shade: 0,
                        shadeClose: false,
                        offset: ['120px', '0px'],
                        closeBtn: 0,
                        title: false,
                        shift: -1,
                        zIndex: 0,
                        area: ['100%', $(window).height() - 120 + 'px'],
                        content: url //iframe的url
                    });
                    //if (layuiTab.find("li[tabid='" + index + "']").length == 0) {
                    var tabid = t.attr("tabid");//原始tabid
                    t.attr("tabid", index);
                    layuiTab.find("li").removeClass("layui-tab-this");
                    if (tabid) {
                        $("#layui-layer" + tabid).remove();
                        $(".layui-tab li[tabid='" + tabid + "']").addClass("layui-tab-this").attr("tabid", index);//选中并更新tabid
                    } else {
                        //layuiTab.find("li").removeClass("layui-tab-this");
                        var tabName = $(t).attr("title") || t.text();
                        var html = '<li tabid="' + index + '" class="layui-tab-this"><span class="openTab">' + tabName + '</span><a class="tab_close" href="javascript:void(0);"></a></li>';
                        layuiTab.append(html);
                    }
                }
                return false;
            });

            var index = 0;
            var left = 0;
            $(".layui-tab-bar .layui-icon").click(function () {
                var li = $(".layui-tab-title>li");
                if ($(this).hasClass("layui-tab-left")) {
                    if (index > 0) {
                        left = left + li.eq(index - 1).outerWidth() + 8;
                        li.eq(0).css("margin-left", left);
                        index--;
                    }
                } else {
                    if (index < li.length - 1) {
                        left = left - li.eq(index).outerWidth() - 8;
                        li.eq(0).css("margin-left", left);
                        index++;
                    }
                }
            });// layui-tab-left

            //切换选项卡
            $(document).on("click", ".layui-tab li", function () {
                $(".layui-tab li").removeClass("layui-tab-this");
                $(this).addClass("layui-tab-this");
                var tabid = $(this).attr("tabid");
                $(".layui-layer-iframe").hide();
                $("#layui-layer" + tabid).show();
            });

            $(document).on("click", ".tab_close", function (e) {
                closeTab($(this).parents("li").attr("tabid"));
                e.stopPropagation();//阻止事件冒泡，否则会调用切换选项卡事件
            });

            //关闭选项卡
            function closeTab(tabId) {
                var cur = $(".layui-tab .layui-tab-this");//获取当前选中选项卡的tabid
                var tar = $(".layui-tab li[tabid='" + tabId + "']");//获取要关闭的选项卡的tabid 
                if (cur.attr("tabid") == tabId) {//关闭当前选中的选项卡
                    cur.prev().click();//获取上一个选项卡
                    cur.remove();//删除当前选项卡 
                } else {
                    tar.remove();
                }
                $(".openTab[tabid='" + tabId + "']").removeAttr("tabid");
                $("#layui-layer" + tabId).remove();
            }
        });
    </script>
</head>
<body class="noscroll">
    <form id="form1" runat="server">
        <div class="top">
            <div class="topbar">
                <div class="topitem">
                    欢迎登录<%=modelSettings.name %>！当前账号：<%=realname %>
                    <a class="mail" href="javascript:void(0)" onclick="addTab(this)" menuid="notify" title="通知公告" url="/admin/settings/notify.aspx">
                        <img src="/images/mail.png" /><span class="read"><%=new BLL.SubNotification().GetCount_NotRead(SessionUid) %></span>
                    </a>，<a class="logout" href="javascript:void(0);">退出登录</a>
                </div>
            </div>
            <div class="menubar">
                <div class="menuitem">
                    <a href="javascript:void(0);">
                        <img class="logo" src="<%=string.IsNullOrEmpty(modelSettings.mlogo)?"/images/logo_jingyi.png":modelSettings.mlogo %>" alt="logo" />
                    </a>
                    <ul class="menu">
                        <% 
                            List<Model.Z_Forms> list1 = GetListByPid(listRecord, 0);//获取一级菜单
                            if (list1 != null && list1.Count != 0)
                            {%>
                        <% foreach (Model.Z_Forms one in list1)//循环每个一级菜单
                           {
                               if (isAdmin || listPowersIds.Contains(one.id))
                               {
                        %>
                        <li>
                            <a href="<%=string.IsNullOrEmpty(one.url)?"":one.url %>" class="m1 openTab"><%=one.name %></a>
                            <% 
                                   List<Model.Z_Forms> list2 = GetListByPid(listRecord, one.id);
                                   if (list2 != null && list2.Count != 0)
                                   {
                            %>
                            <div class="submenu">
                                <% foreach (Model.Z_Forms two in list2)
                                   {
                                       if (isAdmin || listPowersIds.Contains(two.id))
                                       {
                                           List<Model.Z_Forms> list3 = GetListByPid(listRecord, two.id);
                                           bool isRoot = list3 == null || list3.Count == 0 ? false : true;
                                %>
                                <div class="menu2">
                                    <a href="<%=string.IsNullOrEmpty(two.url)?"":two.url %>" class="<%=isRoot ? "root" : ""%> openTab"><%=two.name%></a>
                                    <% if (isRoot)
                                       { %>
                                    <div class="menu3">
                                        <% foreach (Model.Z_Forms three in list3)
                                           {
                                               if (isAdmin || listPowersIds.Contains(three.id))
                                               { %>
                                        <a href="<%=string.IsNullOrEmpty(three.url)?"":("/admin/"+three.url)%>" class="openTab"><%=three.name%></a>
                                        <%}
                                           } %>
                                    </div>
                                    <%} %>
                                </div>
                                <%}
                                   } %>
                            </div>
                            <%} %>
                        </li>
                        <%}
                           }
                            } %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="layui-tab">
            <ul class="layui-tab-title">
                <li class="layui-tab-this">
                    <span>首页</span>
                </li>
            </ul>
            <div class="layui-tab-bar"><span class="layui-icon layui-tab-left">&#xe603;</span><span class="layui-icon layui-tab-right">&#xe602;</span></div>
        </div>
        <div class="mainpage">
            <div class="main">
                <iframe class="iframe" name="iframe" frameborder="0" scrolling="auto" src="welcome.aspx" style="width: 100%; margin: 0px; padding: 0px;"></iframe>
            </div>
        </div>
    </form>
</body>
</html>
