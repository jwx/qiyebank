﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tzck
{
    public partial class WellDinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.WelldrillingAmount().GetModelByID(id);
                    txtmoney .Text = model.money;
                }
            }
        }


        protected int id;

        protected Model.WelldrillingAmount model;

        protected string photo;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.WelldrillingAmount().Add(txtmoney.Text))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.WelldrillingAmount().Update(id,txtmoney.Text))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }




    }
}