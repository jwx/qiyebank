﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tzck
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                List<Model.WelldrillingAmount> list = new BLL.WelldrillingAmount().getList();
                list = list == null ? new List<WelldrillingAmount>() : list;
                ddltxtTZmoney.DataSource = list;
                ddltxtTZmoney.DataValueField = "id";
                ddltxtTZmoney.DataTextField = "money";
                ddltxtTZmoney.DataBind();
                ddltxtTZmoney.Items.Insert(0, new ListItem() { Text = "请选择投资金额", Value = "" });
                if (id != 0)
                {
                    model = new BLL.Investmen().GetModelByID(id);
                    txtAccountNumber.Text = model.AccountNumber;
                    txtAccountNumber.Enabled = false;
                    txtAccountName.Attributes.Remove("ajaxurl");//取消验证用户名重复操作
                    txtAccountName.Text = model.AccountName;
                    ddltxtTZmoney.SelectedItem.Value = model.Wellid.ToString();
                    txtendTime.Text = model.endTime;
                    ddlmoneyAmountName.SelectedItem.Value = model.moneyAmountID.ToString();
                }
            }
        }

        protected int id;

        protected Model.Investmen model;

        protected string photo;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 1000)
            {
                if (new BLL.Investmen().Add(txtAccountNumber.Text, txtAccountName.Text, decimal.Parse(ddltxtTZmoney.SelectedItem.Text), int.Parse(ddlmoneyAmountName.SelectedItem.Value), ddlmoneyAmountName.SelectedItem.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), txtendTime.Text, txtRemark.Text, SessionUid, int.Parse(ddltxtTZmoney.SelectedItem.Value), int.Parse(YearName.Value), YearName.InnerText))
                {

                    string startime = DateTime.Now.ToString("yyyy-MM-dd");//投资开始时间
                    string endtime = txtendTime.Text.Trim().ToString();//投资结束时间


                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                string var = YearName.Value;
                var b=YearName.SelectedIndex;
                
                //if (new BLL.UserInfo().Update(id, txtRealName.Text, txtIdcard.Text, txtEmail.Text, Falcon.Function.ToInt(ddlGender.SelectedValue), txtTel.Text, txtMobile.Text, txtAddress.Text, txtBankCard.Text.Trim(), txtBankUName.Text.Trim(), ddlBank.SelectedItem.Text, txtZhifubao.Text.Trim(), txtRemark.Text, Falcon.Function.GetFormString("hidePhoto"), SessionUid))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                //}
                //else
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                //}
            }
        }





        [AjaxPro.AjaxMethod]
        public Model.UserInfo GetUserInfo(string username)
        {
            return new BLL.UserInfo().GetModel(username);
        }


        [AjaxPro.AjaxMethod]
        public Model.Welldrilling GetDaoqiTime(int wellid)
        {
            return new BLL.Welldrilling().GetModelByID(wellid);
        }


        [AjaxPro.AjaxMethod]
        public List<Model.Welldrilling> getYearName(int wellid)
        {
            List<Welldrilling> wlist = new BLL.Welldrilling().GetWModelByID(wellid);
            return wlist;
        }

    }
}