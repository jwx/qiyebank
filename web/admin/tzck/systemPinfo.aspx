﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="systemPinfo.aspx.cs" Inherits="web.admin.tzck.systemPinfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet" />
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <script src="/Plug/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            $("#txtAccountNumber").blur(function () {
                var json = ajax.GetUserInfo($.trim($(this).val())).value;
                if (json) {
                    $("#hideShoukuan").val(json.id);
                    $("#txtAccountName").val(json.realname);
                } else {
                    layer.alert("账户不存在！", function () {
                        $("#txtAccountNumber").val("");
                        layer.closeAll();
                    });
                }
            });


        });

    </script>
    <style type="text/css">
        .item {
            float: left;
            width: 200px;
            height: 150px;
            margin: 8px;
            position: relative;
        }

            .item .del {
                position: absolute;
                top: -6px;
                right: -5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 16px;
            }

                .item .del:hover {
                    color: red;
                }

            .item a img {
                width: 200px;
                height: 150px;
                border: 0px;
            }
    </style>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo1">
                <tr>
                    <td class="tdl"><span class="required">*</span>年份： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtYear" runat="server" datatype="*"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>个人税： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtpersonalincometax" runat="server" datatype="/^\d+$|^\d+[.]?\d+$/"   Text="0"></asp:TextBox>%
                         <span class="Validform_checktip"></span>
                    </td>
                </tr>

                <tr>
                    <td class="tdl"><span class="required">*</span>管理费： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtManagementexpense" runat="server" datatype=" /^\d+$|^\d+[.]?\d+$/" Text="0" placeholder="请输入管理费"></asp:TextBox>%
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>其他： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtother" runat="server" datatype="/^\d+$|^\d+[.]?\d+$/" placeholder="请输入，不能为空"  Text="0"></asp:TextBox>%
                      
                    </td>
                </tr>
                  <tr>
                    <td class="tdl"><span class="required">*</span>每日开采吨数： 
                    </td>
                    <td>
                        <asp:TextBox ID="txteverydayton" runat="server" datatype="/^\d+$|^\d+[.]?\d+$/" placeholder="请输入，不能为空" Text="0"></asp:TextBox>t
           
                    </td>
                </tr>
                         <tr>
                    <td class="tdl"><span class="required">*</span>损耗： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtloss" runat="server" datatype="/^\d+$|^\d+[.]?\d+$/" placeholder="请输入，不能为空"  Text="0"></asp:TextBox>%
               
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>