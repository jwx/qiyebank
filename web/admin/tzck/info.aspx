﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="web.admin.tzck.info" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet" />
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <script src="/Plug/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            $("#txtAccountNumber").blur(function () {
                var json = ajax.GetUserInfo($.trim($(this).val())).value;
                if (json) {
                    $("#hideShoukuan").val(json.id);
                    $("#txtAccountName").val(json.realname);
                } else {
                    layer.alert("账户不存在！", function () {
                        $("#txtAccountNumber").val("");
                        layer.closeAll();
                    });
                }
            });


        });

        function fnGetDaoqiTime() {
            var wellid = $("#YearName").val();
            if (wellid != "") {
                var json = ajax.GetDaoqiTime($("#YearName").val()).value;
                if (json) {
                    var html = " ";
                    html += "<div>年份：" + json.year + ",所得税：" + json.personalincometax + "%,管理费：" + json.Managementexpense + "%,其他：" + json.other + "%,每日开采吨数：" + json.everydayton + "t,损耗：" + json.loss + "%</div>"
                    $("#mx").html(html);
                }
            } else {
                $("#mx").html("");
            }
        }
        function getYearName() {
            var wellid = $("#ddltxtTZmoney").val();
            if(wellid!="")
                var json = ajax.getYearName($("#ddltxtTZmoney").val()).value;
            if (json) {
                var html = "<option value=''>请选择投资年限</option>";
                $.each(json, function (i, item) {
                    html += "<option value='" + item.id + "'>" + item.year + "</option>";
                })
                $("#YearName").append(html);
            } else {
                $("#YearName").empty();
                    $("#mx").html("");
            }
        }
    </script>
    <style type="text/css">
        .item {
            float: left;
            width: 200px;
            height: 150px;
            margin: 8px;
            position: relative;
        }

            .item .del {
                position: absolute;
                top: -6px;
                right: -5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 16px;
            }

                .item .del:hover {
                    color: red;
                }

            .item a img {
                width: 200px;
                height: 150px;
                border: 0px;
            }
    </style>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo1">
                <tr>
                    <td class="tdl"><span class="required">*</span>账号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAccountNumber" runat="server" datatype="n1-15"></asp:TextBox>
                        <asp:HiddenField ID="hideShoukuan" runat="server" />
                        <span class="Validform_checktip">请输入账号，长度为5-15位的数字</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>户名： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAccountName" runat="server" datatype="*" placeholder="请输入户名" Enabled="false"></asp:TextBox>
                        <span class="Validform_checktip">请输入户名</span>
                    </td>
                </tr>

                <tr>
                    <td class="tdl"><span class="required">*</span>投资金额： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddltxtTZmoney" runat="server" datatype="*"  onchange="getYearName()" >
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择投资金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>投资年限： 
                    </td>
                    <td>
                        <select id="YearName"  name="YearName" runat="server" onchange="fnGetDaoqiTime()">
                           
                        </select>
                        <%--<asp:DropDownList ID="ddlYearName" runat="server" datatype="*" onchange="fnGetDaoqiTime()">
                        </asp:DropDownList>--%>
                        <span class="Validform_checktip">请选择投资金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>明细： 
                    </td>
                    <td id="mx"></td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>到期时间： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtendTime" runat="server" datatype="*" placeholder="请选择到期时间" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" CssClass="Wdate"></asp:TextBox>
                        <span class="Validform_checktip">请选择到期时间</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="red">*</span>金额来源： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlmoneyAmountName" runat="server" datatype="*">
                            <asp:ListItem Value="">--请选择--</asp:ListItem>
                            <asp:ListItem Value="0">直接付款</asp:ListItem>
                            <asp:ListItem Value="1">活期转入</asp:ListItem>
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择金额来源</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
