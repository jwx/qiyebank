﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WellDinfo.aspx.cs" Inherits="web.admin.tzck.WellDinfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet" />
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <script src="/Plug/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            $("#txtAccountNumber").blur(function () {
                var json = ajax.GetUserInfo($.trim($(this).val())).value;
                if (json) {
                    $("#hideShoukuan").val(json.id);
                    $("#txtAccountName").val(json.realname);
                } else {
                    layer.alert("账户不存在！", function () {
                        $("#txtAccountNumber").val("");
                        layer.closeAll();
                    });
                }
            });


        });

    </script>
    <style type="text/css">
        .item {
            float: left;
            width: 200px;
            height: 150px;
            margin: 8px;
            position: relative;
        }

            .item .del {
                position: absolute;
                top: -6px;
                right: -5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 16px;
            }

                .item .del:hover {
                    color: red;
                }

            .item a img {
                width: 200px;
                height: 150px;
                border: 0px;
            }
    </style>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl"><span class="required">*</span>投资金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtmoney" runat="server" datatype="*"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>