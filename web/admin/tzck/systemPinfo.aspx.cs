﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tzck
{
    public partial class systemPinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            aid = Falcon.Function.GetQueryInt("aid", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Welldrilling().GetModelByID(id);
                    txtYear.Text = model.year;
                    txtpersonalincometax.Text = model.personalincometax.ToString();
                    txtother.Text = model.other.ToString();
                    txtManagementexpense.Text = model.Managementexpense.ToString();
                    txtloss.Text = model.loss.ToString();
                    txteverydayton.Text = model.everydayton.ToString();
                }
            }
        }
        private int aid;

        protected int id;

        protected Model.Welldrilling model;

        protected string photo;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Welldrilling().Add(txtYear.Text, int.Parse(txtpersonalincometax.Text), int.Parse(txtManagementexpense.Text), int.Parse(txtother.Text), int.Parse(txteverydayton.Text), int.Parse(txtloss.Text),aid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Welldrilling().Update(id, txtYear.Text, int.Parse(txtpersonalincometax.Text), int.Parse(txtManagementexpense.Text), int.Parse(txtother.Text), int.Parse(txteverydayton.Text), int.Parse(txtloss.Text),aid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }























    }
}