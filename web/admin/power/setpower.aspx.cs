﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power
{
    public partial class setpower : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                ddlRole.DataSource = new BLL.Z_Role().GetListByWhere("isEnable=1");
                ddlRole.DataTextField = "name";
                ddlRole.DataValueField = "id";
                ddlRole.DataBind();
            }
        }

        [AjaxPro.AjaxMethod]
        public string GetJsonString(int role)
        {
            List<Model.Z_Forms> listForm = new BLL.Z_Forms().GetListByWhere("isEnable=1");
            string json = new BLL.Z_Forms().GetJsonString(listForm, "系统菜单", role);
            return json;
        }


        [AjaxPro.AjaxMethod]
        public int Save(int role, string strFids, string strPower)
        {
            if (role > 0)
            {
                if (strFids != "")
                {
                    int[] arrFids = Array.ConvertAll<string, int>(strFids.Split(','), m => { return Falcon.Function.ToInt(m, 0); });
                    List<Model.PowerInfo> list = new List<Model.PowerInfo>();
                    //添加|2,修改|2,删除|2,查询|2,添加|3,修改|3,删除|3,查询|3,添加|4,修改|4,删除|4,查询|4,添加|5,修改|5,添加|6,修改|6,删除|6,查询|6,添加|7,修改|7,删除|7,查询|7,添加|8,修改|8,删除|8,查询|8,添加|18,修改|18,删除|18,查询|18,添加|19,修改|19,删除|19,查询|19

                    List<int> tempFid = new List<int>();
                    List<string> tempPower = new List<string>();
                    string tempStr = string.Empty;

                    if (!string.IsNullOrEmpty(strPower))
                    {
                        string[] arrayPower = strPower.Split(',');
                        int i = 0;
                        foreach (string one in arrayPower)
                        {
                            i++;
                            string[] temp = one.Split('|');
                            int fid = Falcon.Function.ToInt(temp[1]);
                            if (tempFid.Contains(fid))
                            {
                                tempStr += "," + temp[0];
                            }
                            else
                            {
                                tempFid.Add(fid);
                                if (!string.IsNullOrEmpty(tempStr))
                                {
                                    tempPower.Add(tempStr);
                                }
                                tempStr = temp[0];
                            }
                            if (i == arrayPower.Length)
                            {
                                tempPower.Add(tempStr);
                            }
                        }
                    }

                    List<Model.Z_Forms> listForms = new BLL.Z_Forms().GetListByWhere("1=1");
                    //for (int j = 0; j < arrFids.Length; j++)
                    foreach (int one in arrFids)
                    {
                        Model.Z_Forms model = (from m in listForms where m.id == one select m).FirstOrDefault();
                        if (model != null && model.id != 0)
                        {
                            string url = model.url;
                            string power = string.Empty;
                            if (tempFid.Contains(one))
                            {
                                power = tempPower[tempFid.IndexOf(one)];
                            }
                            list.Add(new Model.PowerInfo() { Fid = one, Url = url, Power = power });
                        }
                    }
                    strFids = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(list);
                }
                return new BLL.Z_Role().SetPower(role, strFids);
            }
            else
            {
                return 0;
            }
        }
    }
}