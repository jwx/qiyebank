﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class daikuanBF : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.Daikuan dkModel = new BLL.Daikuan().GetModelByID(id);
                    Model.UserInfo uModel = new BLL.UserInfo().GetModel(dkModel.uid);
                    userid = dkModel.uid;
                    lblUserName.Text = uModel.username;
                    lblRealName.Text = uModel.realname;
                    txtDaikuanTime.Text = dkModel.daikuanTime;
                    txtZHoqi.Text = dkModel.zhouqi;
                    txtDaikuan.Text = dkModel.jine.ToString();
                    txtLilv.Text = dkModel.lilv.ToString();
                    txtDaoqiTime.Text = dkModel.daoqiTime;
                    if (dkModel.huankuanType == 0)
                        rdDkjs0.Checked = true;
                    else if (dkModel.huankuanType == 1)
                        rdDkjs1.Checked = true;
                    else if (dkModel.huankuanType == 2)
                        rdDkjs2.Checked = true;
                    else if (dkModel.huankuanType == 3)
                        rdDkjs3.Checked = true;

                    payBack.InnerHtml = "利息：" + dkModel.lixi + "，本息合计：" + (dkModel.lixi + dkModel.jine) + "元。";

                    List<Model.DaikuanPayback> listModel = new BLL.DaikuanPayback().getListModel(id);
                    string html = "";
                    for (int i = 0; i < listModel.Count; i++)
                    {
                        html += "<p> 本金还款日期：" + listModel[i].huankuanDate + ",还款金额：" + listModel[i].jine + "元 </p >";
                    }
                    lixi.InnerHtml = html;
                    txtAddRemark.Text = dkModel.add_remark;
                    modelSetup = new BLL.Setup().Get_Setup();
                    if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dk_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                    else
                    {
                        lblHuoqi.Text = new BLL.Huoqi().GetYue(dkModel.uid) + "元";
                        lblDingqi.Text = new BLL.Dingqi().GetYue(dkModel.uid) + "元";
                        hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                        string byWhat = modelSetup.dk_byDay == 0 ? "月" : "天";
                    }
                }
            }
        }
        protected int id;

        protected Model.Setup modelSetup;

        protected int userid;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Model.Daikuan dkModel = new BLL.Daikuan().GetModelByID(id);
            if (dkModel.yihuan > 0)  //有已还金额
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败,该记录有已还金额！',{icon:5,time:1000});", true);
            }
            else
            {
                int res=new BLL.Daikuan().DaikuanBF(dkModel.id,SessionUid,txtBFRemark.Text);
                if (res > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }


        }





    }
}