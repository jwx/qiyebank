﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class huankaninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    daikModel = new BLL.DaikuanPayback().GetModelByID(id);
                    txtHuankuanTime.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    txtHKMoney.Text = daikModel.jine.ToString();
                    modelSetup = new BLL.Setup().Get_Setup();
                    if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dk_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                    else
                    {
                        hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                    }
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        protected Model.DaikuanPayback daikModel;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool flag = true;
            int res = new BLL.DaikuanPayback().Update(id, DateTime.Now.ToString("yyyy-MM-dd"), 2, SessionUid, txtRemk.Text);
            if (res > 0)
            {
                daikModel = new BLL.DaikuanPayback().GetModelByID(id);
                int sum = new BLL.Daikuan().Update(daikModel.dkId, daikModel.jine);
                if (sum < 0)
                {
                    flag = false;
                }
            }
            else
            {
                flag = false;
            }

            if (flag)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }


        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }




    }
}