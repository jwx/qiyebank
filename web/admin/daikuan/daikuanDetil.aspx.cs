﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class daikuanDetil : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.Daikuan dkModel = new BLL.Daikuan().GetModelByID(id);
                    Model.UserInfo uModel = new BLL.UserInfo().GetModel(dkModel.uid);
                    userid = dkModel.uid;
                    lblUserName.Text = uModel.username;
                    lblRealName.Text = uModel.realname;
                    txtDaikuanTime.Text = dkModel.daikuanTime;
                    txtZHoqi.Text = dkModel.zhouqi;
                    txtDaikuan.Text = dkModel.jine.ToString();
                    txtLilv.Text = dkModel.lilv.ToString();
                    txtDaoqiTime.Text = dkModel.daoqiTime;
                    if (dkModel.huankuanType == 0)
                        rdDkjs0.Checked = true;
                    else if (dkModel.huankuanType == 1)
                        rdDkjs1.Checked = true;
                    else if (dkModel.huankuanType == 2)
                        rdDkjs2.Checked = true;
                    else if (dkModel.huankuanType == 3)
                        rdDkjs3.Checked = true;

                    payBack.InnerHtml = "利息：" + dkModel.lixi + "，本息合计：" + (dkModel.lixi + dkModel.jine) + "元。";

                   List<Model.DaikuanPayback> listModel=  new BLL.DaikuanPayback().getListModel(id);
                    string html = "";
                    for (int i = 0; i < listModel.Count; i++)
                    {
                        html += "<p> 本金还款日期：" + listModel[i].huankuanDate +",还款金额：" + listModel[i].jine + "元 </p >";
                    }
                    lixi.InnerHtml = html;
                    txtAddRemark.Text = dkModel.add_remark;
                    modelSetup = new BLL.Setup().Get_Setup();
                    if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dk_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                    else
                    {
                        lblHuoqi.Text = new BLL.Huoqi().GetYue(dkModel.uid) + "元";
                        lblDingqi.Text = new BLL.Dingqi().GetYue(dkModel.uid) + "元";
                        hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                        string byWhat = modelSetup.dk_byDay == 0 ? "月" : "天";
                    }
                }
            }
        }
        protected int id;

        protected Model.Setup modelSetup;

        protected int userid;



    }
}