﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class huankuanlist : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
          
            if (!IsPostBack)
            {
                flag = false;
                Query();
                modelSetup = new BLL.Setup().Get_Setup();
                List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dk_lilv);
                string byWhat = modelSetup.dk_byDay == 0 ? "月" : "天";
                foreach (Model.LilvRange one in listLilvRange)
                {
                    ddldkzq.Items.Add(new ListItem() { Text = one.name + "_" + one.min + byWhat, Value = one.name + "_" + one.min + byWhat });//value，存款周期|利率
                }
                ddldkzq.Items.Insert(0, new ListItem() { Text = "请选择贷款周期", Value = "" });
            }
        }
        protected Model.Setup modelSetup;
        protected List<Model.Daikuan> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1 "; //0 没有报废 1 报废
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtdanhao.Text.Trim() != "") where += " and danhao like '%" + txtdanhao.Text + "%'";
                    if (txtjbname.Text.Trim() != "") where += " and jbname like '%" + txtjbname.Text.Trim() + "%'";
                    if (ddldkzq.SelectedValue!="") where += " and zhouqi like '%" + ddldkzq.SelectedValue+ "%'";
                    if (ddlhklx.SelectedValue != "") where += " and huankuanType = " + ddlhklx.SelectedValue + "";
                    if (ddlBaofei.SelectedValue != "") where += " and canBaofei = " + ddlBaofei.SelectedValue + "";
                    if (txteStart.Text.Trim() != "") where += " and daoqiTime>='" + txteStart.Text.Trim() + "'";
                    if (txteEnd.Text.Trim() != "") where += " and daoqiTime<='" + txteEnd.Text.Trim() + "'";
                    if (txtsStart.Text.Trim() != "") where += " and daikuanTime>='" + txtsStart.Text.Trim() + "'";
                    if (txtsEnd.Text.Trim() != "") where += " and daikuanTime<='" + txtsEnd.Text.Trim() + "'";
                }
            }
            listRecord = Tool.Pager_Sqlite.Query<Model.Daikuan>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_HuankuanList", "*", where, "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }



        //[AjaxPro.AjaxMethod]
        //public int Delete(int id)
        //{
        //    return new BLL.Daikuan().DaikuanBF(id);
        //}

    }
}