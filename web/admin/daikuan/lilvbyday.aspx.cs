﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class lilvbyday : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
        }

        [AjaxPro.AjaxMethod]
        public bool Save(string strName, string strDays, string strLilv)
        {
            if (strName != "" && strDays != "" && strLilv != "")
            {
                string[] arrayName = strName.Split(',');
                int[] arrayDays = Array.ConvertAll<string, int>(strDays.Split(','), m => { return Falcon.Function.ToInt(m, 0); });
                decimal[] arrayLilv = Array.ConvertAll<string, decimal>(strLilv.Split(','), m => { return Falcon.Function.ToDecimal(m, 0); });
                List<Model.LilvRange> list = new List<Model.LilvRange>();
                for (int i = 0; i < arrayName.Length; i++)
                {
                    list.Add(new Model.LilvRange() { name = arrayName[i], min = arrayDays[i], max = arrayDays[i], lilv = arrayLilv[i], unit = "天" });
                }
                string json = Tool.JsonHelper.JsonSerializer<List<Model.LilvRange>>(list);

                return new BLL.Setup().Update_DaikuanLilv(json);
            }
            return false;
        }
    }
}