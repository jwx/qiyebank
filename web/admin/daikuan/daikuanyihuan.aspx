﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="daikuanyihuan.aspx.cs" Inherits="web.admin.daikuan.daikuanyihuan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });

        function fnSetLilv() {
            layer.closeAll();
            layer.alert("请先在“基础信息”菜单的“系统设置”栏目中设置零钱利率！");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a class="tab" href="daikuanyuqi.aspx?id=<%=dkid %>">贷款逾期
                    </a>
                    <a class="tab" href="daikuanweihuan.aspx?id=<%=dkid %>">贷款待还
                    </a>
                    <a href="javascript:void(0);" class="tab current">贷款已还
                    </a>
                </div>
                <div class="f_r">
                   <%-- <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名</asp:ListItem>
                        <asp:ListItem Value="danhao">存款单号</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                        <asp:ListItem Value="add_remark">存款备注</asp:ListItem>
                    </asp:DropDownList>--%>
                  <%--  <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>--%>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                  <%--  <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">存款单号：</td>
                            <td>
                                <asp:TextBox ID="txtDanhao" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">交易类型：</td>
                            <td>
                                <asp:DropDownList ID="ddlLaiyuan" runat="server"></asp:DropDownList>
                            </td>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">存款时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCunkuanTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtCunkuanTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">录入时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">存款备注：</td>
                            <td>
                                <asp:TextBox ID="txtAdd_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>--%>
                 <%--   <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>--%>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>还款日期</td>
                            <td>还款金额</td>
                            <td>真实还款日期</td>
                            <td>还款方式</td>
                            <td>经办人</td>
                            <td>备注</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                            {
                                int i = 0;
                                string yonghu = getPower("用户明细");
                                string cunkuan = getPower("存款明细");
                                string print = getPower("打印");
                                string baofei = getPower("报废");
                                foreach (Model.DaikuanPayback one in listRecord)
                                {
                                    i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                           
                                <%--<a href="../users/detail.aspx?id=<%=one.dkId %>" class="label label-primary layer <%=yonghu %>">还款</a>--%>
                                <%--    <a href="cunkuandetail.aspx?id=<%=one.id %>" class="label label-info layer <%=cunkuan %>" title="存款明细_<%=one.realname %>">存款明细</a>
                                <a href="phc1.aspx?id=<%=one.id %>" class="label label-success layer <%=print %>" title="存款打印_<%=one.realname %>">打印</a>
                                <a href="cunkuanbaofeiinfo.aspx?id=<%=one.id %>&account=<%=one.username %>&name=<%=Server.UrlEncode(one.realname) %>" class="label label-danger layer <%=baofei %>" title="零钱存款报废_<%=one.realname %>">报废</a>--%>
                            <td>
                                <%=one.huankuanDate %> 
                            </td>
                            <td ><%=one.jine %></td>
                            <td ><%=one.huankuanTime %></td>
                               <td>
                                <%if (one.huankuanType == 1)
                                    { %>
                                  零钱还款
                                  <%}
                                      else if (one.huankuanType == 2)
                                      {%>
                                  柜台还款
                                  <%} %>
                            </td>
                            <td><%=one.jbname %></td>
                            <td><%=one.remark %></td>
                        </tr>
                        <%}
                            }
                            else
                            { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
