﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="daikuanStages.aspx.cs" Inherits="web.admin.daikuan.daikuanStages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" />
    <script src="/Plug/My97DatePicker/WdatePicker.js"></script>
    <style type="text/css">
        .item {
        }

            .item .title {
                font-size: 14px;
                margin-top: 5px;
                padding-bottom: 5px;
                border-bottom: 1px solid #dedede;
            }

            .item .content {
                padding-bottom: 10px;
            }

                .item .content p input[type='text'] {
                    width: 50px;
                    margin: 0px 3px;
                }

                    .item .content p input[type='text'].zhouqi {
                        width: 80px;
                    }

                .item .content p a {
                    margin-left: 5px;
                }
    </style>
    <script type="text/javascript">  
        function fnAddRow() {
            var html = '<p class="item"> 还款日期：<input type="text" datatype="*" class="Wdate" nullmsg="请填写还款日期" errormsg="请填写正整数"  style="width:110px" onclick="WdatePicker()" />，每期还款金额<input type="text" datatype="*" class="payMoney"  value=""  readonly="readonly"  style="width:90px"/> <a href="javascrpt:void(0);" onclick="fnRemove(this)" class="dellilv">[删除]</a></p>'
            $(".content .data").append(html);
            fnCalc();
        }

        function fnRemove(t) {
            $(t).parent().remove();
            fnCalc();
        }

        function fnCalc() {
            var benjin = Number($("#txtDaikuan", parent.document).val());//本金
            var qi = $(".data .item").length;
            if (qi != 0) {
                var payBack = (benjin / qi).toFixed(2);
                $(".payMoney").val(payBack);
            }
        }

        function fnConfirm() {
            var arrayPayBackDate = [];
            var verifyError = false;
            $(".Wdate").each(function () {
                var t = $(this);
                var date = $.trim(t.val());
                if (date) {
                    arrayPayBackDate.push({ time: date, money: t.parent().find(".payMoney").val() });
                } else {
                    verifyError = true;
                    return false;
                }
            });
            if (verifyError) {
                layer.msg("请选择还款日期", { icon: 5, time: 1500 });
                return false;
            } else {
                parent.arrayDate = JSON.stringify(arrayPayBackDate);
                return true;
            }
        }
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main ">
            <div class="item">
                <div class="title">
                    设置还款期限
                </div>
                <p>
                    <input type="button" id="btnAddRow" value="+ 新增还款期限" onclick="fnAddRow()" />
                </p>
                <div class="content">
                    <div class="data">
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
