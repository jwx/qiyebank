﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="huankaninfo.aspx.cs" Inherits="web.admin.daikuan.huankaninfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquanma = $("#txtShouquanma");
                    if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                        layer.alert("授权码不正确！");
                        return false;
                    }
                    layer.load();
                }
            });
        });

    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <asp:HiddenField ID="hideLixi" runat="server" />
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl"><span class="required">*</span>还款金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtHKMoney" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入还款金额" errormsg="请输入还款金额"  Enabled="false"></asp:TextBox>
                        元 
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtHuankuanTime" runat="server" datatype="*" CssClass="Wdate" placeholder="请输入贷款日期" errormsg="请输入贷款日期" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" Enabled="false"></asp:TextBox>

                    </td>
                </tr>

                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemk" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                        <input type="hidden" id="dkID" runat="server" />
                        <input type="hidden" id="jine" runat="server" />
                        <input type="hidden" id="huankuanDate" runat="server" />
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                    { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="确定" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

