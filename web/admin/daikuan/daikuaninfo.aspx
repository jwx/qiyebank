﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="daikuaninfo.aspx.cs" Inherits="web.admin.daikuan.daikuaninfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquanma = $("#txtShouquanma");
                    if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                        layer.alert("授权码不正确！");
                        return false;
                    }
                    layer.load();
                }
            });
        });

        function fnGetDaoqiTime() {
            var daikuanTime = $("#txtDaikuanTime").val();
            var zhouqi = $("#ddlZhouqi").val();
            if (zhouqi == "") {
                $("#txtLilv").val("");
                $("#txtDaoqiTime").val("");
                $("#payBack").html("");
                $("#lixi").html("");
                return;
            }
            var byDay = Number($("#byWhat").attr("by"));
            if (daikuanTime && zhouqi) {
                $("#txtDaoqiTime,input[name='hideDaoqiTime']").val(ajax.GetDaoqiTime(byDay, daikuanTime, zhouqi).value);
                $("#txtLilv").val(zhouqi.split('|')[1]);
            }
            fnGetLixi();
        }

        function fnGetLixi() {
            var daikuan = Number($("#txtDaikuan").val());
            if (!daikuan) return;
            var lilv = Number($("#txtLilv").val());
            if (!lilv) return;
            //var lixi = Number((daikuan * lilv % 100).toFixed(2)); alert(daikuan + "::" + lilv);
            var lixi = ajax.GetLixi($("#byWhat").attr("by"), daikuan, lilv).value;
            $("#hideLixi").val(lixi);
            var benxi = Number((daikuan + lixi).toFixed(2));

            $("#payBack").html("利息：" + lixi + "元，本息合计：" + benxi + "元。<br/>");
            $("#txtlixi").val(lixi);
            fnGetPaybackDetail();
        }

        var arrayDate; //弹出层返回的数据
        function fnGetPaybackDetail() {

            var daikuan = $("#txtDaikuan").val();
            var zhouq = $("#ddlZhouqi").val();
            if (daikuan == "" || zhouq == "" || daikuan == "undefined" || zhouq=="undefined") {
                return;
            }
            var lixi = Number($("#hideLixi").val()); //利息
            var benji = Number($("#txtDaikuan").val()); //本金
            var zhouqi = $("#ddlZhouqi").val();//周期
            var dkTime = $("#txtDaikuanTime").val(); //月份
            // 3|6  月份|利率  取出月份
            var obj;  //月份
            if (zhouqi != "") {
                obj = zhouqi.split('|'); //月份 obj[0]
            }
            var index = $("input[type='radio'][name='rddkjs']:checked").index() / 2;
            switch (index) {
                case 0:
                    //按月付利息，到期还本金
                    var shijian = new Date(dkTime)
                    shijian.setMonth(shijian.getMonth() + Number(obj[0]) + 1);
                    var format = shijian.getFullYear() + "-" + shijian.getMonth() + "-" + shijian.getDate();
                    //  $("#benjin").html("本金还款日期：" + format);
                    $("#lixi").html("每月还利息：" + Number(lixi / obj[0]).toFixed(2) + "元，" + format + "  到期还款本金" + benji + "元");
                    break;
                case 1:
                    var html = "";
                    $("#lixi").html("");
                    $("#lixi").html("<p>每月还利息："+ Number(lixi / obj[0]).toFixed(2) + "元</p>")
                    var index = layer.open({
                        type: 2,
                        content: "daikuanStages.aspx",//?lixi=" + lixi + "&benji=" + benji + "&zhouqi=" + obj[0],
                        area: ['450px', '500px'],
                        maxmin: true,
                        btn: ['确定', '取消']
                        , yes: function (index, layero) {
                            var frame = window['layui-layer-iframe' + index];
                            if (frame.fnConfirm()) {
                                $.each(JSON.parse(arrayDate), function (i,item) {
                                   html += "<p>本金还款日期：" + item.time +",还款金额：" + item.money + "元</p>";
                                })
                                $("#lixi").append(html);
                                $("#txtfqlx").val(arrayDate)
                                layer.close(index);
                            }
                        }
                    });
                    break;
                case 2:
                    //每月付本息
                    $("#lixi").html("每月还本息：" + Number((lixi + benji) / obj[0]).toFixed(2) + "元");
                    break;
                case 3:
                    //到期付本息
                    var shijian = new Date(dkTime)
                    shijian.setMonth(shijian.getMonth() + Number(obj[0]) + 1);
                    var format = shijian.getFullYear() + "-" + shijian.getMonth() + "-" + shijian.getDate();
                    var benxi = lixi + benji;
                    $("#lixi").html(format + " 到期还本息" + benxi + "元");
                    break;
            }
        }

    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <asp:HiddenField ID="hideLixi" runat="server" />
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<asp:Label ID="lblUserName" runat="server"></asp:Label>]<asp:Label ID="lblRealName" runat="server"></asp:Label>
                        <a href="../users/detail.aspx?id=<%=id %>" class="layer" title="用户明细">查看用户明细</a>
                        <asp:Label ID="uid" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">贷款利率明细： 
                    </td>
                    <td>
                        <% decimal budaoqi = 0;
                            if (modelSetup != null)
                            {
                                string byWhat = modelSetup.dk_byDay == 0 ? "月" : "天";
                        %>
                        <div id="byWhat" by="<%=modelSetup.dk_byDay %>">
                            利息按<%=byWhat %>计算
                        </div>
                        <div>
                            <%
                                if (!string.IsNullOrEmpty(modelSetup.dk_lilv))
                                {
                                    List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dk_lilv);
                                    foreach (Model.LilvRange one in listLilv)
                                    {
                                        budaoqi = one.budaoqi;
                            %>
                            <div>
                                贷款周期：<%=one.name %>，贷款时间：<%=one.min %><%=byWhat %>，年利率:<%=one.lilv %>% 
                            </div>
                            <%}
                                } %>
                        </div>
                        <%} %>
                        <input type="hidden" name="hideBudaoqi" value="<%=budaoqi %>" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>贷款金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDaikuan" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入贷款金额" errormsg="请输入贷款金额" onblur="fnGetLixi()"></asp:TextBox>
                        元 
                        <span class="Validform_checktip">请输入贷款金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>贷款日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDaikuanTime" runat="server" datatype="*" CssClass="Wdate" placeholder="请输入贷款日期" errormsg="请输入贷款日期" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',onpicked:function(){$dp.$('txtCunkuanTime').blur();}})" onblur="fnGetDaoqiTime()"></asp:TextBox>
                        <span class="Validform_checktip">请输入贷款日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>贷款周期： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlZhouqi" runat="server" datatype="*" onchange="fnGetDaoqiTime()"></asp:DropDownList>
                        <span class="Validform_checktip">请输入贷款日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>贷款年利率： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtLilv" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入贷款年利率" errormsg="请输入贷款年利率" onblur="fnGetLixi()"></asp:TextBox>%
                        <span class="Validform_checktip">请输入贷款年利率</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDaoqiTime" runat="server"  Enabled="false"></asp:TextBox>
                        <input  type="hidden" id="txtlixi" runat="server"/>
                          <input  type="hidden" id="txtfqlx" runat="server"/>
                        <input type="hidden" name="hideDaoqiTime"  id="hideDaoqiTime" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期还款方式： 
                    </td>
                    <td>
                        <asp:RadioButton ID="rdDkjs0" runat="server" Text="按月付利息,到期还本金" GroupName="rddkjs" onclick="fnGetPaybackDetail()" Checked="true" />
                        <asp:RadioButton ID="rdDkjs1" runat="server" Text="按月付利息,分期还本金" GroupName="rddkjs" onclick="fnGetPaybackDetail()" />
                        <asp:RadioButton ID="rdDkjs2" runat="server" Text="每月付本息" GroupName="rddkjs" onclick="fnGetPaybackDetail()" />
                        <asp:RadioButton ID="rdDkjs3" runat="server" Text="到期付本息" GroupName="rddkjs" onclick="fnGetPaybackDetail()" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl">还款信息： 
                    </td>
                    <td>
                        <div id="benxi"></div>
                        <div id="payBack"></div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">还款详情： 
                    </td>
                    <td>
                        <div id="benjin"></div>
                        <div id="lixi"></div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                    { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
