﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.daikuan
{
    public partial class daikuaninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
                lblRealName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("name"));

                txtDaikuanTime.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dk_lilv))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                }
                else
                {
                    lblHuoqi.Text = new BLL.Huoqi().GetYue(id) + "元";
                    lblDingqi.Text = new BLL.Dingqi().GetYue(id) + "元";
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);

                    //ddlZhouqi.DataSource = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dk_lilv);
                    //ddlZhouqi.DataTextField = "name";
                    //ddlZhouqi.DataValueField = "min"; 
                    List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dk_lilv);
                    string byWhat = modelSetup.dk_byDay == 0 ? "月" : "天";
                    foreach (Model.LilvRange one in listLilvRange)
                    {
                        ddlZhouqi.Items.Add(new ListItem() { Text = one.name + "_" + one.min + byWhat, Value = one.min + "|" + one.lilv });//value，存款周期|利率
                    }
                    ddlZhouqi.Items.Insert(0, new ListItem() { Text = "请选择贷款周期", Value = "" });
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        [AjaxPro.AjaxMethod]
        public decimal GetLixi(string byWhat, decimal daikuan, decimal lilv)
        {
            int jishu = byWhat == "0" ? 12 : 365;
            return Math.Round((daikuan * lilv / jishu), 2, MidpointRounding.AwayFromZero);
        }

        [AjaxPro.AjaxMethod]
        public string GetDaoqiTime(int dk_byDay, string cunkuanTime, string ddlVal)
        {
            int zhouqi = Falcon.Function.ToInt(ddlVal.Split('|')[0]);//存款周期|利率
            return dk_byDay == 1 ? Falcon.Function.ToDateTime(cunkuanTime).AddDays(zhouqi).ToString("yyyy-MM-dd") : Falcon.Function.ToDateTime(cunkuanTime).AddMonths(zhouqi).ToString("yyyy-MM-dd");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value);
            decimal cunkuan = Falcon.Function.ToDecimal(txtDaikuan.Text.Trim()); //贷款金额
            decimal lixi = Falcon.Function.ToDecimal(txtlixi.Value.Trim());//利息
            string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //当前时间
            string dkTime = txtDaikuanTime.Text;//贷款日期
            string daoqiTIme = hideDaoqiTime.Value;//到期日期
            var zhouqi = ddlZhouqi.SelectedItem.Value; //贷款周期
            var obj = zhouqi.Split('|');
            int dkjs = 0;
            if (rdDkjs1.Checked) dkjs = 1;
            else if (rdDkjs2.Checked) dkjs = 2;
            else if (rdDkjs3.Checked) dkjs = 3;

            int res = new BLL.Daikuan().Add(id, decimal.Parse(txtDaikuan.Text), new Tool.OrderNo().GetOrderNo(), 1, 1, dkjs, ddlZhouqi.SelectedItem.Text, decimal.Parse(txtLilv.Text), lixi, (cunkuan + lixi), 0, dtNow, txtDaikuanTime.Text, daoqiTIme, SessionUid, txtAddRemark.Text, "", 0, "");
            if (res > 0)
            {

                //按月付利息,到期还本金
                if (dkjs == 0)
                {
                    bool count = true;
                    int flag = 0;
                    for (int i = 1; i <= int.Parse(obj[0]); i++) //利息
                    {
                        DateTime dt1 = DateTime.Parse(dkTime);
                        var resTime = dt1.AddMonths(i); //每个月
                        decimal sum = lixi / int.Parse(obj[0]);
                        decimal ylixi = Math.Round(sum, 2); //每月应还利息
                        flag = new BLL.DaikuanPayback().Add(res, ylixi, resTime.ToString("yyyy-MM-dd"), "", 0, 0, "");
                        if (flag < 0)
                        {
                            count = false;
                        }
                    }
                    flag = new BLL.DaikuanPayback().Add(res, cunkuan, daoqiTIme, "", 0, 0, "");//本金
                    if (flag < 0)
                    {
                        count = false;
                    }
                    if (count)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
                else if (dkjs == 2) //每月付本息
                {
                    bool count1 = true;
                    for (int i = 0; i < int.Parse(obj[0]); i++) //几个月
                    {
                        DateTime dt1 = DateTime.Parse(dkTime);
                        var resTime = dt1.AddMonths(i + 1); //每个月
                        //本金加利息
                        decimal sum = (cunkuan + lixi) / int.Parse(obj[0]);
                        decimal ylixi = Math.Round(sum, 2); //每月应还利息
                        int flag = new BLL.DaikuanPayback().Add(res, ylixi, resTime.ToString("yyyy-MM-dd"), "", 0, 0, "");
                        if (flag < 0)
                        {
                            count1 = false;
                        }
                    }
                    if (count1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
                else if (dkjs == 3) //到期付本息
                {
                    int flag = new BLL.DaikuanPayback().Add(res, (cunkuan + lixi), daoqiTIme, "", 0, 0, "");
                    if (flag > 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
                else if (dkjs == 1) //按月付利息,分期还本金
                {
                    bool count3 = true;
                    string fqlx = txtfqlx.Value;
                    List<Model.ListDaikuan> jsonlist = JsonConvert.DeserializeObject<List<Model.ListDaikuan>>(fqlx);
                    //循环添加分期本金
                    for (int i = 0; i < jsonlist.Count; i++)
                    {
                        int flag = new BLL.DaikuanPayback().Add(res, jsonlist[i].money, jsonlist[i].time, "", 0, 0, "");
                        if (flag < 0)
                        {
                            count3 = false;
                        }
                    }
                    for (int i = 0; i < int.Parse(obj[0]); i++) //几个月
                    {
                        DateTime dt1 = DateTime.Parse(dkTime);
                        var resTime = dt1.AddMonths(i + 1); //每个月
                        //利息
                        decimal sum = lixi / int.Parse(obj[0]);
                        decimal ylixi = Math.Round(sum, 2); //每月应还利息
                        int flag = new BLL.DaikuanPayback().Add(res, ylixi, resTime.ToString("yyyy-MM-dd"), "", 0, 0, "");
                        if (flag < 0)
                        {
                            count3 = false;
                        }
                    }
                    if (count3)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }

        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }








    }
}