﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="daikuanyewulist.aspx.cs" Inherits="web.admin.daikuan.daikuanyewulist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });

        function fnSetLilv() {
            layer.alert("请先在“基础信息”菜单的“系统设置”栏目中设置零钱利率！", function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <%-- <a class="btn btnBlue layer <%=getPower("账号开户") %>" href="../users/info.aspx" title="账号开户">+ 账号开户</a>--%>
                    <%--<a class="btn btnGreen <%=getPower("批量存款") %> layer" href="cunkuanimport.aspx" title="零钱存款批量导入">批量存款</a>--%>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="danhao">单号</asp:ListItem>
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名/公司名称</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名/公司名称：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">单号：</td>
                            <td>
                                <asp:TextBox ID="txtdanhao" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtjbname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">贷款周期：</td>
                            <td>
                                <asp:DropDownList ID="ddldkzq" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="tdl">还款类型：</td>
                            <td>
                                <asp:DropDownList ID="ddlhklx" runat="server">
                                    <asp:ListItem Value="">请选择类型</asp:ListItem>
                                    <asp:ListItem Value="0">按月付利息,到期还本金</asp:ListItem>
                                    <asp:ListItem Value="1">按月付利息,分期还本金</asp:ListItem>
                                    <asp:ListItem Value="2">每月付本息</asp:ListItem>
                                    <asp:ListItem Value="3">到期付本息</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="tdl">贷款时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtsStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtsEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">到期时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txteStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txteEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作
                            </td>
                            <td>单号</td>
                            <td>账户</td>
                            <td>户名/公司名称</td>
                            <td>贷款金额</td>
                            <td>贷款周期</td>
                            <td>贷款利率</td>
                            <td>贷款利息</td>
                            <td>本金+利息</td>
                            <td>还款类型</td>
                            <td>当前已还金额</td>
                            <td>贷款时间</td>
                            <td>到期时间</td>
                            <td>经办人</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                            {
                                int i = 0;
                                string daikuaninfo = getPower("贷款明细");
                                string userinfo = getPower("用户明细");
                                foreach (Model.Daikuan one in listRecord)
                                {
                                    i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.uid %>" class="label label-info layer <%=userinfo %>" title="用户明细_<%=one.realname %>">用户明细</a>
                                <a href="daikuanDetil.aspx?id=<%=one.id %>" class="label label-success layer <%=daikuaninfo %>" title="贷款明细_<%=one.realname %>">贷款明细</a>
                            </td>
                            <td>
                                <%=one.danhao %> 
                            </td>
                            <td>
                                <%=one.username %>
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.jine %>
                            </td>
                            <td>
                                <%=one.zhouqi %>
                            </td>
                            <td>
                                <%=one.lilv %>
                            </td>
                            <td>
                                <%=one.lixi %>
                            </td>
                            <td>
                                <%=one.benxi %>
                            </td>
                            <td>
                                <%if (one.huankuanType == 0)
                                    {%>
                                  按月付利息,到期还本金
                                <%}
                                    else if (one.huankuanType == 1)
                                    {%>
                                按月付利息,分期还本金
                                <%}
                                    else if (one.huankuanType == 2)
                                    {%>
                                每月付本息
                                <%}
                                    else if (one.huankuanType == 3)
                                    {%>
                                到期付本息
                                <%} %>
                            </td>

                            <td>
                                <%=one.yihuan %>
                            </td>
                            <td>
                                <%=one.daikuanTime%>
                            </td>
                            <td>
                                <%=one.daoqiTime %>
                            </td>
                 
                            <td>
                                <%=one.jbname %>
                            </td>
                        </tr>
                        <%}
                            }
                            else
                            { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
