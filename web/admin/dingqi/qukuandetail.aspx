﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuandetail.aspx.cs" Inherits="web.admin.dingqi.qukuandetail" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script src="/Plug/Validform/Validform_Datatype.js"></script>
    <style type="text/css">
        .lixi { font-weight: bold; color: red; }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var obj = $("#txtShouquanma");
                    if (obj.length != 0 && !ajax.VerifyShouquanma(obj.val()).value) {
                        layer.alert("授权码错误！");
                        return false;
                    }
                    layer.load();
                }
            });


            var qukuan = 0;
            var qukuanlixi = 0;

            $("#btnBenjin").click(function () {
                if (fnVerify()) {
                    var t = $(this);
                    return confirm("确定只支取" + $("#lblQukuan").text() + "元本金，同时将" + $("#lblLixi").text() + "元利息转入零钱账户吗？");
                }
                return false;
            });


            $("#btnBenjinLixi").click(function () {
                if (fnVerify()) {
                    var t = $(this);
                    return confirm("确定同时本息取款，合计" + t.attr("benxi") + "元吗？");
                }
                return false;
            });

            function fnVerify() {
                if (!ajax.VerifyUserPswd(Number($(".main").attr("id")), $("#txtUserPswd").val()).value) {
                    layer.alert("用户密码错误！");
                    return false;
                }
                var shouquanma = $("#txtShouquanma");
                if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                    layer.alert("授权码错误！");
                    return false;
                }
                return true;
            }
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <div class="main">
            <% if (model != null && model.id != 0)
               { %>
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<%=model.username %>]<%=model.realname %>
                        <a href="../users/detail.aspx?id=<%=model.uid %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款单号： 
                    </td>
                    <td>
                        <%=model.danhao %> <a href="cunkuandetail.aspx?id=<%=model.oid %>" class="layer">[存款信息]</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款金额：<%--本金取款 = 1,
            本息取款 = 2--%> 
                    </td>
                    <td style="font-size: 14px;">本金：<%=model.qukuanType==1? -model.jine:(-model.jine-model.lixi) %>
                        元，
                        利息：<%=model.lixi %>
                        元
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款方式： 
                    </td>
                    <td>
                        <%=(Config.Enums.Dingqi_Qukuan)model.qukuanType %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款时间： 
                    </td>
                    <td>
                        <%=model.cunkuanTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">年利率： 
                    </td>
                    <td>
                        <%=model.lilv %>%
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <%=model.add_remark %>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </form>
</body>
</html>
