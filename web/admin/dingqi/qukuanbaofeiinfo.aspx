﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuanbaofeiinfo.aspx.cs" Inherits="web.admin.dingqi.qukuanbaofeiinfo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquma = $("#txtShouquanma");
                    if (shouquma.length != 0) {
                        if (!ajax.VerifyShouquanma(shouquma.val()).value) {
                            layer.alert("授权码不正确！");
                            return false;
                        }
                    }
                    layer.load();
                }
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<%=model_Cunkuan.username %>]<%=model_Cunkuan.realname %>
                        <a href="../users/detail.aspx?id=<%=model_Cunkuan.uid %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                    <td class="tdl">存款单号： 
                    </td>
                    <td>
                        <%=model_Cunkuan.danhao %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款金额： 
                    </td>
                    <td>
                        <%=model_Cunkuan.jine %>
                        元 
                    </td>
                    <td class="tdl">存款周期： 
                    </td>
                    <td>
                        <%=model_Cunkuan.zhouqi %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款时间： 
                    </td>
                    <td>
                        <%=model_Cunkuan.cunkuanTime %>
                    </td>
                    <td class="tdl">到期时间： 
                    </td>
                    <td>
                        <%=model_Cunkuan.daoqiTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期结算方式： 
                    </td>
                    <td>
                        <%=(Config.Enums.Dingqi_DaoqiHandler)model_Cunkuan.daoqi_handler %>
                    </td>
                    <td class="tdl">到期年利率： 
                    </td>
                    <td>
                        <%=model_Cunkuan.lilv %>%
                    </td>
                </tr>
                <tr>
                    <td class="tdl">不到期年利率： 
                    </td>
                    <td>
                        <%=model_Cunkuan.budaoqi %>%
                    </td>
                    <td class="tdl">存款经办人： 
                    </td>
                    <td>
                        <%=model_Cunkuan.jbname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款备注： 
                    </td>
                    <td>
                        <%=model_Cunkuan.add_remark %>
                    </td>
                    <td class="tdl">存款录入时间： 
                    </td>
                    <td>
                        <%=model_Cunkuan.addTime %>
                    </td>
                </tr>
                <% if (model_Cunkuan.delid != 0)
                   { %>
                <tr>
                    <td class="tdl">存款报废人： 
                    </td>
                    <td>
                        <%=model_Cunkuan.delname %>
                    </td>
                    <td class="tdl">报废时间： 
                    </td>
                    <td>
                        <%=model_Cunkuan.delTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">报废备注： 
                    </td>
                    <td colspan="3">
                        <%=model_Cunkuan.del_remark %>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td class="tdl">取款单号： 
                    </td>
                    <td>
                        <%=model_Qukuan.danhao %>
                    </td>
                    <td class="tdl">取款类型： 
                    </td>
                    <td>
                        <%=(Config.Enums.Dingqi_Qukuan)model_Qukuan.qukuanType %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">本金： 
                    </td>
                    <td>
                        <%=Math.Abs(model_Qukuan.jine) %> 元
                    </td>
                    <td class="tdl">利息： 
                    </td>
                    <td>
                        <%=model_Qukuan.lixi %> 元
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款经办人： 
                    </td>
                    <td>
                        <%=model_Qukuan.jbname %>
                    </td>
                    <td class="tdl">取款时间： 
                    </td>
                    <td>
                        <%=model_Qukuan.addTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款备注： 
                    </td>
                    <td colspan="3">
                        <%=model_Qukuan.add_remark %>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                   { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td class="tdl">报废备注： 
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDelRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="3">
                        <asp:Button ID="btnBaofei" runat="server" Text="报废" CssClass="btn btn-sm btnRed noloading" OnClientClick="return confirm('确实要报废当前取款数据吗？')" OnClick="btnBaofei_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
