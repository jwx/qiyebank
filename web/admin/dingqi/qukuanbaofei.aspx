﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuanbaofei.aspx.cs" Inherits="web.admin.dingqi.qukuanbaofei" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });

        function fnSetLilv() {
            layer.closeAll();
            layer.alert("请先在“基础信息”菜单的“系统设置”栏目中设置零钱利率！");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a href="javascript:void(0);" class="tab current">定期取款报废
                    </a>
                    <a class="tab" href="qukuanyibaofei.aspx?id=<%=uid %>">已报废定期取款
                    </a>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名</asp:ListItem>
                        <asp:ListItem Value="mobile">手机号码</asp:ListItem>
                        <asp:ListItem Value="danhao">存款单号</asp:ListItem>
                        <asp:ListItem Value="qkdanhao">取款单号</asp:ListItem>
                        <asp:ListItem Value="jbname">取款经办人</asp:ListItem>
                        <asp:ListItem Value="add_remark">取款备注</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji" href="javascript:void(0);">高级查询</a>
                    <asp:Button ID="btnExport" runat="server" CssClass="btn btnRed noloading" Text="导出" OnClick="btnExport_Click" />
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">存款单号：</td>
                            <td>
                                <asp:TextBox ID="txtDanhao" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">取款单号：</td>
                            <td>
                                <asp:TextBox ID="txtQkdanhao" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">取款方式：</td>
                            <td>
                                <asp:DropDownList ID="ddlQukuanType" runat="server">
                                    <asp:ListItem>请选择取款方式</asp:ListItem>
                                    <asp:ListItem Value="1">本金取款</asp:ListItem>
                                    <asp:ListItem Value="2">本息取款</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">存款时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCunkuanTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtCunkuanTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">取款时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtAddTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtAddTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">取款经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">取款备注：</td>
                            <td>
                                <asp:TextBox ID="txtAdd_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>账号</td>
                           <td>户名/公司名称</td>
                            <td>手机号码</td>
                            <td>存款单号</td>
                            <td>取款单号</td>
                            <td>本金</td>
                            <td>利息</td>
                            <td>本息合计</td>
                            <td>取款方式</td>
                            <td>存款时间</td>
                            <td>取款时间</td>
                            <td>取款经办人</td>
                            <td>取款备注</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                            {
                                int i = 0;
                                string yonghu = getPower("用户明细");
                                string cunkuan = getPower("存款明细");
                                string print = getPower("打印");
                                string baofei = getPower("报废");
                                foreach (Model.Dingqi one in listRecord)
                                {
                                    i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.uid %>" class="label label-primary layer <%=yonghu %>" title="用户明细_<%=one.realname %>">用户明细</a>
                                <a href="cunkuandetail.aspx?id=<%=one.oid %>" class="label label-info layer <%=cunkuan %>" title="存款明细_<%=one.realname %>">存款明细</a>
                                <a href="pdq1.aspx?id=<%=one.id %>" class="label label-success layer <%=print %>" title="取款打印_<%=one.realname %>">打印</a>
                                <% if (one.hasQukuan == 0)
                                    { %>
                                <a href="qukuanbaofeiinfo.aspx?ckoid=<%=one.oid %>&qkoid=<%=one.id %>" class="label label-danger layer <%=baofei %>" title="定期取款报废_<%=one.realname %>">报废</a>
                                <%} %>
                            </td>
                            <td>
                                <%=one.username %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.mobile %>
                            </td>
                            <td>
                                <%=one.danhao %>
                            </td>
                            <td>
                                <%=one.qkdanhao %>
                            </td>
                            <td class="text_right">
                                <%=one.jine %>
                            </td>
                            <td class="text_right"><%=one.lixi %></td>
                            <td class="text_right"><%=one.jine+one.lixi %></td>
                            <td><%=(Config.Enums.Dingqi_Qukuan)one.qukuanType %></td>
                            <td>
                                <%=one.cunkuanTime %>
                            </td>
                            <td>
                                <%=one.addTime %>
                            </td>
                            <td><%=one.jbname %></td>
                            <td>
                                <%=one.add_remark %>
                            </td>
                        </tr>
                        <%}
                            }
                            else
                            { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
