﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class jiexi : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup != null && modelSetup.isSaved == 1 && !string.IsNullOrEmpty(modelSetup.dq_lilv))
                {
                    hideJiexi.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                }
            }
        }

        protected Model.Setup modelSetup;

        protected void btnJiexi_Click(object sender, EventArgs e)
        {
            DateTime date = Falcon.Function.ToDateTime(txtDate.Text);
            //var dbPath = Server.MapPath("/") + "/App_Data/jizi.db";
            //string dbPath = Tool.DatabaseHelper.dbPath;
            //System.IO.File.Copy(dbPath, dbPath.Replace("jingyi.dll", "Bank_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + date.ToString("yyyyMMdd") + "定期批量结息备份.db"));

            List<Model.Dingqi> listDingqi = new BLL.Dingqi().GetList_Qukuan();//获取系统中所有的存款单
            if (listDingqi != null && listDingqi.Count != 0)
            {
                string msg = string.Empty;
                if (!string.IsNullOrEmpty(hideJiexi.Value))
                {
                    modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideJiexi.Value);
                    //if (new BLL.Dingqi().Qukuan_Jiexi_Auto(listDingqi, DateTime.Now, modelSetup, ref msg))//结息是否成功
                    if (new BLL.Dingqi().Qukuan_Jiexi_Auto(listDingqi, date, modelSetup, ref msg))//结息是否成功
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败," + msg + "！',{icon:5,time:1000});", true);
                    }
                }
            }
        }
    }
}