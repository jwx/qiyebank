﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class qukuanbaofei : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            uid = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                flag = false;
                Query();
            }
        }

        protected List<Model.Dingqi> listRecord;

        private static bool flag;

        protected int uid;

        private string GetWhere()
        {
            string where = "delid=0" + (uid == 0 ? "" : (" and uid=" + uid));//视图中已过滤canBaofei=1

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                    if (txtQkdanhao.Text.Trim() != "") where += " and qkdanhao like '%" + txtQkdanhao.Text + "%'";
                    if (ddlQukuanType.SelectedIndex != 0) where += " and qukuanType=" + ddlQukuanType.SelectedValue;
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                    if (txtAddTime1.Text.Trim() != "") where += " and addTime>='" + txtAddTime1.Text.Trim() + "'";
                    if (txtAddTime2.Text.Trim() != "") where += " and addTime<='" + txtAddTime2.Text.Trim() + "'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);


            listRecord = Tool.Pager_Sqlite.Query<Model.Dingqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Dingqi_Qukuan", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.DataTableHelper().GetDataTableByWhere("V_Dingqi_Qukuan", "*", GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                dt.Columns.Add("benxi");
                dt.Columns.Add("qukuanfangshi");
                foreach (System.Data.DataRow one in dt.Rows)
                {
                    one["benxi"] = Falcon.Function.ToDecimal(one["jine"].ToString().Trim()) + Falcon.Function.ToDecimal(one["lixi"].ToString().Trim());
                    one["qukuanfangshi"] = ((Config.Enums.Dingqi_Qukuan)Falcon.Function.ToInt(one["qukuanType"])).ToString();
                }
                Tool.ExcelHelper.DataTableToExcel("定期业务查询", new List<System.Data.DataTable>() { dt }, new List<string> { "定期业务查询" }, new List<string[]> { new string[] { "账号|username", "户名|realname", "手机号码|mobile", "存款单号|danhao", "取款单号|qkdanhao", "本金|jine", "利息|lixi", "本息合计|benxi", "取款方式|qukuanfangshi", "存款时间|cunkuanTime", "取款时间|addTime", "取款经办人|jbname", "取款备注|add_remark" } });
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('没有数据！',{icon:5,time:1000});", true);
            }
        }
    }
}