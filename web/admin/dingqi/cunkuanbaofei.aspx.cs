﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class cunkuanbaofei : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            uid = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                flag = false;
                Query();
            }
        }

        protected List<Model.Dingqi> listRecord;

        private static bool flag;

        protected int uid;

        private string GetWhere()
        {
            string where = "cunqukuan=1 and canBaofei=1 and hasQukuan=0 and delid=0" + (uid == 0 ? "" : (" and uid=" + uid));

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                    if (txtZhouqi.Text.Trim() != "") where += " and zhouqi like '%" + txtZhouqi.Text + "%'";
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                    if (txtDaoqiTime1.Text.Trim() != "") where += " and daoqiTime>='" + txtDaoqiTime1.Text.Trim() + "'";
                    if (txtDaoqiTime2.Text.Trim() != "") where += " and daoqiTime<='" + txtDaoqiTime2.Text.Trim() + "'";
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);


            listRecord = Tool.Pager_Sqlite.Query<Model.Dingqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Dingqi_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.DataTableHelper().GetDataTableByWhere("V_Dingqi_Info", "*", GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                dt.Columns.Add("dqjsfs");//到期结算方式
                dt.Columns.Add("dqnlilv");
                dt.Columns.Add("bdqnlilv");
                foreach (System.Data.DataRow one in dt.Rows)
                {
                    bool isCunkuan = one["cunqukuan"].ToString() == "1";
                    one["dqjsfs"] = isCunkuan ? ((Config.Enums.Dingqi_DaoqiHandler)Falcon.Function.ToInt(one["daoqi_handler"])).ToString() : "";
                    one["dqnlilv"] = isCunkuan ? (one["lilv"] + "%") : "";
                    one["bdqnlilv"] = isCunkuan ? (one["budaoqi"] + "%") : "";
                }
                Tool.ExcelHelper.DataTableToExcel("定期存款", new List<System.Data.DataTable>() { dt }, new List<string> { "定期存款" }, new List<string[]> { new string[] { "账号|username", "户名|realname", "手机号码|mobile", "存款单号|danhao", "存款金额|jine", "存款周期|zhouqi", "存款时间|cunkuanTime", "到期时间|daoqiTime", "到期结算方式|dqjsfs", "到期年利率|dqnlilv", "不到期年利率|bdqnlilv", "经办人|jbname", "录入时间|addTime", "存款备注|add_remark" } });
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('没有数据！',{icon:5,time:1000});", true);
            }
        }
    }
}