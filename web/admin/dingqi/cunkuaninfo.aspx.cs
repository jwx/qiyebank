﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class cunkuaninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
                lblRealName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("name"));

                txtCunkuanTime.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dq_lilv))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                }
                else
                {
                    lblHuoqi.Text = new BLL.Huoqi().GetYue(id) + "元";
                    lblDingqi.Text = new BLL.Dingqi().GetYue(id) + "元";
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);

                    //ddlZhouqi.DataSource = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);
                    //ddlZhouqi.DataTextField = "name";
                    //ddlZhouqi.DataValueField = "min"; 
                    List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);
                    string byWhat = modelSetup.dq_byDay == 0 ? "月" : "天";
                    foreach (Model.LilvRange one in listLilvRange)
                    {
                        ddlZhouqi.Items.Add(new ListItem() { Text = one.name + "_" + one.min + byWhat, Value = one.min + "|" + one.lilv });//value，存款周期|利率
                    }
                    ddlZhouqi.Items.Insert(0, new ListItem() { Text = "请选择存款周期", Value = "" });

                    //到期结算方式
                    //rdDqjs0.Checked = modelSetup.dq_daoqi == 0;
                    //rdDqjs1.Checked = modelSetup.dq_daoqi == 1;
                    //rdDqjs2.Checked = modelSetup.dq_daoqi == 2;
                    //rdDqjs3.Checked = modelSetup.dq_daoqi == 3;
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        [AjaxPro.AjaxMethod]
        public string GetDaoqiTime(int dq_byDay, string cunkuanTime, string ddlVal)
        {
            int zhouqi = Falcon.Function.ToInt(ddlVal.Split('|')[0]);//存款周期|利率
            return dq_byDay == 1 ? Falcon.Function.ToDateTime(cunkuanTime).AddDays(zhouqi).ToString("yyyy-MM-dd") : Falcon.Function.ToDateTime(cunkuanTime).AddMonths(zhouqi).ToString("yyyy-MM-dd");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value);
            decimal cunkuan = Falcon.Function.ToDecimal(txtCunkuan.Text.Trim());
            string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int dqjs = 0; // 取款时结算本金和利息
                          //if (rdDqjs1.Checked) dqjs = 1;
                          // else if (rdDqjs2.Checked) dqjs = 2;
                          //else if (rdDqjs3.Checked) dqjs = 3;
            int oid = new BLL.Dingqi().Add(id, cunkuan, new Tool.OrderNo().GetOrderNo(),1, 1, 0, 0, 0, 0, dqjs, ddlZhouqi.SelectedItem.Text, Falcon.Function.ToDecimal(txtLilv.Text.Trim()), Falcon.Function.ToDecimal(Falcon.Function.GetFormString("hideBudaoqi")), 0, dtNow, txtCunkuanTime.Text, Falcon.Function.GetFormString("hideDaoqiTime"), SessionUid, txtAddRemark.Text, Config.Config.str_defTime, 0, "");
            if (oid != 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='pdc1.aspx?id=" + oid + "';});", true);
                //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }
    }
}