﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class cunkuanbaofeiinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Dingqi().GetModel(id);
                    modelSetup = new BLL.Setup().Get_Setup();
                }
            }
        }

        protected int id;

        protected Model.Dingqi model;

        protected Model.Setup modelSetup;

        protected void btnBaofei_Click(object sender, EventArgs e)
        {
            if (new BLL.Dingqi().Update_Baofei(id, SessionUid, txtDelRemark.Text))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }
    }
}