﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class qukuaninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            oid = Falcon.Function.GetQueryInt("oid"); //定期id    
            uid = Falcon.Function.GetQueryInt("uid");//uid
            if (!IsPostBack)
            {
                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dq_lilv))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                }
                else
                {
                    Model.Dingqi model = new BLL.Dingqi().GetModel(oid, uid);
                    if (model != null && model.id != 0)
                    {
                        lblUserName.Text = model.username;
                        lblRealName.Text = model.realname;
                        lblHuoqi.Text = new BLL.Huoqi().GetYue(uid) + "元";
                        lblDingqi.Text = new BLL.Dingqi().GetYue(uid) + "元";
                        lblCunkuanTime.Text = model.cunkuanTime;
                        lblDaoqiTime.Text = model.daoqiTime;
                        lblZhouqi.Text = model.zhouqi;
                        dingqiID.Value = oid.ToString();
                        daoqi.Value = model.lilv.ToString();//到期利率
                        budaoqi.Value = model.budaoqi.ToString();//不到期利率
                        hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                        //  lblQukuan.Text = model.jine.ToString();
                        //decimal lixi = Math.Round(GetLixi(modelSetup, model), 0, MidpointRounding.AwayFromZero);
                        decimal lixi = GetLixi(modelSetup, model);
                        //  lblLixi.Text = lixi.ToString();//获取当前存款的利息
                       // btnBenjin.Text += model.jine + "元";
                        string benxi = (model.jine + lixi).ToString();
                      //  btnBenjinLixi.Text += benxi + "元";
                       // btnBenjinLixi.Attributes.Add("benxi", benxi);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('没有找到单据！',{icon:5,time:1500},function(){parent.layer.closeAll();});", true);
                    }
                }
            }
        }

        protected int oid;

        protected int uid;

        protected Model.Setup modelSetup;

        private decimal GetLixi(Model.Setup modelSetup, Model.Dingqi model)
        {
            int lilvJishu = modelSetup.dq_byDay == 1 ? 365 : 12;//年利率基数，按天还是按月  
            DateTime dtNow = DateTime.Today; //当前时间
            DateTime cunkuanTime = Falcon.Function.ToDateTime(model.cunkuanTime);//存款周期
            decimal lilv = (dtNow - Falcon.Function.ToDateTime(model.daoqiTime)).Days >= 0 ? model.lilv : model.budaoqi;//正常利率和不到期利率
            lblLilv.Text = lilv.ToString();
            int cunkuanZhouqi = modelSetup.dq_byDay == 1 ? (dtNow - cunkuanTime).Days : new Tool.TimeHelper().GetFullMonth(dtNow, cunkuanTime);//实际存款天数或月数
            return Math.Round(model.jine * cunkuanZhouqi * lilv / 100 / lilvJishu, 2, MidpointRounding.AwayFromZero);
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyUserPswd(int id, string userpswd)
        {
            Model.UserInfo model = new BLL.UserInfo().GetModel(id);
            if (model != null && model.id != 0)
            {
                return model.userpswd == Falcon.Function.Encrypt(userpswd);
            }
            return false;
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        protected void btnBenjin_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            int oid_qk = new BLL.Dingqi().Qukuan_Benjin(oid, uid, decimal.Parse(txtQukuan.Text), Falcon.Function.ToDecimal(hideLixi.Value), Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value), SessionUid, txtAddRemark.Text, ref msg);
            if (oid_qk != 0)
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='pdq1.aspx?id=" + oid_qk + "';});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }

        protected void btnBenjinLixi_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            int oid_qk = new BLL.Dingqi().Qukuan_Benxi(oid, uid, decimal.Parse(txtQukuan.Text), Falcon.Function.ToDecimal(hideLixi.Value), Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value), SessionUid, txtAddRemark.Text, ref msg);
            if (oid_qk != 0)
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='pdq1.aspx?id=" + oid_qk + "';});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public string GetBenXI(int id, decimal qukuan, decimal daoqi, string daoqiTime, decimal budaoqi,string cunkuanTime)
        {
            DateTime dtNow = DateTime.Today; //当前时间
            if ((dtNow - Falcon.Function.ToDateTime(daoqiTime)).Days >= 0)//到期
            {
                int days = (dtNow - Falcon.Function.ToDateTime(daoqiTime)).Days; //到期后多余的天数
                decimal qdays=(Falcon.Function.ToDateTime(daoqiTime) - Falcon.Function.ToDateTime(cunkuanTime)).Days; //到期-存款的天数
                decimal dylixi= Math.Round(qukuan* days * budaoqi / 100 / 365, 2, MidpointRounding.AwayFromZero) + Math.Round(qukuan * qdays * daoqi / 100 / 365, 2, MidpointRounding.AwayFromZero); ;
                return dylixi.ToString();

            } else //不到期
            {
               int days=(dtNow - Falcon.Function.ToDateTime(cunkuanTime)).Days;// 当前时间-存款时间的天数
                decimal dylixi = Math.Round(qukuan * days * budaoqi / 100 / 365, 2, MidpointRounding.AwayFromZero);
                return dylixi.ToString();
            }
        
        }








    }
}