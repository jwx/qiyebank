﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class cunkuandetail : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (id != 0)
            {
                model = new BLL.Dingqi().GetModel(id);
            }
        }

        protected int id;

        protected Model.Dingqi model;

    }
}