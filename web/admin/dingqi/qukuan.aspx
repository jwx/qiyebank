﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuan.aspx.cs" Inherits="web.admin.dingqi.qukuan" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });

        function fnSetLilv() {
            layer.closeAll();
            layer.alert("请先在“基础信息”菜单的“系统设置”栏目中设置零钱利率！");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <span class="title">定期取款</span>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                      
          <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名/公司名称</asp:ListItem>
                        <asp:ListItem Value="idcard">身份证号/统一社会代码</asp:ListItem>
                        <asp:ListItem Value="email">电子邮箱</asp:ListItem>
                        <asp:ListItem Value="mobile">联系手机</asp:ListItem>
                        <asp:ListItem Value="address">居住地址/公司地址</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">身份证号：</td>
                            <td>
                                <asp:TextBox ID="txtIdCard" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">电子邮箱：</td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">性别：</td>
                            <td>
                                <asp:DropDownList ID="ddlGender" runat="server">
                                    <asp:ListItem Value="-1">请选择性别</asp:ListItem>
                                    <asp:ListItem Value="1">男</asp:ListItem>
                                    <asp:ListItem Value="0">女</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdl">座机号码：</td>
                            <td>
                                <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">地址：</td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">账户状态：</td>
                            <td>
                                <asp:DropDownList ID="ddlIsDel" runat="server">
                                    <asp:ListItem Value="-1">请选择账户状态</asp:ListItem>
                                    <asp:ListItem Value="0">正常</asp:ListItem>
                                    <asp:ListItem Value="1">已销户</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">开户时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                          <tr>
                            <td class="tdl">账户类型：</td>
                            <td>
                                <asp:DropDownList ID="ddlUserOrGs" runat="server">
                                    <asp:ListItem Value="-1">请选择公司类型</asp:ListItem>
                                    <asp:ListItem Value="0">个人账户</asp:ListItem>
                                    <asp:ListItem Value="1">公司账户</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>

                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>账号</td>
                              <td>户名/公司名称</td>
                            <td>零钱余额</td>
                            <td>定期余额</td>
                            <td>总余额</td>
                            <td>性别</td>
                           <td>身份证号/统一社会信用代码</td>
                            <td>手机号码</td>
                           <td>居住地址/公司地址</td>
                            <td>经办人</td>
                            <td>开户时间</td>
                            <td>账户状态</td>
                               <td>账户类型</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                              {
                                  int i = 0;
                                  string mingxi = getPower("用户明细");
                                  string records = getPower("操作记录");
                                  string qukuan = getPower("取款");
                                  foreach (Model.UserInfo one in listRecord)
                                  {
                                      decimal huoqi = new BLL.Huoqi().GetYue(one.id);
                                      decimal dingqi = new BLL.Dingqi().GetYue(one.id);
                                      i++;  
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.id %>" class="label label-info layer <%=mingxi %>" title="用户明细_<%=one.realname %>">用户明细</a>
                                <a href="../users/records.aspx?id=<%=one.id %>" class="label label-success layer <%=records %>" title="用户账号操作记录_<%=one.realname %>">操作记录</a>
                                <% if (one.isDel == 0)
                                   { %>
                                <a href="cunkuanorders.aspx?id=<%=one.id %>" class="label label-danger layer <%=qukuan %>" title="用户取款_<%=one.realname %>" reload="1">取款</a>
                                <%} %>
                            </td>
                            <td>
                                <%=one.username %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td class="text_right">
                                <%=huoqi %>
                            </td>
                            <td class="text_right">
                                <%=dingqi %>
                            </td>
                            <td class="text_right">
                                <%=huoqi+dingqi %>
                            </td>
                            <td>
                                <%=(Config.Enums.Gender)one.gender %>
                            </td>
                            <td>
                                <%=one.idcard %>
                            </td>
                            <td>
                                <%=one.mobile %>
                            </td>
                            <td>
                                <%=one.address %>
                            </td>
                            <td>
                                <%=one.jbname %>
                            </td>
                            <td>
                                <%=one.addTime.ToString("yyyy-MM-dd HH:mm:ss") %>
                            </td>
                            <td class="text_center">
                                <%=one.isDel==0?"正常":"已销户" %>
                            </td>
                              <td class="text_center">
                              
                                <%if (one.UserOrGS == 0)
                                    {
                                %>
                                      个人账户
                                      <%}
                                          else if (one.UserOrGS == 1)
                                          {%>
                                公司账户
                                <%} %>
                            </td>
                        </tr>
                        <%}
                              }
                              else
                              { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
