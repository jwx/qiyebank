﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cunkuandetail.aspx.cs" Inherits="web.admin.dingqi.cunkuandetail" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <% if (model != null && model.id != 0)
               { %>
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<%=model.username %>]<%=model.realname %>
                        <a href="../users/detail.aspx?id=<%=model.uid %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <%--<tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <%--    <tr>
                    <td class="tdl">零钱利率明细： 
                    </td>
                    <td>
                        <% if (!string.IsNullOrEmpty(model.lilv))
                           {
                               List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(model.lilv);
                               if (listLilv != null && listLilv.Count != 0)
                               {
                        %>
                        <div>
                            利息按<%=listLilv[0].unit %>计算
                        </div>
                        <%
                                   foreach (Model.LilvRange one in listLilv)
                                   {
                        %>
                        <div>
                            <%=one.min %><%=one.unit %>< 存款时间 <= <%=one.max==0?"∞":one.max.ToString() %><%=one.unit %>，年利率:<%=one.lilv %>% 
                        </div>
                        <%}
                               } %>
                        <%} %>
                    </td>
                </tr>--%>
                <tr>
                    <td class="tdl">存款单号： 
                    </td>
                    <td>
                        <%=model.danhao %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款金额： 
                    </td>
                    <td>
                        <%=model.jine %>
                        元 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款周期： 
                    </td>
                    <td>
                        <%=model.zhouqi %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款时间： 
                    </td>
                    <td>
                        <%=model.cunkuanTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期时间： 
                    </td>
                    <td>
                        <%=model.daoqiTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期结算方式： 
                    </td>
                    <td>
                        <%=(Config.Enums.Dingqi_DaoqiHandler)model.daoqi_handler %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期年利率： 
                    </td>
                    <td>
                        <%=model.lilv %>%
                    </td>
                </tr>
                <tr>
                    <td class="tdl">不到期年利率： 
                    </td>
                    <td>
                        <%=model.budaoqi %>%
                    </td>
                </tr>
                <tr>
                    <td class="tdl">经办人： 
                    </td>
                    <td>
                        <%=model.jbname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款备注： 
                    </td>
                    <td>
                        <%=model.add_remark %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">录入时间： 
                    </td>
                    <td>
                        <%=model.addTime %>
                    </td>
                </tr>
                <% if (model.delid != 0)
                   { %>
                <tr>
                    <td class="tdl">报废人： 
                    </td>
                    <td>
                        <%=model.delname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">报废备注： 
                    </td>
                    <td>
                        <%=model.del_remark %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">报废时间： 
                    </td>
                    <td>
                        <%=model.delTime %>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </form>
</body>
</html>
