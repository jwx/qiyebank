﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jiexi.aspx.cs" Inherits="web.admin.dingqi.jiexi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/Validform/Validform_Datatype.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script src="/Plug/My97DatePicker/WdatePicker.js"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideJiexi" runat="server" />
        <div class="main">
            <div class="box">
                <div style="padding: 10px;">
                    <% if (modelSetup == null)
                       { %>
                    读取系统设置出错！
                    <%}
                       else if (modelSetup.isSaved == 0)
                       { %>
                    请先保存系统设置！
                    <%}
                       else if (string.IsNullOrEmpty(modelSetup.dq_lilv))
                       {%>
                    请设置定期利率！
                    <%}
                       else
                       {
                           string byWhat = modelSetup.dq_byDay == 0 ? "月" : "天";
                    %>
                    <div style="font-size: 14px;">
                        利息按<%=byWhat %>计算
                    </div>
                    <div>
                        <%
                           decimal budaoqi = 0;
                           if (!string.IsNullOrEmpty(modelSetup.dq_lilv))
                           {
                               List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);
                               foreach (Model.LilvRange one in listLilv)
                               {
                                   budaoqi = one.budaoqi;
                        %>
                        <div style="margin: 5px 0px;">
                            存款周期：<%=one.name %>，存款时间：<%=one.min %><%=byWhat %>，年利率:<%=one.lilv %>% 
                        </div>
                        <%}
                           } %>
                    </div>
                    <div style="margin: 5px 0px;">
                        不到期年利率：<%=budaoqi %>%
                    </div>
                    <div style="padding: 10px 0px;">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                        <asp:Button ID="btnJiexi" runat="server" Text="批量结息" CssClass="btn btnBlue" OnClick="btnJiexi_Click" OnClientClick="return confirm('批量结息的单据无法进行报废操作，确定要进行批量结息操作吗？')" />
                        <span class="Validform_checktip">存款和利息同时取出，共同存入一笔新的存款，存款周期按定期设置的自动转存天数/月份计算</span>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
