﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lilvbyday.aspx.cs" Inherits="web.admin.dingqi.lilvbyday" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <style type="text/css">
        .item { }
            .item .title { font-size: 14px; margin-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dedede; }
            .item .content { padding-bottom: 10px; }
                .item .content p input[type='text'] { width: 50px; margin: 0px 3px; }
                    .item .content p input[type='text'].zhouqi { width: 80px; }
                .item .content p a { margin-left: 5px; }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                //tiptype: 1,//1=> 自定义弹出框提示；
                tiptype: function (msg, o, cssctl) {
                    //msg：提示信息;
                    //o:{obj:*,type:*,curform:*}, obj指向的是当前验证的表单元素（或表单对象），type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, curform为当前form对象;
                    //cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
                    if (!o.obj.is("form")) {//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
                        //var objtip = o.obj.siblings(".Validform_checktip");
                        //cssctl(objtip, o.type);
                        //objtip.text(msg); 
                        if (o.type == 3) {
                            layer.msg(msg, { icon: 5, time: 1500 }, function () {
                                layer.closeAll();
                            });
                        }
                    }
                }, beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;	

                    var arrayName = [];
                    var arrayDays = [];
                    var arrayLilv = [];

                    $(".content .data p").each(function () {
                        var obj_input = $(this).find("input");
                        arrayName.push(obj_input.eq(0).val());
                        arrayDays.push($.trim(obj_input.eq(1).val()));
                        arrayLilv.push($.trim(obj_input.eq(2).val()));
                    });
                    if (ajax.Save(arrayName.toString(), arrayDays.toString(), arrayLilv.toString(), Number($.trim($("#budaoqi").val())), Number($.trim($("#dq_zc").val())), Number($.trim($("#dq_zclilv").val()))).value) {
                        layer.msg("操作成功！", { icon: 6, time: 1500 }, function () {
                            parent.layer.closeAll();
                        });
                    } else {
                        layer.msg("操作失败！", { icon: 5, time: 1500 }, function () {
                            layer.closeAll();
                        });
                    }
                    return false;
                }
            });

            var html = '<p>存款周期名称：<input type="text" class="zhouqi" datatype="*" nullmsg="请填写存款周期名称" errormsg="请填写存款周期名称" />， 等效存款天数：<input type="text" datatype="n" nullmsg="请填写等效存款天数" errormsg="请填写正整数" />天，年利率<input type="text" datatype="*" nullmsg="请填写年利率" />% <a href="javascrpt:void(0);" class="dellilv">[删除]</a></p>'
            $("#btnAddRow").click(function () {
                $(".content .data").append(html);
            });

            $(document).on("click", ".dellilv", function () {
                $(this).parent().remove();
            });
        });

        function showDemo() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 1,
                area: '525px',
                scrollbar: false,
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: '<img src="dingqi_byday.png" style="width:525px; height:300px;" />'
            });
        }
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main ">
            <div class="item">
                <div class="title">
                    设置定期利率_按天计算  
                </div>
                <p>
                    <input type="button" id="btnAddRow" value="+ 新增定期利率" />
                    <a href="javascript:showDemo();">查看填写示例</a>
                </p>
                <div class="content">
                    <div class="data">
                        <%
                            decimal budaoqi = 0;
                            Model.Setup model = new BLL.Setup().Get_DingqiLilv();
                            model = model == null ? new Model.Setup() : model;
                            if (string.IsNullOrEmpty(model.dq_lilv))
                            {
                        %>
                        <p>
                            存款周期名称：<input type="text" class="zhouqi" datatype="*" nullmsg="请填写存款周期名称" errormsg="请填写存款周期名称" />，
                            等效存款天数：<input type="text" datatype="n" nullmsg="请填写等效存款天数" errormsg="请填写正整数" />天，年利率<input type="text" datatype="*" nullmsg="请填写年利率" />% 
                        <a href="javascrpt:void(0);" class="dellilv">[删除]</a>
                        </p>
                        <%}
                            else if (model.dq_byDay == 1)
                            {
                                List<Model.LilvRange> list = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(model.dq_lilv);
                                foreach (Model.LilvRange one in list)
                                {
                                    budaoqi = one.budaoqi;
                        %>
                        <p>
                            存款周期名称：<input type="text" class="zhouqi" datatype="*" nullmsg="请填写存款周期名称" errormsg="请填写存款周期名称" value="<%=one.name %>" />，
                        等效存款天数：<input type="text" datatype="n" nullmsg="请填写等效存款天数" errormsg="请填写正整数" value="<%=one.max %>" />天，年利率<input type="text" datatype="*" nullmsg="请填写年利率" value="<%=one.lilv %>" />% 
                        <a href="javascrpt:void(0);" class="dellilv">[删除]</a>
                        </p>
                        <%}
                            } %>
                    </div>
                    <p>
                        不到期年利率：<input type="text" id="budaoqi" class="zhouqi" value="<%=budaoqi==0?"":budaoqi.ToString() %>" datatype="*" nullmsg="请填写不到期年利率" />%
                    </p>
                    <p>
                        自动转存天数：<input type="text" class="zhouqi" id="dq_zc" datatype="n" nullmsg="请填写自动转存天数" errormsg="请填写正整数" value="<%=model.dq_zc==0?"":model.dq_zc.ToString() %>" />天，年利率<input type="text" id="dq_zclilv" datatype="*" nullmsg="请填写年利率" value="<%=model.dq_zclilv==0?"":model.dq_zclilv.ToString() %>" />% 
                    </p>
                </div>
                <div>
                    <input type="submit" id="btnSave" value="保存" class="layui-btn layui-btn-small layui-btn-normal" />
                    <input type="button" id="Submit1" value="重置" class="layui-btn layui-btn-small" onclick="window.location.href = window.location;" />
                    <input type="button" id="btnClose" value="关闭" class="layui-btn layui-btn-small layui-btn-primary" onclick="parent.layer.closeAll();" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
