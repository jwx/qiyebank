﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.dingqi
{
    public partial class qukuanbaofeiinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            ckoid = Falcon.Function.GetQueryInt("ckoid");
            qkoid = Falcon.Function.GetQueryInt("qkoid");
            if (!IsPostBack)
            {
                if (ckoid != 0 && qkoid != 0)
                {
                    model_Cunkuan = new BLL.Dingqi().GetModel(ckoid);
                    model_Qukuan = new BLL.Dingqi().GetModel(qkoid);
                    modelSetup = new BLL.Setup().Get_Setup();
                }
            }
            model_Cunkuan = model_Cunkuan == null ? new Model.Dingqi() : model_Cunkuan;
            model_Qukuan = model_Qukuan == null ? new Model.Dingqi() : model_Qukuan;
        }

        protected int ckoid;

        protected int qkoid;

        protected Model.Dingqi model_Cunkuan;

        protected Model.Dingqi model_Qukuan;

        protected Model.Setup modelSetup;

        protected void btnBaofei_Click(object sender, EventArgs e)
        {
            Model.Setup modelSetup = new BLL.Setup().Get_Setup();
            if (modelSetup != null)
            {
                string msg = string.Empty;
                if (new BLL.Dingqi().Update_QukuanBaofei(ckoid, qkoid, SessionUid, txtDelRemark.Text, modelSetup, ref msg))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('系统设置读取失败！',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }
    }
}