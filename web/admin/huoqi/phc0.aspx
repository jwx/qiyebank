﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="phc0.aspx.cs" Inherits="web.admin.huoqi.phc0" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="/Scripts/LodopFuncs.js"></script>
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        var LODOP;
        function CheckIsInstall() {
            try {
                LODOP = getLodop();
                if (LODOP.VERSION) {
                    if (LODOP.CVERSION) {
                        //alert("当前有C-Lodop云打印可用!\n C-Lodop版本:" + LODOP.CVERSION + "(内含Lodop" + LODOP.VERSION + ")");
                    }
                    else {
                        alert("本机已成功安装了Lodop控件！\n 版本号:" + LODOP.VERSION);
                    }
                }
            } catch (err) {

            }
        }

        function print_Preview() {
            CheckIsInstall();
            printOrder();
            LODOP.PREVIEW();
        }

        function printOrder() {
            var obj = $("#webPrint");
            LODOP.SET_PRINT_PAGESIZE(1, 2410, 930, "存款单据");//纵向打印，固定纸张，打印纸张大小为214mm*93mm
            LODOP.ADD_PRINT_BARCODE(15, "2%", 220, 40, "128A", obj.attr("danhao"));//设置单号条码
            //LODOP.SET_PRINT_STYLEA(0, "ShowBarText", 0);//不在条码下方显示文字 
            LODOP.ADD_PRINT_HTM("2%", "1%", "98%", "98%", document.getElementById("danju").innerHTML);//设置html为打印内容
        }
    </script>
    <style type="text/css">
        html, body, form { margin: 0px; padding: 0px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="webPrint" danhao="<%=model.danhao %>">
            <div id="danju">
                <style type="text/css">
                    .text_center { text-align: center; }
                    .text_right { text-align: right; }
                    table { width: 100%; height: 100%; border-top: 1px solid #000000; border-left: 1px solid #000000; border-collapse: collapse; font-size: 14px; }
                        table thead td { font-weight: bold; font-size: 18px; }
                        table tr td { border-right: 1px solid #000000; border-bottom: 1px solid #000000; padding: 2px; }
                        table tfoot td span { padding: 5px; font-size: 12px; }
                </style>
                <table>
                    <thead>
                        <tr class="text_center">
                            <td colspan="6"><%=modelSettings.name %>零钱存款单</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text_right" style="width: 10%">姓名：</td>
                            <td><%=model.realname %></td>
                            <td class="text_right" style="width: 10%">账号：</td>
                            <td><%=model.username %></td>
                            <td class="text_right" style="width: 10%">存款时间：</td>
                            <td><%=model.cunkuanTime %></td>
                        </tr>
                        <tr>
                            <td class="text_right">存款金额：</td>
                            <td><%=model.yuanjine %>元</td>
                            <td class="text_right">大写：</td>
                            <td><%=Tool.MoneyToUpper.ToUpper(model.yuanjine.ToString()) %></td>
                            <td rowspan="3" class="text_right">
                                <div style="width: 20px; margin: 0px auto;">业务专用章</div>
                            </td>
                            <td rowspan="3"></td>
                        </tr>
                        <tr>
                            <td class="text_right">备注：</td>
                            <td colspan="3"><%=model.add_remark %></td>
                        </tr>
                        <tr>
                            <td class="text_right">经办人：</td>
                            <td><%=model.jbname %></td>
                            <td class="text_right">客户签字：</td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr style="height: 10%;">
                            <td colspan="6" class="text_center"><span>1 白联:存根</span> <span>2 红联:上缴</span> <span>3 黄联:客户</span></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div style="margin-top: 20px; text-align: center;">
                <input type="button" id="btnReload" runat="server" value="刷新页面" onclick="window.location.href = window.location;" />
                &nbsp;
                  <input type="button" id="btnClose" runat="server" value="关闭" onclick="parent.layer.closeAll();" />
                &nbsp;
                <input type="button" id="btnPrint" runat="server" value="打印预览" onclick="print_Preview();" />
            </div>
        </div>
    </form>
</body>
</html>
