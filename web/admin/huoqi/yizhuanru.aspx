﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="yizhuanru.aspx.cs" Inherits="web.admin.huoqi.yizhuanru" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script type="text/javascript">
      
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a class="tab" href="zhuanru.aspx">转入申请
                    </a>
                    <a href="javascript:void(0);" class="tab current">转入记录
                    </a>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="realname">付款人姓名</asp:ListItem>
                        <asp:ListItem Value="mobile">手机号码</asp:ListItem>
                        <asp:ListItem Value="zhifubao">支付宝</asp:ListItem>
                        <asp:ListItem Value="add_remark">转入备注</asp:ListItem>
                        <asp:ListItem Value="ckname">审核人</asp:ListItem>
                        <asp:ListItem Value="ck_remark">审核备注</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji" href="javascript:void(0);">高级查询</a>
                    <asp:Button ID="btnExport" runat="server" CssClass="btn btnRed noloading" Text="导出" OnClick="btnExport_Click" />
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">付款人姓名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">支付宝：</td>
                            <td>
                                <asp:TextBox ID="txtZhifubao" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">转入备注：</td>
                            <td>
                                <asp:TextBox ID="txtAdd_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">转入时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtAddTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtAddTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">审核人：</td>
                            <td>
                                <asp:TextBox ID="txtCkName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">审核备注：</td>
                            <td>
                                <asp:TextBox ID="txtCk_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">审核时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCkTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtCkTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">审核结果：</td>
                            <td>
                                <asp:DropDownList ID="ddlOid" runat="server">
                                    <asp:ListItem>请选择审核结果</asp:ListItem>
                                    <asp:ListItem>审核未通过</asp:ListItem>
                                    <asp:ListItem>审核通过</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>付款人姓名</td>
                            <td>手机号码</td>
                            <td>支付宝</td>
                            <td>转入金额</td>
                            <td>转入时间</td>
                            <td>转入备注</td>
                            <td>审核人</td>
                            <td>审核时间</td>
                            <td>审核备注</td>
                            <td>审核结果</td>
                        </tr>
                    </thead>
                    <tbody>
                        <% if (listRecord != null && listRecord.Count != 0)
                           {
                               int i = (AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize;
                               foreach (Model.HuoqiIn one in listRecord)
                               {
                                   i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.uid %>" class="layer" title="<%=one.realname %>"><%=one.realname %> </a>
                            </td>
                            <td>
                                <%=one.mobile %>
                            </td>
                            <td><%=one.zhifubao %></td>
                            <td style="text-align: right;"><%=one.jine %></td>
                            <td><%=one.addTime %></td>
                            <td>
                                <%=one.add_remark %>
                            </td>
                            <td><%=one.ckname %></td>
                            <td><%=one.ckTime %></td>
                            <td><%=one.ck_remrak %></td>
                            <td><%=one.oid==-1?"审核未通过":"审核通过" %></td>
                        </tr>
                        <%}
                           }
                           else
                           { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
