﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class qukuanYS : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
             //   Tool.EnumToList.FillDropDownList(ddlLaiyuan, (Type)(typeof(Config.Enums.Huoqi_Laiyuan)));
               // ddlLaiyuan.Items.Insert(0, new ListItem() { Text = "请选择交易类型", Value = "-1" });
                flag = false;
                Query();
            }
        }

        protected List<Model.Huoqi> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "delid=0 and ExStaut=1";

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                   // if (ddlLaiyuan.SelectedIndex != 0) where += " and laiyuan=" + ddlLaiyuan.SelectedValue;
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                }
            }
            return where;
        }


        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);
            listRecord = Tool.Pager_Sqlite.Query<Model.Huoqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Huoqi_SP", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }
    }
}