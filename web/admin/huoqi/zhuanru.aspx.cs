﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class zhuanru : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.HuoqiIn> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "oid=0";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtZhifubao.Text.Trim() != "") where += " and zhifubao like '%" + txtZhifubao.Text + "%'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                    if (txtAddTime1.Text.Trim() != "") where += " and addTime>='" + txtAddTime1.Text.Trim() + "'";
                    if (txtAddTime2.Text.Trim() != "") where += " and addTime<='" + txtAddTime2.Text.Trim() + "'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);


            listRecord = Tool.Pager_Sqlite.Query<Model.HuoqiIn>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_HuoqiIn_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Pay(string id)
        {
            List<int> listIds = Array.ConvertAll<string, int>(id.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            if (listIds != null && listIds.Count != 0)
            {
                return new BLL.Apply().Update_Fukuan_Piliang(listIds, 1, SessionUid).ToString();
            }
            return "0";
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.DataTableHelper().GetDataTableByWhere("V_HuoqiIn_Info", "*", GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                Tool.ExcelHelper.DataTableToExcel("转入申请", new List<System.Data.DataTable>() { dt }, new List<string>() { "转入申请" }, new List<string[]>() { new string[] { "付款人姓名|realname", "手机号码|mobile", "支付宝|zhifubao", "转入金额|jine", "转入时间|addTime", "备注|add_remark" } });
            }
        }

        protected void btnPihao_Click(object sender, EventArgs e)
        {
            string pihao = new BLL.Apply().CreatePihao();
            if (string.IsNullOrEmpty(pihao))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000},function(){window.location.href=window.location;});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href=window.location;});", true);
            }
        }
    }
}