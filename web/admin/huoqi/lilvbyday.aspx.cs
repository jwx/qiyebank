﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class lilvbyday : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
        }

        [AjaxPro.AjaxMethod]
        public bool Save(string strMin, string strMax, string strLilv)
        {
            //if (strMin != "" && strMax != "" && strLilv != "")
            if(strMin!="")
            {
                int[] arrayMin = Array.ConvertAll<string, int>(strMin.Split(','), m => { return Falcon.Function.ToInt(m, 0); });
                int[] arrayMax = Array.ConvertAll<string, int>(strMax.Split(','), m => { return Falcon.Function.ToInt(m, 0); });
                decimal[] arrayLilv = Array.ConvertAll<string, decimal>(strLilv.Split(','), m => { return Falcon.Function.ToDecimal(m, 0); });
                List<Model.LilvRange> list = new List<Model.LilvRange>();
                for (int i = 0; i < arrayMin.Length; i++)
                {
                    list.Add(new Model.LilvRange() { name = "活期按天", min = arrayMin[i], max = arrayMax[i], lilv = arrayLilv[i], unit = "天" });
                }
                string json = Tool.JsonHelper.JsonSerializer<List<Model.LilvRange>>(list);

                return new BLL.Setup().Update_HuoqiLilv(json);
            }
            return false;
        }
    }
}