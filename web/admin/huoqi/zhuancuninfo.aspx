﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zhuancuninfo.aspx.cs" Inherits="web.admin.huoqi.zhuancuninfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script src="/Plug/Validform/Validform_Datatype.js"></script>
    <style type="text/css">
        .lixi { font-weight: bold; color: red; }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquanma = $("#txtShouquanma");
                    if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                        layer.alert("授权码错误！");
                        return false;
                    }
                    layer.load();
                }
            });

            var qukuan = 0;
            var qukuanlixi = 0;
            $("#txtQukuan").blur(function () {
                qukuan = Number($.trim($("#txtQukuan").val()));
                if (qukuan) {
                    var max = Number($(this).attr("max"));
                    if (qukuan > max) {
                        layer.alert("转存金额不能大于" + max + "元！");
                        return;
                    }
                    layer.load();
                    ajax.GetLixi(parseInt($(".main").attr("id")), qukuan, $("#hideSetup").val(), function (data) {//异步调用
                        //alert(data.value);
                        layer.closeAll();
                        if (data.value) {
                            $("#hideQukuan").val(data.value);
                            //$("#txtAddRemark").val(data.value);
                            var json = $.parseJSON(data.value);
                            //alert(json.lixi_sum);

                            $(".lixi").text("利息合计：" + json.lixi_sum + "元");
                            $("#btnZhuancun").attr("benjin", qukuan).attr("lixi", json.lixi_sum);
                        } else {
                            layer.alert("零钱账户没有存款信息！");
                        }
                    });
                }
            });

            $("#btnZhuancun").click(function () {
                if (fnVerify()) {
                    var t = $(this);
                    return confirm("确定将本金" + t.attr("benjin") + "元转入定期账户，利息" + t.attr("lixi") + "元转入零钱账户吗？");
                }
                return false;
            });

            function fnVerify() {
                var qukuan = Number($.trim($("#txtQukuan").val()));
                if (!qukuan) {
                    layer.alert("请填写取款金额！");
                    return false;
                }
                if (!ajax.VerifyUserPswd(Number($(".main").attr("id")), $("#txtUserPswd").val()).value) {
                    layer.alert("支付密码错误！");
                    return false;
                }
                var shouquanma = $("#txtShouquanma");
                if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                    layer.alert("授权码错误！");
                    return false;
                }
                return true;
            }
        });

        function fnGetDaoqiTime() {
            var cunkuanTime = $("#txtCunkuanTime").val();
            var zhouqi = $("#ddlZhouqi").val();
            var byDay = Number($("#byWhat").attr("by"));
            if (cunkuanTime && zhouqi) {
                $("#txtDaoqiTime,input[name='hideDaoqiTime']").val(ajax.GetDaoqiTime(byDay, cunkuanTime, zhouqi).value);
                $("#txtLilv").val(zhouqi.split('|')[1]);
            }
        }
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <asp:HiddenField ID="hideQukuan" runat="server" /> 
        <%--<input type="hidden" id="hideQukuan" value="" />--%>
        <div class="main" id="<%=id %>">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<asp:Label ID="lblUserName" runat="server"></asp:Label>]<asp:Label ID="lblRealName" runat="server"></asp:Label>
                        <a href="../users/detail.aspx?id=<%=id %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>转存金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtQukuan" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入转存金额" errormsg="请正确输入转存金额"></asp:TextBox>
                        元
                        <span class="lixi"></span>
                        <span class="Validform_checktip">请输入转存金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>转存日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCunkuanTime" runat="server" datatype="*" Enabled="false" CssClass="Wdate" placeholder="请输入转存日期" errormsg="请输入转存日期" onclick="WdatePicker()"></asp:TextBox>
                        <span class="Validform_checktip">请输入转存日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">转存备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期利率明细： 
                    </td>
                    <td>
                        <% decimal budaoqi = 0;
                           if (modelSetup != null)
                           {
                               string byWhat = modelSetup.dq_byDay == 0 ? "月" : "天";
                        %>
                        <div id="byWhat" by="<%=modelSetup.dq_byDay %>">
                            利息按<%=byWhat %>计算
                        </div>
                        <div>
                            <%
                               if (!string.IsNullOrEmpty(modelSetup.dq_lilv))
                               {
                                   List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);
                                   foreach (Model.LilvRange one in listLilv)
                                   {
                                       budaoqi = one.budaoqi;
                            %>
                            <div>
                                存款周期：<%=one.name %>，存款时间：<%=one.min %><%=byWhat %>，年利率:<%=one.lilv %>% 
                            </div>
                            <%}
                               } %>
                        </div>
                        <div>
                            不到期年利率：<%=budaoqi %>%
                        </div>
                        <%} %>
                        <input type="hidden" name="hideBudaoqi" value="<%=budaoqi %>" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>存款周期： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlZhouqi" runat="server" datatype="*" onchange="fnGetDaoqiTime()"></asp:DropDownList>
                        <span class="Validform_checktip">请输入存款日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>存款年利率： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtLilv" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入存款年利率" errormsg="请输入存款年利率"></asp:TextBox>%
                        <span class="Validform_checktip">请输入存款年利率</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDaoqiTime" runat="server" Enabled="false"></asp:TextBox>
                        <input type="hidden" name="hideDaoqiTime" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl">到期结算方式： 
                    </td>
                    <td>
                        <asp:RadioButton ID="rdDqjs0" runat="server" Text="取款时结算本金和利息" GroupName="rdDqjs" Checked="true" />
                        <asp:RadioButton ID="rdDqjs1" runat="server" Text="本金转定期_利息转零钱" GroupName="rdDqjs" />
                        <asp:RadioButton ID="rdDqjs2" runat="server" Text="本金和利息转定期账户" GroupName="rdDqjs" />
                        <asp:RadioButton ID="rdDqjs3" runat="server" Text="本金和利息转零钱账户" GroupName="rdDqjs" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl">存款备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDingqiRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>支付密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd" runat="server" datatype="*" TextMode="Password" placeholder="请输入支付密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入支付密码</span>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                   { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnZhuancun" runat="server" Text="本金转定期，利息转零钱账户" CssClass="btn btn-sm btnRed noloading" OnClick="btnZhuancun_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
