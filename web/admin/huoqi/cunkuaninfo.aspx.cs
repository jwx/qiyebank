﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class cunkuaninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
                lblRealName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("name"));

                txtCunkuanTime.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup == null || modelSetup.isSaved == 0)
                {
                    if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                }
                else
                {
                    lblHuoqi.Text = new BLL.Huoqi().GetYue(id) + "元";
                    lblDingqi.Text = new BLL.Dingqi().GetYue(id) + "元";
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value);
            decimal cunkuan = Falcon.Function.ToDecimal(txtCunkuan.Text.Trim());
            string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int oid = new BLL.Huoqi().Add(id, cunkuan, cunkuan, new Tool.OrderNo().GetOrderNo(), 1, 1, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, Falcon.Function.ToDateTime(txtCunkuanTime.Text).ToString("yyyy-MM-dd"), SessionUid, txtAddRemark.Text, Config.Config.str_defTime, 0, "", "", 0, 0);
            if (oid != 0)
            {
                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    string msg = string.Empty;
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("page", "huoqiinfocun");
                    dict.Add("id", oid.ToString());
                    dict.Add("uid", id.ToString());
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + id }, "零钱存款通知", "您于" + Falcon.Function.ToDateTime(txtCunkuanTime.Text).ToString("yyyy年MM月dd日") + "零钱存款" + cunkuan + "元", dict, ref msg);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='phc1.aspx?id=" + oid + "';});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }
    }
}