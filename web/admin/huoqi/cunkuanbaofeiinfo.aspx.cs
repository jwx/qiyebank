﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class cunkuanbaofeiinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Huoqi().GetModel(id);
                    modelSetup = new BLL.Setup().Get_Setup();
                    if (model != null && model.id != 0)
                    {
                        lblDanhao.Text = model.danhao;
                        lblJinE.Text = model.jine.ToString();
                        hideUid.Value = model.uid.ToString();
                    }
                }
            }
        }

        protected int id;

        protected Model.Huoqi model;

        protected Model.Setup modelSetup;

        protected void btnBaofei_Click(object sender, EventArgs e)
        {
            if (new BLL.Huoqi().Update_CunkuanBaofei(id, SessionUid, txtDelRemark.Text))
            {
                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    string msg = string.Empty;
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + hideUid.Value }, "活期存款报废通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "报废了一笔活期存款，金额" + lblJinE.Text + "元，单号：" + lblDanhao.Text, new Dictionary<string, string>(), ref msg);
                }

                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }
    }
}