﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class yitixian : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Apply> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "isFukuan=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtPihao.Text.Trim() != "") where += " and pihao like '%" + txtPihao.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtBankName.Text.Trim() != "") where += " and bankname like '%" + txtBankName.Text + "%'";
                    if (txtBank_UName.Text.Trim() != "") where += " and bank_uname like '%" + txtBank_UName.Text + "%'";
                    if (txtBankCard.Text.Trim() != "") where += " and bankcard like '%" + txtBankCard.Text + "%'";
                    if (txtZhifubao.Text.Trim() != "") where += " and zhifubao like '%" + txtZhifubao.Text + "%'";
                    if (txtAddTime1.Text.Trim() != "") where += " and addTime>='" + txtAddTime1.Text.Trim() + "'";
                    if (txtAddTime2.Text.Trim() != "") where += " and addTime<='" + txtAddTime2.Text.Trim() + "'";
                    if (txtRemark.Text.Trim() != "") where += " and remark like '%" + txtRemark.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtFk_Remark.Text.Trim() != "") where += " and fk_remark like '%" + txtFk_Remark.Text + "%'";
                    if (txtFkTime1.Text.Trim() != "") where += " and fk_time>='" + txtFkTime1.Text.Trim() + "'";
                    if (txtFkTime2.Text.Trim() != "") where += " and fk_time<='" + txtFkTime2.Text.Trim() + "'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);


            listRecord = Tool.Pager_Sqlite.Query<Model.Apply>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Apply_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Pay(string id)
        {
            List<int> listIds = Array.ConvertAll<string, int>(id.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            if (listIds != null && listIds.Count != 0)
            {
                return new BLL.Apply().Update_Fukuan_Piliang(listIds, 1, SessionUid).ToString();
            }
            return "0";
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.Apply().GetDataTableByWhere(GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                Tool.ExcelHelper.DataTableToExcel("提现记录", new List<System.Data.DataTable>() { dt }, new List<string>() { "提现记录" }, new List<string[]>() { new string[] { "批号|pihao", "申请人|realname", "开户行|bankname", "户名|bank_uname", "银行卡号|bankcard", "支付宝|zhifubao", "提现金额|jine", "本金|benjin", "利息|lixi", "申请时间|addTime", "提现备注|remark", "经办人|jbname", "付款时间|fk_time", "付款备注|fk_remark" } });
            }
        }
    }
}