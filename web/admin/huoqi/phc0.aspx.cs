﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class phc0 : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            modelSettings = new BLL.Z_Settings().GetModel_Top();
            modelSettings = modelSettings == null ? new Model.Z_Settings() : modelSettings;

            int id = Falcon.Function.GetQueryInt("id");
            if (id != 0)
            {
                model = new BLL.Huoqi().GetModel(id);
            }
            model = model == null ? new Model.Huoqi() : model;
        }

        protected Model.Huoqi model;

        protected Model.Z_Settings modelSettings;

    }
}