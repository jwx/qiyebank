﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuanbaofeiinfo.aspx.cs" Inherits="web.admin.huoqi.qukuanbaofeiinfo" %>

<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquma = $("#txtShouquanma");
                    if (shouquma.length != 0) {
                        if (!ajax.VerifyShouquanma(shouquma.val()).value) {
                            layer.alert("授权码不正确！");
                            return false;
                        }
                    }
                    layer.load();
                }
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideUid" runat="server" Value="0" />
        <div class="main">
            <% if (model != null && model.id != 0)
                { %>
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<%=model.username %>]<%=model.realname %>
                        <a href="../users/detail.aspx?id=<%=model.uid %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <%--<tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <tr>
                    <td class="tdl">取款单号： 
                    </td>
                    <td>
                        <asp:Label ID="lblDanhao" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款金额： 
                    </td>
                    <td>
                        <asp:Label ID="lblQukuan" runat="server"></asp:Label>
                        <%--<%=-model.jine %> --%>元
                    </td>
                </tr>
                <tr>
                    <td class="tdl">利息： 
                    </td>
                    <td>
                        <%--<%=model.zonglixi %>--%>
                        <asp:Label ID="lblZonglixi" runat="server"></asp:Label>
                        元
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款类型： 
                    </td>
                    <td>
                        <%=(Config.Enums.Huoqi_Laiyuan)model.laiyuan %>
                        <asp:HiddenField ID="hideJinE" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl">取款日期： 
                    </td>
                    <td>
                        <%=model.cunkuanTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <%=model.add_remark %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">经办人： 
                    </td>
                    <td>
                        <%=model.jbname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">录入时间： 
                    </td>
                    <td>
                        <%=model.addTime %>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                    { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td class="tdl">报废备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDelRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <%--  <asp:Button ID="btnBaofei" runat="server" Text="报废" CssClass="btn btn-sm btnRed noloading" OnClientClick="return confirm('确实要报废当前取款数据吗？')" OnClick="btnBaofei_Click" />--%>
                        <asp:Button ID="btnBaofei" runat="server" Text="报废" CssClass="btn btn-sm btnRed noloading" OnClick="btnBaofei_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </form>
</body>
</html>
