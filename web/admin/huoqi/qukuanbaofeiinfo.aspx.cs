﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class qukuanbaofeiinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Huoqi().GetModel(id);
                    modelSetup = new BLL.Setup().Get_Setup();
                    if (model != null && model.id != 0)
                    {
                        hideUid.Value = model.uid.ToString();
                        lblDanhao.Text = model.danhao;
                        lblQukuan.Text = (-model.jine).ToString();
                        lblZonglixi.Text = model.zonglixi.ToString();
                        if (model.laiyuan == (int)Config.Enums.Huoqi_Laiyuan.本金取款)
                        {
                            hideJinE.Value = (-model.jine).ToString();
                        }
                        else if (model.laiyuan == (int)Config.Enums.Huoqi_Laiyuan.本息取款)
                        {
                            hideJinE.Value = (-model.jine + model.lixi).ToString();
                        }
                    }
                }
            }
        }

        protected int id;

        protected Model.Huoqi model;

        protected Model.Setup modelSetup;
         

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        protected void btnBaofei_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            if (new BLL.Huoqi().Update_QukuanBaofei(id, SessionUid, txtDelRemark.Text, ref msg))
            {
                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + hideUid.Value }, "活期取款报废通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "报废了一笔活期取款，金额-" + hideJinE.Value + "元，取款单号：" + lblDanhao.Text, new Dictionary<string, string>(), ref msg);
                }

                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000});", true);
            }
        }
    }
}