﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class zhuancuninfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
                lblRealName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("name"));

                txtCunkuanTime.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                //if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.hq_lilv))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                //}
                if (modelSetup == null || modelSetup.isSaved == 0)
                {
                    if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                }
                else
                {
                    decimal huoqi = new BLL.Huoqi().GetYue(id);
                    lblHuoqi.Text = huoqi + "元";
                    lblDingqi.Text = new BLL.Dingqi().GetYue(id) + "元";
                    txtQukuan.Attributes.Add("max", huoqi.ToString());
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);

                    List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);
                    string byWhat = modelSetup.dq_byDay == 0 ? "月" : "天";
                    foreach (Model.LilvRange one in listLilvRange)
                    {
                        ddlZhouqi.Items.Add(new ListItem() { Text = one.name + "_" + one.min + byWhat, Value = one.min + "|" + one.lilv });//value，存款周期|利率
                    }
                    ddlZhouqi.Items.Insert(0, new ListItem() { Text = "请选择存款周期", Value = "" });

                    //到期结算方式
                    rdDqjs0.Checked = modelSetup.dq_daoqi == 0;
                    rdDqjs1.Checked = modelSetup.dq_daoqi == 1;
                    rdDqjs2.Checked = modelSetup.dq_daoqi == 2;
                    rdDqjs3.Checked = modelSetup.dq_daoqi == 3;
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        [AjaxPro.AjaxMethod]
        public string GetDaoqiTime(int dq_byDay, string cunkuanTime, string ddlVal)
        {
            int zhouqi = Falcon.Function.ToInt(ddlVal.Split('|')[0]);//存款周期|利率
            return dq_byDay == 1 ? Falcon.Function.ToDateTime(cunkuanTime).AddDays(zhouqi + 1).ToString("yyyy-MM-dd") : Falcon.Function.ToDateTime(cunkuanTime).AddMonths(zhouqi).AddDays(1).ToString("yyyy-MM-dd");
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyUserPswd(int id, string userpswd)
        {
            Model.UserInfo model = new BLL.UserInfo().GetModel(id);
            if (model != null && model.id != 0)
            {
                return model.userpswd == Falcon.Function.Encrypt(userpswd);
            }
            return false;
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        [AjaxPro.AjaxMethod]
        public string GetLixi(int uid, decimal qukuan, string strSetup)
        {
            return new BLL.Huoqi().GetLixi(uid, qukuan, strSetup);
        }

        protected void btnBenjin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hideQukuan.Value))
            {
                string msg = string.Empty;
                int oid = new BLL.Huoqi().Qukuan_Benjin(id, hideQukuan.Value, Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value), SessionUid, txtAddRemark.Text, ref msg);
                if (oid != 0)
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='phq1.aspx?id=" + oid + "';});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('读取取款信息失败！',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }

        protected void btnZhuancun_Click(object sender, EventArgs e)
        {
            modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value);
            decimal cunkuan = Falcon.Function.ToDecimal(txtQukuan.Text.Trim());
            int dqjs = 0;
            if (rdDqjs1.Checked) dqjs = 1;
            else if (rdDqjs2.Checked) dqjs = 2;
            else if (rdDqjs3.Checked) dqjs = 3;

            if (!string.IsNullOrEmpty(hideQukuan.Value))
            {
                string msg = string.Empty;
                int oid = new BLL.Huoqi().Qukuan_Zhuancun(id, hideQukuan.Value, Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value), SessionUid, txtAddRemark.Text, dqjs, ddlZhouqi.SelectedItem.Text, Falcon.Function.ToDecimal(txtLilv.Text.Trim()), Falcon.Function.ToDecimal(Falcon.Function.GetFormString("hideBudaoqi")), Falcon.Function.GetFormString("hideDaoqiTime"), txtDingqiRemark.Text, ref msg);
                if (oid != 0)
                {
                    if (!string.IsNullOrEmpty(Config.Config.AppName))
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        dict.Add("page", "huoqiinfoqu");
                        dict.Add("id", oid.ToString());
                        dict.Add("uid", id.ToString());
                        Tool.JPushHelper.PushMessage(new string[] { "uid_" + id }, "零钱转定期通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "从零钱账户转入定期账户" + Falcon.Function.ToDecimal(txtQukuan.Text.Trim()) + "元", dict, ref msg);
                    }
                    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='phq1.aspx?id=" + oid + "';});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('读取取款信息失败！',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }
    }
}