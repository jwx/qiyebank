﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class cunkuancheckinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Huoqi().GetModel(id);
                }
            }
        }

        protected int id;

        protected Model.Huoqi model;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (new BLL.Huoqi().Update_Check(id, SessionUid, txtCkRemark.Text, Config.Enums.CheckStatus.审核通过))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        protected void btnNotPass_Click(object sender, EventArgs e)
        {
            if (new BLL.Huoqi().Update_Check(id, SessionUid, txtCkRemark.Text, Config.Enums.CheckStatus.审核未通过))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}