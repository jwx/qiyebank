﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class cunkuanimport : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                modelSetup = new BLL.Setup().Get_Setup();
                if (modelSetup == null || modelSetup.isSaved == 0)
                {
                    if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))//|| string.IsNullOrEmpty(modelSetup.hq_lilv)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                }
                else
                {
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                String fileName = fileUpload.FileName;
                String fileExt = Path.GetExtension(fileName).ToLower();
                if (fileExt != ".xls" && fileExt != ".xlsx")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('只允许上传.xls和.xlsx格式的文件！');", true);
                }
                else
                {
                    String dirPath = Server.MapPath("/uploadFile/Excel/");
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    HttpPostedFile imgFile = Request.Files[0];
                    String newFileName = (DateTime.Now.ToString("yyyyMMddHHmmss_ffff") + fileExt);
                    String filePath = dirPath + fileName + newFileName;
                    imgFile.SaveAs(filePath);

                    List<System.Data.DataTable> listDt = Tool.ExcelHelper.ExcelToDataTable(filePath, 0);
                    if (listDt != null && listDt.Count != 0)
                    {
                        System.Data.DataTable dt = null;
                        foreach (System.Data.DataTable one in listDt)
                        {
                            if (one != null && one.Rows.Count != 0)
                            {
                                dt = one;
                                break;
                            }
                        }

                        if (dt != null)
                        {
                            string msg = string.Empty;
                            modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value);
                            if (new BLL.Huoqi().Import(dt, SessionUid, modelSetup, ref msg))
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                            }
                            else
                            {
                                Falcon.FileOP.DeleteFile(filePath);
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('" + msg + "！');", true);
                            }
                        }
                        else
                        {
                            Falcon.FileOP.DeleteFile(filePath);
                            ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('Excel中没有要导入的数据！');", true);
                        }
                    }
                    else
                    {
                        Falcon.FileOP.DeleteFile(filePath);
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('Excel中没有数据！');", true);
                    }
                }
            }
        }
    }
}