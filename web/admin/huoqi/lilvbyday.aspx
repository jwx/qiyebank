﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lilvbyday.aspx.cs" Inherits="web.admin.huoqi.lilvbyday" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <link href="/Plug/layui/css/layui.css" rel="stylesheet" />
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <style type="text/css">
        .item { }
            .item .title { font-size: 14px; margin-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dedede; }
            .item .content { padding-bottom: 10px; }
                .item .content p input[type='text'] { width: 50px; margin: 0px 3px; }
                .item .content p a { margin-left: 5px; }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                //tiptype: 1,//1=> 自定义弹出框提示；
                tiptype: function (msg, o, cssctl) {
                    //msg：提示信息;
                    //o:{obj:*,type:*,curform:*}, obj指向的是当前验证的表单元素（或表单对象），type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, curform为当前form对象;
                    //cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
                    if (!o.obj.is("form")) {//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
                        //var objtip = o.obj.siblings(".Validform_checktip");
                        //cssctl(objtip, o.type);
                        //objtip.text(msg); 
                        if (o.type == 3) {
                            layer.msg(msg, { icon: 5, time: 1500 }, function () {
                                layer.closeAll();
                            });
                        }
                    }
                }, beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;	

                    var arrayMin = [];
                    var arrayMax = [];
                    var arrayLilv = [];

                    $(".content p").each(function () {
                        var obj_input = $(this).find("input");
                        arrayMin.push($.trim(obj_input.eq(2).val()));
                        arrayMax.push($.trim(obj_input.eq(1).val())); 
                        arrayLilv.push($.trim(obj_input.eq(0).val())); //利率
                    });

                    if (ajax.Save(arrayMin.toString(), arrayMax.toString(), arrayLilv.toString()).value) {
                        layer.msg("操作成功！", { icon: 6, time: 1500 }, function () {
                            parent.layer.closeAll();
                        });
                    } else {
                        layer.msg("操作失败！", { icon: 5, time: 1500 }, function () {
                            layer.closeAll();
                        });
                    }
                    return false;
                }
            });

            var html = '<p><input type="text" datatype="n" nullmsg="请填写天数" errormsg="请填写正整数" />天< 存款时间 <= <input type="text" datatype="n" ignore="ignore" errormsg="请填写正整数" />天，年利率<input type="text" datatype="*" nullmsg="请填写年利率" />% <a href="javascrpt:void(0);" class="dellilv">[删除]</a></p>'
            //$("#btnAddRow").click(function () {
            //    $(".content").append(html);
            //});

            $(document).on("click", ".dellilv", function () {
                $(this).parent().remove();
            });
        });

        function showDemo() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 1,
                area: '400px',
                scrollbar: false,
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: '<img src="huoqi_byday.png" style="width:400px; height:230px;" />'
            });
        }
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main ">
            <div class="item">
                <div class="title">
                    设置零钱利率_按天计算 
                    <%--<input type="button" id="btnAddRow" value="+ 新增零钱利率" />--%>
                </div>
                <p>
                 <%--   <input type="button" id="btnAddRow" value="+ 新增零钱利率" />
                    <a href="javascript:showDemo();">查看填写示例</a>--%>
                </p>
                <div class="content">
                    <% Model.Setup model = new BLL.Setup().Get_HuoqiLilv();
                       if (string.IsNullOrEmpty(model.hq_lilv))
                       {
                    %>
                    <p>
                      <%--  <input type="text" datatype="n" nullmsg="请填写天数" errormsg="请填写正整数" />天< 存款时间 <=
                        <input type="text" datatype="n" ignore="ignore" errormsg="请填写正整数" />天，--%>年利率<input type="text" datatype="*" nullmsg="请填写年利率" />% 
         <%--               <a href="javascrpt:void(0);" class="dellilv">[删除]</a>--%>
                    </p>
                    <%}
                       else if (model.hq_byDay == 1)
                       {
                           List<Model.LilvRange> list = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(model.hq_lilv);
                           foreach (Model.LilvRange one in list)
                           {
                    %>
                    <p>
                    <%--    <input type="text" datatype="n" nullmsg="请填写天数" errormsg="请填写正整数" value="<%=one.min %>" />天< 存款时间 <=
                        <input type="text" datatype="n" ignore="ignore" errormsg="请填写正整数" value="<%=one.max==0?"":one.max.ToString() %>" />天，--%>年利率<input type="text" datatype="*" nullmsg="请填写年利率" value="<%=one.lilv %>" />% 
                        <%--<a href="javascrpt:void(0);" class="dellilv">[删除]</a>--%>
                    </p>
                    <%}
                       } %>
                </div>
                <div>
                    <input type="submit" id="btnSave" value="保存" class="layui-btn layui-btn-small layui-btn-normal" />
                    <input type="button" id="Submit1" value="重置" class="layui-btn layui-btn-small" onclick="window.location.href = window.location;" />
                    <input type="button" id="btnClose" value="关闭" class="layui-btn layui-btn-small layui-btn-primary" onclick="parent.layer.closeAll();" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
