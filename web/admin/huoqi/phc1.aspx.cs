﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class phc1 : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            //modelSettings = new BLL.Z_Settings().GetModel_Top();
            //modelSettings = modelSettings == null ? new Model.Z_Settings() : modelSettings;

            int id = Falcon.Function.GetQueryInt("id");
            if (id != 0)
            {
                modelPrint = new BLL.SetupPrint().GetModel(Config.Enums.Print_Type.活期存款);
            }
            modelPrint = modelPrint == null ? new Model.SetupPrint() : modelPrint;

            HttpCookie cookie = Request.Cookies["jyjz_name"];
            if (cookie != null)
            {
                name = cookie.Value;
            }
        }

        protected Model.SetupPrint modelPrint;

        protected string name;

        //protected Model.Z_Settings modelSettings;

        [AjaxPro.AjaxMethod]
        public Model.Huoqi GetModel(int id, string name)
        {
            Model.Huoqi model = new BLL.Huoqi().GetModel(id);
            if (model != null && model.id != 0)
            {
                Model.Setup modelSetup = new BLL.Setup().Get_Setup();
                modelSetup = modelSetup == null ? new Model.Setup() : modelSetup;
                //string title = HttpContext.Current.Request.Cookies["jyjz_name"].Value;必须得用HttpContext.Current
                model.title = name + "零钱存款";
                model.money = model.yuanjine + "元";
                model.op_type = model.isAutoCalc == 0 ? "取款时结算" : ("每月" + modelSetup.days + "日结算");
                model.chs_money = Tool.MoneyToUpper.ToUpper(model.yuanjine.ToString());
                model.type = ((Config.Enums.Huoqi_Laiyuan)model.laiyuan).ToString();
                return model;
            }
            return null;
        }
    }
}