﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qukuaninfo.aspx.cs" Inherits="web.admin.huoqi.qukuaninfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script src="/Plug/Validform/Validform_Datatype.js"></script>
    <style type="text/css">
        .lixi {
            font-weight: bold;
            color: red;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquanma = $("#txtShouquanma");
                    if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                        layer.alert("授权码错误！");
                        return false;
                    }
                    layer.load();
                }
            });


            var qukuan = 0;
            var qukuanlixi = 0;
            $("#txtQukuan").blur(function () {
                var huoqi = Number($.trim($("#lblHuoqi").text()));
                if (huoqi == 0) {
                    layer.alert("零钱账户余额不足！");
                    return false;
                }

                qukuan = Number($.trim($("#txtQukuan").val()));
                if (qukuan) {
                    layer.load();
                    ajax.GetLixi(parseInt($(".main").attr("id")), qukuan, $("#hideSetup").val(), function (data) {//异步调用
                        //alert(data.value);
                        layer.closeAll();
                        if (data.value) {
                            $("#hideQukuan").val(data.value);
                            //$("#txtAddRemark").val(data.value);
                            var json = $.parseJSON(data.value);
                            //alert(json.lixi_sum);

                           // $(".lixi").text("利息合计：" + json.lixi_sum + "元");
                            $("#hideLixi").val(json.lixi_sum);
                            $("#btnBenjin").val("取款" + qukuan + "元").attr("benjin", qukuan).attr("lixi", json.lixi_sum);
                         //   $("#btnBenjinLixi").val("本息取款" + json.benjinlixi + "元").attr("benxi", json.benjinlixi);
                        } else {
                            $("#hideLixi").val("0");
                            layer.alert("利息核算错误！");
                        }
                    });
                }
            });

            $("#btnBenjin").click(function () {
                if (fnVerify()) {
                    return true;
                 //   var t = $(this);
                  //  return confirm("确定只支取" + t.attr("benjin") + "元本金，同时将" + t.attr("lixi") + "元利息转入零钱账户吗？");
                }
                return false;
            });


            $("#btnBenjinLixi").click(function () {
                if (fnVerify()) {
                      return true;
                    //var t = $(this);
                    //return confirm("确定同时本息取款，合计" + t.attr("benxi") + "元吗？");
                }
                return false;
            });

            function fnVerify() {
                var qukuan = Number($.trim($("#txtQukuan").val()));
                if (!qukuan) {
                    layer.alert("请填写取款金额！");
                    return false;
                }
                if (!ajax.VerifyUserPswd(Number($(".main").attr("id")), $("#txtUserPswd").val()).value) {
                    layer.alert("支付密码错误！");
                    return false;
                }
                var shouquanma = $("#txtShouquanma");
                if (shouquanma.length != 0 && !ajax.VerifyShouquanma(shouquanma.val()).value) {
                    layer.alert("授权码错误！");
                    return false;
                }
                return true;
            }
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <asp:HiddenField ID="hideQukuan" runat="server" />
        <%--<input type="hidden" id="hideQukuan" value="" />--%>
        <div class="main" id="<%=id %>">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<asp:Label ID="lblUserName" runat="server"></asp:Label>]<asp:Label ID="lblRealName" runat="server"></asp:Label>
                        <a href="../users/detail.aspx?id=<%=id %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                        元
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                        元
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>取款金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtQukuan" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/,numrange" min="0" placeholder="请输入取款金额" errormsg="请正确输入取款金额"></asp:TextBox>
                        元
                        <span class="lixi"></span>
                        <span class="Validform_checktip">请输入取款金额</span>
                        <asp:HiddenField ID="hideLixi" runat="server" Value="0" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>取款日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCunkuanTime" runat="server" datatype="*" Enabled="false" CssClass="Wdate" placeholder="请输入存款日期" errormsg="请输入存款日期" onclick="WdatePicker()"></asp:TextBox>
                        <span class="Validform_checktip">请输入取款日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>支付密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd" runat="server" datatype="*" TextMode="Password" placeholder="请输入支付密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入支付密码</span>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                    { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnBenjin" runat="server" Text="取款" CssClass="btn btn-sm btnBlue noloading" OnClick="btnBenjin_Click" />
                <%--        <asp:Button ID="btnBenjinLixi" runat="server" Text="本息取款" CssClass="btn btn-sm btnRed noloading" OnClick="btnBenjinLixi_Click" />--%>
                   <%--     <asp:Button ID="btnQukuan" runat="server" Text="取款" CssClass="btn btn-sm btnGreen noloading" OnClick="btnQukuan_Click" />--%>
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
