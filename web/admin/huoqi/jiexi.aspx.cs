﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class jiexi : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                //if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.hq_lilv))
                //{
                //    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('读取系统设置出错！',{icon:5,time:1000});", true);
                //}
                //else if (modelSetup.hq_autoCalc != 0)
                //{
                //    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('批量结息只适用于取款时结算利息！',{icon:5,time:1000});", true);
                //}
                //else
                //{
                //    hideJiexi.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                //}
                //if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.hq_lilv))
                //{
                //    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('读取系统设置出错！',{icon:5,time:1000});", true);
                //}
                //else if (modelSetup.hq_autoCalc != 0)
                //{
                //    //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('批量结息只适用于取款时结算利息！',{icon:5,time:1000});", true);
                //}
                if (modelSetup != null && modelSetup.isSaved == 1 && !string.IsNullOrEmpty(modelSetup.hq_lilv) && modelSetup.hq_autoCalc == 0)
                {
                    hideJiexi.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                }
            }
        }

        protected Model.Setup modelSetup;

        protected void btnJiexi_Click(object sender, EventArgs e)
        {
            DateTime date = Falcon.Function.ToDateTime(txtDate.Text);
            //string dbPath = Tool.DatabaseHelper.dbPath;
            //System.IO.File.Copy(dbPath, dbPath.Replace("jingyi.dll", "Bank_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + date.ToString("yyyyMMdd") + "活期批量结息备份.db"));

            List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(null);//获取系统中所有的存款单
            if (listHuoqi != null && listHuoqi.Count != 0)
            {
                string msg = string.Empty;
                if (!string.IsNullOrEmpty(hideJiexi.Value))
                {
                    modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideJiexi.Value);
                    //if (new BLL.Huoqi().ManualAutoLixi(listHuoqi, DateTime.Now, modelSetup, ref msg))//结息是否成功
                    if (new BLL.Huoqi().ManualAutoLixi(listHuoqi, date, modelSetup, ref msg))//结息是否成功
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败," + msg + "！',{icon:5,time:1000});", true);
                    }
                }
            }
        }
    }
}