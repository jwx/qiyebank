﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="phq1.aspx.cs" Inherits="web.admin.huoqi.phq1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="/Scripts/LodopFuncs.js"></script>
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        var LODOP;
        function CheckIsInstall() {
            try {
                LODOP = getLodop();
                if (LODOP.VERSION) {
                    if (LODOP.CVERSION) {
                        //alert("当前有C-Lodop云打印可用!\n C-Lodop版本:" + LODOP.CVERSION + "(内含Lodop" + LODOP.VERSION + ")");
                    }
                    else {
                        //alert("本机已成功安装了Lodop控件！\n 版本号:" + LODOP.VERSION);
                    }
                }
            } catch (err) {

            }
        }

        function print_Preview() {
            CheckIsInstall();
            printOrder();
            LODOP.PREVIEW();
        }

        function print_Setup() {
            CheckIsInstall();
            printOrder();
            LODOP.PRINT_SETUP();
        }


        function print_Design() {
            CheckIsInstall();
            printOrder();
            LODOP.PRINT_DESIGN();
        }

        function printOrder() {
            var obj = $("#main");
            var model = ajax.GetModel(Number(obj.attr("oid")), obj.attr("name")).value;
            if (model) {
                <%=modelPrint.jsCode %>
            }
        }

        $(function () {
            setTimeout("print_Preview()", 1000);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="main" oid="<%=Falcon.Function.GetQueryInt("id") %>" name="<%=name %>" style="margin: 30px 50px;">
            <input type="button" runat="server" value="打印预览" onclick="print_Preview();" />
            <input type="button" runat="server" value="打印维护" onclick="print_Setup();" />
            <%--<input type="button" runat="server" value="打印设计" onclick="print_Design();" />--%>
            <input type="button" runat="server" value="刷新" onclick="window.location.href = window.location" />
            <input type="button" runat="server" value="关闭" onclick="parent.layer.closeAll();" />
        </div>
    </form>
</body>
</html>
