﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zhuanruinfo.aspx.cs" Inherits="web.admin.huoqi.zhuanruinfo" %>

<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquma = $("#txtShouquanma");
                    if (shouquma.length != 0) {
                        if (!ajax.VerifyShouquanma(shouquma.val()).value) {
                            layer.alert("授权码不正确！");
                            return false;
                        }
                    }
                    layer.load();
                }
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideUid" runat="server" Value="0" />
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl">零钱利率明细： 
                    </td>
                    <td>
                        <%
                            int useShouquanma = 0;
                            if (!string.IsNullOrEmpty(model.model_setup))
                            {
                                Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(model.model_setup);
                                useShouquanma = modelSetup.useShouquanma;
                                string byWhat = modelSetup.hq_byDay == 0 ? "月" : "天";
                        %>
                      <%--  <div>
                            利息按<%=byWhat %>计算
                        </div>--%>
                        <div>
                            <% if (!string.IsNullOrEmpty(modelSetup.hq_lilv))
                               {
                                   List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.hq_lilv);
                                   foreach (Model.LilvRange one in listLilv)
                                   {
                            %>
                            <div>
                               <%-- <%=one.min %><%=byWhat %>< 存款时间 <= <%=one.max==0?"∞":one.max.ToString() %><%=byWhat %>，--%>年利率:<%=one.lilv %>% 
                            </div>
                            <%}
                               }
                               else
                               { %>
                            年利率：<%=modelSetup.hq_daylilv %>%
                            <%} %>
                        </div>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="width: 200px;">付款人姓名： 
                    </td>
                    <td>
                        <%=model.realname %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">手机号码： 
                    </td>
                    <td>
                        <%=model.mobile %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">支付宝账号： 
                    </td>
                    <td>
                        <%=model.zhifubao %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>转入金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtJinE" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入转入金额" errormsg="请输入转入金额"></asp:TextBox>
                        元
                        <span class="Validform_checktip">请输入转入金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>转入日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCunkuanTime" runat="server" datatype="*" CssClass="Wdate" placeholder="请选择转入日期" errormsg="请选择转入日期" onclick="WdatePicker()"></asp:TextBox>
                        <span class="Validform_checktip">请选择转入日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">转入备注： 
                    </td>
                    <td>
                        <%=model.add_remark %>
                    </td>
                </tr>

                <tr>
                    <td class="tdl">审核意见： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCk_Remark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <% if (model.model_setup != null && useShouquanma == 1)
                   { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="审核通过" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" OnClientClick="return confirm('确实要通过审核吗？')" />
                        <asp:Button ID="btnNotPass" runat="server" Text="审核不通过" CssClass="btn btn-sm btnRed noloading" OnClick="btnNotPass_Click" OnClientClick="return confirm('确实要不通过审核吗？')" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
