﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cunkuaninfo.aspx.cs" Inherits="web.admin.huoqi.cunkuaninfo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var shouquma = $("#txtShouquanma");
                    if (shouquma.length != 0) {
                        if (!ajax.VerifyShouquanma(shouquma.val()).value) {
                            layer.alert("授权码不正确！");
                            return false;
                        }
                    }
                    layer.load();
                }
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideSetup" runat="server" />
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">账号/户名： 
                    </td>
                    <td>[<asp:Label ID="lblUserName" runat="server"></asp:Label>]<asp:Label ID="lblRealName" runat="server"></asp:Label>
                        <a href="../users/detail.aspx?id=<%=id %>" class="layer" title="用户明细">查看用户明细</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">零钱账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblHuoqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">定期账户余额： 
                    </td>
                    <td>
                        <asp:Label ID="lblDingqi" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">零钱利率明细： 
                    </td>
                    <td>
                        <% if (modelSetup != null)
                           {
                               string byWhat = modelSetup.hq_byDay == 0 ? "月" : "天";
                        %>
                      <%--  <div>
                            利息按<%=byWhat %>计算
                        </div>--%>
                        <div>
                            <% if (!string.IsNullOrEmpty(modelSetup.hq_lilv))
                               {
                                   List<Model.LilvRange> listLilv = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.hq_lilv);
                                   foreach (Model.LilvRange one in listLilv)
                                   {
                            %>
                            <div>
                           <%--     <%=one.min %><%=byWhat %>< 存款时间 <= <%=one.max==0?"∞":one.max.ToString() %><%=byWhat %>，--%>年利率:<%=one.lilv %>% 
                            </div>
                            <%}
                               }
                               else
                               { %>
                            年利率：<%=modelSetup.hq_daylilv %>%
                            <%} %>
                        </div>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>存款金额： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCunkuan" runat="server" datatype="/^[0-9]+(.[0-9]{1,2})?$/" placeholder="请输入存款金额" errormsg="请输入存款金额"></asp:TextBox>
                        元
                        <span class="Validform_checktip">请输入存款金额</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>存款日期： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCunkuanTime" runat="server" datatype="*" CssClass="Wdate" placeholder="请输入存款日期" errormsg="请输入存款日期" onclick="WdatePicker()"></asp:TextBox>
                        <span class="Validform_checktip">请输入存款日期</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddRemark" runat="server" TextMode="MultiLine" Style="width: 90%; height: 80px;"></asp:TextBox>
                    </td>
                </tr>
                <% if (modelSetup != null && modelSetup.useShouquanma == 1)
                   { %>
                <tr>
                    <td class="tdl"><span class="required">*</span>授权码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtShouquanma" runat="server" datatype="*" TextMode="Password" placeholder="请输入授权码"></asp:TextBox>
                        <span class="Validform_checktip">请输入授权码</span>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
