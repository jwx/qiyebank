﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class zhuanruinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.HuoqiIn().GetModel(id);
                    if (model != null && model.id != 0)
                    {
                        txtJinE.Text = model.jine.ToString();
                        txtCunkuanTime.Text = model.cunkuanTime;
                        hideUid.Value = model.uid.ToString();
                    }
                }
            }
            model = model == null ? new Model.HuoqiIn() : model;
        }

        protected Model.HuoqiIn model;

        protected int id;

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id != 0)
            {
                string msg = string.Empty;
                if (new BLL.HuoqiIn().Update_CheckPass(id, SessionUid, txtCk_Remark.Text, ref msg))
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("page", "huoqiinapplyinfo");
                    dict.Add("id", id.ToString());
                    dict.Add("uid", hideUid.Value);
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + hideUid.Value }, "零钱存款审核通知", "恭喜，您于" + Falcon.Function.ToDateTime(txtCunkuanTime.Text).ToString("yyyy年MM月dd日") + "申请转入的零钱存款" + txtJinE.Text + "元已通过审核。", dict, ref msg);

                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "！',{icon:5,time:1000});", true);
                }
            }
        }

        protected void btnNotPass_Click(object sender, EventArgs e)
        {
            if (new BLL.HuoqiIn().Update_CheckNotPass(id, SessionUid, txtCk_Remark.Text))
            {
                string msg = string.Empty;
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("page", "huoqiinapplyinfo");
                dict.Add("id", id.ToString());
                dict.Add("uid", hideUid.Value);
                Tool.JPushHelper.PushMessage(new string[] { "uid_" + hideUid.Value }, "零钱存款审核通知", "抱歉，您于" + Falcon.Function.ToDateTime(txtCunkuanTime.Text).ToString("yyyy年MM月dd日") + "申请转入的零钱存款" + txtJinE.Text + "元未未通过审核，请联系管理人员！", dict, ref msg);

                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}