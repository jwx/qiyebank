﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class zhuanzhanginfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
                lblRealName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("name"));

                txtZhuanzhangTime.Text = DateTime.Now.ToString("yyyy-MM-dd");

                modelSetup = new BLL.Setup().Get_Setup();
                //if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.hq_lilv))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                //}
                if (modelSetup == null || modelSetup.isSaved == 0)
                {
                    if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "parent.fnSetLilv();", true);
                    }
                }
                else
                {
                    decimal huoqi = new BLL.Huoqi().GetYue(id);
                    lblHuoqi.Text = huoqi.ToString();
                    lblDingqi.Text = new BLL.Dingqi().GetYue(id).ToString();
                    txtQukuan.Attributes.Add("max", huoqi.ToString());
                    hideSetup.Value = Tool.JsonHelper.JsonSerializer<Model.Setup>(modelSetup);
                    //hideAutoCalc.Value = modelSetup.hq_autoCalc.ToString();
                    //btnQukuan.Visible = modelSetup.hq_autoCalc != 0;
                    //btnBenjin.Visible = btnBenjinLixi.Visible = modelSetup.hq_autoCalc == 0;
                }
            }
        }

        protected int id;

        protected Model.Setup modelSetup;

        [AjaxPro.AjaxMethod]
        public bool VerifyUserPswd(int id, string userpswd)
        {
            Model.UserInfo model = new BLL.UserInfo().GetModel(id);
            if (model != null && model.id != 0)
            {
                return model.userpswd == Falcon.Function.Encrypt(userpswd);
            }
            return false;
        }

        [AjaxPro.AjaxMethod]
        public bool VerifyShouquanma(string shouquanma)
        {
            return shouquanma == SessionShouquan;
        }

        [AjaxPro.AjaxMethod]
        public string GetLixi(int uid, decimal qukuan, string strSetup)
        {
            Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(strSetup);
            List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(uid, modelSetup.hq_houcunxianqu);

            Model.HuoqiQuKuan modelQukuan = new Model.HuoqiQuKuan();
            decimal sum_lixi = 0;
            if (listHuoqi != null && listHuoqi.Count != 0)
            {
                //List<Model.HuoqiQuKuan> listQukuan = new List<Model.HuoqiQuKuan>();//在取款单中定义本次取款的json数据，供报废使用
                bool isAutoLixi = modelSetup.hq_autoCalc != 0;
                decimal zongqukuan = qukuan;
                List<Model.HuoqiLixi> listLixi = new List<Model.HuoqiLixi>();
                DateTime dtNow = DateTime.Now;
                foreach (Model.Huoqi one in listHuoqi)
                {
                    Model.HuoqiQuKuan m_qk = new Model.HuoqiQuKuan();
                    //Model.HuoqiLixi m_lixi = new Model.HuoqiLixi();
                    if (one.jine >= qukuan)
                    {
                        //m_qk.id = one.id;
                        //m_qk.sy = one.jine - qukuan;
                        //m_qk.qc = qukuan;
                        //listQukuan.Add(m_qk);
                        //还需要核算每笔存款的利息
                        decimal lixi = isAutoLixi ? 0 : new BLL.Huoqi().GetLixiByOrder(qukuan, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = qukuan });//目前只知道存款单id，利息，本次取款，不知道取款单id
                        break;
                    }
                    else
                    {
                        qukuan = qukuan - one.jine;
                        //m_qk.id = one.id;
                        //m_qk.sy = 0;
                        //m_qk.qc = one.jine;
                        //还需要核算每笔存款的利息 
                        decimal lixi = isAutoLixi ? 0 : new BLL.Huoqi().GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = one.jine });//目前只知道存款单id，利息，本次取款，不知道取款单id
                    }
                    //listQukuan.Add(m_qk);
                }
                modelQukuan.benjinlixi = zongqukuan + sum_lixi;
                modelQukuan.lixi_sum = sum_lixi;
                modelQukuan.list_lixi = listLixi;
                //modelQukuan.list_qukuan = listQukuan;
                return Tool.JsonHelper.JsonSerializer<Model.HuoqiQuKuan>(modelQukuan);
            }
            return null;
        }

        [AjaxPro.AjaxMethod]
        public Model.UserInfo GetUserInfo(string username)
        {
            return new BLL.UserInfo().GetModel(username);
        }

        protected void btnZhuanzhang_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hideQukuan.Value))
            {
                string msg = string.Empty;
                int oid = new BLL.Huoqi().Qukuan_Benjin_Zhanzhang(id, lblRealName.Text, Falcon.Function.ToInt(hideShoukuan.Value), lblRealName_Shoukuan.Text, hideQukuan.Value, Tool.JsonHelper.JsonDeserialize<Model.Setup>(hideSetup.Value), SessionUid, txtAddRemark.Text, Falcon.Function.ToDateTime(txtZhuanzhangTime.Text.Trim(), DateTime.Now), ref msg);
                if (oid != 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href='phq1.aspx?id=" + oid + "';});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('读取取款信息失败！',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
            }
        }
    }
}