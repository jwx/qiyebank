﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.huoqi
{
    public partial class qunkuanSHdetail : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id");

            if (id != 0)
            {
                model = new BLL.Huoqi().GetModel(id);
            }
        }
        protected int id;

        protected Model.Huoqi model;
        /// <summary>
        /// 同意   同意1 拒绝2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                string msg = string.Empty;
                string json_Qukuan = model.qyQK_json; //可取款的id
                Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
                if (new BLL.Huoqi().Update_qukuanStatus(id, (int)Config.Enums.Huoqi_SHstatus.已审核, txtExRemark.Text, SessionUid, ref msg, modelQukuan) == 0) //修改审核
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + ex.ToString() + "！',{icon:6,time:1000});", true);
            }
        }
        /// <summary>
        /// 审核拒绝
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNO_Click(object sender, EventArgs e)
        {
            try
            {
                string msg = string.Empty;
                string json_Qukuan = model.qyQK_json; //可取款的id
                Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
                if (new BLL.Huoqi().Update_qukuanStatus(id, (int)Config.Enums.Huoqi_SHstatus.已拒绝, txtExRemark.Text, SessionUid) == 0) //修改审核
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + msg + "',{icon:5,time:1000}),function(){window.location.href=window.location;};", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('" + ex.ToString() + "！',{icon:6,time:1000});", true);
            }
        }
    }
}