﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main1.aspx.cs" Inherits="web.admin.main1" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>精益软件管理系统</title>
    <link href="/Css/jingyi.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        function initSize() {
            $(".frame").height($(window).height() - $(".top").height());
        }

        $(function () {

            $(window).resize(function () {
                initSize();
            });

            $(".menu>li").hover(function () {
                $(this).find(".menu2").fadeIn();
            }, function () {
                $(this).find(".menu2").hide();
            });


            $(".menu2").width($(".menu").width());

        });
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <div class="top">
            <div class="topbar">
                <div class="topitem">
                    欢迎登录精益软件管理系统
                </div>
            </div>
            <div class="menubar">
                <div class="menuitem">
                    <a>
                        <img class="logo" src="/images/" />
                    </a>
                    <ul class="menu">
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 360px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                                <div class="module_wrap" style="width: 180px;">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">类功能管理视图</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                <div class="module_wrap">
                                    <%--  <div class="module_hd">
                                        通用解决方案
                                    </div>--%>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                的范德萨发犯嘀咕若风退役让他让他同一天人也
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                完钱而已非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="menu2">
                                顺丰王瑞峰我热热污染热温热污染
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="menu2">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="menu2">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="menu2">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="frame">
        </div>
    </form>
</body>
</html>
