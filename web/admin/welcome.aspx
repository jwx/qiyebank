﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="welcome.aspx.cs" Inherits="web.admin.welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <script src="../Plug/Highcharts-5.0.7/code/highcharts.js"></script>
    <script src="../Plug/Highcharts-5.0.7/code/highcharts-more.js"></script>
    <style type="text/css">
        html, body, form { height: 100%; background-color: #EAEDEF; font-family: 'Microsoft YaHei'; }
        .chartTitle { font-family: 'Microsoft YaHei'; font-size: 14px; margin-top: 0px; }
        .chartTip { font-family: 'Microsoft YaHei'; font-size: 12px; font-weight: normal; }
        .main { height: 100%; padding: 5px; }
            .main .charts { background-color: white; margin: 5px; height: 320px; }
            .main #line_basic { background-color: white; margin: 8px; height: 500px; }

            .main .charts .dingqiData { font-size: 14px; padding-top: 24px; text-align: center; margin-bottom: 10px; }
            .main .charts.tongji table { width: 100%; margin: 0px; color: white; }
            .main .charts.tongji p { text-align: center; color: white; padding: 10px 0px; font-size: 14px; font-weight: bold; border-bottom: 1px dotted white; }
            .main .charts.tongji table div { margin: 20px 10px; }
                .main .charts.tongji table div a { display: block; color: white; text-decoration: none; }
    </style>
    <script type="text/javascript">
        $(function () {
            //饼形图
            $('#sysYue').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: '<div class="chartTitle">系统余额：<%=money_huoqi+money_dingqi %>元</div>',
                    y: 30
                },
                tooltip: {
                    pointFormat: '{series.name}: <b class="chartTip">{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true,
                        size :200,
                    }
                },
                series: [{
                    type: 'pie',
                    name: '所占比例',
                    data: [ {
                        name: '<span class="chartTip">零钱账户:<%=money_huoqi %>元</span>',
                        y: <%=rate_huoqi %>,
                        sliced: true,
                        selected: true,
                        color:"#DDDF00"
                    },
                         {
                             name: '<span class="chartTip">定期账户:<%=money_dingqi %>元</span>',
                             y: <%=rate_dingqi %>, 
                             color:"#028DC7"
                         } 
                    ]
                }]
            });

            //扇形图
            $('#sysLixi_Payed').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: '<div class="chartTitle">已付利息：<%=lixi_huoqipayed+lixi_dingqipayed %>元</div>', 
                    y: 30
                },
                tooltip: { 
                    pointFormat: '{series.name}: <b class="chartTip">{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            //distance: -50,
                            //style: {
                            //    //fontWeight: 'bold',
                            //    //color: 'white',
                            //    //textShadow: '0px 1px 2px black'
                            //}
                        },
                        cursor: 'pointer',
                        showInLegend: true,
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '85%'],
                        size :280,
                    }
                },
                series: [{
                    type: 'pie',
                    name: '所占比例',
                    innerSize: '50%',
                    data: [ {
                        name: '<span class="chartTip">零钱利息:<%=lixi_huoqipayed %>元</span>',
                        y: <%=rate_huoqipayedlixi %>,
                        color:"#DDDF00",
                        //dataLabels: {
                        //    enabled: false
                        //}
                    },{
                        name: '<span class="chartTip">定期利息:<%=lixi_dingqipayed %>元</span>',
                        y: <%=rate_dingqipayedlixi %>,
                        color:"#028DC7",
                        //dataLabels: {
                        //    enabled: true
                        //}
                    }]
                }]
            });
              
            //折线图-零钱定期存取款
            $('#dinghuoqi_line').highcharts({
                title: {
                    text: '<div class="chartTitle">系统存取款日统计表</div>',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: [<%=string.Join(",",listX_HalfMonth) %>]
                },
                yAxis: {
                    title: {
                        text: '系统存款（元）'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '元'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: '总存款',
                    data: [<%=string.Join(",",listTotalJine_Cun) %>],
                    color:"#DDDF00",
                }, {
                    name: '总取款',
                    data: [<%=string.Join(",",listTotalJine_Qu) %>],
                    color:"#028DC7",
                }, {
                    name: '净流入',
                    data: [<%=string.Join(",",listTotalJine) %>],
                    color:"#FF0000",
                }]
            });

            //折线图-零钱存取款
            $('#huoqi_line').highcharts({
                title: {
                    text: '<div class="chartTitle">零钱存取款日统计表</div>',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: [<%=string.Join(",",listX_HalfMonth) %>]
                },
                yAxis: {
                    title: {
                        text: '系统存款（元）'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '元'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: '存款',
                    data: [<%=string.Join(",",listHuoqiJine_Cun) %>],
                    color:"#DDDF00",
                }, {
                    name: '取款',
                    data: [<%=string.Join(",",listHuoqiJine_Qu) %>],
                    color:"#028DC7",
                }]
            });
   
            //折线图-dingqi存取款
            $('#dingqi_line').highcharts({
                title: {
                    text: '<div class="chartTitle">定期存取款日统计表</div>',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: [<%=string.Join(",",listX_HalfMonth) %>]
                },
                yAxis: {
                    title: {
                        text: '系统存款（元）'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '元'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: '存款',
                    data: [<%=string.Join(",",listDingqiJine_Cun) %>],
                    color:"#DDDF00",
                }, {
                    name: '取款',
                    data: [<%=string.Join(",",listDingqiJine_Qu) %>],
                    color:"#028DC7",
                }]
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 30%">
                        <div class="charts" id="sysYue">
                        </div>
                    </td>
                    <td style="width: 30%">
                        <div class="charts" id="sysLixi_Payed">
                        </div>
                    </td>
                    <td>
                        <div class="charts tongji">
                            <div class="dingqiData">定期存款到期数据</div>
                            <table>
                                <tr>
                                    <td>
                                        <div style="background-color: #F7A35C; height: 100%;">
                                            <%  
                                                decimal jine = 0;
                                                decimal lixi = 0;
                                                string strDtNow = dtNow.ToString("yyyy-MM-dd");
                                                foreach (Model.Dingqi one in listDingqi_Days)
                                                {
                                                    jine += one.jine;
                                                    lixi += GetLixi(modelSetup, one);
                                                } %>
                                            <p>
                                                三日数据
                                            </p>
                                            <div>
                                                <a href="tongji/dingqi_daoqi.aspx?d=3&title=<%=Server.UrlEncode("定期存款到期数据_三日数据") %>" class="layer" title="定期存款到期数据_三日数据">单数：<%=listDingqi_Days.Count %>笔</a>
                                            </div>
                                            <div>
                                                本金：<%=jine %>元
                                            </div>
                                            <div>
                                                利息：<%=lixi %>元
                                            </div>
                                            <div>
                                                合计：<%=jine+lixi %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #7CB5EC; height: 100%;">
                                            <%
                                                jine = lixi = 0;
                                                foreach (Model.Dingqi one in listDingqi_Week)
                                                {
                                                    jine += one.jine;
                                                    lixi += GetLixi(modelSetup, one);
                                                }  %>
                                            <p>
                                                一周数据
                                            </p>
                                            <div>
                                                <a href="tongji/dingqi_daoqi.aspx?d=7&title=<%=Server.UrlEncode("定期存款到期数据_一周数据") %>" class="layer" title="定期存款到期数据_一周数据">单数：<%=listDingqi_Week.Count %>笔</a>
                                            </div>
                                            <div>
                                                本金：<%=jine %>元
                                            </div>
                                            <div>
                                                利息：<%=lixi %>元
                                            </div>
                                            <div>
                                                合计：<%=jine+lixi %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #028DC7; height: 100%;">
                                            <%
                                                jine = lixi = 0;
                                                foreach (Model.Dingqi one in listDingqi_Month)
                                                {
                                                    jine += one.jine;
                                                    lixi += GetLixi(modelSetup, one);
                                                }  %>
                                            <p>
                                                一月数据
                                            </p>
                                            <div>
                                                <a href="tongji/dingqi_daoqi.aspx?d=30&title=<%=Server.UrlEncode("定期存款到期数据_一月数据") %>" class="layer" title="定期存款到期数据_一月数据">单数：<%=listDingqi_Month.Count %>笔</a>
                                            </div>
                                            <div>
                                                本金：<%=jine %>元
                                            </div>
                                            <div>
                                                利息：<%=lixi %>元
                                            </div>
                                            <div>
                                                合计：<%=jine+lixi %>元
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 60%" colspan="2">
                        <div class="charts" id="dinghuoqi_line">
                        </div>
                    </td>
                    <td>
                        <div class="charts tongji">
                            <div class="dingqiData">系统存取款数据</div>
                            <table>
                                <tr>
                                    <td>
                                        <div style="background-color: #F7A35C; height: 100%;">
                                            <%   
                                                List<Model.Huoqi> listHuoqiCun = (from m in listHuoqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                List<Model.Huoqi> listHuoqiQu = (from m in listHuoqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                List<Model.Dingqi> listDingqiCun = (from m in listDingqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                List<Model.Dingqi> listDingqiQu = (from m in listDingqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                decimal totalCun = (from m in listHuoqiCun select m.yuanjine).Sum() + (from m in listDingqiCun select m.jine).Sum();
                                                decimal totalQu = -(from m in listHuoqiQu select m.jine).Sum() - (from m in listDingqiQu select m.jine).Sum();
                                            %>
                                            <p>
                                                当日数据
                                            </p>
                                            <div>
                                                总存款：<%=totalCun %>元
                                            </div>
                                            <div>
                                                总取款：<%=totalQu %>元
                                            </div>
                                            <div>
                                                总利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum()+(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                            <div>
                                                净流入：<%=totalCun-totalQu %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #7CB5EC; height: 100%;">
                                            <%  
                                                listHuoqiCun = (from m in listHuoqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                listHuoqiQu = (from m in listHuoqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                listDingqiCun = (from m in listDingqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                listDingqiQu = (from m in listDingqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                totalCun = (from m in listHuoqiCun select m.yuanjine).Sum() + (from m in listDingqiCun select m.jine).Sum();
                                                totalQu = -(from m in listHuoqiQu select m.jine).Sum() - (from m in listDingqiQu select m.jine).Sum();
                                            %>
                                            <p>
                                                昨日数据
                                            </p>
                                            <div>
                                                总存款：<%=totalCun %>元
                                            </div>
                                            <div>
                                                总取款：<%=totalQu %>元
                                            </div>
                                            <div>
                                                总利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum()+(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                            <div>
                                                净流入：<%=totalCun-totalQu %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #028DC7; height: 100%;">
                                            <%   
                                                listHuoqiCun = (from m in listHuoqi_Cunkuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                listHuoqiQu = (from m in listHuoqi_Qukuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                listDingqiCun = (from m in listDingqi_Cunkuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                listDingqiQu = (from m in listDingqi_Qukuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                totalCun = (from m in listHuoqiCun select m.yuanjine).Sum() + (from m in listDingqiCun select m.jine).Sum();
                                                totalQu = -(from m in listHuoqiQu select m.jine).Sum() - (from m in listDingqiQu select m.jine).Sum();
                                            %>
                                            <p>
                                                一周数据
                                            </p>
                                            <div>
                                                总存款：<%=totalCun %>元
                                            </div>
                                            <div>
                                                总取款：<%=totalQu %>元
                                            </div>
                                            <div>
                                                总利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum()+(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                            <div>
                                                净流入：<%=totalCun-totalQu %>元
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 60%" colspan="2">
                        <div class="charts" id="huoqi_line">
                        </div>
                    </td>
                    <td>
                        <div class="charts tongji">
                            <div class="dingqiData">零钱存取款数据</div>
                            <table>
                                <tr>
                                    <td>
                                        <div style="background-color: #F7A35C; height: 100%;">
                                            <%  
                                                listHuoqiCun = (from m in listHuoqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                listHuoqiQu = (from m in listHuoqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();  %>
                                            <p>
                                                当日数据
                                            </p>
                                            <div>
                                                单数：<%=listHuoqiCun.Count+listHuoqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listHuoqiCun select m.yuanjine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listHuoqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #7CB5EC; height: 100%;">
                                            <% 
                                                listHuoqiCun = (from m in listHuoqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                listHuoqiQu = (from m in listHuoqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();  %>
                                            <p>
                                                昨日数据
                                            </p>
                                            <div>
                                                单数：<%=listHuoqiCun.Count+listHuoqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listHuoqiCun select m.yuanjine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listHuoqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #028DC7; height: 100%;">
                                            <% 
                                                listHuoqiCun = (from m in listHuoqi_Cunkuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                listHuoqiQu = (from m in listHuoqi_Qukuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();   %>
                                            <p>
                                                一周数据
                                            </p>
                                            <div>
                                                单数：<%=listHuoqiCun.Count+listHuoqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listHuoqiCun select m.yuanjine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listHuoqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listHuoqiQu select m.zonglixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 60%" colspan="2">
                        <div class="charts" id="dingqi_line">
                        </div>
                    </td>
                    <td>
                        <div class="charts tongji">
                            <div class="dingqiData">定期存取款数据</div>
                            <table>
                                <tr>
                                    <td>
                                        <div style="background-color: #F7A35C; height: 100%;">
                                            <%   
                                                listDingqiCun = (from m in listDingqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();
                                                listDingqiQu = (from m in listDingqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == strDtNow select m).ToList();  %>
                                            <p>
                                                当日数据
                                            </p>
                                            <div>
                                                单数：<%=listDingqiCun.Count+listDingqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listDingqiCun select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listDingqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #7CB5EC; height: 100%;">
                                            <% 
                                                listDingqiCun = (from m in listDingqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();
                                                listDingqiQu = (from m in listDingqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == dtNow.AddDays(-1).ToString("yyyy-MM-dd") select m).ToList();  %>
                                            <p>
                                                昨日数据
                                            </p>
                                            <div>
                                                单数：<%=listDingqiCun.Count+listDingqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listDingqiCun select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listDingqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="background-color: #028DC7; height: 100%;">
                                            <% 
                                                listDingqiCun = (from m in listDingqi_Cunkuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();
                                                listDingqiQu = (from m in listDingqi_Qukuan where (Falcon.Function.ToDateTime(dtNow.ToString("yyyy-MM-dd")) - Falcon.Function.ToDateTime(Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd"))).Days <= 7 select m).ToList();   %>
                                            <p>
                                                一周数据
                                            </p>
                                            <div>
                                                单数：<%=listDingqiCun.Count+listDingqiQu.Count %>笔
                                            </div>
                                            <div>
                                                存款：<%=(from m in listDingqiCun select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                取款：<%=-(from m in listDingqiQu select m.jine).Sum() %>元
                                            </div>
                                            <div>
                                                利息：<%=(from m in listDingqiQu select m.lixi).Sum() %>元
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <%-- <div id="line_basic">
            </div>--%>
        </div>
    </form>
</body>
</html>
