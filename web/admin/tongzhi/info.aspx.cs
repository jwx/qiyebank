﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongzhi
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.Tongzhi model = new BLL.Tongzhi().GetModel(id);
                    if (model != null && model.id != 0)
                    {
                        txtTitle.Text = model.title;
                        txtContent.Text = model.content;
                        ckDisable.Checked = model.isEnable == 0;
                        txtSort.Text = model.sort.ToString();
                    }
                }
            }
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Tongzhi().Add(txtTitle.Text, txtContent.Text, ckDisable.Checked ? 0 : 1, Falcon.Function.ToInt(txtSort.Text, 100), SessionUid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Tongzhi().Update(id, txtTitle.Text, txtContent.Text, ckDisable.Checked ? 0 : 1, Falcon.Function.ToInt(txtSort.Text, 100), SessionUid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        } 
    }
}