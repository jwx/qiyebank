﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin
{
    public partial class welcome : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            dtNow = Falcon.Function.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));

            #region 系统余额
            money_huoqi = new BLL.Huoqi().GetSysYue();
            money_dingqi = new BLL.Dingqi().GetSysYue();
            decimal chushu = (money_huoqi + money_dingqi);
            if (chushu != 0)
            {
                rate_huoqi = Math.Round(money_huoqi / chushu, 4, MidpointRounding.AwayFromZero) * 100;
                rate_dingqi = 100 - rate_huoqi;
            }
            #endregion

            #region 系统已付利息
            List<Model.Huoqi> listHuoqiYifuLixi = new BLL.Huoqi().GetList_YifuLixi(); //new BLL.Huoqi().GetSysLixi_Payed();
            listHuoqiYifuLixi = listHuoqiYifuLixi == null ? new List<Model.Huoqi>() : listHuoqiYifuLixi;
        //    decimal sum1= (from m in listHuoqiYifuLixi where m.laiyuan == 101 select m.zonglixi).Sum();
            decimal sum2=(from m in listHuoqiYifuLixi where m.laiyuan == 1 select m.jine).Sum();
            lixi_huoqipayed = sum2;
            List<Model.Dingqi> listDingqiYifuLixi = new BLL.Dingqi().GetList_YifuLixi(); //new BLL.Dingqi().GetSysLixi_Payed();
            listDingqiYifuLixi = listDingqiYifuLixi == null ? new List<Model.Dingqi>() : listDingqiYifuLixi;
            lixi_dingqipayed = (from m in listDingqiYifuLixi select m.lixi).Sum();

            chushu = (lixi_huoqipayed + lixi_dingqipayed);
            if (chushu != 0)
            {
                rate_huoqipayedlixi = Math.Round(lixi_huoqipayed / chushu, 4, MidpointRounding.AwayFromZero) * 100;
                rate_dingqipayedlixi = 100 - rate_huoqipayedlixi;
            }
            #endregion

            #region 日存款变化折线图
            List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetListByWhere(" jine!=0 and cunqukuan=1 and delid=0");//系统中所有的有效的活期存款单据
            listHuoqi = listHuoqi == null ? new List<Model.Huoqi>() : listHuoqi;

            //系统中所有的定期存款单据
            List<Model.Dingqi> listDingqi = new BLL.Dingqi().GetListByWhere("cunqukuan=1 and delid=0");//cunqukuan=1 and hasQukuan=0 and delid=0
            listDingqi = listDingqi == null ? new List<Model.Dingqi>() : listDingqi;
            listX_Month = new List<string>();
            listHuoqiJine = new List<decimal>();
            listDingqiJine = new List<decimal>();
            listHejiJine = new List<decimal>();
            for (int i = 0; i < 30; i++)
            {
                DateTime targetTime = Falcon.Function.ToDateTime(dtNow.AddDays(-i).ToString("yyyy-MM-dd"));
                listX_Month.Add("'" + targetTime.ToString("MM-dd") + "'");
                decimal huoqi = (from m in listHuoqi where (targetTime - Falcon.Function.ToDateTime(m.cunkuanTime)).Days >= 0 select m.yuanjine).Sum();
                listHuoqiJine.Add(huoqi);
                decimal dingqi = (from m in listDingqi where (targetTime - Falcon.Function.ToDateTime(m.cunkuanTime)).Days >= 0 select m.jine).Sum();
                listDingqiJine.Add(dingqi);
                listHejiJine.Add(huoqi + dingqi);
            }

            listX_Month.Reverse();
            listHuoqiJine.Reverse();
            listDingqiJine.Reverse();
            listHejiJine.Reverse();
            #endregion

            #region 定期存款到期数据

            listDingqi_Days = (from m in listDingqi where m.hasQukuan == 0 && (Falcon.Function.ToDateTime(m.daoqiTime) - dtNow).Days <= 3 select m).ToList();
            listDingqi_Week = (from m in listDingqi where m.hasQukuan == 0 && (Falcon.Function.ToDateTime(m.daoqiTime) - dtNow).Days <= 7 select m).ToList();
            listDingqi_Month = (from m in listDingqi where m.hasQukuan == 0 && (Falcon.Function.ToDateTime(m.daoqiTime) - dtNow).Days <= 30 select m).ToList();

            modelSetup = listDingqi != null && listDingqi.Count != 0 ? new BLL.Setup().Get_Setup() : new Model.Setup();
            modelSetup = modelSetup == null ? new Model.Setup() : modelSetup;
            #endregion

            #region 零钱存取款日统计表
            listHuoqi_Cunkuan = (from m in listHuoqi where (dtNow - Falcon.Function.ToDateTime(m.cunkuanTime)).Days <= 15 select m).ToList();//系统中半个月前的活期存款数据
            listHuoqi_Qukuan = new BLL.Huoqi().GetListByWhere(" jine!=0 and cunqukuan=2 and delid=0 and addTime>='" + dtNow.AddDays(-15).ToString("yyyy-MM-dd") + "'");//系统中半个月前的活期取款数据
            listHuoqi_Qukuan = listHuoqi_Qukuan == null ? new List<Model.Huoqi>() : listHuoqi_Qukuan;
            listX_HalfMonth = new List<string>();
            listHuoqiJine_Cun = new List<decimal>();
            listHuoqiJine_Qu = new List<decimal>();
            for (int i = 0; i < 15; i++)
            {
                DateTime targetTime = dtNow.AddDays(-i);
                listX_HalfMonth.Add("'" + targetTime.ToString("MM-dd") + "'");
                listHuoqiJine_Cun.Add((from m in listHuoqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == targetTime.ToString("yyyy-MM-dd") select m.jine).Sum());
                listHuoqiJine_Qu.Add(-(from m in listHuoqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == targetTime.ToString("yyyy-MM-dd") select m.jine).Sum());
            }
            listX_HalfMonth.Reverse();
            listHuoqiJine_Cun.Reverse();
            listHuoqiJine_Qu.Reverse();
            #endregion

            #region 定期存取款日统计表
            listDingqi_Cunkuan = (from m in listDingqi where (dtNow - Falcon.Function.ToDateTime(m.cunkuanTime)).Days <= 15 select m).ToList();//系统中半个月前的活期存款数据
            listDingqi_Qukuan = new BLL.Dingqi().GetListByWhere(" jine!=0 and cunqukuan=2 and hasQukuan=1 and delid=0 and addTime>='" + dtNow.AddDays(-15).ToString("yyyy-MM-dd") + "'");//系统中半个月前的活期取款数据
            listDingqi_Cunkuan = listDingqi_Cunkuan == null ? new List<Model.Dingqi>() : listDingqi_Cunkuan;
            listDingqiJine_Cun = new List<decimal>();
            listDingqiJine_Qu = new List<decimal>();
            for (int i = 0; i < 15; i++)
            {
                DateTime targetTime = dtNow.AddDays(-i);
                listDingqiJine_Cun.Add((from m in listDingqi_Cunkuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == targetTime.ToString("yyyy-MM-dd") select m.jine).Sum());
                listDingqiJine_Qu.Add(-(from m in listDingqi_Qukuan where Falcon.Function.ToDateTime(m.cunkuanTime).ToString("yyyy-MM-dd") == targetTime.ToString("yyyy-MM-dd") select m.jine).Sum());
            }
            listDingqiJine_Cun.Reverse();
            listDingqiJine_Qu.Reverse();

            //系统存款日统计表 
            listTotalJine_Cun = new List<decimal>();
            listTotalJine_Qu = new List<decimal>();
            listTotalJine = new List<decimal>();
            for (int i = 0; i < listDingqiJine_Cun.Count; i++)
            {
                listTotalJine_Cun.Add(listHuoqiJine_Cun[i] + listDingqiJine_Cun[i]);
                listTotalJine_Qu.Add(listHuoqiJine_Qu[i] + listDingqiJine_Qu[i]);
                listTotalJine.Add(listTotalJine_Cun[i] - listTotalJine_Qu[i]);
            }

            #endregion
        }

        protected DateTime dtNow;

        protected decimal money_huoqi;//系统活期剩余总金额 
        protected decimal money_dingqi;//系统定期剩余总金额 
        protected decimal rate_huoqi;
        protected decimal rate_dingqi;

        protected decimal lixi_huoqipayed;//系统活期已付总利息 
        protected decimal lixi_dingqipayed;//系统定期已付总利息
        protected decimal rate_huoqipayedlixi;
        protected decimal rate_dingqipayedlixi;

        protected List<string> listX_Month;
        protected List<decimal> listHuoqiJine;
        protected List<decimal> listDingqiJine;
        protected List<decimal> listHejiJine;
        //protected decimal lixi_huoqiyuji;//系统中活期利息预计
        //protected decimal lixi_dingqiyuji;//系统中定期利息预计
        //protected decimal rate_huoqiyujilixi;
        //protected decimal rate_dingqiyujilixi;

        protected List<Model.Dingqi> listDingqi_Days;
        protected List<Model.Dingqi> listDingqi_Week;
        protected List<Model.Dingqi> listDingqi_Month;


        protected List<decimal> listTotalJine_Cun;
        protected List<decimal> listTotalJine_Qu;
        protected List<decimal> listTotalJine;


        protected List<string> listX_HalfMonth;
        protected List<decimal> listHuoqiJine_Cun;
        protected List<decimal> listHuoqiJine_Qu;
        protected List<Model.Huoqi> listHuoqi_Cunkuan;
        protected List<Model.Huoqi> listHuoqi_Qukuan;

        protected List<decimal> listDingqiJine_Cun;
        protected List<decimal> listDingqiJine_Qu;
        protected List<Model.Dingqi> listDingqi_Cunkuan;
        protected List<Model.Dingqi> listDingqi_Qukuan;


        protected Model.Setup modelSetup;


        protected decimal GetLixi(Model.Setup modelSetup, Model.Dingqi model)
        {
            int lilvJishu = modelSetup.dq_byDay == 1 ? 365 : 12;//年利率基数，按天还是按月  
            //DateTime dtNow = DateTime.Now;
            DateTime cunkuanTime = Falcon.Function.ToDateTime(model.cunkuanTime);//存款周期
            DateTime daoqiTime = Falcon.Function.ToDateTime(model.daoqiTime);
            //decimal lilv = (dtNow - Falcon.Function.ToDateTime(model.daoqiTime)).Days >= 0 ? model.lilv : model.budaoqi;//正常利率和不到期利率

            int cunkuanZhouqi = modelSetup.dq_byDay == 1 ? (daoqiTime - cunkuanTime).Days : new Tool.TimeHelper().GetFullMonth(daoqiTime, cunkuanTime);//实际存款天数或月数
            return Math.Round(model.jine * cunkuanZhouqi * model.lilv / 100 / lilvJishu, 2, MidpointRounding.AwayFromZero);
        }
    }
}