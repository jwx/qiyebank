﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="import.aspx.cs" Inherits="web.admin.users.import" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" />
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>导入模板： 
                    </td>
                    <td>
                        <a class="red" href="/template/账号批量导入模板.xlsx">导入模板下载，请严格按照导入模板格式导入数据。</a>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>数据导入： 
                    </td>
                    <td>
                        <asp:FileUpload ID="fileUpload" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="导入" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
