﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qyinfo.aspx.cs" Inherits="web.admin.users.qyinfo" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet" />
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <script src="/Plug/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor = K.editor({
                allowFileManager: true,
                imageSizeLimit: '2MB', //批量上传图片单张最大容量
                imageUploadLimit: 20, //批量上传图片同时上传最多个数
                uploadJson: '/Ajax/upload_json.ashx?type=1&size=2'
            });

            K('#upphotos').click(function () {
                var t = $(this);
                editor.loadPlugin('multiimage', function () {
                    editor.plugin.multiImageDialog({
                        clickFn: function (urlList) {
                            var div = K('#photos');
                            K.each(urlList, function (i, data) {
                                div.append('<div class="item"><span class="del">X</span><a href="pics.aspx?pic=' + data.url + '" class="layer" title="照片"><img src="' + data.url + '" /></a><input type="hidden" name="hidePhoto" value="' + data.url + '" /></div>');
                            });
                            editor.hideDialog();
                        }
                    });
                });
            });
        });


        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });

            $(document).on("click", ".del", function () {
                layer.confirm("确实要删除当前照片吗？", function () {
                    var img = $(this).parent().find("img").attr("src");
                    ajax.DeleteImg(img).value;
                    $(this).parents(".item").remove();
                    layer.closeAll();
                });
            });

            //$("#txtBankCard").blur(function () {
            //    var bankcard = $.trim($(this).val());
            //    if (bankcard) {
            //        $("#ddlBank option").not(":first").each(function () {
            //            var startNo = $(this).attr("value");
            //            if (bankcard.indexOf(startNo) == 0) {//匹配到开始 
            //                $(this).attr("selected", "selected");
            //                return;//跳出循环
            //            } else {
            //                $(this).removeAttr("selected");
            //            }
            //        });
            //    }
            //});
        });
    </script>
    <style type="text/css">
        .item { float: left; width: 200px; height: 150px; margin: 8px; position: relative; }

            .item .del { position: absolute; top: -6px; right: -5px; cursor: pointer; font-weight: bold; font-size: 16px; }

                .item .del:hover { color: red; }

            .item a img { width: 200px; height: 150px; border: 0px; }
    </style>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo1">
                <tr>
                    <td class="tdl"><span class="required">*</span>账号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" datatype="n5-15" ajaxurl="/Ajax/checkNameRepeat.ashx?t=1" placeholder="请输入登录账号"></asp:TextBox>
                        <span class="Validform_checktip">请输入账号，长度为5-15位的数字</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>公司名称： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRealName" runat="server" datatype="*" placeholder="请输入公司名称"></asp:TextBox>
                        <span class="Validform_checktip">请输入公司名称</span>
                    </td>
                </tr>

                <tr>
                    <td class="tdl"><span class="required">*</span>登录密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd" runat="server" datatype="*5-15" TextMode="Password" placeholder="请输入登录密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入登录密码，长度为5-15位字符</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>确认登录密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd1" runat="server" datatype="*5-15" recheck="txtUserPswd" TextMode="Password" placeholder="请确认登录密码" errormsg="两次密码不一致"></asp:TextBox>
                        <span class="Validform_checktip">请确认登录密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>支付密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtPayPswd" runat="server" datatype="*6-6" TextMode="Password" placeholder="请输入支付密码" errormsg="请输入支付密码，长度为6位数字"></asp:TextBox>
                        <span class="Validform_checktip">请输入支付密码，长度为6位的数字</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>支付密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtPayPswd1" runat="server" datatype="*6-6" recheck="txtPayPswd" TextMode="Password" placeholder="请输入支付密码" nullmsg="请输入支付密码" errormsg="两次密码不一致"></asp:TextBox>
                        <span class="Validform_checktip">请确认支付密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="red">*</span>开户行： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBank" runat="server" datatype="*"></asp:DropDownList>
                        <span class="Validform_checktip">请选择开户行</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="red">*</span>户名： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtBankUName" runat="server" placeholder="请输入户名" datatype="*"></asp:TextBox>
                        <span class="Validform_checktip">请输入户名</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="red">*</span>银行卡号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtBankCard" runat="server" placeholder="请输入银行卡号" datatype="*"></asp:TextBox>
                        <span class="Validform_checktip">请输入银行卡号</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="red">*</span>手机号码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobile" runat="server" datatype="m|/[\d]{7}/" datetype="*" placeholder="请输入手机号码"></asp:TextBox>
                        <span class="Validform_checktip">请输入手机号码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">支付宝账号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtZhifubao" runat="server" placeholder="请输入支付宝账号"></asp:TextBox>
                        <span class="Validform_checktip">请输入支付宝账号</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">社会统一信用代码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtIdcard" runat="server" datatype="*" ignore="ignore" placeholder="请输入社会统一信用代码"></asp:TextBox>
                        <span class="Validform_checktip">请输入社会统一信用代码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">电子邮箱： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" datatype="e" ignore="ignore" placeholder="请输入电子邮箱" errormsg="请正确输入电子邮箱"></asp:TextBox>
                        <span class="Validform_checktip">请输入电子邮箱</span>
                    </td>
                </tr>
                <tr style="display: none">
                    <td class="tdl">性别： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGender" runat="server">
                            <asp:ListItem Value="-1">保密</asp:ListItem>
                            <asp:ListItem Value="1">男</asp:ListItem>
                            <asp:ListItem Value="0">女</asp:ListItem>
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择性别</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">座机号码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtTel" runat="server" dplaceholder="请输入座机号码"></asp:TextBox>
                        <span class="Validform_checktip">请输入座机号码</span>
                    </td>
                </tr>
                <%--   <tr>
                    <td class="tdl">手机号码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobile" runat="server" datatype="m|/[\d]{7}/" ignore="ignore" placeholder="请输入手机号码"></asp:TextBox>
                        <span class="Validform_checktip">请输入手机号码</span>
                    </td>
                </tr>--%>
                <tr>
                    <td class="tdl">公司地址： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress" runat="server" placeholder="请输入公司地址"></asp:TextBox>
                        <span class="Validform_checktip">请输入公司地址</span>
                    </td>
                </tr>

                <tr>
                    <td class="tdl">公司类型： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGSTYPENAme" runat="server">
                            <asp:ListItem Value="0">有限责任公司</asp:ListItem>
                            <asp:ListItem Value="1">股份有限公司</asp:ListItem>
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择公司类型</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">法定代表人： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtFDDBR" runat="server" placeholder="请输入法定代表人"></asp:TextBox>
                        <span class="Validform_checktip">请输入法定代表人</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">注册资本： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtZCZB" runat="server" placeholder="请输入注册资本"></asp:TextBox>
                        <span class="Validform_checktip">请输入注册资本</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">成立时间： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtCLTime" runat="server" placeholder="请输入成立时间"></asp:TextBox>
                        <span class="Validform_checktip">请输入成立时间</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">营业期限： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtYYQX" runat="server" placeholder="请输入营业期限"></asp:TextBox>
                        <span class="Validform_checktip">请输入营业期限</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">经营范围： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtJYFW" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">公司分类： 
                    </td>
                    <td>
                        <asp:DropDownList ID="dllGSFl" runat="server">
                            <asp:ListItem Value="0">直属单位</asp:ListItem>
                            <asp:ListItem Value="1">联合单位</asp:ListItem>
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择公司分类</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">照片（可多张）： 
                    </td>
                    <td>
                        <input type="button" id="upphotos" class="photo" value="选择照片" />
                        <div id="photos">
                            <% if (!string.IsNullOrEmpty(photo))
                                {
                                    string[] arrayPhoto = photo.Split(',');
                                    foreach (string one in arrayPhoto)
                                    {
                                        if (!string.IsNullOrEmpty(one))
                                        {%>
                            <div class="item">
                                <span class="del">X</span>
                                <a href="pics.aspx?pic=<%=one %>" class="layer" title="照片">
                                    <img src="<%=one %>" />
                                </a>
                                <input type="hidden" name="hidePhoto" value="<%=one %>" />
                            </div>
                            <%}
                                    }
                                } %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
