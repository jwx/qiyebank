﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detail.aspx.cs" Inherits="web.admin.users.detail" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <style type="text/css">
        .item { float: left; width: 200px; height: 150px; margin: 8px; position: relative; }
            .item .del { position: absolute; top: -6px; right: -5px; cursor: pointer; font-weight: bold; font-size: 16px; }
                .item .del:hover { color: red; }
            .item a img { width: 200px; height: 150px; border: 0px; }
    </style>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <% if (model != null && model.id != 0)
               { %>
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 10%"><span class="required">*</span>账号： 
                    </td>
                    <td style="width: 40%">
                        <%=model.username %>
                    </td>
                    <td class="tdl" style="width: 10%;"><span class="required">*</span>户名： 
                    </td>
                    <td>
                        <%=model.realname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">身份证号： 
                    </td>
                    <td>
                        <%=model.idcard %>
                    </td>
                    <td class="tdl">电子邮箱： 
                    </td>
                    <td>
                        <%=model.email %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">性别： 
                    </td>
                    <td>
                        <%=(Config.Enums.Gender)model.gender %>
                    </td>
                    <td class="tdl">座机号码： 
                    </td>
                    <td>
                        <%=model.tel %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">手机号码： 
                    </td>
                    <td>
                        <%=model.mobile %>
                    </td>
                    <td class="tdl">居住地址： 
                    </td>
                    <td>
                        <%=model.address %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">银行卡号： 
                    </td>
                    <td>
                        <%=model.bankcard %>
                    </td>
                    <td class="tdl">户名： 
                    </td>
                    <td>
                        <%=model.bank_uname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">开户行： 
                    </td>
                    <td>
                        <%=model.bankname %>
                    </td>
                    <td class="tdl">支付宝账号： 
                    </td>
                    <td>
                        <%=model.zhifubao %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">照片： 
                    </td>
                    <td colspan="3">
                        <div id="photos">
                            <% if (!string.IsNullOrEmpty(model.photo))
                               {
                                   string[] arrayPhoto = model.photo.Split(',');
                                   foreach (string one in arrayPhoto)
                                   {
                                       if (!string.IsNullOrEmpty(one))
                                       {%>
                            <div class="item">
                                <a href="pics.aspx?pic=<%=one %>" class="layer" title="照片">
                                    <img src="<%=one %>" />
                                </a>
                            </div>
                            <%}
                                   }
                               } %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td colspan="3">
                        <%=model.remark %>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="3">
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </form>
</body>
</html>
