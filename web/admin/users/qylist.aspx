﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qylist.aspx.cs" Inherits="web.admin.users.qylist" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a class="btn btnBlue layer <%=getPower("添加") %>" href="qyinfo.aspx" title="添加账号">+ 添加</a>
                    <%--      <a class="btn btnGreen <%=getPower("批量导入") %> layer" href="import.aspx">批量导入</a>--%>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名</asp:ListItem>
                        <asp:ListItem Value="FDDBR">法定代表人</asp:ListItem>
                        <asp:ListItem Value="mobile">联系手机</asp:ListItem>
                        <asp:ListItem Value="GSDZ">公司地址地址</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                        <asp:ListItem Value="GSMC">公司名称</asp:ListItem>
                        <asp:ListItem Value="TYSHXYDM">统一社会信用代码</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">身份证号：</td>
                            <td>
                                <asp:TextBox ID="txtIdCard" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">电子邮箱：</td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">性别：</td>
                            <td>
                                <asp:DropDownList ID="ddlGender" runat="server">
                                    <asp:ListItem Value="-1">请选择性别</asp:ListItem>
                                    <asp:ListItem Value="1">男</asp:ListItem>
                                    <asp:ListItem Value="0">女</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdl">座机号码：</td>
                            <td>
                                <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">地址：</td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">开户行：</td>
                            <td>
                                <asp:TextBox ID="txtBankName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtBank_UName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">银行卡号：</td>
                            <td>
                                <asp:TextBox ID="txtBankCard" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">支付宝：</td>
                            <td>
                                <asp:TextBox ID="txtZhifubao" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">账户状态：</td>
                            <td>
                                <asp:DropDownList ID="ddlIsDel" runat="server">
                                    <asp:ListItem Value="-1">请选择账户状态</asp:ListItem>
                                    <asp:ListItem Value="0">正常</asp:ListItem>
                                    <asp:ListItem Value="1">已销户</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">开户时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>账号</td>
                           <td>户名/公司名称</td>
                            <td>统一社会信用代码</td>
                            <td>公司名称</td>
                            <td>法定代表人</td>
                            <td>注册资本</td>
                            <td>成立日期</td>
                            <td>营业期限</td>
                            <td>经办人</td>
                            <td>开户时间</td>
                            <td>账户状态</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                            {
                                int i = 0;
                                string update = getPower("修改");
                                string uppswd = getPower("登录密码修改");
                                string gspswd = getPower("登录密码挂失");
                                string uppswd_pay = getPower("支付密码修改");
                                string gspswd_pay = getPower("支付密码挂失");
                                string zhgs = getPower("账号挂失");
                                string xiaohu = getPower("销户");
                                string records = getPower("操作记录");
                                foreach (Model.UserInfo one in listRecord)
                                {
                                    i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="qyinfo.aspx?id=<%=one.id %>" onclick="return false;" class="label label-primary layer <%=update %>" title="用户信息修改_<%=one.realname %>">修改</a>
                                <a href="uppswd.aspx?id=<%=one.id %>" onclick="return false;" class="label label-info layer <%=uppswd %>" title="登录密码修改_<%=one.realname %>">登录密码修改</a>
                                <a href="gs_pswd.aspx?id=<%=one.id %>" onclick="return false;" class="label label-info layer <%=gspswd %>" title="登录密码挂失_<%=one.realname %>">登录密码挂失</a>
                                <a href="uppswd_pay.aspx?id=<%=one.id %>" onclick="return false;" class="label label-warning layer <%=gspswd_pay %>" title="支付密码挂失_<%=one.realname %>">支付密码修改</a>
                                <a href="gs_pswd_pay.aspx?id=<%=one.id %>" onclick="return false;" class="label label-warning layer <%=gspswd_pay %>" title="支付密码挂失_<%=one.realname %>">支付密码挂失</a>
                                <a href="gs_account.aspx?id=<%=one.id %>&account=<%=one.username %>" onclick="return false;" class="label label-default layer <%=zhgs %>" title="用户账号挂失_<%=one.realname %>">账号挂失</a>
                                <% if (one.isDel == 0)
                                    { %>
                                <a href="xiaohu.aspx?id=<%=one.id %>&account=<%=Server.UrlEncode(one.realname) %>" onclick="return false;" class="label label-danger layer <%=xiaohu %>" title="用户账号销户_<%=one.realname %>">销户</a>
                                <%} %>
                                <a href="records.aspx?id=<%=one.id %>&account=<%=Server.UrlEncode(one.realname) %>" onclick="return false;" class="label label-success layer <%=records %>" title="用户账号操作记录_<%=one.realname %>">操作记录</a>
                            </td>
                            <td>
                                <%=one.username %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.TYSHXYDM %>
                            </td>
                            <td>
                                <%=one.GSMC %>
                            </td>
                            <td>
                                <%=one.FDDBR %>
                            </td>
                            <td>
                                <%=one.ZCZB %>
                            </td>
                            <td>
                                <%=one.CLTIME %>
                            </td>
                            <td>
                                <%=one.YYQX %>
                            </td>
                            <td>
                                <%=one.jbname %>
                            </td>
                            <td>
                                <%=one.addTime.ToString("yyyy-MM-dd HH:mm:ss") %>
                            </td>
                            <td class="text_center">
                                <%=one.isDel==0?"正常":"已销户" %>
                            </td>
                        </tr>
                        <%}
                            }
                            else
                            { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
