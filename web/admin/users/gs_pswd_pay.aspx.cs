﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.users
{
    public partial class gs_pswd_pay : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Falcon.Function.GetQueryInt("id");
            if (new BLL.UserInfo().UpdatePswd_Pay(id, txtUserPswd.Text.Trim()))
            {
                new BLL.UserLogs().Add(id, SessionUid, "支付密码挂失");
                //if (new BLL.Duanxin().GetModel().mimaxiugai == 1)
                //{
                //    Model.UserInfo model = new BLL.UserInfo().GetModel(id);
                //    //SubmitResult sr = new sms().Submit(Config.Config.account, Config.Config.password, model.mobile, Config.Config.msg + model.realname + "您好，您于" + DateTime.Now.ToString("yyyy-MM-dd") + "修改了登录密码。如非被人操作，请立即联系" + Config.Config.company + "的工作人员。您可录手机银行：" + Falcon.UrlPath.GetRootPath() + "/mobile/login.aspx" + " 查询个人信息。");

                //    if (model.mobile != "" && model.mobile.Length == 11)
                //    {
                //        new UCSRestRequest.InitUCSRestRequest().SendSMS(model.mobile, UCSRestRequest.InitUCSRestRequest.mimaxiugai, model.realname + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                //    }
                //}
                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    string msg = string.Empty;
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + id }, "支付密码挂失通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "挂失了支付密码", new Dictionary<string, string>(), ref msg);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功，请牢记新密码！', {icon:6,time:1500},function(){ parent.layer.closeAll()})", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败，请重试！', {icon:5,time:1500})", true);
            }
        }
    }
}