﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.users
{
    public partial class import : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                String fileName = fileUpload.FileName;
                String fileExt = Path.GetExtension(fileName).ToLower();
                if (fileExt != ".xls" && fileExt != ".xlsx")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('只允许上传.xls和.xlsx格式的文件！');", true);
                }
                else
                {
                    String dirPath = Server.MapPath("/uploadFile/Excel/");
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    HttpPostedFile imgFile = Request.Files[0];
                    String newFileName = (DateTime.Now.ToString("yyyyMMddHHmmss_ffff") + fileExt);
                    String filePath = dirPath + fileName + newFileName;
                    imgFile.SaveAs(filePath);

                    List<System.Data.DataTable> listDt = Tool.ExcelHelper.ExcelToDataTable(filePath, 0);
                    if (listDt != null && listDt.Count != 0)
                    {
                        System.Data.DataTable dt = null;
                        foreach (System.Data.DataTable one in listDt)
                        {
                            if (one != null && one.Rows.Count != 0)
                            {
                                dt = one;
                                break;
                            }
                        }

                        if (dt != null)
                        {
                            string msg = string.Empty;
                            if (new BLL.UserInfo().Import(dt, SessionUid, ref msg))
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                            }
                            else
                            {
                                Falcon.FileOP.DeleteFile(filePath);
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('" + msg + "！');", true);
                            }
                        }
                        else
                        {
                            Falcon.FileOP.DeleteFile(filePath);
                            ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('Excel中没有要导入的数据！');", true);
                        }
                    }
                    else
                    {
                        Falcon.FileOP.DeleteFile(filePath);
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('Excel中没有数据！');", true);
                    }
                }
            }
        }
    }
}