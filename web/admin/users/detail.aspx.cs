﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.users
{
    public partial class detail : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            int id = Falcon.Function.GetQueryInt("id", 0);

            if (id != 0)
            {
                model = new BLL.UserInfo().GetModel(id);
            }
        }

        protected Model.UserInfo model;

    }
}