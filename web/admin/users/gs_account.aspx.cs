﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.users
{
    public partial class gs_account : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            if (!IsPostBack)
            {
                lblUserName.Text = Falcon.Function.GetQueryString("account");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Falcon.Function.GetQueryInt("id");
            if (new BLL.UserInfo().Update_GusShi(id, lblUserName.Text, txtUserName.Text.Trim()))
            {
                new BLL.UserLogs().Add(id, SessionUid, "账号挂失，从原始账号：" + lblUserName.Text + "变更为：" + txtUserName.Text.Trim());

                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    string msg = string.Empty;
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + id }, "账号挂失通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "挂失了账号", new Dictionary<string, string>(), ref msg);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！', {icon:6,time:1500},function(){ parent.location.reload();parent.layer.closeAll();})", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败，请重试！', {icon:5,time:1500})", true);
            }
        }
    }
}