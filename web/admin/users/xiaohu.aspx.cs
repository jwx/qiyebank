﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.users
{
    public partial class xiaohu : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                int id = Falcon.Function.GetQueryInt("id");
                lblUserName.Text = Server.UrlDecode(Falcon.Function.GetQueryString("account"));
                decimal huoqi = new BLL.Huoqi().GetYue(id);
                lblHuoqi.Text = huoqi + "元";
                decimal dingqi = new BLL.Dingqi().GetYue(id);
                lblDingqi.Text = dingqi + "元";

                if (huoqi != 0 || dingqi != 0)
                {
                    btnSave.CssClass += " btnRed disabled";
                    btnSave.Enabled = false;
                    btnSave.Text = "账户有余额，无法销户";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Falcon.Function.GetQueryInt("id");
            if (new BLL.UserInfo().Delete(id))
            {
                new BLL.UserLogs().Add(id, SessionUid, "销户");
                //if (new BLL.Duanxin().GetModel().mimaxiugai == 1)
                //{
                //    Model.UserInfo model = new BLL.UserInfo().GetModel(id);
                //    //SubmitResult sr = new sms().Submit(Config.Config.account, Config.Config.password, model.mobile, Config.Config.msg + model.realname + "您好，您于" + DateTime.Now.ToString("yyyy-MM-dd") + "修改了登录密码。如非被人操作，请立即联系" + Config.Config.company + "的工作人员。您可录手机银行：" + Falcon.UrlPath.GetRootPath() + "/mobile/login.aspx" + " 查询个人信息。");

                //    if (model.mobile != "" && model.mobile.Length == 11)
                //    {
                //        new UCSRestRequest.InitUCSRestRequest().SendSMS(model.mobile, UCSRestRequest.InitUCSRestRequest.mimaxiugai, model.realname + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                //    }
                //}
                if (!string.IsNullOrEmpty(Config.Config.AppName))
                {
                    string msg = string.Empty;
                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + id }, "销户通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "注销了账户", new Dictionary<string, string>(), ref msg);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！', {icon:6,time:1500},function(){ parent.layer.closeAll()})", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败，请重试！', {icon:5,time:1500})", true);
            }
        }
    }
}