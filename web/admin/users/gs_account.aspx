﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gs_account.aspx.cs" Inherits="web.admin.users.gs_account" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () { 
            $("#txtUserName").focus();
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">原账号： 
                    </td>
                    <td>
                        <asp:Label ID="lblUserName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>账号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" datatype="n5-15" ajaxurl="/Ajax/checkNameRepeat.ashx?t=1" placeholder="请输入登录账号"></asp:TextBox>
                        <span class="Validform_checktip">请输入账号，长度为5-15位的数字</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue noloading" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
