﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.print
{
    public partial class dqc_design : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                model = new BLL.SetupPrint().GetModel(Config.Enums.Print_Type.定期存款);
                model = model == null ? new Model.SetupPrint() : model;
                txtDefCode.Text = model.defCode;
                hideSysCode.Value = model.sysCode;
            }
        }

        protected Model.SetupPrint model;

        protected void btnSysCode_Click(object sender, EventArgs e)
        {
            string sysCode = "LODOP.PRINT_INITA(\"0mm\",\"0mm\",\"24.101cm\",\"9.3cm\",\"打印控件功能演示\");\r\nLODOP.SET_PRINT_PAGESIZE(1,2410,930,\"打印控件功能演示\");\r\nLODOP.SET_PRINT_MODE(\"PRINT_PAGE_PERCENT\",\"100%\");\r\nLODOP.ADD_PRINT_RECT(5,5,900,340,0,1);\r\nLODOP.ADD_PRINT_BARCODEA(\"danhao\",20,20,190,45,\"Code39\",\"[danhao]\");\r\nLODOP.ADD_PRINT_LINE(76,5,75,905,0,1);\r\nLODOP.ADD_PRINT_LINE(116,5,115,905,0,1);\r\nLODOP.ADD_PRINT_LINE(156,5,155,571,0,1);\r\nLODOP.ADD_PRINT_LINE(195,5,196,571,0,1);\r\nLODOP.ADD_PRINT_LINE(235,5,236,571,0,1);\r\nLODOP.ADD_PRINT_LINE(275,5,276,571,0,1);\r\nLODOP.ADD_PRINT_LINE(316,5,315,905,0,1);\r\nLODOP.ADD_PRINT_TEXT(326,5,899,20,\"1.白联:存根  2.红联:上缴  3.黄联:客户\");\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.ADD_PRINT_TEXT(90,7,75,20,\"户名：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,83,75,84,0,1);\r\nLODOP.ADD_PRINT_TEXT(32,221,610,36,\"[title]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",15);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.SET_PRINT_STYLEA(0,\"Bold\",1);\r\nLODOP.ADD_PRINT_TEXT(130,7,75,20,\"存款周期：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(90,88,185,20,\"[realname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(130,88,183,20,\"[zhouqi]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(195,281,75,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,285,85,20,\"账号：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(195,371,75,372,0,1);\r\nLODOP.ADD_PRINT_TEXT(210,7,75,20,\"金额大写：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(90,377,185,20,\"[username]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(210,88,477,20,\"[chs_money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(315,571,75,572,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,576,75,20,\"存款时间：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,652,75,653,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,657,185,20,\"[cunkuanTime]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(170,8,75,20,\"存款金额：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(170,88,183,20,\"[money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(250,6,75,20,\"存款说明：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(250,88,477,20,\"[add_remark]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(290,7,75,20,\"经办人：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(290,88,183,20,\"[jbname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(130,285,85,20,\"到期时间：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(130,377,185,20,\"[daoqiTime]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(315,281,275,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(290,285,75,20,\"客户签字：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,371,275,372,0,1);\r\nLODOP.ADD_PRINT_TEXT(119,599,27,190,\"\\r\\n\\r\\n\\r\\n业务专用章\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.ADD_PRINT_TEXT(170,275,95,20,\"到期年利率：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(170,377,185,20,\"[nianlilv]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);";
            string jsCode = sysCode.Replace("\"[", "model.").Replace("]\"", "");//"[xxx]"→model.xxx
            if (new BLL.SetupPrint().Update_Reset(Config.Enums.Print_Type.定期存款, sysCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        protected void btnDefCode_Click(object sender, EventArgs e)
        {
            string defCode = txtDefCode.Text;
            string jsCode = defCode.Replace("\"[", "model.").Replace("]\"", "");//"[xxx]"→model.xxx
            if (new BLL.SetupPrint().Update(Config.Enums.Print_Type.定期存款, defCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}