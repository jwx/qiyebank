﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.print
{
    public partial class dqq_design : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                model = new BLL.SetupPrint().GetModel(Config.Enums.Print_Type.定期取款);
                model = model == null ? new Model.SetupPrint() : model;
                txtDefCode.Text = model.defCode;
                hideSysCode.Value = model.sysCode;
            }
        }

        protected Model.SetupPrint model;

        protected void btnSysCode_Click(object sender, EventArgs e)
        {
            string sysCode = "LODOP.PRINT_INITA(\"0mm\",\"0mm\",\"24.101cm\",\"9.3cm\",\"打印控件功能演示\");\r\nLODOP.SET_PRINT_PAGESIZE(1,2410,930,\"打印控件功能演示\");\r\nLODOP.SET_PRINT_MODE(\"PRINT_PAGE_PERCENT\",\"100%\");\r\nLODOP.ADD_PRINT_RECT(5,5,900,340,0,1);\r\nLODOP.ADD_PRINT_BARCODEA(\"danhao\",10,20,190,45,\"Code39\",\"[danhao]\");\r\nLODOP.ADD_PRINT_LINE(66,5,65,905,0,1);\r\nLODOP.ADD_PRINT_LINE(102,5,101,905,0,1);\r\nLODOP.ADD_PRINT_LINE(138,5,137,571,0,1);\r\nLODOP.ADD_PRINT_LINE(173,5,174,571,0,1);\r\nLODOP.ADD_PRINT_LINE(281,5,282,571,0,1);\r\nLODOP.ADD_PRINT_LINE(318,5,317,905,0,1);\r\nLODOP.ADD_PRINT_TEXT(328,5,899,20,\"1.白联:存根  2.红联:上缴  3.黄联:客户\");\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.ADD_PRINT_TEXT(78,7,75,20,\"户名：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(317,83,65,84,0,1);\r\nLODOP.ADD_PRINT_TEXT(22,221,610,36,\"[title]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",15);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.SET_PRINT_STYLEA(0,\"Bold\",1);\r\nLODOP.ADD_PRINT_TEXT(114,7,75,20,\"存款周期：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(78,88,185,20,\"[realname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(114,88,183,20,\"[zhouqi]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(173,281,65,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(78,285,85,20,\"账号：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(173,371,65,372,0,1);\r\nLODOP.ADD_PRINT_TEXT(186,7,75,20,\"金额大写：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(78,377,185,20,\"[username]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(186,88,477,20,\"[chs_money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(317,571,65,572,0,1);\r\nLODOP.ADD_PRINT_TEXT(78,576,75,20,\"存款时间：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(317,652,65,653,0,1);\r\nLODOP.ADD_PRINT_TEXT(78,657,185,20,\"[cunkuanTime]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(148,8,75,20,\"取款金额：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(148,88,183,20,\"[money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(258,6,75,20,\"取款说明：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(258,88,477,20,\"[add_remark]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(290,7,75,20,\"经办人：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(290,88,183,20,\"[jbname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(114,285,85,20,\"取款时间：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(114,377,185,20,\"[addTime]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(317,281,281,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(290,285,75,20,\"客户签字：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(317,371,281,372,0,1);\r\nLODOP.ADD_PRINT_TEXT(119,599,27,190,\"\\r\\n\\r\\n\\r\\n业务专用章\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.ADD_PRINT_TEXT(148,285,85,20,\"年利率：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(148,377,185,20,\"[nianlilv]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(210,5,209,571,0,1);\r\nLODOP.ADD_PRINT_LINE(246,5,245,571,0,1);\r\nLODOP.ADD_PRINT_TEXT(222,6,75,20,\"利息：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(222,286,85,20,\"取款方式：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(222,375,185,20,\"[qukuanfangshi]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(222,88,183,20,\"[zonglixi]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(245,281,209,282,0,1);\r\nLODOP.ADD_PRINT_LINE(245,371,209,372,0,1);";
            string jsCode = sysCode.Replace("\"[", "model.").Replace("]\"", "");//"[xxx]"→model.xxx
            if (new BLL.SetupPrint().Update_Reset(Config.Enums.Print_Type.定期取款, sysCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        protected void btnDefCode_Click(object sender, EventArgs e)
        {
            string defCode = txtDefCode.Text;
            string jsCode = defCode.Replace("\"[", "model.").Replace("]\"", "");//"[xxx]"→model.xxx
            if (new BLL.SetupPrint().Update(Config.Enums.Print_Type.定期取款, defCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}