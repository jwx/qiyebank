﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="hqc_design.aspx.cs" Inherits="web.admin.print.hqc_design" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="/Scripts/LodopFuncs.js"></script>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        var LODOP;
        function CheckIsInstall() {
            try {
                LODOP = getLodop();
                if (LODOP.VERSION) {
                    if (LODOP.CVERSION) {
                        //alert("当前有C-Lodop云打印可用!\n C-Lodop版本:" + LODOP.CVERSION + "(内含Lodop" + LODOP.VERSION + ")");
                    }
                    else {
                        alert("本机已成功安装了Lodop控件！\n 版本号:" + LODOP.VERSION);
                    }
                }
            } catch (err) {

            }
        }

        function print_Preview() {
            CheckIsInstall();
            printOrder();
            LODOP.PREVIEW();
        }

        function print_Design() {
            CheckIsInstall();
            printOrder();
            LODOP.PRINT_DESIGN();
        }

        function printOrder() {
            <%=txtDefCode.Text %>
        }
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <asp:HiddenField ID="hideSysCode" runat="server" />
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">打印设计： 
                    </td>
                    <td>
                        <input type="button" id="btnPrint" runat="server" value="打印预览" onclick="print_Preview();" />
                        &nbsp;
                        <input type="button" id="Button1" runat="server" value="打印设计" onclick="print_Design();" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>程序代码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtDefCode" runat="server" datatype="*" placeholder="请输入程序代码" TextMode="MultiLine" Style="width: 60%; height: 500px;"></asp:TextBox>
                           <span class="Validform_checktip">请输入程序代码，XP和Win7打印比例需要手动设置为83.6%</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSysCode" runat="server" Text="使用默认打印代码" CssClass="btn btn-sm btnRed" OnClick="btnSysCode_Click" OnClientClick="return confirm('确定要重置打印模板吗？')" />
                        <asp:Button ID="btnDefCode" runat="server" Text="使用当前打印代码" CssClass="btn btn-sm btnBlue" OnClick="btnDefCode_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
