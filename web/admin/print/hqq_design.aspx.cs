﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.print
{
    public partial class hqq_design : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                model = new BLL.SetupPrint().GetModel(Config.Enums.Print_Type.活期取款);
                model = model == null ? new Model.SetupPrint() : model;
                txtDefCode.Text = model.defCode;
                hideSysCode.Value = model.sysCode;
            }
        }

        protected Model.SetupPrint model;

        protected void btnSysCode_Click(object sender, EventArgs e)
        {
            //if(new BLL.SetupPrint().Update_Reset(Config.Enums.Print_Type.活期存款))
            string sysCode = "LODOP.PRINT_INITA(\"0mm\",\"0mm\",\"24.101cm\",\"9.3cm\",\"打印控件功能演示\");\r\nLODOP.SET_PRINT_PAGESIZE(1,2410,930,\"打印控件功能演示\");\r\nLODOP.SET_PRINT_MODE(\"PRINT_PAGE_PERCENT\",\"100%\");\r\nLODOP.ADD_PRINT_RECT(5,5,900,340,0,1);\r\nLODOP.ADD_PRINT_BARCODEA(\"danhao\",20,20,190,45,\"Code39\",\"[danhao]\");\r\nLODOP.ADD_PRINT_LINE(76,5,75,905,0,1);\r\nLODOP.ADD_PRINT_LINE(116,5,115,905,0,1);\r\nLODOP.ADD_PRINT_LINE(156,5,155,561,0,1);\r\nLODOP.ADD_PRINT_LINE(195,5,196,561,0,1);\r\nLODOP.ADD_PRINT_LINE(235,5,236,561,0,1);\r\nLODOP.ADD_PRINT_LINE(275,5,276,561,0,1);\r\nLODOP.ADD_PRINT_LINE(316,5,315,905,0,1);\r\nLODOP.ADD_PRINT_TEXT(326,5,899,20,\"1.白联:存根  2.红联:上缴  3.黄联:客户\");\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.ADD_PRINT_TEXT(90,7,75,20,\"户名：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,83,75,84,0,1);\r\nLODOP.ADD_PRINT_TEXT(32,221,610,36,\"[title]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",15);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\nLODOP.SET_PRINT_STYLEA(0,\"Bold\",1);\r\nLODOP.ADD_PRINT_TEXT(130,7,75,20,\"取款金额：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(90,88,185,20,\"[realname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(130,88,183,20,\"[money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(155,281,75,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,285,75,20,\"账号：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(155,361,75,362,0,1);\r\nLODOP.ADD_PRINT_TEXT(210,7,75,20,\"利息：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(90,367,185,20,\"[username]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(210,88,183,20,\"[lixi]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(315,561,75,562,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,566,75,20,\"取款时间：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,642,75,643,0,1);\r\nLODOP.ADD_PRINT_TEXT(90,647,185,20,\"[cunkuanTime]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(170,8,75,20,\"金额大写：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(170,88,467,20,\"[chs_money]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(250,6,75,20,\"取款说明：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(250,88,467,20,\"[add_remark]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(290,7,75,20,\"经办人：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(290,88,183,20,\"[jbname]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_TEXT(130,285,75,20,\"取款方式：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_TEXT(130,367,185,20,\"[op_type]\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.ADD_PRINT_LINE(315,281,275,282,0,1);\r\nLODOP.ADD_PRINT_TEXT(290,285,75,20,\"客户签字：\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",3);\r\nLODOP.ADD_PRINT_LINE(315,361,275,362,0,1);\r\nLODOP.ADD_PRINT_TEXT(119,589,27,190,\"\\r\\n\\r\\n\\r\\n业务专用章\");\r\nLODOP.SET_PRINT_STYLEA(0,\"FontSize\",10);\r\nLODOP.SET_PRINT_STYLEA(0,\"Alignment\",2);\r\n";
            string jsCode = sysCode.Replace("\"[", "\"\"+model.").Replace("]\"", "+\"\"");//"[xxx]"→model.xxx
            if (new BLL.SetupPrint().Update_Reset(Config.Enums.Print_Type.活期取款, sysCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }

        protected void btnDefCode_Click(object sender, EventArgs e)
        {
            string defCode = txtDefCode.Text;
            string jsCode = defCode.Replace("\"[", "model.").Replace("]\"", "");//"[xxx]"→model.xxx 
            if (new BLL.SetupPrint().Update(Config.Enums.Print_Type.活期取款, defCode, jsCode))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.layer.closeAll();});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}