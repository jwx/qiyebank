﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lixi_weifu_orders.aspx.cs" Inherits="web.admin.tongji.lixi_weifu_orders" %>

<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideDingqiByDay" runat="server" />
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <span class="title">定期利息预估</span>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名</asp:ListItem>
                        <asp:ListItem Value="mobile">手机号码</asp:ListItem>
                        <asp:ListItem Value="danhao">交易单号</asp:ListItem>
                        <asp:ListItem Value="zhouqi">存款周期</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                        <asp:ListItem Value="add_remark">存款备注</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <asp:Button ID="btnExport" runat="server" CssClass="btn btnRed noloading" Text="导出" OnClick="btnExport_Click" />
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">交易单号：</td>
                            <td>
                                <asp:TextBox ID="txtDanhao" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">交易周期：</td>
                            <td>
                                <asp:TextBox ID="txtZhouqi" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">交易时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCunkuanTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtCunkuanTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">到期时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDaoqiTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtDaoqiTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">录入时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">备注：</td>
                            <td>
                                <asp:TextBox ID="txtAdd_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>账号</td>
                           <td>户名/公司名称</td>
                            <td>手机号码</td>
                            <td>交易单号</td>
                            <td>交易金额</td>
                            <td>交易周期</td>
                            <td>交易时间</td>
                            <td>到期时间</td>
                            <td>当前预估利息</td>
                            <%--<td>到期结算方式</td>
                            <td>到期年利率</td>
                            <td>不到期年利率</td>
                            <td>取款类型</td>
                            <td>经办人</td>
                            <td>录入时间</td>
                            <td>备注</td>--%>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                              {
                                  int i = 0;
                                  string yonghu = getPower("用户明细");
                                  string cunkuan = getPower("存款明细");
                                  string qukuan = getPower("取款明细");
                                  foreach (Model.Dingqi one in listRecord)
                                  {
                                      bool isCunkuan = one.cunqukuan == 1;
                                      i++;  
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.uid %>" class="label label-primary layer <%=yonghu %>" title="用户明细_<%=one.realname %>">用户明细</a>
                                <% if (isCunkuan)
                                   { %>
                                <a href="../dingqi/cunkuandetail.aspx?id=<%=one.id %>" class="label label-info layer <%=cunkuan %>" title="存款明细_<%=one.realname %>">存款明细</a>
                                <%}
                                   else
                                   { %>
                                <a href="../dingqi/qukuandetail.aspx?id=<%=one.id %>" class="label label-warning layer <%=qukuan %>" title="取款明细_<%=one.realname %>">取款明细</a>
                                <%} %>
                            </td>
                            <td>
                                <%=one.username %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.mobile %>
                            </td>
                            <td>
                                <%=one.danhao %>
                            </td>
                            <td class="text_right">
                                <%=one.jine %>
                            </td>
                            <td><%=one.zhouqi %></td>
                            <td>
                                <%=one.cunkuanTime %>
                            </td>
                            <td>
                                <%=one.daoqiTime %>
                            </td>
                            <td style="text-align: right;">
                                <%= new BLL.Dingqi().GetLixi_Yugu(Falcon.Function.ToInt(hideDingqiByDay.Value), one) %>
                            </td>
                            <%-- <td><%=isCunkuan?((Config.Enums.Dingqi_DaoqiHandler)one.daoqi_handler).ToString():"" %></td>
                            <td>
                                <%=isCunkuan?(one.lilv+"%"):"" %>
                            </td>
                            <td>
                                <%=isCunkuan?(one.budaoqi+"%"):"" %>
                            </td>
                            <td><%=isCunkuan?"":((Config.Enums.Dingqi_Qukuan)one.qukuanType).ToString() %></td>
                            <td><%=one.jbname %></td>
                            <td><%=one.addTime %></td>
                            <td>
                                <%=one.add_remark %>
                            </td>--%>
                        </tr>
                        <%}
                              }
                              else
                              { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                        <div class="tongji">
                            系统定期账户总余额：<asp:Label ID="lblDingqi" runat="server"></asp:Label>元，合计总预估利息：<asp:Label ID="lblHeji" runat="server"></asp:Label>元
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

