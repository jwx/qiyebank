﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lixi_yifu.aspx.cs" Inherits="web.admin.tongji.lixi_yifu" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hideHuoqiList" runat="server" />
        <asp:HiddenField ID="hideDingqiList" runat="server" />
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <span class="title">
                        <asp:Label ID="lblTongji" runat="server"></asp:Label>
                    </span>
                    <a href="lixi_yifu.html" class="layer" style="margin-left: 10px;" title="已付利息计算规则">已付利息计算规则</a>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="username">账号</asp:ListItem>
                        <asp:ListItem Value="realname">户名</asp:ListItem>
                        <asp:ListItem Value="mobile">手机号码</asp:ListItem>
                        <asp:ListItem Value="address">居住地址</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <asp:Button ID="btnExport" runat="server" CssClass="btn btnRed noloading" Text="导出" OnClick="btnExport_Click" />
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">账号：</td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">户名：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">手机号码：</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">开户时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">账户状态：</td>
                            <td>
                                <asp:DropDownList ID="ddlIsDel" runat="server">
                                    <asp:ListItem Value="-1">请选择账户状态</asp:ListItem>
                                    <asp:ListItem Value="0">正常</asp:ListItem>
                                    <asp:ListItem Value="1">已销户</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>账号</td>
                           <td>户名/公司名称</td>
                            <td>手机号码</td>
                            <td>经办人</td>
                            <td>开户时间</td>
                            <td>账户状态</td>
                            <td>零钱余额</td>
                            <td>已付零钱利息</td>
                            <td>定期余额</td>
                            <td>已付定期利息</td>
                            <td>总金额</td>
                            <td>总已付利息</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%    if (listRecord != null && listRecord.Count != 0)
                              {
                                  int i = 0;
                                  string yonghu = getPower("用户明细");
                                  List<Model.Huoqi> listHuoqi = Tool.JsonHelper.JsonDeserialize<List<Model.Huoqi>>(hideHuoqiList.Value);
                                  List<Model.Dingqi> listDingqi = Tool.JsonHelper.JsonDeserialize<List<Model.Dingqi>>(hideDingqiList.Value);
                                  foreach (Model.UserInfo one in listRecord)
                                  {
                                      decimal huoqi = new BLL.Huoqi().GetYue(one.id);
                                      decimal dingqi = new BLL.Dingqi().GetYue(one.id);
                                      decimal yifuhuoqi = (from m in listHuoqi where m.uid == one.id && m.laiyuan == 101 select m.zonglixi).Sum() + (from m in listHuoqi where m.uid == one.id && m.laiyuan == 1 select m.yuanjine - m.jine).Sum();//本息取款（取款单据）的总利息+零钱利息存入中已取出的利息
                                      decimal yifudingqi = (from m in listDingqi where m.uid == one.id select m.lixi).Sum();
                                      i++;  
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <a href="../users/detail.aspx?id=<%=one.id %>" class="label label-primary layer <%=yonghu %>" title="用户明细_<%=one.realname %>">用户明细</a>
                            </td>
                            <td>
                                <%=one.username %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.mobile %>
                            </td>
                            <td>
                                <%=one.jbname %>
                            </td>
                            <td>
                                <%=one.addTime.ToString("yyyy-MM-dd HH:mm:ss") %>
                            </td>
                            <td class="text_center">
                                <%=one.isDel==0?"正常":"已销户" %>
                            </td>
                            <td class="text_right"><%=huoqi %></td>
                            <td class="text_right"><%=yifuhuoqi %></td>
                            <td class="text_right"><%=dingqi %></td>
                            <td class="text_right"><%=yifudingqi %></td>
                            <td class="text_right"><%=huoqi+dingqi %></td>
                            <td class="text_right"><%=yifuhuoqi+yifudingqi %></td>
                        </tr>
                        <%}
                              }
                              else
                              { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                        <div class="tongji">
                            系统零钱账户总余额：<asp:Label ID="lblHuoqi" runat="server"></asp:Label>元，定期账户总余额：<asp:Label ID="lblDingqi" runat="server"></asp:Label>元，合计总余额：<asp:Label ID="lblHeji" runat="server"></asp:Label>元
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
