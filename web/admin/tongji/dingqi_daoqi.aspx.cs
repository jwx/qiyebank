﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class dingqi_daoqi : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            if (!IsPostBack)
            {
                flag = false;
                Query();
            }
        }

        protected List<Model.Dingqi> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "cunqukuan=1 and hasQukuan=0 and delid=0 and daoqiTime<='" + DateTime.Now.AddDays(Falcon.Function.GetQueryInt("d")).ToString("yyyy-MM-dd") + "'";

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                    if (txtZhouqi.Text.Trim() != "") where += " and zhouqi like '%" + txtZhouqi.Text + "%'";
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                    if (txtDaoqiTime1.Text.Trim() != "") where += " and daoqiTime>='" + txtDaoqiTime1.Text.Trim() + "'";
                    if (txtDaoqiTime2.Text.Trim() != "") where += " and daoqiTime<='" + txtDaoqiTime2.Text.Trim() + "'";
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                }
            }
            listRecord = Tool.Pager_Sqlite.Query<Model.Dingqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Dingqi_Info", "*", where, "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }
    }
}