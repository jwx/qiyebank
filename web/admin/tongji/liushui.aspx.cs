﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class liushui : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                flag = false;
                Query();
                decimal huoqi = new BLL.Huoqi().GetSysYue();
                decimal dingqi = new BLL.Dingqi().GetSysYue();
                lblHuoqi.Text = huoqi.ToString();
                lblDingqi.Text = dingqi.ToString();
                lblHeji.Text = (huoqi + dingqi).ToString();
            }
        }

        protected List<Model.UserInfo> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtIdCard.Text.Trim() != "") where += " and idcard like '%" + txtIdCard.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text.Trim() + "%'";
                    if (txtEmail.Text.Trim() != "") where += " and email like '%" + txtEmail.Text + "%'";
                    if (ddlGender.SelectedIndex != 0) where += " and gender=" + ddlGender.SelectedValue;
                    if (txtTel.Text.Trim() != "") where += " and tel like '%" + txtTel.Text + "%'";
                    if (txtAddress.Text.Trim() != "") where += " and address like '%" + txtAddress.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (ddlIsDel.SelectedIndex != 0) where += " and isDel=" + ddlIsDel.SelectedValue;
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                    if (ddlUserOrGs.SelectedIndex != 0) where += " and UserOrGS=" + ddlUserOrGs.SelectedValue;


                }
            }
            listRecord = Tool.Pager_Sqlite.Query<Model.UserInfo>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_UserInfo_Users", "*", where, "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }
    }
}