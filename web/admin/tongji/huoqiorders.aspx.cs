﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class huoqiorders : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            if (!IsPostBack)
            {
                Tool.EnumToList.FillDropDownList(ddlLaiyuan, (Type)(typeof(Config.Enums.Huoqi_Laiyuan)));
                ddlLaiyuan.Items.Insert(0, new ListItem() { Text = "请选择交易类型", Value = "-1" });
                btnExport.CssClass += getPower("导出");

                flag = false;
                Query();
                decimal huoqi = new BLL.Huoqi().GetSysYue();
                decimal dingqi = new BLL.Dingqi().GetSysYue();
                lblHuoqi.Text = huoqi.ToString();
                lblDingqi.Text = dingqi.ToString();
                lblHeji.Text = (huoqi + dingqi).ToString();
            }
        }

        protected List<Model.Huoqi> listRecord;

        private static bool flag;

        protected int uid;

        private string GetWhere()
        {
            string where = "delid=0";

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                    if (ddlLaiyuan.SelectedIndex != 0) where += " and laiyuan=" + ddlLaiyuan.SelectedValue;
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            listRecord = Tool.Pager_Sqlite.Query<Model.Huoqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Huoqi_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.DataTableHelper().GetDataTableByWhere("V_Huoqi_Info", "id,username,mobile,realname,danhao,yuanjine,laiyuan,cunkuanTime,jbname,addTime,add_remark", GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                dt.Columns.Add("jiaoyiType");
                foreach (System.Data.DataRow one in dt.Rows)
                {
                    one["jiaoyiType"] = (Config.Enums.Huoqi_Laiyuan)Falcon.Function.ToInt(one["laiyuan"]);
                }
                Tool.ExcelHelper.DataTableToExcel("零钱业务查询", new List<System.Data.DataTable>() { dt }, new List<string> { "零钱业务查询" }, new List<string[]> { new string[] { "账号|username", "户名|realname", "交易单号|danhao", "交易金额|yuanjine", "交易类型|jiaoyiType", "交易时间|cunkuanTime", "经办人|jbname", "录入时间|addTime", "备注|add_remark" } });
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('没有数据！',{icon:5,time:1000});", true);
            }
        }
    }
}