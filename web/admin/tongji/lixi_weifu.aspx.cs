﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class lixi_weifu : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                Model.Setup modelSettings = new BLL.Setup().Get_Setup();
                modelSettings = modelSettings == null ? new Model.Setup() : modelSettings;
                hideHuoqiByDay.Value = modelSettings.hq_byDay.ToString();
                hideDingqiByDay.Value = modelSettings.dq_byDay.ToString();

                flag = false;
                Query();
                List<Model.Huoqi> listHuoqiLixi = new BLL.Huoqi().GetList_WeifuLixi();//活期本息取款单据的总利息zonglixi，活期结转单据的原金额-剩余金额,yuanjine-jine
                listHuoqiLixi = listHuoqiLixi == null ? new List<Model.Huoqi>() : listHuoqiLixi;
                hideHuoqiWeifuList.Value = Tool.JsonHelper.JsonSerializer<List<Model.Huoqi>>(listHuoqiLixi);

                List<Model.Dingqi> listDingqiLixi = new BLL.Dingqi().GetList_WeifuLixi();
                listDingqiLixi = listDingqiLixi == null ? new List<Model.Dingqi>() : listDingqiLixi;
                hideDingqiWeifuList.Value = Tool.JsonHelper.JsonSerializer<List<Model.Dingqi>>(listDingqiLixi);

                //lblTongji.Text = "已付零钱利息合计：" + t_huoqi + "元，已付定期利息合计：" + t_dingqi + "元，总合计：" + (t_huoqi + t_dingqi) + "元";
                decimal huoqi = new BLL.Huoqi().GetSysYue();
                decimal dingqi = new BLL.Dingqi().GetSysYue();
                lblHuoqi.Text = huoqi.ToString();
                lblDingqi.Text = dingqi.ToString();
                lblHeji.Text = (huoqi + dingqi).ToString();
            }
        }

        protected List<Model.UserInfo> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (ddlIsDel.SelectedIndex != 0) where += " and isDel=" + ddlIsDel.SelectedValue;
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            listRecord = Tool.Pager_Sqlite.Query<Model.UserInfo>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_UserInfo_Users", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected decimal GetHuoqiLixiByUser(List<Model.Huoqi> listHuoqiOrder)
        {
            decimal lixi = 0;
            if (listHuoqiOrder.Count != 0)
            {
                DateTime dtNow = DateTime.Now;
                foreach (Model.Huoqi one in listHuoqiOrder)
                {
                    lixi += new BLL.Huoqi().GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), Falcon.Function.ToInt(hideHuoqiByDay.Value), dtNow);
                }
            }
            return lixi;
        }

        protected decimal GetDingqiLixiByUser(List<Model.Dingqi> listDingqiOrder)
        {
            decimal lixi = 0;
            if (listDingqiOrder.Count != 0)
            {
                foreach (Model.Dingqi one in listDingqiOrder)
                {
                    lixi += new BLL.Dingqi().GetLixi_Yugu(Falcon.Function.ToInt(hideDingqiByDay.Value), one);
                }
            }
            return lixi;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.UserInfo().GetListDtByWhere(GetWhere() + " order by id desc");
            if (dt != null)
            {
                dt.Columns.Add("_addTime");
                dt.Columns.Add("_isDel");
                dt.Columns.Add("huoqi");
                dt.Columns.Add("yijiezhuanhuoqi");
                dt.Columns.Add("huoqiyugu");
                dt.Columns.Add("dingqi");
                dt.Columns.Add("yijiezhuandingqi");
                dt.Columns.Add("dingqiyugu");
                dt.Columns.Add("all");
                dt.Columns.Add("yijiezhuanall");
                dt.Columns.Add("yuguall");
                dt.Columns.Add("lixiall");

                List<Model.Huoqi> listHuoqiLixi = Tool.JsonHelper.JsonDeserialize<List<Model.Huoqi>>(hideHuoqiWeifuList.Value);
                List<Model.Dingqi> listDingqiLixi = Tool.JsonHelper.JsonDeserialize<List<Model.Dingqi>>(hideDingqiWeifuList.Value);

                List<Model.Huoqi> listHuoqiOrders = new BLL.Huoqi().GetListByWhere(" jine!=0 and cunqukuan=1 and delid=0");
                List<Model.Dingqi> listDingqiOrders = new BLL.Dingqi().GetListByWhere("cunqukuan=1 and hasQukuan=0 and delid=0");
                listHuoqiOrders = listHuoqiOrders == null ? new List<Model.Huoqi>() : listHuoqiOrders;
                listDingqiOrders = listDingqiOrders == null ? new List<Model.Dingqi>() : listDingqiOrders;

                foreach (System.Data.DataRow one in dt.Rows)
                {
                    int id = Falcon.Function.ToInt(one["id"]);
                    decimal huoqi = new BLL.Huoqi().GetYue(id);
                    decimal dingqi = new BLL.Dingqi().GetYue(id);
                    decimal yijiezhuanhuoqi = (from m in listHuoqiLixi where m.uid == id && m.laiyuan == ((int)Config.Enums.Huoqi_Laiyuan.零钱利息存入) select m.jine).Sum();
                    decimal yijiezhuandingqi = (from m in listHuoqiLixi where m.uid == id && (m.laiyuan == ((int)Config.Enums.Huoqi_Laiyuan.定期利息存入) || m.laiyuan == ((int)Config.Enums.Huoqi_Laiyuan.定期本息存入)) select m.jine).Sum() + (from m in listDingqiLixi where m.uid == id select m.lixi).Sum();

                    List<Model.Huoqi> listHuoqiOrder_User = (from m in listHuoqiOrders where m.uid == id select m).ToList();
                    List<Model.Dingqi> listDingqiOrder_User = (from m in listDingqiOrders where m.uid == id select m).ToList();

                    decimal huoqiyugu = GetHuoqiLixiByUser(listHuoqiOrder_User);
                    decimal dingqiyugu = GetDingqiLixiByUser(listDingqiOrder_User);

                    one["huoqi"] = huoqi;
                    one["yijiezhuanhuoqi"] = yijiezhuanhuoqi;
                    one["huoqiyugu"] = huoqiyugu;
                    one["dingqi"] = dingqi;
                    one["yijiezhuandingqi"] = yijiezhuandingqi;
                    one["dingqiyugu"] = dingqiyugu;
                    one["all"] = dingqi + huoqi;
                    one["yijiezhuanall"] = yijiezhuanhuoqi + yijiezhuandingqi;
                    one["yuguall"] = huoqiyugu + dingqiyugu;
                    one["lixiall"] = yijiezhuanhuoqi + yijiezhuandingqi + huoqiyugu + dingqiyugu;
                }
                Tool.ExcelHelper.DataTableToExcel("存款未付利息统计", new List<System.Data.DataTable>() { dt }, new List<string>() { "未付利息" }, new List<string[]>() { new string[] { "账号|username", "户名|realname", "手机号码|mobile", "经办人|jbname", "开户时间|_addTime", "账户状态|_isDel", "零钱余额|huoqi", "零钱已结转利息|yijiezhuanhuoqi", "零钱预估利息|huoqiyugu", "定期余额|dingqi", "定期已结转利息|yijiezhuandingqi", "定期预估利息|dingqiyugu", "总金额|all", "总已结转利息|yijiezhuanall", "总预估利息|yuguall", "总利息|lixiall" } });
            }
        }
    }
}