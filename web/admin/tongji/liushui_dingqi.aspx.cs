﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class liushui_dingqi : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();
            uid = Falcon.Function.GetQueryInt("id");
            if (!IsPostBack)
            {
                flag = false;
                Query();
            }
        }

        protected List<Model.Dingqi> listRecord;

        private static bool flag;

        protected int uid;

        private string GetWhere()
        {
            string where = "delid=0" + (uid == 0 ? "" : (" and uid=" + uid));

            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtDanhao.Text.Trim() != "") where += " and danhao like '%" + txtDanhao.Text + "%'";
                    if (ddlLaiyuan.SelectedIndex != 0) where += " and " + ddlLaiyuan.SelectedValue;
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (txtCunkuanTime1.Text.Trim() != "") where += " and cunkuanTime>='" + txtCunkuanTime1.Text.Trim() + "'";
                    if (txtCunkuanTime2.Text.Trim() != "") where += " and cunkuanTime<='" + txtCunkuanTime2.Text.Trim() + "'";
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                    if (txtAdd_Remark.Text.Trim() != "") where += " and add_remark like '%" + txtAdd_Remark.Text + "%'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            listRecord = Tool.Pager_Sqlite.Query<Model.Dingqi>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Dingqi_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.DataTableHelper().GetDataTableByWhere("V_Dingqi_Info", "id,username,realname,danhao,jine,cunqukuan,qukuanType,cunkuanTime,jbname,addTime,add_remark", GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                dt.Columns.Add("jiaoyiType");
                foreach (System.Data.DataRow one in dt.Rows)
                {
                    one["jiaoyiType"] = one["cunqukuan"].ToString() == "1" ? "存款" : ((Config.Enums.Dingqi_Qukuan)Falcon.Function.ToInt(one["qukuanType"])).ToString();
                }
                Tool.ExcelHelper.DataTableToExcel(Server.UrlDecode(Falcon.Function.GetQueryString("name")) + "_个人定期流水导出", new List<System.Data.DataTable>() { dt }, new List<string> { "定期流水" }, new List<string[]> { new string[] { "账号|username", "户名|realname", "交易单号|danhao", "交易金额|jine", "交易类型|jiaoyiType", "交易时间|cunkuanTime", "经办人|jbname", "录入时间|addTime", "备注|add_remark" } });
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('没有数据！',{icon:5,time:1000});", true);
            }
        }
    }
}