﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.tongji
{
    public partial class lixi_yifu : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                flag = false;
                Query();


                List<Model.Huoqi> listHuoqiLixi = new BLL.Huoqi().GetList_YifuLixi();//活期本息取款单据的总利息zonglixi，活期结转单据的原金额-剩余金额,yuanjine-jine
                listHuoqiLixi = listHuoqiLixi == null ? new List<Model.Huoqi>() : listHuoqiLixi;
                hideHuoqiList.Value = Tool.JsonHelper.JsonSerializer<List<Model.Huoqi>>(listHuoqiLixi);

                List<Model.Dingqi> listDingqiLixi = new BLL.Dingqi().GetList_YifuLixi();
                listDingqiLixi = listDingqiLixi == null ? new List<Model.Dingqi>() : listDingqiLixi;
                hideDingqiList.Value = Tool.JsonHelper.JsonSerializer<List<Model.Dingqi>>(listDingqiLixi);

                decimal t_huoqi = (from m in listHuoqiLixi where m.laiyuan == 101 select m.zonglixi).Sum() + (from m in listHuoqiLixi where m.laiyuan == 1 select m.yuanjine - m.jine).Sum();
                decimal t_dingqi = (from m in listDingqiLixi select m.lixi).Sum();
                lblTongji.Text = "已付零钱利息合计：" + t_huoqi + "元，已付定期利息合计：" + t_dingqi + "元，总合计：" + (t_huoqi + t_dingqi) + "元";

                decimal huoqi = new BLL.Huoqi().GetSysYue();
                decimal dingqi = new BLL.Dingqi().GetSysYue();
                lblHuoqi.Text = huoqi.ToString();
                lblDingqi.Text = dingqi.ToString();
                lblHeji.Text = (huoqi + dingqi).ToString();
            }
        }

        protected List<Model.UserInfo> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                    if (ddlIsDel.SelectedIndex != 0) where += " and isDel=" + ddlIsDel.SelectedValue;
                    if (txtStart.Text.Trim() != "") where += " and addTime>='" + txtStart.Text.Trim() + "'";
                    if (txtEnd.Text.Trim() != "") where += " and addTime<='" + txtEnd.Text.Trim() + "'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            listRecord = Tool.Pager_Sqlite.Query<Model.UserInfo>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_UserInfo_Users", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.UserInfo().GetListDtByWhere(GetWhere() + " order by id desc");
            if (dt != null)
            {
                List<Model.Huoqi> listHuoqi = Tool.JsonHelper.JsonDeserialize<List<Model.Huoqi>>(hideHuoqiList.Value);
                List<Model.Dingqi> listDingqi = Tool.JsonHelper.JsonDeserialize<List<Model.Dingqi>>(hideDingqiList.Value);

                dt.Columns.Add("_addTime");
                dt.Columns.Add("_isDel");
                dt.Columns.Add("huoqi");
                dt.Columns.Add("yifuhuoqi");
                dt.Columns.Add("dingqi");
                dt.Columns.Add("yifudingqi");
                dt.Columns.Add("all");
                dt.Columns.Add("yifuall");

                foreach (System.Data.DataRow one in dt.Rows)
                {
                    int id = Falcon.Function.ToInt(one["id"]);
                    decimal huoqi = new BLL.Huoqi().GetYue(id);
                    decimal dingqi = new BLL.Dingqi().GetYue(id);
                    decimal yifuhuoqi = (from m in listHuoqi where m.uid == id && m.laiyuan == 101 select m.zonglixi).Sum() + (from m in listHuoqi where m.uid == id && m.laiyuan == 1 select m.yuanjine - m.jine).Sum();//本息取款（取款单据）的总利息+零钱利息存入中已取出的利息
                    decimal yifudingqi = (from m in listDingqi where m.uid == id select m.lixi).Sum();

                    one["_addTime"] = Falcon.Function.ToDateTime(one["addTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                    one["_isDel"] = one["isDel"].ToString() == "0" ? "正常" : "已销户";
                    one["huoqi"] = huoqi;
                    one["yifuhuoqi"] = yifuhuoqi;
                    one["dingqi"] = dingqi;
                    one["yifudingqi"] = yifudingqi;
                    one["all"] = huoqi + dingqi;
                    one["yifuall"] = yifuhuoqi + yifudingqi;

                }
                Tool.ExcelHelper.DataTableToExcel("存款已付利息统计", new List<System.Data.DataTable>() { dt }, new List<string>() { "已付利息" }, new List<string[]>() { new string[] { "账号|username","户名|realname","手机号码|mobile","经办人|jbname","开户时间|_addTime","账户状态|_isDel","零钱余额|huoqi","已付零钱利息|yifuhuoqi","定期余额|dingqi","已付定期利息|yifudingqi"
,"总金额|all","总已付利息|yifuall" } });
            }
        }
    }
}