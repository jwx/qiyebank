﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.baseinfo
{
    public partial class tixian : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Apply> listRecord;

        private static bool flag;

        private string GetWhere()
        {
            string where = "isFukuan=0";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtPihao.Text.Trim() != "") where += " and pihao like '%" + txtPihao.Text + "%'";
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text + "%'";
                    if (txtAddTime1.Text.Trim() != "") where += " and addTime>='" + txtAddTime1.Text.Trim() + "'";
                    if (txtAddTime2.Text.Trim() != "") where += " and addTime<='" + txtAddTime2.Text.Trim() + "'";
                    if (txtRemark.Text.Trim() != "") where += " and remark like '%" + txtRemark.Text + "%'";
                }
            }
            return where;
        }

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);


            listRecord = Tool.Pager_Sqlite.Query<Model.Apply>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Apply_Info", "*", GetWhere(), "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Pay(string id)
        {
            List<int> listIds = Array.ConvertAll<string, int>(id.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            if (listIds != null && listIds.Count != 0)
            {
                return new BLL.Apply().Update_Fukuan_Piliang(listIds, 1, SessionUid).ToString();
            }
            return "0";
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new BLL.Apply().GetDataTableByWhere(GetWhere() + " order by id desc");
            if (dt != null && dt.Rows.Count != 0)
            {
                Tool.ExcelHelper.DataTableToExcel("提现申请", new List<System.Data.DataTable>() { dt }, new List<string>() { "提现申请" }, new List<string[]>() { new string[] { "批号|pihao", "申请人|realname", "提现金额|jine", "申请时间|addTime", "备注|remark" } });
            }
        }

        protected void btnPihao_Click(object sender, EventArgs e)
        {
            string pihao = new BLL.Apply().CreatePihao();
            if (string.IsNullOrEmpty(pihao))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000},function(){window.location.href=window.location;});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){window.location.href=window.location;});", true);
            }
        }
    }
}