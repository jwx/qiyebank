﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="yitixian.aspx.cs" Inherits="web.admin.baseinfo.yitixian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a href="tixian.aspx" class="tab">提现申请
                    </a>
                    <a class="tab current" href="javascript:void(0);">提现记录
                    </a>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="pihao">批号</asp:ListItem>
                        <asp:ListItem Value="realname">申请人</asp:ListItem>
                        <asp:ListItem Value="remark">申请备注</asp:ListItem>
                        <asp:ListItem Value="jbname">经办人</asp:ListItem>
                        <asp:ListItem Value="jbname">付款备注</asp:ListItem>

                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji" href="javascript:void(0);">高级查询</a>
                    <asp:Button ID="btnExport" runat="server" CssClass="btn btnRed noloading" Text="导出" OnClick="btnExport_Click" />
                    <a class="refresh" href="javascript:window.location.href = window.location;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">批号：</td>
                            <td>
                                <asp:TextBox ID="txtPihao" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">申请人：</td>
                            <td>
                                <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">申请时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtAddTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtAddTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">申请备注：</td>
                            <td>
                                <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">经办人：</td>
                            <td>
                                <asp:TextBox ID="txtJbName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">付款时间：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtFkTime1" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtFkTime2" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">付款备注：</td>
                            <td>
                                <asp:TextBox ID="txtFk_Remark" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>批号</td>
                            <td>申请人</td>
                            <td>提现金额</td>
                            <td>本金</td>
                            <td>利息</td>
                            <td>申请时间</td>
                            <td>申请备注</td>
                            <td>经办人</td>
                            <td>付款时间</td>
                            <td>付款备注</td>
                        </tr>
                    </thead>
                    <tbody>
                        <% if (listRecord != null && listRecord.Count != 0)
                           {
                               int i = (AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize;
                               string pay = getPower("付款");
                               foreach (Model.Apply one in listRecord)
                               {
                                   i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td>
                                <%=one.pihao %> 
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td style="text-align: right;"><%=one.jine %></td>
                            <td style="text-align: right;"><%=one.benjin %></td>
                            <td style="text-align: right;"><%=one.lixi %></td>
                            <td><%=one.addTime %></td>
                            <td>
                                <%=one.remark %>
                            </td>
                            <td><%=one.jbname %></td>
                            <td><%=one.fk_time %></td>
                            <td><%=one.fk_remark %></td>
                        </tr>
                        <%}
                           }
                           else
                           { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
