﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bankinfo.aspx.cs" Inherits="web.admin.baseinfo.bankinfo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>银行名称： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" datatype="*" placeholder="请输入银行名称"></asp:TextBox>
                        <span class="Validform_checktip">请输入银行名称</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">号段： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtStartNo" runat="server" placeholder="请输入号段"></asp:TextBox>
                        <span class="Validform_checktip">请输入号段</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">排序： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSort" runat="server" datatype="n1-4" placeholder="请输入排序级别" Text="100" errormsg="请填写0-9999之间的整数数字"></asp:TextBox>
                        <span class="Validform_checktip">请输入0-9999之间的整数数字，数字越小，排序越靠前</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
