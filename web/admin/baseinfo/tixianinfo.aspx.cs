﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.baseinfo
{
    public partial class tixianinfo : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    model = new BLL.Apply().GetModel(id); 
                }
            }
        }

        protected Model.Apply model;

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id != 0)
            {
                if (new BLL.Apply().Update_Fukuan(id, 1, SessionUid, txtRemark.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }
    }
}