﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.baseinfo
{
    public partial class bank : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Bank> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "isDel=0";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtName.Text.Trim() != "") where += " and name like '%" + txtName.Text + "%'";
                    if (txtStartNo.Text.Trim() != "") where += " and startNo like '%" + txtStartNo.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                }
            }
            listRecord = Tool.Pager_Sqlite.Query<Model.Bank>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Bank_Users", "*", where, "id asc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(string id)
        {
            return new BLL.Bank().Delete(id);
        }
    }
}