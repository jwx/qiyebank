﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tixianinfo.aspx.cs" Inherits="web.admin.baseinfo.tixianinfo" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;">批号： 
                    </td>
                    <td>
                        <%=model.pihao %> 
                    </td>
                </tr>
                <tr>
                    <td class="tdl">申请人： 
                    </td>
                    <td>
                        <%=model.realname %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">提现金额： 
                    </td>
                    <td>
                        <%=model.jine %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">申请时间： 
                    </td>
                    <td>
                        <%=model.addTime %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">申请备注： 
                    </td>
                    <td>
                        <%=model.remark %>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">付款备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
