﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.baseinfo
{
    public partial class backup : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Backup> listRecord;

        private static bool flag;

        private void Query()
        {
            int recordCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtDbName.Text.Trim() != "") where += " and dbname like '%" + txtDbName.Text + "%'";
                    if (txtRemark.Text.Trim() != "") where += " and remark like '%" + txtRemark.Text + "%'";
                    if (txtJbName.Text.Trim() != "") where += " and jbname like '%" + txtJbName.Text + "%'";
                }
            }
            listRecord = Tool.Pager_Sqlite.Query<Model.Backup>(AspNetPager1.CurrentPageIndex, PageSize, out recordCount, "V_Backup_Users", "*", where, "id desc");
            AspNetPager1.RecordCount = recordCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Backup(string dbname)
        {
            //string dbPath = Tool.DatabaseHelper.dbPath;
            //try
            //{
            //    string dbRootPath = System.Web.HttpContext.Current.Server.MapPath("/") + "App_Data\\";
            //    System.IO.File.Move(dbPath, dbRootPath + "jingyi_" + dbname);//精益数据库备份 
            //    System.IO.File.Copy(dbRootPath + dbname, dbRootPath + "jingyi.dll");
            return "1";
            //}
            //catch (Exception ex)
            //{ 
            //    return ex.Message;
            //}
        }
    }
}