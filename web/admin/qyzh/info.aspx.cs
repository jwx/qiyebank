﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.qyzh
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);

            if (!IsPostBack)
            {
                List<Model.Bank> listBank = new BLL.Bank().GetList();
                listBank = listBank == null ? new List<Model.Bank>() : listBank;
                ddlBank.DataSource = listBank;
                ddlBank.DataValueField = "name";
                ddlBank.DataTextField = "name";
                ddlBank.DataBind();
                ddlBank.Items.Insert(0, new ListItem() { Text = "请选择开户行", Value = "" });

                if (id != 0)
                {
                    model = new BLL.UserInfo().GetModel(id);
                    txtUserName.Text = model.username;
                    txtUserName.Enabled = false;
                    txtUserName.Attributes.Remove("ajaxurl");//取消验证用户名重复操作
                    string pswd = Falcon.Function.Decrypt(model.userpswd);
                    txtUserPswd.TextMode = txtUserPswd1.TextMode = TextBoxMode.SingleLine;
                    Regex reg = new Regex("\\w");
                    txtUserPswd.Text = txtUserPswd1.Text = reg.Replace(pswd, "·");
                    txtUserPswd.Enabled = txtUserPswd1.Enabled = false;
                    pswd = Falcon.Function.Decrypt(model.loginpswd);
                    txtPayPswd.TextMode = txtPayPswd1.TextMode = TextBoxMode.SingleLine;
                    reg = new Regex("\\w");
                    txtPayPswd.Text = txtPayPswd1.Text = reg.Replace(pswd, "·");
                    txtPayPswd.Enabled = txtPayPswd1.Enabled = false;
                    txtRealName.Text = model.realname;
                    txtIdcard.Text = model.idcard;
                    txtEmail.Text = model.email;
                    ddlGender.SelectedValue = model.gender.ToString();
                    txtTel.Text = model.tel;
                    txtMobile.Text = model.mobile;
                    txtAddress.Text = model.address;
                    txtBankCard.Text = model.bankcard;
                    txtBankUName.Text = model.bank_uname;
                    for (int i = 0; i < ddlBank.Items.Count; i++)
                    {
                        if (ddlBank.Items[i].Text == model.bankname)
                        {
                            ddlBank.Items[i].Selected = true;
                            break;
                        }
                    }

                    txtZhifubao.Text = model.zhifubao;
                    photo = model.photo;
                    txtRemark.Text = model.remark;
                    txtIdcard.Text = model.TYSHXYDM;
                    txtAddress.Text = model.GSMC;
                    ddlGSTYPENAme.SelectedIndex = model.GSTYPEID;
                    txtRealName.Text = model.GSDZ;
                    txtFDDBR.Text = model.FDDBR;
                    txtZCZB.Text = model.ZCZB;
                    txtYYQX.Text = model.YYQX;
                    txtCLTime.Text = model.CLTIME;
                    txtJYFW.Text = model.JYFW;
                    dllGSFl.SelectedIndex = model.TYPEID;
                }
            }
        }
        protected int id;

        protected Model.UserInfo model;

        protected string photo;

       

        [AjaxPro.AjaxMethod]
        public void DeleteImg(string img)
        {
            Falcon.FileOP.DeleteFile(Server.MapPath(img));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
         
                if (id == 0)
                {
                    if (new BLL.UserInfo().Add(txtUserName.Text, txtUserPswd.Text, txtPayPswd.Text, txtRealName.Text, txtIdcard.Text, txtEmail.Text, Falcon.Function.ToInt(ddlGender.SelectedValue), txtTel.Text, txtMobile.Text, txtAddress.Text, txtBankCard.Text.Trim(), txtBankUName.Text.Trim(), ddlBank.SelectedItem.Text, txtZhifubao.Text.Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), txtRemark.Text, Falcon.Function.GetFormString("hidePhoto"), "", SessionUid, ddlGSTYPENAme.SelectedIndex, ddlGSTYPENAme.SelectedItem.Text.ToString(), txtFDDBR.Text.ToString(), txtZCZB.Text.ToString(), txtCLTime.Text.ToString(), txtYYQX.Text.ToString(), txtJYFW.Text.ToString(), dllGSFl.SelectedIndex, dllGSFl.SelectedItem.Text.ToString(),1))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
                else
                {
                    if (new BLL.UserInfo().Update(id, txtRealName.Text, txtIdcard.Text, txtEmail.Text, Falcon.Function.ToInt(ddlGender.SelectedValue), txtTel.Text, txtMobile.Text, txtAddress.Text, txtBankCard.Text.Trim(), txtBankUName.Text.Trim(), ddlBank.SelectedItem.Text, txtZhifubao.Text.Trim(), txtRemark.Text, Falcon.Function.GetFormString("hidePhoto"), SessionUid))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
        }




    }
}