﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace web
{
    [AjaxPro.AjaxNamespace("ajax")]
    public class Power : System.Web.UI.Page, System.Web.SessionState.IRequiresSessionState
    {
        //<summary>
        //是否登录
        //</summary>
        //<returns></returns>
        public void checkLogin()
        {
            Session["uid"] = 2;
            Session["role"] = 0;
            Session["shouquan"] = "123456";
            if (Session["uid"] == null || Session["role"] == null || Session["shouquan"] == null)
            {
                Response.Write("<script type='text/javascript'>window.top.location.href='/login.aspx';</script>");
                Response.End();
                return;
            }

            AjaxPro.Utility.RegisterTypeForAjax(this.GetType());
        }

        //public Power()
        //{
        //    Session["uid"] = 1;
        //    Session["role"] = 0;
        //    Session["depart"] = 1;
        //    Session["manager"] = true;

        //    if (Session["uid"] == null || Session["role"] == null)
        //    {
        //        Response.Write("<script type='text/javascript'>window.top.location.href='/login.aspx';</script>");
        //        Response.End();
        //        return;
        //    }

        //    AjaxPro.Utility.RegisterTypeForAjax(this.GetType());
        //}

        public int SessionUid
        {
            get
            {
                return Falcon.Function.ToInt(Session["uid"], 0);
            }
            set
            {
                Session["uid"] = value;
            }
        }

        public int SessionRole
        {
            get
            {
                return Falcon.Function.ToInt(Session["role"], 0);
            }
            set
            {
                Session["role"] = value;
            }
        }


        public int SessionDepart
        {
            get
            {
                return Falcon.Function.ToInt(Session["depart"], 0);
            }
            set
            {
                Session["depart"] = value;
            }
        }

        public bool SessionManager
        {
            get
            {
                return Falcon.Function.ToBoolean(Session["manager"]);
            }
            set
            {
                Session["manager"] = value;
            }
        }

        public string SessionShouquan
        {
            get
            {
                return Session["shouquan"] == null ? null : Session["shouquan"].ToString();
            }
            set
            {
                Session["shouquan"] = value;
            }
        }

        //public string SessionUserName
        //{
        //    get
        //    {
        //        return Session["username"] == null ? "" : Session["username"].ToString();
        //    }
        //    set
        //    {
        //        Session["username"] = value;
        //    }
        //}

        private List<string> listPowerInfo;

        public string getPower(string power)
        {
            if (SessionRole == 0 && SessionUid != 0) return "";
            if (listPowerInfo == null || listPowerInfo.Count == 0)//如果已经注册，则直接调用上次注册的结果
            {
                List<Model.PowerInfo> list = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Model.PowerInfo>>(new BLL.Z_Role().GetModel(SessionRole).power);
                if (list != null)
                {
                    string LocalPath = Request.Url.LocalPath;//"/admin/power/setpower.aspx"
                    LocalPath = LocalPath.Substring(1);//去掉第一个/
                    LocalPath = LocalPath.Substring(LocalPath.IndexOf('/') + 1);//power/setpower.aspx
                    list = (from m in list where m.Url == LocalPath select m).ToList();
                    Model.PowerInfo PowerInfo = list.Count == 1 ? list[0] : null;//匹配出唯一数据
                    if (PowerInfo != null)
                    {
                        listPowerInfo = string.IsNullOrEmpty(PowerInfo.Power) ? new List<string>() : PowerInfo.Power.Split(',').ToList<string>();
                    }
                }
            }
            if (listPowerInfo != null && listPowerInfo.Count != 0)
            {
                return listPowerInfo.Contains(power) ? "" : " no";
            }
            return " no";
        }

    }
}
