﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Model
{
    public class UserInfo
    {
        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// username
        /// </summary>		
        private string _username;
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
        /// <summary>
        /// loginpswd
        /// </summary>		
        private string _loginpswd;
        public string loginpswd
        {
            get { return _loginpswd; }
            set { _loginpswd = value; }
        }
        /// <summary>
        /// userpswd
        /// </summary>		
        private string _userpswd;
        public string userpswd
        {
            get { return _userpswd; }
            set { _userpswd = value; }
        }
        /// <summary>
        /// realname
        /// </summary>		
        private string _realname;
        public string realname
        {
            get { return _realname; }
            set { _realname = value; }
        }
        /// <summary>
        /// idcard
        /// </summary>		
        private string _idcard;
        public string idcard
        {
            get { return _idcard; }
            set { _idcard = value; }
        }
        /// <summary>
        /// email
        /// </summary>		
        private string _email;
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
        /// <summary>
        /// gender
        /// </summary>		
        private int _gender;
        public int gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        /// <summary>
        /// tel
        /// </summary>		
        private string _tel;
        public string tel
        {
            get { return _tel; }
            set { _tel = value; }
        }
        /// <summary>
        /// mobile
        /// </summary>		
        private string _mobile;
        public string mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        /// <summary>
        /// address
        /// </summary>		
        private string _address;
        public string address
        {
            get { return _address; }
            set { _address = value; }
        }
        /// <summary>
        /// addTime
        /// </summary>		
        private DateTime _addtime;
        public DateTime addTime
        {
            get { return _addtime; }
            set { _addtime = value; }
        }
        /// <summary>
        /// remark
        /// </summary>		
        private string _remark;
        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
        /// <summary>
        /// photo
        /// </summary>		
        private string _photo;
        public string photo
        {
            get { return _photo; }
            set { _photo = value; }
        }
        /// <summary>
        /// jbid
        /// </summary>		
        private int _jbid;
        public int jbid
        {
            get { return _jbid; }
            set { _jbid = value; }
        }
        /// <summary>
        /// isDel
        /// </summary>		
        private int _isDel;
        public int isDel
        {
            get { return _isDel; }
            set { _isDel = value; }
        }
        private int _UserOrGS;
        public int UserOrGS
        {
            get { return _UserOrGS; }
            set { _UserOrGS = value; }
        }
        /// <summary>
        /// 银行卡号
        /// </summary>
        public string bankcard { get; set; }

        /// <summary>
        /// 户名
        /// </summary>
        public string bank_uname { get; set; }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string bankname { get; set; }

        /// <summary>
        /// 支付宝账号
        /// </summary>
        public string zhifubao { get; set; }

        public string jbname { get; set; }

        //public List<Model.Select> list_bankname { get; set; }

        public string avatar { get; set; }



        private string _TYSHXYDM;
        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string TYSHXYDM
        {
            get { return _TYSHXYDM; }
            set { _TYSHXYDM = value; }
        }

        private string _GSMC;
        /// <summary>
        /// 公司名称
        /// </summary>
        public string GSMC
        {
            get { return _GSMC; }
            set { _GSMC = value; }
        }

        private int _GSTYPEID;
        /// <summary>
        /// 公司类型编号
        /// </summary>
        public int GSTYPEID
        {
            get { return _GSTYPEID; }
            set { _GSTYPEID = value; }
        }


        private string _GSTYPENAme;
        /// <summary>
        /// 公司类型名称
        /// </summary>
        public string GSTYPENAme
        {
            get { return _GSTYPENAme; }
            set { _GSTYPENAme = value; }
        }


        private string _GSDZ;
        /// <summary>
        /// 公司地址
        /// </summary>
        public string GSDZ
        {
            get { return _GSDZ; }
            set { _GSDZ = value; }
        }


        private string _FDDBR;
        /// <summary>
        /// 法定代表人
        /// </summary>
        public string FDDBR
        {
            get { return _FDDBR; }
            set { _FDDBR = value; }
        }




        private string _ZCZB;
        /// <summary>
        /// 注册资本
        /// </summary>
        public string ZCZB
        {
            get { return _ZCZB; }
            set { _ZCZB = value; }
        }




        private string _CLTIME;
        /// <summary>
        /// 成立时间
        /// </summary>
        public string CLTIME
        {
            get { return _CLTIME; }
            set { _CLTIME = value; }
        }

        private string _YYQX;
        /// <summary>
        /// 营业期限
        /// </summary>
        public string YYQX
        {
            get { return _YYQX; }
            set { _YYQX = value; }
        }


        private string _JYFW;
        /// <summary>
        /// 经营范围
        /// </summary>
        public string JYFW
        {
            get { return _JYFW; }
            set { _JYFW = value; }
        }


        private int _TYPEID;
        /// <summary>
        /// 类型编号
        /// </summary>
        public int TYPEID
        {
            get { return _TYPEID; }
            set { _TYPEID = value; }
        }


        private string _TYPENAME;
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TYPENAME
        {
            get { return _TYPENAME; }
            set { _TYPENAME = value; }
        }

    }
}