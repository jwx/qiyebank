﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Tongzhi
    {
        public int id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string addTime { get; set; }

        public int isEnable { get; set; }

        public int sort { get; set; }

        public int jbid { get; set; }

        public string jbname { get; set; }
    }
}
