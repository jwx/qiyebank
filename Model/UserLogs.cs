﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class UserLogs
    {
        public int id { get; set; }

        public int uid { get; set; }

        public int jbid { get; set; }

        public string remark { get; set; }

        public DateTime addTime { get; set; }

        public string username { get; set; }

        public string realname { get; set; }

        public string jbname { get; set; }
    }
}
