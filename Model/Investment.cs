﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Investmen
    {

        private int _id;

        public int id { get { return _id; } set { _id = value; } }


        private string _AccountNumber;
        /// <summary>
        /// 账户
        /// </summary>
        public string AccountNumber { get { return _AccountNumber; }set { _AccountNumber = value; } }


        private string _AccountName;
        /// <summary>
        /// 户名
        /// </summary>
        public string AccountName { get { return _AccountName; } set { _AccountName = value; } }


        private decimal _TZmoney;
        /// <summary>
        /// 投资金额
        /// </summary>
        public decimal TZmoney { get { return _TZmoney; }set { _TZmoney = value; } }


        private int _moneyAmountID;
        /// <summary>
        /// 金额来源id
        /// </summary>
        public int moneyAmountID { get { return _moneyAmountID; } set { _moneyAmountID = value; } }



        private string _moneyAmountName;
        /// <summary>
        /// 金额来源
        /// </summary>
        public string moneyAmountName { get { return _moneyAmountName; } set { _moneyAmountName = value; } }



        private string _tzTime;
        /// <summary>
        /// 投资时间
        /// </summary>
        public string tzTime { get { return _tzTime; } set { _tzTime = value; } }


        private string _endTime;
        /// <summary>
        /// 到期时间
        /// </summary>
        public string endTime { get { return _endTime; } set { _endTime = value; } }



        private string _remarks;
        /// <summary>
        /// 备注
        /// </summary>
        public string remarks { get { return _remarks; } set { _remarks = value; } }





        private int _jbid;
        /// <summary>
        /// 经办人id
        /// </summary>
        public int jbid { get { return _jbid; } set { _jbid = value; } }



        private decimal _fhmoney;
        /// <summary>
        /// 分红金额
        /// </summary>
        public decimal fhmoney { get { return _fhmoney; } set { _fhmoney = value; } }

        private int _shid;
        /// <summary>
        /// 审核人id
        /// </summary>
        public int shid { get { return _shid; } set { _shid = value; } }



        private int _isDel;
        /// <summary>
        /// 是否报废  0 否 1是
        /// </summary>
        public int isDel { get { return _isDel; } set { _isDel = value; } }


        private string _shTime;
        /// <summary>
        /// 审核时间
        /// </summary>
        public string shTime { get { return _shTime; } set { _shTime = value; } }




        private string _shRemarks;
        /// <summary>
        /// 审核备注
        /// </summary>
        public string shRemarks { get { return _shRemarks; } set { _shRemarks = value; } }


        public string jbname { get; set; }


        private int _Wellid;
        public int Wellid { get { return _Wellid; }set { _Wellid = value; } }



        private int _yearid;
        public int yearid { get { return _yearid; } set { _Wellid = _yearid; } }


        private string _yearName;
        /// <summary>
        /// 年份
        /// </summary>
        public string yearName { get { return _yearName; } set { _yearName = value; } }

    }
}
