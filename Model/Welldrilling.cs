﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Welldrilling
    {

        private int _id;
        public int id { get { return _id; } set { _id = value; } }


        private string _year;
        /// <summary>
        /// 年份
        /// </summary>
        public string year { get { return _year; } set { _year = value; } }


        private int _personalincometax;
        /// <summary>
        /// 个人税
        /// </summary>
        public int personalincometax { get { return _personalincometax; } set { _personalincometax = value; } }



        private int _Managementexpense;
        /// <summary>
        /// 管理费
        /// </summary>
        public int Managementexpense { get { return _Managementexpense; } set { _Managementexpense = value; } }


        private int _other;
        /// <summary>
        /// 其他费
        /// </summary>
        public int other { get { return _other; } set { _other = value; } }


        private int _everydayton;
        /// <summary>
        /// 每日开采吨数
        /// </summary>
        public int everydayton { get { return _everydayton; } set { _everydayton = value; } }



        private int _loss;
        /// <summary>
        /// 每日开采吨数
        /// </summary>
        public int loss { get { return _loss; } set { _loss = value; } }


        private int _aid;
        /// <summary>
        /// 
        /// </summary>
        public int aid { get { return _aid; } set { _aid = value; } }

    }
}
