﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HuoqiZhuanzhang
    {
        public int id { get; set; }

        /// <summary>
        /// 转账用户id
        /// </summary>
        public int uid { get; set; }

        /// <summary>
        /// 转账金额
        /// </summary>
        public decimal jine { get; set; }

        /// <summary>
        /// 转账金额产生的利息
        /// </summary>
        public decimal lixi { get; set; }

        /// <summary>
        /// 转账人取款单id
        /// </summary>
        public int oid_qk { get; set; }

        /// <summary>
        /// 收款人id
        /// </summary>
        public int to_uid { get; set; }

        /// <summary>
        /// 收款人存款单id
        /// </summary>
        public int oid_ck { get; set; }

        /// <summary>
        /// 转账备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 转账时间
        /// </summary>
        public string addTime { get; set; }

        /// <summary>
        /// 付款人真实姓名
        /// </summary>
        public string realname_fukuan { get; set; }

        /// <summary>
        /// 收款人真实姓名
        /// </summary>
        public string realname_shoukuan { get; set; }
    }
}
