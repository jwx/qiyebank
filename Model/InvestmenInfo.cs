﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
  public  class InvestmenInfo
    {
        private int _id;
        public int id { get { return _id; }set { _id = value; } }


        private string _month;
        /// <summary>
        /// 月份
        /// </summary>
        public string Month { get { return _month; }set { _month = value; } }


        private int _everyMonthDay;
        /// <summary>
        /// 每月天数
        /// </summary>
        public int EveryMonthDay { get { return _everyMonthDay; }set { _everyMonthDay = value; } }



        private decimal _thismonthMoney;
        /// <summary>
        /// 当月分红金额
        /// </summary>
        public decimal thisMonthMoney { get { return _thismonthMoney; }set { _thismonthMoney = value; } }


        private int _isinto;
        /// <summary>
        /// 是否转入零钱  0否 1 是
        /// </summary>
        public int isInto { get { return _isinto; }set { _isinto = value; } }


        private int _isOperation;
        /// <summary>
        /// 是否分红 0否 1 是
        /// </summary>
        public int isOperation { get { return _isOperation; }set { _isOperation = value; } }

    }
}
