﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Huoqi
    {
        public int id { get; set; }

        /// <summary>
        /// 存款人id
        /// </summary>
        public int uid { get; set; }

        /// <summary>
        /// 本笔存款剩余金额
        /// </summary>
        public decimal jine { get; set; }

        /// <summary>
        /// 存款的原金额
        /// </summary>
        public decimal yuanjine { get; set; }

        /// <summary>
        /// 活期存款单号
        /// </summary>
        public string danhao { get; set; }

        /// <summary>
        /// 存款还是取款，存款0，取款1
        /// </summary>
        public int cunqukuan { get; set; }

        /// <summary>
        /// 说明当前存取款是否允许报废
        /// </summary>
        public int canBaofei { get; set; }

        /// <summary>
        /// 交易类型，见枚举
        /// </summary>
        public int laiyuan { get; set; }



        /// <summary>
        /// 存款为利息时，存款是否是程序自动结算而来
        /// </summary>
        public int isAutoCalc { get; set; }

        /// <summary>
        /// 本笔存款的年利率
        /// </summary>
        public string lilv { get; set; }

        /// <summary>
        /// 存款单已取出金额产生的总利息。取款单取出时的利息
        /// </summary>
        public decimal zonglixi { get; set; }

        /// <summary>
        /// 存款添加时间
        /// </summary>
        public string addTime { get; set; }

        /// <summary>
        /// 具体存款时间
        /// </summary>
        public string cunkuanTime { get; set; }

        /// <summary>
        /// 结息时间，暂时没用到
        /// </summary>
        public string jxTime { get; set; }

        /// <summary>
        /// 经办人
        /// </summary>
        public int jbid { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string add_remark { get; set; }

        /// <summary>
        /// 报废时间
        /// </summary>
        public string delTime { get; set; }

        /// <summary>
        /// 是否报废
        /// </summary>
        public int delid { get; set; }

        /// <summary>
        /// 报废备注
        /// </summary>
        public string del_remark { get; set; }

        /// <summary>
        /// 单据的json数据，供报废使用
        /// </summary>
        public string del_json { get; set; }

        /// <summary>
        /// 取款单id，仅对存款为利息时有效
        /// </summary>
        public int oid_qk { get; set; }

        /// <summary>
        /// 生成利息的本金,非利息存款本金为0
        /// </summary>
        public decimal benjin { get; set; }

        /// <summary>
        /// 存款人账号
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// 存款人真实姓名
        /// </summary>
        public string realname { get; set; }

        /// <summary>
        /// 存款人手机号码
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// 存款经办人真实姓名
        /// </summary>
        public string jbname { get; set; }

        /// <summary>
        /// 存款报废人真实姓名
        /// </summary>
        public string delname { get; set; }

        /// <summary>
        /// 单据打印扩展，标题
        /// </summary>
        public string title { get; set; }

        /// <summary>
        ///  单据打印扩展，存款金额
        /// </summary>
        public string money { get; set; }

        /// <summary>
        /// 单据打印扩展，op_type
        /// </summary>
        public string op_type { get; set; }

        /// <summary>
        /// 单据打印扩展，金额大写
        /// </summary>
        public string chs_money { get; set; }

        /// <summary>
        /// 单据打印扩展，交易类型，laiyuan
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 单据打印扩展，交易类型，laiyuan
        /// </summary>
        public string lixi { get; set; }

        /// <summary>
        /// 单据剩余金额
        /// </summary>
        public string str_jine { get; set; }


        /// <summary>
        /// 审核id  个人-1  公司默认0
        /// </summary>
        public int ExamineId { get; set; }


        /// <summary>
        /// 审核图片路径
        /// </summary>
        public string ExPath { get; set; }

        /// <summary>
        /// 审核备注
        /// </summary>
        public string ExRemark { get; set; }

        /// <summary>
        /// 企业取款josn
        /// </summary>
        public string qyQK_json { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public int ExStaut { get; set; }
    }

    //public class HuoqiQuKuan
    //{
    //    /// <summary>
    //    /// 存款单id
    //    /// </summary>
    //    public int id { get; set; }

    //    /// <summary>
    //    /// 存款单取款后的剩余金额
    //    /// </summary>
    //    public decimal sy { get; set; }

    //    /// <summary>
    //    /// 存款单取出的金额
    //    /// </summary>
    //    public decimal qc { get; set; }
    //}

    public class HuoqiQuKuan
    {
        public decimal benjinlixi { get; set; }

        public decimal lixi_sum { get; set; }

        public List<Model.HuoqiLixi> list_lixi { get; set; }

        //public List<Model.HuoqiQuKuan> list_qukuan { get; set; }
    }
}
