﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HuoqiLixi
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 存款单id
        /// </summary>
        public int oid_ck { get; set; }

        /// <summary>
        /// 取款单id
        /// </summary>
        public int oid_qk { get; set; }

        /// <summary>
        /// 本次取款
        /// </summary>
        public decimal qukuan { get; set; }

        /// <summary>
        /// 本次取款产生的利息
        /// </summary>
        public decimal lixi { get; set; }
    }
}
