﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Apply
    {
        public int id { get; set; }

        public string pihao { get; set; }

        public int uid { get; set; }

        public decimal jine { get; set; }

        public decimal benjin { get; set; }

        public decimal lixi { get; set; }

        public string addTime { get; set; }

        public string remark { get; set; }

        public int isFukuan { get; set; }

        public int jbid { get; set; }

        public string fk_remark { get; set; }

        public string fk_time { get; set; }

        public string json { get; set; }

        public string realname { get; set; }

        public string jbname { get; set; }

        public string str_isFukuan { get; set; }

        public string bankcard { get; set; }

        public string bank_uname { get; set; }

        public string bankname { get; set; }

        public string zhifubao { get; set; }
    }
}
