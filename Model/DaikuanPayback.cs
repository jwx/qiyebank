﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class DaikuanPayback
    {


        private int _id;
        public int id { get { return _id; } set { _id = value; } }



        private int _dkId;
        /// <summary>
        /// 贷款单据id
        /// </summary>
        public int dkId { get { return _dkId; } set { _dkId = value; } }

        private decimal _jine;
        /// <summary>
        /// 贷款金额
        /// </summary>
        public decimal jine { get { return _jine; } set { _jine = value; } }



        private string _huankuanDate;
        /// <summary>
        /// 应还款日期
        /// </summary>
        public string huankuanDate { get { return _huankuanDate; } set { _huankuanDate = value; } }


        private string _huankuanTime;
        /// <summary>
        /// 客户真实还款日期
        /// </summary>
        public string huankuanTime { get { return _huankuanTime; } set { _huankuanTime = value; } }


        private string _remark;
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get { return _remark; } set { _remark = value; } }


        private decimal _huankuanType;
        /// <summary>
        /// 还款类型，0未还款,1零钱还款，2柜台还款
        /// </summary>
        public decimal huankuanType { get { return _huankuanType; } set { _huankuanType = value; } }



        private decimal _jbid;
        /// <summary>
        /// 经办人id
        /// </summary>
        public decimal jbid { get { return _jbid; } set { _jbid = value; } }

        public string jbname { get; set; }

        public int uid { get; set; }


    }
    public class ListDaikuan
    {
        public string time { get; set; }
        public decimal money { get; set; }
    }
}
