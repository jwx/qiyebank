﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class QYZHUser
    {

        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _TYSHXYDM;
        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string TYSHXYDM
        {
            get { return _TYSHXYDM; }
            set { _TYSHXYDM = value; }
        }

        private string _GSMC;
        /// <summary>
        /// 公司名称
        /// </summary>
        public string GSMC
        {
            get { return _GSMC; }
            set { _GSMC = value; }
        }

        private int _GSTYPEID;
        /// <summary>
        /// 公司类型编号
        /// </summary>
        public int GSTYPEID
        {
            get { return _GSTYPEID; }
            set { _GSTYPEID = value; }
        }


        private string _GSTYPENAme;
        /// <summary>
        /// 公司类型名称
        /// </summary>
        public string GSTYPENAme
        {
            get { return _GSTYPENAme; }
            set { _GSTYPENAme = value; }
        }


        private string _GSDZ;
        /// <summary>
        /// 公司地址
        /// </summary>
        public string GSDZ
        {
            get { return _GSDZ; }
            set { _GSDZ = value; }
        }


        private string _FDDBR;
        /// <summary>
        /// 法定代表人
        /// </summary>
        public string FDDBR
        {
            get { return _FDDBR; }
            set { _FDDBR = value; }
        }




        private string _ZCZB;
        /// <summary>
        /// 注册资本
        /// </summary>
        public string ZCZB
        {
            get { return _ZCZB; }
            set { _ZCZB = value; }
        }




        private string _CLTIME;
        /// <summary>
        /// 成立时间
        /// </summary>
        public string CLTIME
        {
            get { return _CLTIME; }
            set { _CLTIME = value; }
        }

        private string _YYQX;
        /// <summary>
        /// 营业期限
        /// </summary>
        public string YYQX
        {
            get { return _YYQX; }
            set { _YYQX = value; }
        }


        private string _JYFW;
        /// <summary>
        /// 经营范围
        /// </summary>
        public string JYFW
        {
            get { return _JYFW; }
            set { _JYFW = value; }
        }


        private int _TYPEID;
        /// <summary>
        /// 类型编号
        /// </summary>
        public int TYPEID
        {
            get { return _TYPEID; }
            set { _TYPEID = value; }
        }


        private string _TYPENAME;
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TYPENAME
        {
            get { return _TYPENAME; }
            set { _TYPENAME = value; }
        }

        private string _IMGPATH;
        /// <summary>
        /// 图片地址
        /// </summary>
        public string IMGPATH
        {
            get { return _IMGPATH; }
            set { _IMGPATH = value; }
        }



        private int _isDel;
        /// <summary>
        /// 是否删除
        /// </summary>
        public int isDel
        {
            get { return _isDel; }
            set { _isDel = value; }
        }

    }
}
