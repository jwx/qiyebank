﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class ServiceLog
    {
        public int id { get; set; }

        /// <summary>
        /// 日志添加时间
        /// </summary>
        public DateTime addTime { get; set; }

        /// <summary>
        /// 数据库是否备份
        /// </summary>
        public int isBackup { get; set; }

        /// <summary>
        /// 活期单据是否结息
        /// </summary>
        public int huoqiJiexi { get; set; }

        /// <summary>
        /// 定期单据是否转存
        /// </summary>
        public int dingqiZhuancun { get; set; }

        /// <summary>
        /// 贷款还款服务是否执行
        /// </summary>
        public int daikuanHuankuan { get; set; }
    }
}
