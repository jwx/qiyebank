﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Model
{
    public class Reflect
    {
        public static List<string> GetFilter(string model)
        {
            List<string> list = new List<string>();

            try
            {
                object temp = Activator.CreateInstance(Type.GetType("Model." + model, true, true)); //Type.GetType只会在当前程序集中进行类型搜索
                foreach (var one in temp.GetType().GetProperties())
                {
                    list.Add(one.Name);
                }
            }
            catch { }
            return list;
        }
    }

    /// <summary>
    /// 自定义列管理类
    /// </summary>
    public class CustomFilter
    {
        /// <summary>
        /// 默认名称，开发人员设置
        /// </summary>
        public string defName { get; set; }

        /// <summary>
        /// 显示名称，用户自定义设置
        /// </summary>
        public string disName { get; set; }

        /// <summary>
        /// 是否显示该列
        /// </summary>
        public bool isShow { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int sort { get; set; }

        /// <summary>
        /// 默认宽度，0表示不限
        /// </summary>
        public int width { get; set; }
    }

    public class Service<T>
    {
        /// <summary>
        /// 列表数据
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int pc { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public int rc { get; set; }

        /// <summary>
        /// 正常信息提示
        /// </summary>
        public string success { get; set; }

        /// <summary>
        /// 异常信息提示
        /// </summary>
        public string error { get; set; }
    }

    /// <summary>
    /// 用于修改时提示，0失败，非0成功
    /// </summary>
    public class ServiceResult
    {
        public string res { get; set; }
    }

    /// <summary>
    /// 手机端select
    /// </summary>
    public class Select
    {
        public string txt { get; set; }

        public string val { get; set; }
    }

    public class ListView_Mobile
    {
        public int id { get; set; }

        public string title { get; set; }

        public string subTitle { get; set; }

        public string remark { get; set; }
    }

    public class Users_Mobile
    {
        public int id { get; set; }

        public string avatar { get; set; }

        public string realname { get; set; }

        public string username { get; set; }
    }

    public class PicInfo
    {
        public string pic { get; set; }

        public string remark { get; set; }
    }

    public class CheckDT
    {
        public int id { get; set; }

        public string SheetName { get; set; }

        //所有列名用,分割
        public string ColName { get; set; }

        /// <summary>
        /// 0 检测数据是否为空【此处cannull失效】 ;1 int ;2 double; 3 Decimal;4 Datetime;5 检测是否有列名称【此处cannull失效】
        /// </summary>
        public int DType { get; set; }

        public bool cannull { get; set; }
    }

    /// <summary>
    /// dthmlxtree
    /// </summary>
    public class dhtmlxTree
    {
        public int id { get; set; }

        public int open { get; set; }

        public string text { get; set; }

        public int ckchecked { get; set; }//ckchecked为关键字

        public List<dhtmlxTree> item { get; set; }

        public List<UserData> userdata { get; set; }
    }

    /// <summary>
    /// dhtmlxtree
    /// </summary>
    public class UserData
    {
        public int sort { get; set; }
    }

    public class Notification
    {
        public int id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public DateTime addTime { get; set; }

        public int isEnable { get; set; }

        public int sort { get; set; }

        public int jbid { get; set; }

        public string jbname { get; set; }
    }

    public class SubNotification
    {
        public int id { get; set; }

        public int nid { get; set; }

        public int uid { get; set; }

        public int isRead { get; set; }

        public DateTime readTime { get; set; }

        public string realname { get; set; }



        public string title { get; set; }

        public string content { get; set; }

        public DateTime addTime { get; set; }

        public int isEnable { get; set; }

        public int sort { get; set; }

        public int jbid { get; set; }

        public string jbname { get; set; }
    }

    public class NPOI_Cell
    {
        public int rowIndex { get; set; }

        public int columnIndex { get; set; }
    }
}
