﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class SetupPrint
    {
        public int id { get; set; }

        /// <summary>
        /// 打印类型，见枚举
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 系统默认打印代码
        /// </summary>
        public string sysCode { get; set; }

        /// <summary>
        /// 用户自定义打印代码
        /// </summary>
        public string defCode { get; set; }

        /// <summary>
        /// 页面直接调用的用户自定义代码，已经处理好了字段
        /// </summary>
        public string jsCode { get; set; }
    }
}
