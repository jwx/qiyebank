﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Bank
    {
        public int id { get; set; }

        public string name { get; set; }

        public string startNo { get; set; }

        public int sort { get; set; }

        public int jbid { get; set; }

        public string addTime { get; set; }

        public int isDel { get; set; }

        public string jbname { get; set; }
    }
}
