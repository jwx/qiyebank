﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Setup
    {
        /// <summary>
        /// 记录系统设置是否被保存过
        /// </summary>
        public int isSaved { get; set; }

        /// <summary>
        /// 每日备份数据库时间
        /// </summary>
        public int backupTime { get; set; }

        /// <summary>
        /// 存取款操作时是否使用授权码
        /// </summary>
        public int useShouquanma { get; set; }

        /// <summary>
        /// 活期利息是否按天计算,0按月，1按天，2每天
        /// </summary>
        public int hq_byDay { get; set; }

        /// <summary>
        /// 活期利息是否自动结算，只对hq_byDay为0和1时有效
        /// </summary>
        public int hq_autoCalc { get; set; }

        /// <summary>
        /// 每天计算利息的利率
        /// </summary>
        public decimal hq_daylilv { get; set; }

        /// <summary>
        /// 活期自动结算的天数
        /// </summary>
        public int days { get; set; }

        /// <summary>
        /// 活期存款是否后存先取
        /// </summary>
        public int hq_houcunxianqu { get; set; }

        /// <summary>
        /// 活期利率的json数据
        /// </summary>
        public string hq_lilv { get; set; }

        ///// <summary>
        ///// 活期存款是否自动审核
        ///// </summary>
        //public int hqck_autoCk { get; set; }

        ///// <summary>
        ///// 活期取款是否自动审核
        ///// </summary>
        //public int hqqk_autoCk { get; set; }

        /// <summary>
        /// 定期利息是否按天计算
        /// </summary>
        public int dq_byDay { get; set; }

        /// <summary>
        /// 定期存款到期后处理方式_枚举
        /// </summary>
        public int dq_daoqi { get; set; }

        /// <summary>
        /// 定期利率的json数据
        /// </summary>
        public string dq_lilv { get; set; }

        /// <summary>
        /// 定期到期自动转存（天数或月数）
        /// </summary>
        public int dq_zc { get; set; }

        /// <summary>
        /// 定期到期自动转存时的利率
        /// </summary>
        public decimal dq_zclilv { get; set; }


        /// <summary>
        /// 贷款利息是否按天计算
        /// </summary>
        public int dk_byDay { get; set; }

        /// <summary>
        /// 贷款存款到期后处理方式_枚举
        /// </summary>
        public int dk_daoqi { get; set; }

        /// <summary>
        /// 贷款利率的json数据
        /// </summary>
        public string dk_lilv { get; set; }

        /// <summary>
        /// 贷款到期自动转存（天数或月数）
        /// </summary>
        public int dk_zc { get; set; }

        /// <summary>
        /// 贷款到期自动转存时的利率
        /// </summary>
        public decimal dk_zclilv { get; set; }



        /// <summary>
        ///// 定期存款是否自动审核
        ///// </summary>
        //public int dqck_autoCk { get; set; }

        ///// <summary>
        ///// 定期取款是否自动审核
        ///// </summary>
        //public int dqqk_autoCk { get; set; }
    }

    public class LilvRange
    {
        public string name { get; set; }

        public int min { get; set; }

        public int max { get; set; }

        public string unit { get; set; }

        public decimal lilv { get; set; }

        public decimal budaoqi { get; set; }

    }
}
