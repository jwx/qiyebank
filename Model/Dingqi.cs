﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Model
{
    public class Dingqi
    {
        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// uid
        /// </summary>		
        private int _uid;
        public int uid
        {
            get { return _uid; }
            set { _uid = value; }
        }
        /// <summary>
        /// 存款或取款金额
        /// </summary>		
        private decimal _jine;
        public decimal jine
        {
            get { return _jine; }
            set { _jine = value; }
        }
        /// <summary>
        /// 单号
        /// </summary>		
        private string _danhao;
        public string danhao
        {
            get { return _danhao; }
            set { _danhao = value; }
        }
        /// <summary>
        /// 自动转存存款0，人工存款1，取款2
        /// </summary>		
        private int _cunqukuan;
        public int cunqukuan
        {
            get { return _cunqukuan; }
            set { _cunqukuan = value; }
        }
        /// <summary>
        /// 说明当前存取款是否允许报废
        /// </summary>		
        private int _canBaofei;
        public int canBaofei
        {
            get { return _canBaofei; }
            set { _canBaofei = value; }
        }
        /// <summary>
        /// 是否已取款
        /// </summary>		
        private int _hasqukuan;
        public int hasQukuan
        {
            get { return _hasqukuan; }
            set { _hasqukuan = value; }
        }
        /// <summary>
        /// 见枚举
        /// </summary>		
        private int _autoQukuan;
        public int autoQukuan
        {
            get { return _autoQukuan; }
            set { _autoQukuan = value; }
        }
        /// <summary>
        /// 取款方式，见枚举Dingqi_Qukuan
        /// </summary>		
        private int _qukuanType;
        public int qukuanType
        {
            get { return _qukuanType; }
            set { _qukuanType = value; }
        }
        /// <summary>
        /// 存款单或取款单id
        /// </summary>		
        private int _oid;
        public int oid
        {
            get { return _oid; }
            set { _oid = value; }
        }
        /// <summary>
        /// 到期处理方式，见枚举
        /// </summary>		
        private int _daoqi_handler;
        public int daoqi_handler
        {
            get { return _daoqi_handler; }
            set { _daoqi_handler = value; }
        }
        /// <summary>
        /// 存款周期
        /// </summary>		
        private string _zhouqi;
        public string zhouqi
        {
            get { return _zhouqi; }
            set { _zhouqi = value; }
        }
        /// <summary>
        /// 存款利率
        /// </summary>		
        private decimal _lilv;
        public decimal lilv
        {
            get { return _lilv; }
            set { _lilv = value; }
        }
        /// <summary>
        /// 不到期利率
        /// </summary>		
        private decimal _budaoqi;
        public decimal budaoqi
        {
            get { return _budaoqi; }
            set { _budaoqi = value; }
        }
        /// <summary>
        /// 本笔存款产生的利息
        /// </summary>		
        private decimal _lixi;
        public decimal lixi
        {
            get { return _lixi; }
            set { _lixi = value; }
        }
        /// <summary>
        /// addTime
        /// </summary>		
        private string _addtime;
        public string addTime
        {
            get { return _addtime; }
            set { _addtime = value; }
        }
        /// <summary>
        /// 存款时间
        /// </summary>		
        private string _cunkuantime;
        public string cunkuanTime
        {
            get { return _cunkuantime; }
            set { _cunkuantime = value; }
        }
        /// <summary>
        /// 到期时间
        /// </summary>		
        private string _daoqitime;
        public string daoqiTime
        {
            get { return _daoqitime; }
            set { _daoqitime = value; }
        }
        /// <summary>
        /// jbid
        /// </summary>		
        private int _jbid;
        public int jbid
        {
            get { return _jbid; }
            set { _jbid = value; }
        }
        /// <summary>
        /// add_remark
        /// </summary>		
        private string _add_remark;
        public string add_remark
        {
            get { return _add_remark; }
            set { _add_remark = value; }
        }
        /// <summary>
        /// delTime
        /// </summary>		
        private string _deltime;
        public string delTime
        {
            get { return _deltime; }
            set { _deltime = value; }
        }
        /// <summary>
        /// delid
        /// </summary>		
        private int _delid;
        public int delid
        {
            get { return _delid; }
            set { _delid = value; }
        }
        /// <summary>
        /// del_remark
        /// </summary>		
        private string _del_remark;
        public string del_remark
        {
            get { return _del_remark; }
            set { _del_remark = value; }
        }

        public decimal lixi_taxed { get; set; }

        public string username { get; set; }

        public string realname { get; set; }

        public string mobile { get; set; }

        public string jbname { get; set; }

        public string delname { get; set; }

        /// <summary>
        /// 取款单号，供V_Dingqi_Qukuan使用
        /// </summary>
        public string qkdanhao { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string money { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string chs_money { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string nianlilv { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string zonglixi { get; set; }

        /// <summary>
        /// 单据打印扩展
        /// </summary>
        public string qukuanfangshi { get; set; }

        /// <summary>
        /// 手机端扩展
        /// </summary>
        public string str_hasQukuan { get; set; }

        public string str_lixi { get; set; }

        public string str_daoqi_handler { get; set; }

        public Model.Setup modelSetup { get; set; }

        public decimal yuanjine { get; set; }

        public string yuandanhao { get; set; }

        public string lixishui { get; set; }
    }

}