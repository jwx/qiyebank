﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Mobile
    {
        public class Money
        {
            /// <summary>
            /// 活期余额
            /// </summary>
            public string huoqi { get; set; }

            /// <summary>
            /// 定期余额
            /// </summary>
            public string dingqi { get; set; }

            /// <summary>
            /// 总资产
            /// </summary>
            public string all { get; set; }

            /// <summary>
            /// 活期昨日收益
            /// </summary>
            public string huoqi_shouyi { get; set; }

            /// <summary>
            /// 定期昨日收益
            /// </summary>
            public string dingqi_shouyi { get; set; }

            /// <summary>
            /// 系统设置
            /// </summary>
            public Model.Setup modelSetup { get; set; }
        }

        public class ListHuoqi
        {
            public int id { get; set; }

            public string danhao { get; set; }

            public decimal jine { get; set; }

            public decimal yuanjine { get; set; }

            public string cunkuanTime { get; set; }
        }

        public class ListDaikuan
        {
            public int id { get; set; }

            public string danhao { get; set; }

            public decimal jine { get; set; }
             
            public string daikuanTime { get; set; }
        }


        public class ListApply
        {
            public int id { get; set; }
             
            public decimal jine { get; set; }

            public int isFukuan { get; set; }

            public string addTime { get; set; }
        }

        public class ListDingqi
        {
            public int id { get; set; }

            public string danhao { get; set; }

            public decimal jine { get; set; }

            public int cunqukuan { get; set; }

            public string cunkuanTime { get; set; }

            public string daoqiTime { get; set; }
        } 
    }
}
