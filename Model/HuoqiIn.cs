﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class HuoqiIn
    {
        public int id { get; set; }

        public int uid { get; set; }

        public decimal jine { get; set; }

        public string zhifubao { get; set; }

        public string realname { get; set; }

        public string mobile { get; set; }

        public string model_setup { get; set; }

        public string addTime { get; set; }

        public string cunkuanTime { get; set; }

        public string add_remark { get; set; }

        public int ckid { get; set; }

        public string ck_remrak { get; set; }

        public string ckTime { get; set; }

        public int oid { get; set; }

        public string ckname { get; set; }

        public string str_oid { get; set; }

        public string str_jine { get; set; }
    }

    public class HuoqiInInfo
    {
        public string zhifubao { get; set; }

        public string zhifubao_uname { get; set; }

        public string zhifubao_pay { get; set; }

        public string realname { get; set; }

        public string mobile { get; set; }

        public Model.Setup modelSetup { get; set; }
    }
}
