﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Backup
    {
        public int id { get; set; }

        public string dbname { get; set; }

        public string remark { get; set; }

        public string addTime { get; set; }

        public int jbid { get; set; }

        public string jbname { get; set; }
    }
}
