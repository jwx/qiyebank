﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Daikuan
    {
        private int _id;
        public int id { get { return _id; } set { _id = value; } }


        private int _uid;
        /// <summary>
        /// 用户id
        /// </summary>
        public int uid { get { return _uid; } set { _uid = value; } }


        private decimal _jine;
        /// <summary>
        /// 贷款金额
        /// </summary>
        public decimal jine { get { return _jine; } set { _jine = value; } }



        private string _danhao;
        /// <summary>
        /// 单号
        /// </summary>
        public string danhao { get { return _danhao; } set { _danhao = value; } }


        private int _daihuankuan;
        /// <summary>
        /// 贷款1，还款2
        /// </summary>
        public int daihuankuan { get { return _daihuankuan; } set { _daihuankuan = value; } }


        private int _canBaofei;
        /// <summary>
        /// 单据是否允许报废，1允许，0不允许
        /// </summary>
        public int canBaofei { get { return _canBaofei; } set { _canBaofei = value; } }


        private int _huankuanType;
        /// <summary>
        /// 还款类型 见枚举字典，Dic_Daikuan_Type
        /// </summary>
        public int huankuanType { get { return _huankuanType; } set { _huankuanType = value; } }


        private string _zhouqi;
        /// <summary>
        /// 贷款周期
        /// </summary>
        public string zhouqi { get { return _zhouqi; } set { _zhouqi = value; } }



        private decimal _lilv;
        /// <summary>
        /// 贷款利率
        /// </summary>
        public decimal lilv { get { return _lilv; } set { _lilv = value; } }



        private decimal _lixi;
        /// <summary>
        /// 贷款产生的利息
        /// </summary>
        public decimal lixi { get { return _lixi; } set { _lixi = value; } }



        private decimal _benxi;
        /// <summary>
        /// 本金+利息
        /// </summary>
        public decimal benxi { get { return _benxi; } set { _benxi = value; } }




        private decimal _yihuan;
        /// <summary>
        /// 当前贷款已还款金额
        /// </summary>
        public decimal yihuan { get { return _yihuan; } set { _yihuan = value; } }


        private string _addTime;
        /// <summary>
        /// 生成时间
        /// </summary>
        public string addTime { get { return _addTime; } set { _addTime = value; } }

        private string _daikuanTime;
        /// <summary>
        /// 贷款时间
        /// </summary>
        public string daikuanTime { get { return _daikuanTime; } set { _daikuanTime = value; } }




        private string _daoqiTime;
        /// <summary>
        /// 到期时间
        /// </summary>
        public string daoqiTime { get { return _daoqiTime; } set { _daoqiTime = value; } }




        private int _jbid;
        /// <summary>
        /// 经办人id
        /// </summary>
        public int jbid { get { return _jbid; } set { _jbid = value; } }




        private string _add_remark;
        /// <summary>
        /// 备注
        /// </summary>
        public string add_remark { get { return _add_remark; } set { _add_remark = value; } }


        private string _delTime;
        public string delTime { get { return _delTime; } set { _delTime = value; } }


        private string _del_remark;
        public string del_remark { get { return _del_remark; } set { _del_remark = value; } }


        private string _delid;
        public string delid { get { return _delid; } set { _delid = value; } }


        public int isFinish { get; set; }


        public string realname { get; set; }

        public string username { get; set; }

        public string jbname { get; set; }
    }


    public class Daikuan_Mobile
    {
        public decimal jine { get; set; }

        public decimal benxi { get; set; }

        public decimal lixi { get; set; }

        public decimal yihuan { get; set; }

        public List<Model.DaikuanPayback> listPayback { get; set; }
    }
}
