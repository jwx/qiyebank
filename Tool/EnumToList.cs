﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Tool
{
    public class EnumToList
    {
        //ex:  Tool.EnumToList.FillDropDownList(ddlXianqu, (Type)(typeof(Config.Enum.XianQu)));

        /// <summary>
        /// 将枚举转换成下拉框
        /// </summary>
        public static void FillDropDownList(DropDownList ddl, Type enumType)
        {
            ddl.Items.Clear();
            ddl.DataSource = EnumToIList(enumType);
            ddl.DataValueField = "value";
            ddl.DataTextField = "text";
            ddl.DataBind();
        }

        /// <summary>
        /// 将枚举转换成ArrayList
        /// </summary>
        /// <returns></returns>
        private static IList EnumToIList(Type enumType)
        {
            ArrayList list = new ArrayList();
            foreach (int i in Enum.GetValues(enumType))
            {
                ListItem listitem = new ListItem(Enum.GetName(enumType, i), i.ToString());
                list.Add(listitem);
            }
            return list;
        }
    }
}
