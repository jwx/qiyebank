﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    public class TimeHelper
    {
        /// <summary>
        /// 计算两个时间差，考虑天数，2017-2-10和2017-1-11,差0个月，和2017-1-10差1个月
        /// </summary>
        /// <param name="endTime"></param>
        /// <param name="startTime"></param>
        /// <returns></returns>
        public int GetFullMonth(DateTime endTime, DateTime startTime)
        {
            //return (endTime.Year - startTime.Year) * 12 + endTime.Month - startTime.Month 
            return (endTime.Year - startTime.Year) * 12 + endTime.Month - startTime.Month - (endTime.Day - startTime.Day >= 0 ? 0 : 1);
        }

        /// <summary>    
        /// Asp.net 计算两个时间相差的月份数,不考虑天数 具体几号,2017-2-1和2017-1-30,差1个月
        /// </summary>
        /// <param name="endTime"></param>
        /// <param name="startTime"></param>
        /// <returns></returns>
        public int GetPartMonth(DateTime endTime, DateTime startTime)
        {
            //DateTime startTime = DateTime.Parse("2012-7-30"); //起始日期  
            //DateTime endTime = DateTime.Parse("2012-9-01"); //结束日期  
            return (endTime.Year * 12 - (12 - endTime.Month)) - (startTime.Year * 12 - (12 - startTime.Month));
        }
    }
}
