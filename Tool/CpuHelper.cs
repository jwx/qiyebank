﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace Tool
{
    public class CpuHelper
    {
        public static string GetCpuInfo()
        {
            //可以取CPU的序列号。
            string cpuInfo = "";//cpu序列号
            ManagementClass cimobject = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = cimobject.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                cpuInfo += mo.Properties["ProcessorId"].Value.ToString();
                //Response.Write(cpuInfo);
            }
            return cpuInfo;
        }

        public static string GetEncryptedCpuInfo()
        {
            return Falcon.Function.Encrypt(GetCpuInfo());
        }
    }
}
