﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Tool
{
    public class Pager_Sqlite
    {
        /// <summary>
        /// 分页查询，返回List，调用存储过程Query
        /// </summary>
        /// <typeparam name="T">返回的对象</typeparam> 
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param> 
        /// <returns></returns>
        public static List<T> Query<T>(int PageIndex, int PageSize, out int recordCount, string tableName, string fields, string filter, string order)
        {
            List<T> list = new List<T>();
            Database db =Tool.DatabaseHelper.CreateDatabase();
            //string sql = "select " + fields + " from " + tableName + " where " + filter + " order by " + order + " limit @pagesize offset @pagesize*@pageindex";
            string sql = PageSize > 0 ? ("select " + fields + " from " + tableName + " where " + filter + " order by " + order + " offset " + (PageIndex - 1) * PageSize + " rows fetch next " + PageSize + " rows only") : ("select " + fields + " from " + tableName + " where " + filter + " order by " + order);
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex - 1);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);

            recordCount = GetRecordCount(tableName, filter);

            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }

            return list;
        }

        private static int GetRecordCount(string tableName, string where)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select count(0) from " + tableName + " where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
            }
            catch
            {
                return 0;
            }
        }
    }
}
