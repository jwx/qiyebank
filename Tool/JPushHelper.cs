﻿using cn.jpush.api;
using cn.jpush.api.common;
using cn.jpush.api.common.resp;
using cn.jpush.api.push.mode;
using cn.jpush.api.push.notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    public class JPushHelper
    {
        #region 基础信息配置

        private static string app_key = "e5d730b67cc62a9b091f6c68";
        private static string master_secret = "9d0a23de93fb71f460de38b5";

        #endregion

        public static void PushMessage(string[] tags, string title, string msg_content, Dictionary<string, string> dic_extra, ref string msg)
        {
            PushPayload pushPayload = new PushPayload();
            pushPayload.platform = Platform.all();
            pushPayload.audience = tags.Length == 0 ? Audience.all() : Audience.s_tag(tags);

            Notification notification = new Notification();
            notification.AndroidNotification = new AndroidNotification().setAlert(msg_content);//.setBadge(5).setSound("happy").AddExtra("from", "JPush"); 

            if (!string.IsNullOrEmpty(title))
            {
                notification.AndroidNotification.setTitle(title);
            }
            foreach (KeyValuePair<string, string> dic in dic_extra)
            {
                notification.AndroidNotification.AddExtra(dic.Key, dic.Value);
            }
            pushPayload.notification = notification;

            try
            {
                JPushClient client = new JPushClient(app_key, master_secret);
                var result = client.SendPush(pushPayload);
                var apiResult = client.getReceivedApi(result.msg_id.ToString());
                //var apiResultv3 = client.getReceivedApi_v3(result.msg_id.ToString());
                //var queryResultWithV2 = client.getReceivedApi("1739302794");
                //var querResultWithV3 = client.getReceivedApi_v3("1739302794");
            }
            catch (APIRequestException e)
            {
                msg += "Error response from JPush server. Should review and fix it. ";
                msg += "HTTP Status: " + e.Status;
                msg += "Error Code: " + e.ErrorCode;
                msg += "Error Message: " + e.ErrorMessage;
            }
            catch (APIConnectionException e)
            {
                msg += e.Message;
            }
        }

        ///// <summary>
        ///// 所有平台，所有设备，内容为 ALERT 的通知
        ///// </summary>
        ///// <param name="Alert"></param>
        ///// <returns></returns>
        //public PushPayload PushObject_All_All_Alert(string alert)
        //{
        //    PushPayload pushPayload = new PushPayload();
        //    pushPayload.platform = Platform.all();
        //    pushPayload.audience = Audience.all();
        //    pushPayload.notification = new Notification().setAlert(alert);
        //    return pushPayload;
        //}

        ///// <summary>
        ///// 所有平台，推送目标是别名为Alias，内容为Alert的通知
        ///// </summary>
        ///// <param name="Alert"></param>
        ///// <returns></returns>
        //public PushPayload PushObject_all_alias_alert(string alias, string alert)
        //{
        //    PushPayload pushPayload_alias = new PushPayload();
        //    pushPayload_alias.platform = Platform.android();
        //    pushPayload_alias.audience = Audience.s_alias(alias);
        //    pushPayload_alias.notification = new Notification().setAlert(alert);
        //    return pushPayload_alias;
        //}

        ///// <summary>
        ///// 平台是 Android，目标是tag为Tag的设备，内容是Alert，并且标题为Title的通知
        ///// </summary>
        ///// <param name="Tag"></param>
        ///// <param name="Alert"></param>
        ///// <param name="Title"></param>
        ///// <returns></returns>
        //public PushPayload PushObject_Android_Tag_AlertWithTitle(string tag, string alert, string title)
        //{
        //    PushPayload pushPayload = new PushPayload();
        //    pushPayload.platform = Platform.android();
        //    pushPayload.audience = Audience.s_tag(tag);
        //    pushPayload.notification = Notification.android(alert, title);
        //    return pushPayload;
        //}

        ////"tag1", "tag2"
        //public PushPayload PushObject_ios_audienceMore_messageWithExtras(string[] tags, string msg_content, Dictionary<string, string> dic_extra)
        //{
        //    PushPayload pushPayload = new PushPayload();
        //    pushPayload.platform = Platform.android_ios();
        //    pushPayload.audience = tags.Length == 0 ? Audience.all() : Audience.s_tag(tags);
        //    Message message = Message.content(msg_content);
        //    foreach (KeyValuePair<string, string> dic in dic_extra)
        //    {
        //        message.AddExtras(dic.Key, dic.Value);
        //    }
        //    pushPayload.message = message;
        //    return pushPayload;
        //}
    }
}
