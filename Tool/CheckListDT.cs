﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool
{
    public class CheckListDT
    {
        /// <summary>
        ///<para>循环ListModel 根据每个 Model.CheckDT 检查传入的 ListDataTable 的指定DataTable 是否都能按照Model.CheckDT给出的条件通过验证。</para>
        ///<para>其中请注意：</para>
        ///<para> ListModel 其中Model.CheckDT 中 DType 的值及其检查方式如下列：</para>
        /// <para>0 检测数据是否为空【此处cannull失效】 ;</para>
        /// <para>1 int ;</para>
        /// <para>2 double; </para>
        /// <para>3 Decimal;</para>
        /// <para>4 Datetime;</para>
        /// <para>5 检测是否有列名称【此处cannull失效】</para>
        /// </summary>
        /// <param name="listExcelDT">ListDataTable</param>
        /// <param name="listCheckDT">
        /// ListModel 其中Model.CheckDT 中 DType 的值及其检查方式如下列：
        /// 0 检测数据是否为空【此处cannull失效】 ;
        /// 1 int ;
        /// 2 double; 
        /// 3 Decimal;
        /// 4 Datetime;
        /// 5 检测是否有列名称【此处cannull失效】
        /// </param>
        /// <param name="ckall">有错误即返回还是全部验证完成才返回</param>
        /// <param name="msg">返回错误信息</param>
        /// <returns>是否通过验证</returns>
        public static bool CheckLDT(List<DataTable> listExcelDT, List<Model.CheckDT> listCheckDT, bool ckall, out string msg)
        {
            bool isok = true, allok = true;
            msg = "";
            string ret = string.Empty;
            foreach (Model.CheckDT one in listCheckDT)
            {
                string SheetName = one.SheetName;
                DataTable dt = (from m in listExcelDT where m.TableName.Replace("$", "") == SheetName select m).FirstOrDefault();
                if (dt != null)
                {
                    if (one.DType == 0)
                    {
                        isok = DTColNotNULL(dt, one.SheetName, one.ColName, out ret);
                        allok = (allok && isok);
                        msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else if (one.DType == 1)
                    {
                        isok = DTColNotInBZ_Int(dt, one.SheetName, one.ColName, one.cannull, out ret);
                        allok = (allok && isok);
                        msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else if (one.DType == 2)
                    {
                        isok = DTColNotInBZ_Doublel(dt, one.SheetName, one.ColName, one.cannull, out ret);
                        allok = (allok && isok); msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else if (one.DType == 3)
                    {
                        isok = DTColNotInBZ_Decimal(dt, one.SheetName, one.ColName, one.cannull, out ret);
                        allok = (allok && isok);
                        msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else if (one.DType == 4)
                    {
                        isok = DTColNotInBZ_DateTime(dt, one.SheetName, one.ColName, one.cannull, out ret);
                        allok = (allok && isok);
                        msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else if (one.DType == 5)
                    {
                        isok = CheckColNameExist(dt, one.SheetName, one.ColName, out ret);
                        allok = (allok && isok);
                        msg += ret;
                        if (!ckall)
                        {
                            if (!allok)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        msg += ("检测条件有误！");
                        allok = false;
                    }
                }
                else
                {
                    msg += ("未查询到Sheet名为" + one.SheetName + "的工作表！");
                    return false;
                }
            }
            return allok;
        }
        /// <summary>
        /// 检查DataTable中是否存在指定列
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="SheetName">Sheet名称</param>
        /// <param name="colnames">列名称</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool CheckColNameExist(DataTable dt, string SheetName, string colnames, out string msg)
        {
            bool isok = true;
            msg = string.Empty;
            string[] cols = colnames.Split(',');
            foreach (string one in cols)
            {
                if (!dt.Columns.Contains(one))
                {
                    isok = false; msg += ("Sheet[" + SheetName + "]不存在名为【" + one + "】的列！<br/>");
                }
            }
            return isok;
        }
        /// <summary>
        /// 检测datatable数据 指定列数据不能为空
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="colnames">列名称（用,分隔开）</param>
        /// <param name="canull">数据能否为空</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool DTColNotNULL(DataTable dt, string SheetName, string colnames, out string msg)
        {
            msg = string.Empty;
            bool hasnull = true;
            string[] cols = colnames.Split(',');
            hasnull = CheckColNameExist(dt, SheetName, colnames, out msg);
            if (hasnull)
            {
                if (dt.Rows.Count != 0)
                {
                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        i++;
                        foreach (string one in cols)
                        {
                            if (row[one].ToString().Trim().Length == 0) { hasnull = false; msg += ("{Sheet:" + SheetName + "}第 " + i + " 行，[" + one + "]不能为空！<br/>"); }
                        }
                    }
                }
            }
            return hasnull;
        }
        /// <summary>
        /// 检测datatable数据 能否转换成Decimal类型数据
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="colnames">列名称（用,分隔开）</param>
        /// <param name="canull">数据能否为空</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool DTColNotInBZ_Decimal(DataTable dt, string SheetName, string colnames, bool canull, out string msg)
        {
            msg = string.Empty;
            bool ret_bz = true;
            string[] cols = colnames.Split(',');
            ret_bz = CheckColNameExist(dt, SheetName, colnames, out msg);
            if (ret_bz)
            {
                if (dt.Rows.Count != 0)
                {
                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        i++;
                        foreach (string one in cols)
                        {
                            string value = row[one].ToString().Trim();
                            try
                            {
                                if (canull)
                                {
                                    if (value.Length == 0) { } else { Decimal.Parse(value); }
                                }
                                else
                                {
                                    Decimal.Parse(value);
                                }
                            }
                            catch
                            {
                                ret_bz = false; msg += ("{Sheet:" + SheetName + "}第 " + i + " 行，[" + one + "]数据有误，请检查！<br/>");
                            }
                        }
                    }
                }
            }
            return ret_bz;
        }
        /// <summary>
        /// 检测datatable数据 能否转换成double类型数据
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="colnames">列名称（用,分隔开）</param>
        /// <param name="canull">数据能否为空</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool DTColNotInBZ_Doublel(DataTable dt, string SheetName, string colnames, bool canull, out string msg)
        {
            msg = string.Empty;
            bool ret_bz = true;
            string[] cols = colnames.Split(',');
            ret_bz = CheckColNameExist(dt, SheetName, colnames, out msg);
            if (ret_bz)
            {
                if (dt.Rows.Count != 0)
                {
                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        i++;
                        foreach (string one in cols)
                        {
                            string value = row[one].ToString().Trim();
                            try
                            {
                                if (canull)
                                {
                                    if (value.Length == 0) { } else { Double.Parse(value); }
                                }
                                else
                                {
                                    Decimal.Parse(value);
                                }
                            }
                            catch
                            {
                                ret_bz = false; msg += ("{Sheet:" + SheetName + "}第 " + i + " 行，[" + one + "]数据有误，请检查！<br/>");
                            }
                        }
                    }
                }
            }
            return ret_bz;
        }
        /// <summary>
        /// 检测datatable数据 能否转换成Int类型数据
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="colnames">列名称（用,分隔开）</param>
        /// <param name="canull">数据能否为空</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool DTColNotInBZ_Int(DataTable dt, string SheetName, string colnames, bool canull, out string msg)
        {
            msg = string.Empty;
            bool ret_bz = true;
            string[] cols = colnames.Split(',');
            ret_bz = CheckColNameExist(dt, SheetName, colnames, out msg);
            if (ret_bz)
            {
                if (dt.Rows.Count != 0)
                {
                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        i++;
                        foreach (string one in cols)
                        {
                            string value = row[one].ToString().Trim();
                            try
                            {
                                if (canull)
                                {
                                    if (value.Length == 0) { } else { Int32.Parse(value); }
                                }
                                else
                                {
                                    Int32.Parse(value);
                                }
                            }
                            catch
                            {
                                ret_bz = false; msg += ("{Sheet:" + SheetName + "}第 " + i + " 行，[" + one + "]数据有误，请检查！<br/>");
                            }
                        }
                    }
                }
            }
            return ret_bz;
        }
        /// <summary>
        /// 检测datatable数据 能否转换成DateTime类型数据
        /// </summary>
        /// <param name="dt">数据源</param>
        /// <param name="colnames">列名称（用,分隔开）</param>
        /// <param name="canull">数据能否为空</param>
        /// <param name="msg">返回的错误信息</param>
        /// <returns>是否全部检测通过</returns>
        public static bool DTColNotInBZ_DateTime(DataTable dt, string SheetName, string colnames, bool canull, out string msg)
        {
            msg = string.Empty;
            bool ret_bz = true;
            string[] cols = colnames.Split(',');
            ret_bz = CheckColNameExist(dt, SheetName, colnames, out msg);
            if (ret_bz)
            {
                if (dt.Rows.Count != 0)
                {
                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        i++;
                        foreach (string one in cols)
                        {
                            string value = row[one].ToString().Trim();
                            try
                            {
                                if (canull)
                                {
                                    if (value.Length == 0) { } else { DateTime.Parse(value); }
                                }
                                else
                                {
                                    DateTime.Parse(value);
                                }
                            }
                            catch
                            {
                                ret_bz = false; msg += ("{Sheet:" + SheetName + "}第 " + i + " 行，[" + one + "]数据有误，请检查！<br/>");
                            }
                        }
                    }
                }
            }
            return ret_bz;
        }

    }
}
