﻿using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Tool
{
    public class ExcelHelper
    {
        #region 公共方法

        /// <summary>
        /// 读取Excel文件为DataTable合集
        /// </summary>
        /// <param name="strFileName">Excel文件路径，网站路径或服务器全路径均可</param>
        /// <param name="firstRowIndex">读取的表头所在索引，表头默认从0开始</param>
        /// <returns></returns>
        public static List<DataTable> ExcelToDataTable(string strFileName, int firstRowIndex)
        {
            List<DataTable> listDataTable = new List<DataTable>();

            if (!strFileName.Contains(":\\"))//判断是否为服务器全路径
            {
                strFileName = System.Web.HttpContext.Current.Server.MapPath(strFileName);
            }

            string ext = System.IO.Path.GetExtension(strFileName).ToLower();
            if (ext == ".xls")
            {
                HSSFWorkbook workbook;
                using (FileStream file = new FileStream(strFileName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(file);
                }
                HSSFFormulaEvaluator e = new HSSFFormulaEvaluator(workbook);
                for (int sheetIndex = 0; sheetIndex < workbook.NumberOfSheets; sheetIndex++)
                {
                    DataTable dt = new DataTable();
                    ISheet sheet = workbook.GetSheetAt(sheetIndex);
                    //System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                    sheet.ForceFormulaRecalculation = true;
                    IRow headerRow = sheet.GetRow(firstRowIndex);
                    if (headerRow == null) continue;
                    int cellCount = headerRow.LastCellNum;

                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                        dt.Columns.Add(cell.ToString());
                    }

                    for (int i = (firstRowIndex + 1); i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        if (row != null)
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int j = row.FirstCellNum; j < cellCount; j++)
                            {
                                if (row.GetCell(j) != null)
                                    dataRow[j] = e.EvaluateInCell(row.GetCell(j));
                            }
                            dt.Rows.Add(dataRow);
                        }
                    }
                    dt.TableName = sheet.SheetName;
                    listDataTable.Add(dt);
                }
            }
            else if (ext == ".xlsx")
            {
                XSSFWorkbook workbook;
                using (FileStream file = new FileStream(strFileName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new XSSFWorkbook(file);
                }
                XSSFFormulaEvaluator e = new XSSFFormulaEvaluator(workbook);
                for (int sheetIndex = 0; sheetIndex < workbook.NumberOfSheets; sheetIndex++)
                {
                    DataTable dt = new DataTable();
                    ISheet sheet = workbook.GetSheetAt(sheetIndex);
                    //System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                    sheet.ForceFormulaRecalculation = true;
                    IRow headerRow = sheet.GetRow(firstRowIndex);
                    if (headerRow == null) continue;
                    int cellCount = headerRow.LastCellNum;

                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                        dt.Columns.Add(cell.ToString());
                    }

                    for (int i = (firstRowIndex + 1); i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        if (row != null)
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int j = row.FirstCellNum; j < cellCount; j++)
                            {
                                if (row.GetCell(j) != null)

                                    dataRow[j] = e.EvaluateInCell(row.GetCell(j));
                            }
                            dt.Rows.Add(dataRow);
                        }
                    }
                    dt.TableName = sheet.SheetName;
                    listDataTable.Add(dt);
                }
            }
            return listDataTable;
        }

        /// <summary>
        /// 导出Excel文件，用于Web导出
        /// </summary>
        /// <param name="strFileName">导出时默认的Excel文件名，没有后缀默认.xlsx</param>
        /// <param name="listDataTable">DataTable数据合集</param>
        /// <param name="listSheetName">DataTable对应的Sheet名字</param>
        /// <param name="listColumnName">每个Sheet对应的列名数组，格式为new string[]{"显示名称|字段名称","显示名称|字段名称"}</param>
        public static void DataTableToExcel(string strFileName, List<DataTable> listDataTable, List<string> listSheetName, List<string[]> listColumnName)
        {
            strFileName = strFileName.ToLower();
            if (!(strFileName.EndsWith(".xls") || strFileName.EndsWith(".xlsx")))
            {
                strFileName += ".xlsx";
            }

            // 设置编码和附件格式
            HttpContext curContext = HttpContext.Current;
            curContext.Response.ContentType = "application/vnd.ms-excel";
            curContext.Response.ContentEncoding = Encoding.UTF8;
            curContext.Response.Charset = "";

            //注：分浏览器进行编码（IE必须编码，FireFox不能编码，Chrome可编码也可不编码）
            strFileName = curContext.Request.UserAgent.ToLower().IndexOf("firefox", System.StringComparison.Ordinal) > 0 ? strFileName : HttpUtility.UrlEncode(strFileName, Encoding.UTF8);
            curContext.Response.AppendHeader("Content-Disposition", "attachment;filename=" + strFileName);
            curContext.Response.BinaryWrite(strFileName.EndsWith(".xls") ? Export_Xls(listDataTable, listSheetName, listColumnName).ToArray() : Export_Xlsx(listDataTable, listSheetName, listColumnName).ToArray());
            curContext.Response.End();
        }

        /// <summary>
        /// 根据模板读取数据并导出Excel文件，用于Web导出，适用于复杂表头的Excel导出
        /// </summary>
        /// <param name="strTemplateName">Excel模板文件路径，网站路径或服务器全路径均可</param>
        /// <param name="listDataTable">DataTable数据合集</param>
        /// <param name="listSheetCell">DataTable对应的Sheet中数据区域起始的单元格所在行和列，CellIndex从1开始计算</param>
        public static void DataTableToExcel_Template(string strTemplateName, List<DataTable> listDataTable, List<Model.NPOI_Cell> listSheetCell)
        {
            if (!strTemplateName.Contains(":\\"))//判断是否为服务器全路径
            {
                strTemplateName = System.Web.HttpContext.Current.Server.MapPath(strTemplateName);
            }

            MemoryStream stream = new MemoryStream();
            string ext = System.IO.Path.GetExtension(strTemplateName).ToLower();
            if (ext == ".xls")
            {
                HSSFWorkbook workbook;
                using (FileStream file = new FileStream(strTemplateName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(file);
                }
                int index = 0;
                foreach (DataTable dtSource in listDataTable)
                {
                    ISheet sheet = workbook.GetSheetAt(index);
                    ICellStyle dateStyle = workbook.CreateCellStyle();
                    IDataFormat format = workbook.CreateDataFormat();
                    dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");

                    int rowIndex = listSheetCell[index].rowIndex - 1;
                    rowIndex = rowIndex < 0 ? 0 : rowIndex;
                    int columnIndex = listSheetCell[index].columnIndex - 1;
                    columnIndex = columnIndex < 0 ? 0 : columnIndex;
                    foreach (DataRow row in dtSource.Rows)
                    {
                        #region 填充内容
                        IRow dataRow = sheet.CreateRow(rowIndex);
                        rowIndex++;
                        foreach (DataColumn column in dtSource.Columns)
                        {
                            ICell newCell = dataRow.CreateCell(column.Ordinal + columnIndex);

                            string drValue = row[column].ToString();

                            switch (column.DataType.ToString())
                            {
                                case "System.String"://字符串类型
                                    newCell.SetCellValue(drValue);
                                    break;
                                case "System.DateTime"://日期类型
                                    DateTime dateV;
                                    DateTime.TryParse(drValue, out dateV);
                                    newCell.SetCellValue(dateV);

                                    newCell.CellStyle = dateStyle;//格式化显示
                                    break;
                                case "System.Boolean"://布尔型
                                    bool boolV = false;
                                    bool.TryParse(drValue, out boolV);
                                    newCell.SetCellValue(boolV);
                                    break;
                                case "System.Int16"://整型
                                case "System.Int32":
                                case "System.Int64":
                                case "System.Byte":
                                    int intV = 0;
                                    int.TryParse(drValue, out intV);
                                    newCell.SetCellValue(intV);
                                    break;
                                case "System.Single":
                                case "System.Decimal"://浮点型
                                case "System.Double":
                                    double doubV = 0;
                                    double.TryParse(drValue, out doubV);
                                    newCell.SetCellValue(doubV);
                                    break;
                                case "System.DBNull"://空值处理
                                    newCell.SetCellValue("");
                                    break;
                                default:
                                    newCell.SetCellValue("");
                                    break;
                            }
                        }
                        #endregion
                    }
                    index++;
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    workbook.Write(ms);
                    ms.Flush();
                    ms.Position = 0;
                    stream = ms;
                }
            }
            else if (ext == ".xlsx")
            {
                XSSFWorkbook workbook;
                using (FileStream file = new FileStream(strTemplateName, FileMode.Open, FileAccess.Read))
                {
                    workbook = new XSSFWorkbook(file);
                }
                int index = 0;
                foreach (DataTable dtSource in listDataTable)
                {
                    ISheet sheet = workbook.GetSheetAt(index);
                    ICellStyle dateStyle = workbook.CreateCellStyle();
                    IDataFormat format = workbook.CreateDataFormat();
                    dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");

                    int rowIndex = listSheetCell[index].rowIndex - 1;
                    rowIndex = rowIndex < 0 ? 0 : rowIndex;
                    int columnIndex = listSheetCell[index].columnIndex - 1;
                    foreach (DataRow row in dtSource.Rows)
                    {
                        #region 填充内容
                        IRow dataRow = sheet.CreateRow(rowIndex);
                        rowIndex++;
                        foreach (DataColumn column in dtSource.Columns)
                        {
                            ICell newCell = dataRow.CreateCell(column.Ordinal + columnIndex);

                            string drValue = row[column].ToString();

                            switch (column.DataType.ToString())
                            {
                                case "System.String"://字符串类型
                                    newCell.SetCellValue(drValue);
                                    break;
                                case "System.DateTime"://日期类型
                                    DateTime dateV;
                                    DateTime.TryParse(drValue, out dateV);
                                    newCell.SetCellValue(dateV);

                                    newCell.CellStyle = dateStyle;//格式化显示
                                    break;
                                case "System.Boolean"://布尔型
                                    bool boolV = false;
                                    bool.TryParse(drValue, out boolV);
                                    newCell.SetCellValue(boolV);
                                    break;
                                case "System.Int16"://整型
                                case "System.Int32":
                                case "System.Int64":
                                case "System.Byte":
                                    int intV = 0;
                                    int.TryParse(drValue, out intV);
                                    newCell.SetCellValue(intV);
                                    break;
                                case "System.Single":
                                case "System.Decimal"://浮点型
                                case "System.Double":
                                    double doubV = 0;
                                    double.TryParse(drValue, out doubV);
                                    newCell.SetCellValue(doubV);
                                    break;
                                case "System.DBNull"://空值处理
                                    newCell.SetCellValue("");
                                    break;
                                default:
                                    newCell.SetCellValue("");
                                    break;
                            }
                        }
                        #endregion
                    }
                    index++;
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    workbook.Write(ms);
                    ms.Flush();
                    stream = ms;
                }
            }

            HttpContext curContext = HttpContext.Current;
            curContext.Response.ContentType = "application/vnd.ms-excel";
            curContext.Response.ContentEncoding = Encoding.UTF8;
            curContext.Response.Charset = "";

            string strFileName = System.IO.Path.GetFileName(strTemplateName);
            //注：分浏览器进行编码（IE必须编码，FireFox不能编码，Chrome可编码也可不编码）
            strFileName = curContext.Request.UserAgent.ToLower().IndexOf("firefox", System.StringComparison.Ordinal) > 0 ? strFileName : HttpUtility.UrlEncode(strFileName, Encoding.UTF8);
            curContext.Response.AppendHeader("Content-Disposition", "attachment;filename=" + strFileName);
            curContext.Response.BinaryWrite(stream.ToArray());
            curContext.Response.End();
        }


        ///// <summary>
        ///// DataTable导出到Excel文件，并保存到服务器
        ///// </summary>
        ///// <param name="dtSource">源DataTable</param>
        ///// <param name="strHeaderText">表头文本</param>
        ///// <param name="strFileName">保存位置</param>
        //private static void Export(DataTable dtSource, string strHeaderText, string strFileName)
        //{
        //    using (MemoryStream ms = Export(new List<DataTable>() { dtSource }, strHeaderText))
        //    {
        //        using (FileStream fs = new FileStream(strFileName, FileMode.Create, FileAccess.Write))
        //        {
        //            byte[] data = ms.ToArray();
        //            fs.Write(data, 0, data.Length);
        //            fs.Flush();
        //        }
        //    }
        //}

        #endregion

        #region 私有方法

        /// <summary>
        /// 导出到Excel2003的MemoryStream
        /// </summary>
        /// <param name="ListDataTable"></param>
        /// <param name="listSheetName"></param>
        /// <param name="listColumnName"></param>
        /// <returns></returns>
        private static MemoryStream Export_Xls(List<DataTable> ListDataTable, List<string> listSheetName, List<string[]> listColumnName)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();

            #region 右击文件 属性信息
            {
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "临沂精益软件有限公司";
                workbook.DocumentSummaryInformation = dsi;

                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Author = "临沂精益软件有限公司"; //填加xls文件作者信息
                si.ApplicationName = "临沂精益软件有限公司"; //填加xls文件创建程序信息
                si.LastAuthor = "临沂精益软件有限公司"; //填加xls文件最后保存者信息
                si.Comments = "临沂精益软件有限公司"; //填加xls文件作者信息
                si.Title = "临沂精益软件有限公司"; //填加xls文件标题信息 

                si.Subject = "临沂精益软件有限公司";//填加文件主题信息
                si.CreateDateTime = DateTime.Now;
                workbook.SummaryInformation = si;
            }
            #endregion

            int sheetIndex = 0;
            foreach (System.Data.DataTable dtSource in ListDataTable)
            {
                ISheet sheet = workbook.CreateSheet(listSheetName[sheetIndex]);

                ICellStyle dateStyle = workbook.CreateCellStyle();
                IDataFormat format = workbook.CreateDataFormat();
                dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");

                ////取得列宽
                ////int[] arrColWidth = new int[dtSource.Columns.Count];
                //int[] arrColWidth = new int[listColumnName[sheetIndex].Length];
                //foreach (DataColumn item in dtSource.Columns)
                //{
                //    arrColWidth[item.Ordinal] = Encoding.GetEncoding(936).GetBytes(item.ColumnName.ToString()).Length;
                //}
                //for (int i = 0; i < dtSource.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dtSource.Columns.Count; j++)
                //    {
                //        int intTemp = Encoding.GetEncoding(936).GetBytes(dtSource.Rows[i][j].ToString()).Length;
                //        if (intTemp > arrColWidth[j])
                //        {
                //            arrColWidth[j] = intTemp;
                //        }
                //    }
                //}
                int rowIndex = 0;
                foreach (DataRow row in dtSource.Rows)
                {
                    int columnIndex = 0;
                    List<string> listColumnHeader = new List<string>();//显示名称

                    #region 新建表，填充表头，填充列头，样式
                    if (rowIndex == 65535 || rowIndex == 0)
                    {
                        if (rowIndex != 0)
                        {
                            sheet = workbook.CreateSheet();
                        }

                        //#region 表头及样式
                        //{
                        //    if (!string.IsNullOrEmpty(strHeaderText))
                        //    {
                        //        IRow headerRow = sheet.CreateRow(rowIndex);
                        //        rowIndex++;
                        //        headerRow.HeightInPoints = 25;
                        //        headerRow.CreateCell(0).SetCellValue(strHeaderText);

                        //        ICellStyle headStyle = workbook.CreateCellStyle();
                        //        headStyle.Alignment = HorizontalAlignment.Center;
                        //        IFont font = workbook.CreateFont();
                        //        font.FontHeightInPoints = 20;
                        //        font.Boldweight = 700;
                        //        headStyle.SetFont(font);
                        //        headerRow.GetCell(0).CellStyle = headStyle;
                        //        sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, dtSource.Columns.Count - 1));
                        //    }
                        //}
                        //#endregion

                        #region 列头及样式
                        {
                            IRow headerRow = sheet.CreateRow(rowIndex);
                            rowIndex++;
                            ICellStyle headStyle = workbook.CreateCellStyle();
                            headStyle.Alignment = HorizontalAlignment.Center;
                            IFont font = workbook.CreateFont();
                            font.FontHeightInPoints = 10;
                            font.Boldweight = 700;
                            headStyle.SetFont(font);

                            //foreach (DataColumn column in dtSource.Columns)
                            //{
                            //    //headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                            //    headerRow.CreateCell(column.Ordinal).SetCellValue(listColumnName[sheetIndex][columnIndex]);
                            //    columnIndex++;
                            //    headerRow.GetCell(column.Ordinal).CellStyle = headStyle;

                            //    //设置列宽
                            //    //sheet.SetColumnWidth(column.Ordinal, (arrColWidth[column.Ordinal] + 1) * 256);
                            //}

                            foreach (string one in listColumnName[sheetIndex])
                            {
                                string[] columnInfo = listColumnName[sheetIndex][columnIndex].Split('|');
                                listColumnHeader.Add(columnInfo[1]);
                                headerRow.CreateCell(columnIndex).SetCellValue(columnInfo[0]);
                                headerRow.GetCell(columnIndex).CellStyle = headStyle;
                                columnIndex++;
                            }

                        }
                        #endregion
                    }
                    #endregion

                    #region 填充内容
                    IRow dataRow = sheet.CreateRow(rowIndex);
                    rowIndex++;
                    //foreach (DataColumn column in dtSource.Columns)
                    columnIndex = 0;
                    //foreach (string one in listColumnName[sheetIndex])
                    for (int i = 0; i < listColumnName[sheetIndex].Length; i++)
                    {
                        ICell newCell = dataRow.CreateCell(columnIndex);

                        string colName = listColumnHeader[i];
                        string drValue = row[colName].ToString();

                        switch (dtSource.Columns[colName].DataType.ToString())
                        {
                            case "System.String"://字符串类型
                                newCell.SetCellValue(drValue);
                                break;
                            case "System.DateTime"://日期类型
                                DateTime dateV;
                                DateTime.TryParse(drValue, out dateV);
                                newCell.SetCellValue(dateV);

                                newCell.CellStyle = dateStyle;//格式化显示
                                break;
                            case "System.Boolean"://布尔型
                                bool boolV = false;
                                bool.TryParse(drValue, out boolV);
                                newCell.SetCellValue(boolV);
                                break;
                            case "System.Int16"://整型
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Byte":
                                int intV = 0;
                                int.TryParse(drValue, out intV);
                                newCell.SetCellValue(intV);
                                break;
                            case "System.Single":
                            case "System.Decimal"://浮点型
                            case "System.Double":
                                double doubV = 0;
                                double.TryParse(drValue, out doubV);
                                newCell.SetCellValue(doubV);
                                break;
                            case "System.DBNull"://空值处理
                                newCell.SetCellValue("");
                                break;
                            default:
                                newCell.SetCellValue("");
                                break;
                        }
                        columnIndex++;
                    }
                    #endregion
                }
                sheetIndex++;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                ms.Flush();
                ms.Position = 0;
                return ms;
            }
        }

        /// <summary>
        /// 导出到Excel2007的MemoryStream
        /// </summary>
        /// <param name="ListDataTable"></param>
        /// <param name="listSheetName"></param>
        /// <param name="listColumnName"></param>
        /// <returns></returns>
        private static MemoryStream Export_Xlsx(List<DataTable> ListDataTable, List<string> listSheetName, List<string[]> listColumnName)
        {
            XSSFWorkbook workbook = new XSSFWorkbook();

            int sheetIndex = 0;
            foreach (System.Data.DataTable dtSource in ListDataTable)
            {
                ISheet sheet = workbook.CreateSheet(listSheetName[sheetIndex]);

                ICellStyle dateStyle = workbook.CreateCellStyle();
                IDataFormat format = workbook.CreateDataFormat();
                dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");

                //取得列宽
                int[] arrColWidth = new int[dtSource.Columns.Count];
                foreach (DataColumn item in dtSource.Columns)
                {
                    arrColWidth[item.Ordinal] = Encoding.GetEncoding(936).GetBytes(item.ColumnName.ToString()).Length;
                }
                for (int i = 0; i < dtSource.Rows.Count; i++)
                {
                    for (int j = 0; j < dtSource.Columns.Count; j++)
                    {
                        int intTemp = Encoding.GetEncoding(936).GetBytes(dtSource.Rows[i][j].ToString()).Length;
                        if (intTemp > arrColWidth[j])
                        {
                            arrColWidth[j] = intTemp;
                        }
                    }
                }

                //#region 表头及样式
                //{
                //    if (!string.IsNullOrEmpty(strHeaderText))
                //    {
                //        IRow headerRow = sheet.CreateRow(rowIndex);
                //        rowIndex++;
                //        headerRow.HeightInPoints = 25;
                //        headerRow.CreateCell(0).SetCellValue(strHeaderText);

                //        ICellStyle headStyle = workbook.CreateCellStyle();
                //        headStyle.Alignment = HorizontalAlignment.Center;
                //        IFont font = workbook.CreateFont();
                //        font.FontHeightInPoints = 20;
                //        font.Boldweight = 700;
                //        headStyle.SetFont(font);
                //        headerRow.GetCell(0).CellStyle = headStyle;
                //        sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, dtSource.Columns.Count - 1));
                //    }
                //}
                //#endregion

                int rowIndex = 0;

                List<string> listColumnHeader = new List<string>();//显示名称
                #region 列头及样式
                {
                    IRow headerRow = sheet.CreateRow(rowIndex);
                    rowIndex++;
                    ICellStyle headStyle = workbook.CreateCellStyle();
                    headStyle.Alignment = HorizontalAlignment.Center;
                    IFont font = workbook.CreateFont();
                    font.FontHeightInPoints = 10;
                    font.Boldweight = 700;
                    headStyle.SetFont(font);
                    int columnIndex = 0;
                    //foreach (DataColumn column in dtSource.Columns)
                    //{
                    //    //headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                    //    headerRow.CreateCell(column.Ordinal).SetCellValue(listColumnName[sheetIndex][columnIndex]);
                    //    columnIndex++;
                    //    headerRow.GetCell(column.Ordinal).CellStyle = headStyle;

                    //    //设置列宽
                    //    sheet.SetColumnWidth(column.Ordinal, (arrColWidth[column.Ordinal] + 1) * 256);
                    //} 
                    foreach (string one in listColumnName[sheetIndex])
                    {
                        string[] columnInfo = listColumnName[sheetIndex][columnIndex].Split('|');
                        listColumnHeader.Add(columnInfo[1]);
                        headerRow.CreateCell(columnIndex).SetCellValue(columnInfo[0]);
                        headerRow.GetCell(columnIndex).CellStyle = headStyle;
                        columnIndex++;
                    }
                }
                #endregion

                foreach (DataRow row in dtSource.Rows)
                {
                    int columnIndex = 0;

                    #region 填充内容
                    IRow dataRow = sheet.CreateRow(rowIndex);
                    rowIndex++;
                    //foreach (DataColumn column in dtSource.Columns)
                    for (int i = 0; i < listColumnName[sheetIndex].Length; i++)
                    {
                        ICell newCell = dataRow.CreateCell(columnIndex);
                        string colName = listColumnHeader[i];
                        string drValue = row[colName].ToString();

                        switch (dtSource.Columns[colName].DataType.ToString())
                        {
                            case "System.String"://字符串类型
                                newCell.SetCellValue(drValue);
                                break;
                            case "System.DateTime"://日期类型
                                DateTime dateV;
                                DateTime.TryParse(drValue, out dateV);
                                newCell.SetCellValue(dateV);

                                newCell.CellStyle = dateStyle;//格式化显示
                                break;
                            case "System.Boolean"://布尔型
                                bool boolV = false;
                                bool.TryParse(drValue, out boolV);
                                newCell.SetCellValue(boolV);
                                break;
                            case "System.Int16"://整型
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Byte":
                                int intV = 0;
                                int.TryParse(drValue, out intV);
                                newCell.SetCellValue(intV);
                                break;
                            case "System.Single":
                            case "System.Decimal"://浮点型
                            case "System.Double":
                                double doubV = 0;
                                double.TryParse(drValue, out doubV);
                                newCell.SetCellValue(doubV);
                                break;
                            case "System.DBNull"://空值处理
                                newCell.SetCellValue("");
                                break;
                            default:
                                newCell.SetCellValue("");
                                break;
                        }
                        columnIndex++;
                    }
                    #endregion
                }
                sheetIndex++;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                ms.Flush();
                return ms;
            }
        }

        #endregion
    }
}