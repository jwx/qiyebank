﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    public class OrderNo
    {
        public string GetOrderNo()
        {
            System.Threading.Thread.Sleep(100);//线程延时，防止单号重复
            return DateTime.Now.ToString("yyyyMMddHHmmssff") + new Random().Next(0, 100).ToString().PadLeft(2, '0');
        }
    }
}
