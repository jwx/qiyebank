﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Backup
    {
        Database db;

        public Backup()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(string dbname, string remark, int jbid)
        {
            string sql = "insert into Backup (dbname,remark,addTime,jbid) values (@dbname,@remark,@addTime,@jbid)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@dbname", System.Data.DbType.String, dbname);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }
}
