﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Apply
    {
        Database db;

        public Apply()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(int uid, decimal jine, decimal benjin, decimal lixi, string addTime, string remark, string json, DbTransaction tran)
        {
            string sql = "insert into Apply (pihao,uid,jine,benjin,lixi,addTime,remark,isFukuan,jbid,fk_remark,fk_time,json) values (@pihao,@uid,@jine,@benjin,@lixi,@addTime,@remark,@isFukuan,@jbid,@fk_remark,@fk_time,@json)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@pihao", System.Data.DbType.String, "");
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@benjin", System.Data.DbType.Decimal, benjin);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.DateTime, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@isFukuan", System.Data.DbType.Int32, 0);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, 0);
            db.AddInParameter(cmd, "@fk_remark", System.Data.DbType.String, "");
            db.AddInParameter(cmd, "@fk_time", System.Data.DbType.String, "");
            db.AddInParameter(cmd, "@json", System.Data.DbType.String, json);

            try
            {
                //  return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, int isFukuan, int jbid, string fk_remark, string fk_time, DbTransaction tran)
        {
            string sql = "update Apply set isFukuan=@isFukuan,jbid=@jbid,fk_remark=@fk_remark,fk_time=@fk_time where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@isFukuan", System.Data.DbType.Int32, isFukuan);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@fk_remark", System.Data.DbType.String, fk_remark);
            db.AddInParameter(cmd, "@fk_time", System.Data.DbType.String, fk_time);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int CreatePihao(string pihao)
        {
            string sql = "update Apply set pihao=@pihao where pihao=''";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@pihao", System.Data.DbType.String, pihao);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Apply GetModel(int id)
        {
            string sql = "select * from V_Apply_Info where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Apply model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Apply>(reader);
            }
            return model;
        }

        public Model.Apply GetModel(int id, int uid)
        {
            string sql = "select * from V_Apply_Info where id=@id and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.Apply model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Apply>(reader);
            }
            return model;
        }


        public List<Model.Apply> GetListByWhere(string where)
        {
            string sql = "select * from Apply where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Apply> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Apply>(reader);
            }
            return list;
        }

        public System.Data.DataTable GetDataTableByWhere(string where)
        {
            string sql = "select * from V_Apply_Info where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch
            {
                return null;
            }
        }
    }
}
