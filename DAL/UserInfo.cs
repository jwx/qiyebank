﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class UserInfo
    {
        Database db;

        public UserInfo()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        #region 增加

        public int Add(string username, string loginpswd, string userpswd, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string addTime, string remark, string photo, string avatar, int jbid, int UserOrGS ,DbTransaction tran)
        {
            //string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0);select last_insert_rowid()";
            string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel,UserOrGS) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0,@UserOrGS)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            string loginpwd = Falcon.Function.Encrypt(loginpswd);
            string userpwd = Falcon.Function.Encrypt(userpswd);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@loginpswd", System.Data.DbType.String, loginpwd);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, userpwd);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@idcard", System.Data.DbType.String, idcard);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@gender", System.Data.DbType.Int32, gender);
            db.AddInParameter(cmd, "@tel", System.Data.DbType.String, tel);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@bankcard", System.Data.DbType.String, bankcard);
            db.AddInParameter(cmd, "@bank_uname", System.Data.DbType.String, bank_uname);
            db.AddInParameter(cmd, "@bankname", System.Data.DbType.String, bankname);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@photo", System.Data.DbType.String, photo);
            db.AddInParameter(cmd, "@avatar", System.Data.DbType.String, avatar);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@UserOrGS", System.Data.DbType.Int32, UserOrGS);
            try
            {
                //if (tran == null)
                //{
                //    int res= db.ExecuteNonQuery(cmd);
                //  //  int res = Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                //    return res;
                //}
                //cmd.Transaction = tran;
                //cmd.Connection = tran.Connection;
                //int res1 = Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
                //return res1;
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Add(string username, string loginpswd, string userpswd, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string addTime, string remark, string photo, string avatar, int jbid, DbTransaction tran)
        {
            //string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0);select last_insert_rowid()";
            string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            string loginpwd = Falcon.Function.Encrypt(loginpswd);
            string userpwd = Falcon.Function.Encrypt(userpswd);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@loginpswd", System.Data.DbType.String, loginpwd);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, userpwd);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@idcard", System.Data.DbType.String, idcard);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@gender", System.Data.DbType.Int32, gender);
            db.AddInParameter(cmd, "@tel", System.Data.DbType.String, tel);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@bankcard", System.Data.DbType.String, bankcard);
            db.AddInParameter(cmd, "@bank_uname", System.Data.DbType.String, bank_uname);
            db.AddInParameter(cmd, "@bankname", System.Data.DbType.String, bankname);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@photo", System.Data.DbType.String, photo);
            db.AddInParameter(cmd, "@avatar", System.Data.DbType.String, avatar);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            try
            {
                //if (tran == null)
                //{
                //    int res= db.ExecuteNonQuery(cmd);
                //  //  int res = Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                //    return res;
                //}
                //cmd.Transaction = tran;
                //cmd.Connection = tran.Connection;
                //int res1 = Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
                //return res1;
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int Add(string username, string loginpswd, string userpswd, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string addTime, string remark, string photo, string avatar, int jbid, DbTransaction tran,int gslxid,string gslxname,string fddbr,string zczb,string clsj,string yyqx,string yyfw,int dwlxid,string dwlxname,int UserOrGS)
        {
            //string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0);select last_insert_rowid()";
            string sql = "insert into UserInfo(username,loginpswd,userpswd,realname,idcard,email,gender,tel,mobile,address,bankcard,bank_uname,bankname,zhifubao,addTime,remark,photo,avatar,jbid,isDel,GSTYPEID,GSTYPENAme,FDDBR,ZCZB,CLTIME,YYQX,JYFW,TYPEID,TYPENAME,UserOrGS) values (@username,@loginpswd,@userpswd,@realname,@idcard,@email,@gender,@tel,@mobile,@address,@bankcard,@bank_uname,@bankname,@zhifubao,@addTime,@remark,@photo,@avatar,@jbid,0,@gslxid,@gslxname,@fddbr,@zczb,@clsj,@yyqx,@yyfw,@dwlxid,@dwlxname,@UserOrGS)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            string loginpwd = Falcon.Function.Encrypt(loginpswd);
            string userpwd = Falcon.Function.Encrypt(userpswd);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@loginpswd", System.Data.DbType.String, loginpwd);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, userpwd);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@idcard", System.Data.DbType.String, idcard);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@gender", System.Data.DbType.Int32, gender);
            db.AddInParameter(cmd, "@tel", System.Data.DbType.String, tel);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@bankcard", System.Data.DbType.String, bankcard);
            db.AddInParameter(cmd, "@bank_uname", System.Data.DbType.String, bank_uname);
            db.AddInParameter(cmd, "@bankname", System.Data.DbType.String, bankname);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@photo", System.Data.DbType.String, photo);
            db.AddInParameter(cmd, "@avatar", System.Data.DbType.String, avatar);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@gslxid", System.Data.DbType.Int32, gslxid);
            db.AddInParameter(cmd, "@gslxname", System.Data.DbType.String, gslxname);
            db.AddInParameter(cmd, "@fddbr", System.Data.DbType.String, fddbr);
            db.AddInParameter(cmd, "@zczb", System.Data.DbType.String, zczb);
            db.AddInParameter(cmd, "@clsj", System.Data.DbType.String, clsj);
            db.AddInParameter(cmd, "@yyqx", System.Data.DbType.String, yyqx);
            db.AddInParameter(cmd, "@yyfw", System.Data.DbType.String, yyfw);
            db.AddInParameter(cmd, "@dwlxid", System.Data.DbType.Int32, dwlxid);
            db.AddInParameter(cmd, "@dwlxname", System.Data.DbType.String, dwlxname);
            db.AddInParameter(cmd, "@UserOrGS", System.Data.DbType.Int32, UserOrGS);
            try
            {
                //if (tran == null)
                //{
                //    int res= db.ExecuteNonQuery(cmd);
                //  //  int res = Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                //    return res;
                //}
                //cmd.Transaction = tran;
                //cmd.Connection = tran.Connection;
                //int res1 = Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
                //return res1;
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        public int Update(int id, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string remark, string photo, int jbid,  int gslxid, string gslxname,string fddbr, string zczb, string clsj, string yyqx, string yyfw, int dwlxid, string dwlxname)
        {
            string sql = "update UserInfo set realname=@realname,idcard=@idcard,email=@email,gender=@gender,tel=@tel,mobile=@mobile,address=@address,bankcard=@bankcard,bank_uname=@bank_uname,bankname=@bankname,zhifubao=@zhifubao,remark=@remark,photo=@photo,jbid=@jbid ,GSTYPEID=@GSTYPEID,GSTYPENAme=@GSTYPENAme,FDDBR=@FDDBR,ZCZB=@ZCZB,CLTIME=@CLTIME,YYQX=@YYQX,JYFW=@JYFW,TYPEID=@TYPEID,TYPENAME=@TYPENAME  where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            //db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            //db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, userpswd);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@idcard", System.Data.DbType.String, idcard);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@gender", System.Data.DbType.Int32, gender);
            db.AddInParameter(cmd, "@tel", System.Data.DbType.String, tel);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@bankcard", System.Data.DbType.String, bankcard);
            db.AddInParameter(cmd, "@bank_uname", System.Data.DbType.String, bank_uname);
            db.AddInParameter(cmd, "@bankname", System.Data.DbType.String, bankname);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            //db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@photo", System.Data.DbType.String, photo);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@GSTYPEID", System.Data.DbType.Int32, gslxid);
            db.AddInParameter(cmd, "@GSTYPENAme", System.Data.DbType.String, gslxname);
            db.AddInParameter(cmd, "@FDDBR", System.Data.DbType.String, fddbr);
            db.AddInParameter(cmd, "@ZCZB", System.Data.DbType.String, zczb);
            db.AddInParameter(cmd, "@CLTIME", System.Data.DbType.String, clsj);
            db.AddInParameter(cmd, "@YYQX", System.Data.DbType.String, yyqx);
            db.AddInParameter(cmd, "@JYFW", System.Data.DbType.String, yyfw);
            db.AddInParameter(cmd, "@TYPEID", System.Data.DbType.Int32, dwlxid);
            db.AddInParameter(cmd, "@TYPENAME", System.Data.DbType.String, dwlxname);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string remark, string photo, int jbid)
        {
            string sql = "update UserInfo set realname=@realname,idcard=@idcard,email=@email,gender=@gender,tel=@tel,mobile=@mobile,address=@address,bankcard=@bankcard,bank_uname=@bank_uname,bankname=@bankname,zhifubao=@zhifubao,remark=@remark,photo=@photo,jbid=@jbid  where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            //db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            //db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, userpswd);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@idcard", System.Data.DbType.String, idcard);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@gender", System.Data.DbType.Int32, gender);
            db.AddInParameter(cmd, "@tel", System.Data.DbType.String, tel);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@bankcard", System.Data.DbType.String, bankcard);
            db.AddInParameter(cmd, "@bank_uname", System.Data.DbType.String, bank_uname);
            db.AddInParameter(cmd, "@bankname", System.Data.DbType.String, bankname);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            //db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@photo", System.Data.DbType.String, photo);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 修改登录密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="loginpswd"></param>
        /// <returns></returns>
        public int UpdatePswd_Login(int id, string loginpswd)
        {
            string sql = "update UserInfo set loginpswd=@loginpswd where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@loginpswd", System.Data.DbType.String, Falcon.Function.Encrypt(loginpswd));
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 修改支付密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public int UpdatePswd_Pay(int id, string userpswd)
        {
            string sql = "update UserInfo set userpswd=@userpswd where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, Falcon.Function.Encrypt(userpswd));
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }


        public bool Update_GuaShi(int id, string username, DbTransaction tran)
        {
            string sql = "update UserInfo set username=@username where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran) == 1;
            }
            catch
            {
                return false;
            }
        }

        public int UpdateBaseInfo(int uid, string email, string mobile, string address)
        {
            string sql = "update UserInfo set email=@email,mobile=@mobile,address=@address where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@address", System.Data.DbType.String, address);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, uid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Mobile(int uid, string mobile)
        {
            string sql = "update UserInfo set mobile=@mobile where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, uid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Avatar(int uid, string avatar)
        {
            string sql = "update UserInfo set avatar=@avatar where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@avatar", System.Data.DbType.String, avatar);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, uid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 查询

        public Model.UserInfo GetModel(int id)
        {
            string sql = "select * from UserInfo where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.UserInfo model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.UserInfo>(reader);
            }
            return model;
        }

        public Model.UserInfo GetModel(string username)
        {
            string sql = "select * from UserInfo where username=@username and isDel=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            Model.UserInfo model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.UserInfo>(reader);
            }
            return model;
        }

        public Model.UserInfo GetModel(string filter, int id)
        {
            string sql = "select " + filter + " from UserInfo where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.UserInfo model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.UserInfo>(reader);
            }
            return model;
        }

        public Model.UserInfo GetModel(string filter, string username)
        {
            string sql = "select " + filter + " from UserInfo where username=@username and isDel=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            Model.UserInfo model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.UserInfo>(reader);
            }
            return model;
        }

        public List<string> GetListUserName(DbTransaction tran)
        {
            string sql = "select username from UserInfo";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<string> list = new List<string>();
            using (var reader = db.ExecuteReader(cmd, tran))
            {
                while (reader.Read())
                {
                    list.Add(reader.GetString(reader.GetOrdinal("username")));
                }
            }
            return list;
        }

        /// <summary>
        /// 获取用户id和用户账号
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.UserInfo> GetListUidAndUname(DbTransaction tran)
        {
            string sql = "select id,username from UserInfo where isDel=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.UserInfo> list = new List<Model.UserInfo>();
            using (var reader = db.ExecuteReader(cmd, tran))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.UserInfo>(reader);
            }
            return list;
        }

        public List<Model.UserInfo> GetListModel()
        {
            string sql = "select * from UserInfo";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.UserInfo> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.UserInfo>(reader);
            }
            return list;
        }

        public List<Model.UserInfo> GetListByWhere(string where)
        {
            string sql = "select * from V_UserInfo_Users where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.UserInfo> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.UserInfo>(reader);
            }
            return list;
        }

        public System.Data.DataTable GetListDtByWhere(string where)
        {
            string sql = "select * from V_UserInfo_Users where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch
            {
                return null;
            }
        }

        public int GetRecordCount(string where)
        {
            string sql = "select count(0) from UserInfo where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
            }
            catch
            {
                return 0;
            }
        }

        public List<Model.UserInfo> GetList_Pager(int pageindex, int pagesize, string where)
        {
            //string sql = "select * from UserInfo where " + where + " limit @pagesize offset @pagesize*@pageindex";
            string sql = "select * from UserInfo where  1=1  " + where + " order by id desc offset  (@pageindex - 1) * @pagesize  rows fetch next @pagesize rows only";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@pageindex", System.Data.DbType.Int32, pageindex);
            db.AddInParameter(cmd, "@pagesize", System.Data.DbType.Int32, pagesize);

            List<Model.UserInfo> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.UserInfo>(reader);
            }
            return list;
        }

        public int isExist_UserName(int id, string username)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "select count(0) from UserInfo where id!=@id and username=@username";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 1);
            }
            catch
            {
                return 1;
            }
        }

        /// <summary>
        /// 手机端登录接口
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Model.UserInfo Login(string username)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from UserInfo where username=@username";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            Model.UserInfo model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.UserInfo>(reader);
            }
            return model;
        }

        #endregion

        #region  删除

        public int Delete(int id, DbTransaction tran)
        {
            string sql = "update UserInfo set isDel=1 where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}