﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Bank
    {
        Database db;

        public Bank()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(string name, string startNo, int sort, int jbid)
        {
            string sql = "insert into Bank (name,startNo,sort,jbid,addTime,isDel) values (@name,@startNo,@sort,@jbid,@addTime,@isDel)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@startNo", System.Data.DbType.String, startNo);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.DateTime, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@isDel", System.Data.DbType.Int32, 0);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string name, string startNo, int sort, int jbid)
        {
            string sql = "update Bank set name=@name,startNo=@startNo,sort=@sort,jbid=@jbid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@startNo", System.Data.DbType.String, startNo);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Bank GetModel(int id)
        {
            string sql = "select * from [Bank] where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Bank model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Bank>(reader);
            }
            return model;
        }

        public List<Model.Bank> GetList()
        {
            string sql = "select * from Bank where isDel=0 order by sort asc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Bank> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Bank>(reader);
            }
            return list;
        }

        public int Delete(int id)
        {
            string sql = "update Bank set isDel=1 where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }
}
