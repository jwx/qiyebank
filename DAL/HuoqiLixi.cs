﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class HuoqiLixi
    {
        Database db;

        public HuoqiLixi()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public HuoqiLixi(bool isService)
        {
            db = isService ? Tool.DatabaseHelper_Service.CreateDatabase() : Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(int oid_ck, int oid_qk, decimal qukuan, decimal lixi, DbTransaction tran)
        {
            string sql = "insert into HuoqiLixi (oid_ck,oid_qk,qukuan,lixi) values (@oid_ck,@oid_qk,@qukuan,@lixi)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@oid_ck", System.Data.DbType.Int32, oid_ck);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);
            db.AddInParameter(cmd, "@qukuan", System.Data.DbType.Decimal, qukuan);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);

            try
            {
                // return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public List<Model.HuoqiLixi> GetList(int oid_qk, DbTransaction tran)
        {
            string sql = "select * from HuoqiLixi where oid_qk=@oid_qk";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);

            List<Model.HuoqiLixi> list;
            if (tran == null)
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.HuoqiLixi>(reader);
                }
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                using (var reader = db.ExecuteReader(cmd, tran))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.HuoqiLixi>(reader);
                }
            }
            return list;
        }
    }
}
