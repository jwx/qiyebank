﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Daikuan
    {

        Database db;

        public Daikuan()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public Daikuan(bool isService)
        {
            db = isService ? Tool.DatabaseHelper_Service.CreateDatabase() : Tool.DatabaseHelper.CreateDatabase();
        }

        public Model.Daikuan GetModelByID(int id)
        {
            string sql = "select * from Daikuan where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Daikuan model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Daikuan>(reader);
            }
            return model;
        }


        public int Add(int uid, decimal jine, string danhao, int daihuankuan, int canBaofei, int huankuanType, string zhouqi, decimal lilv, decimal lixi, decimal benxi, decimal yihuan, string addTime, string daikuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark, DbTransaction tran)
        {
            try
            {
                string sql = "insert into Daikuan(uid,jine,danhao,daihuankuan,canBaofei,huankuanType,zhouqi,lilv,lixi,benxi,yihuan,addTime,daikuanTime,daoqiTime,jbid,add_remark,delTime,delid,del_remark,isFinish)values(@uid,@jine,@danhao,@daihuankuan,@canBaofei,@huankuanType,@zhouqi,@lilv,@lixi,@benxi,@yihuan,@addTime,@daikuanTime,@daoqiTime,@jbid,@add_remark,@delTime,@delid,@del_remark,@isFinish)SELECT @@IDENTITY";
                DbCommand cmd = db.GetSqlStringCommand(sql);
                db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
                db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
                db.AddInParameter(cmd, "@danhao", System.Data.DbType.String, danhao);
                db.AddInParameter(cmd, "@daihuankuan", System.Data.DbType.Int32, daihuankuan);
                db.AddInParameter(cmd, "@canBaofei", System.Data.DbType.Int32, canBaofei);
                db.AddInParameter(cmd, "@huankuanType", System.Data.DbType.Int32, huankuanType);
                db.AddInParameter(cmd, "@zhouqi", System.Data.DbType.String, zhouqi);
                db.AddInParameter(cmd, "@lilv", System.Data.DbType.Decimal, lilv);
                db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
                db.AddInParameter(cmd, "@benxi", System.Data.DbType.Decimal, benxi);
                db.AddInParameter(cmd, "@yihuan", System.Data.DbType.Decimal, yihuan);
                db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
                db.AddInParameter(cmd, "@daikuanTime", System.Data.DbType.String, daikuanTime);
                db.AddInParameter(cmd, "@daoqiTime", System.Data.DbType.String, daoqiTime);
                db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
                db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
                db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, delTime);
                db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
                db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
                db.AddInParameter(cmd, "@isFinish", System.Data.DbType.Int32, 0);

                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        //public int Update(int id, decimal yihuan,DbTransaction tran)
        //{
        //    try
        //    {
        //        string sql = " update  Daikuan set  yihuan=@yihuan  where id=@id";
        //        DbCommand cmd = db.GetSqlStringCommand(sql);
        //        db.AddInParameter(cmd, "@yihuan", System.Data.DbType.Decimal, yihuan);
        //        db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
        //        return db.ExecuteNonQuery(cmd);
        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }
        //}


        public int Update(int id, decimal jine, int isFinish, DbTransaction tran)
        {
            string sql = "update Daikuan set canbaofei=0,yihuan=yihuan+" + jine + ",isFinish=@isFinish where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@isFinish", System.Data.DbType.Int32, isFinish);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd, tran), 0);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public Model.Daikuan_Mobile GetDaikuan(int uid, DbTransaction tran = null)
        {
            string sql = "select sum(jine) as jine,sum(benxi) as benxi,sum(lixi) as lixi,sum(yihuan) as yihuan from Daikuan where uid=@uid and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            try
            {
                if (tran != null)
                {
                    cmd.Transaction = tran;
                    cmd.Connection = tran.Connection;
                }
                Model.Daikuan_Mobile model;
                using (var reader = tran == null ? db.ExecuteReader(cmd) : db.ExecuteReader(cmd, tran))
                {
                    model = Tool.DataReaderToModel.ReaderToModel<Model.Daikuan_Mobile>(reader);
                }
                return model;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int DaikuanBF(int id,int uid,string remark, DbTransaction tran)
        {
            string upsql = "update Daikuan set canBaofei=1,delTime=@deTime,delid=@delid,del_remark=@del_remark where id=@id";
            try
            {
                DbCommand cmd = db.GetSqlStringCommand(upsql);
                db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
                db.AddInParameter(cmd, "@deTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, uid);
                db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, remark);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd, tran), 0);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }






    }
}
