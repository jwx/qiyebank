﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
   public class Welldrilling
    {

        Database db;

        public Welldrilling()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public List<Model.Welldrilling> GetWModelByID(int id)
        {
            string sql = "select * from Welldrilling where aid=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            List<Model.Welldrilling> model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToListModel<Model.Welldrilling>(reader);
                return model;
            }
        }




        public Model.Welldrilling GetModelByID(int id)
        {
            string sql = "select * from Welldrilling where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Welldrilling model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Welldrilling>(reader);
            }
            return model;
        }

        public int Add(string year, int personalincometax, int Managementexpense, int other, int everydayton, int loss, int aid, DbTransaction tran)
        {
            string sql = "insert into Welldrilling(year,personalincometax,Managementexpense,other,everydayton,loss,aid) values (@year,@personalincometax,@Managementexpense,@other,@everydayton,@loss,@aid)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@year", System.Data.DbType.String, year);
            db.AddInParameter(cmd, "@personalincometax", System.Data.DbType.Int32, personalincometax);
            db.AddInParameter(cmd, "@Managementexpense", System.Data.DbType.Int32, Managementexpense);
            db.AddInParameter(cmd, "@other", System.Data.DbType.Int32, other);
            db.AddInParameter(cmd, "@everydayton", System.Data.DbType.Int32, everydayton);
            db.AddInParameter(cmd, "@loss", System.Data.DbType.Int32, loss);
            db.AddInParameter(cmd, "@aid", System.Data.DbType.Int32, aid);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update(int id, string year, int personalincometax, int Managementexpense, int other, int everydayton, int loss,int aid)
        {
            string sql = "update Welldrilling set  year=@year,personalincometax=@personalincometax,Managementexpense=@Managementexpense,other=@other,everydayton=@everydayton,loss=@loss" +
                " ,aid=@aid where  id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@year", System.Data.DbType.String, year);
            db.AddInParameter(cmd, "@personalincometax", System.Data.DbType.Int32, personalincometax);
            db.AddInParameter(cmd, "@Managementexpense", System.Data.DbType.Int32, Managementexpense);
            db.AddInParameter(cmd, "@other", System.Data.DbType.Int32, other);
            db.AddInParameter(cmd, "@everydayton", System.Data.DbType.Int32, everydayton);
            db.AddInParameter(cmd, "@loss", System.Data.DbType.Int32, loss);
            db.AddInParameter(cmd, "@aid", System.Data.DbType.Int32, aid);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Delete(int id)
        {
            string sql = "delete from  Welldrilling  where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }
}
