﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class DaikuanPayback
    {


        Database db;

        public DaikuanPayback()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public DaikuanPayback(bool isService)
        {
            db = isService ? Tool.DatabaseHelper_Service.CreateDatabase() : Tool.DatabaseHelper.CreateDatabase();
        }


        public Model.DaikuanPayback GetModelByID(int id)
        {
            string sql = "select * from DaikuanPayback where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.DaikuanPayback model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.DaikuanPayback>(reader);
            }
            return model;
        }
        public List<Model.DaikuanPayback> getListModel(int id)
        {
            string sql = "select * from DaikuanPayback where dkId=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            List<Model.DaikuanPayback> model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToListModel<Model.DaikuanPayback>(reader);
            }
            return model;
        }


        public int GetWeihuanCount(int dkId, DbTransaction tran)
        {
            string sql = "select count(0) from DaikuanPayback where dkId=@dkId and huankuanTime=''";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@dkId", System.Data.DbType.Int32, dkId);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd));
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran));
            }
            catch
            {
                return -1;
            }
        }


        public int Add(int dkId, decimal jine, string huankuanDate, string huankuanTime, int huankuanType, int jbid, string remark, DbTransaction tran)
        {
            try
            {
                string sql = " insert into DaikuanPayback(dkId,jine,huankuanDate,huankuanTime,huankuanType,jbid,remark)values(@dkId,@jine,@huankuanDate,@huankuanTime,@huankuanType,@jbid,@remark)SELECT @@IDENTITY";
                DbCommand cmd = db.GetSqlStringCommand(sql);
                db.AddInParameter(cmd, "@dkId", System.Data.DbType.Int32, dkId);
                db.AddInParameter(cmd, "@huankuanType", System.Data.DbType.Int32, huankuanType);
                db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
                db.AddInParameter(cmd, "@huankuanDate", System.Data.DbType.String, huankuanDate);
                db.AddInParameter(cmd, "@huankuanTime", System.Data.DbType.String, huankuanTime);
                db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
                db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);

                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);

            }
            catch (Exception ex)
            {
                return 0;
            }

        }


        public int Update(int id, string huankuanTime, int huankuanType, int jbid, string remark, DbTransaction tran)
        {
            try
            {
                string sql = "update DaikuanPayback set  huankuanTime=@huankuanTime,huankuanType=@huankuanType,jbid=@jbid,remark=@remark  where id=@id";
                DbCommand cmd = db.GetSqlStringCommand(sql);
                db.AddInParameter(cmd, "@huankuanType", System.Data.DbType.Int32, huankuanType);
                db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
                db.AddInParameter(cmd, "@huankuanTime", System.Data.DbType.String, huankuanTime);
                db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
                db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

                return db.ExecuteNonQuery(cmd);

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Payback(int id, string huankuanTime, int huankuanType, int jbid, string remark, int hqId, DbTransaction tran = null)
        {
            string sql = "update DaikuanPayback set huankuanTime=@huankuanTime,huankuanType=@huankuanType,jbid=@jbid,remark=@remark,hqId=@hqId where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@huankuanTime", System.Data.DbType.String, huankuanTime);
            db.AddInParameter(cmd, "@huankuanType", System.Data.DbType.Int32, huankuanType);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@hqId", System.Data.DbType.Int32, hqId);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd, tran), 0);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public List<Model.DaikuanPayback> GetList_NotPay(DateTime dtNow)
        {
            string sql = "select * from V_DaikuanPayback where huankuanDate<='" + dtNow.ToString("yyyy-MM-dd") + "' and huankuanTime=''";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.DaikuanPayback> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.DaikuanPayback>(reader);
            }
            return list;
        }

        public List<Model.DaikuanPayback> GetList_Yuqi(int uid)
        {
            string sql = "select * from V_DaikuanPayback where huankuanDate<='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' and huankuanTime='' and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            List<Model.DaikuanPayback> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.DaikuanPayback>(reader);
            }
            return list;
        }

        public List<Model.DaikuanPayback> GetList(int dkId)
        {
            string sql = "select * from DaikuanPayback where dkId=@dkId";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@dkId", System.Data.DbType.Int32, dkId);

            List<Model.DaikuanPayback> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.DaikuanPayback>(reader);
            }
            return list;
        }






    }
}
