﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class SetupPrint
    {
        Database db;

        public SetupPrint()
        {
            db =Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(Config.Enums.Print_Type type, string sysCode)
        {
            string sql = "update SetupPrint set sysCode=@sysCode where type=@type";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@sysCode", System.Data.DbType.String, sysCode);
            db.AddInParameter(cmd, "@type", System.Data.DbType.Int32, (int)type);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 用户自己修改打印代码
        /// </summary>
        /// <param name="type"></param>
        /// <param name="defCode"></param>
        /// <param name="jsCode"></param>
        /// <returns></returns>
        public int Update(Config.Enums.Print_Type type, string defCode, string jsCode)
        {
            string sql = "update SetupPrint set defCode=@defCode,jsCode=@jsCode where type=@type";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@type", System.Data.DbType.Int32, (int)type);
            db.AddInParameter(cmd, "@defCode", System.Data.DbType.String, defCode);
            db.AddInParameter(cmd, "@jsCode", System.Data.DbType.String, jsCode);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 用户重置打印代码
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sysCode"></param>
        /// <param name="jsCode"></param>
        /// <returns></returns>
        public int Update_Reset(Config.Enums.Print_Type type, string sysCode, string jsCode)
        {
            string sql = "update SetupPrint set sysCode=@sysCode,defCode=@sysCode,jsCode=@jsCode where type=@type";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@type", System.Data.DbType.Int32, (int)type);
            db.AddInParameter(cmd, "@sysCode", System.Data.DbType.String, sysCode);
            db.AddInParameter(cmd, "@jsCode", System.Data.DbType.String, jsCode);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.SetupPrint GetModel(Config.Enums.Print_Type type)
        {
            string sql = "select top 1* from SetupPrint where type=@type order by id desc";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@type", System.Data.DbType.Int32, (int)type);

            Model.SetupPrint model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.SetupPrint>(reader);
            }
            return model;
        }
    }
}
