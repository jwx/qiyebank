﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Investmen
    {
        Database db;

        public Investmen()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }



        public Model.Investmen GetModelByID(int id)
        {
            string sql = "select * from Investmen where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Investmen model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Investmen>(reader);
            }
            return model;
        }

        public int Add(string accountnumber, string accountName, decimal tzmoney, int moneyAmountID, string moneyAmountName, string tzTime, string endTime, string remarks, int jbid, DbTransaction tran,int wellid,int yearid,string yearName)
        {
            string sql = "insert into Investmen(AccountNumber,AccountName,TZmoney,moneyAmountID,moneyAmountName,tzTime,endTime,remarks,jbid,Wellid,yearid,yearname) values (@accountNumber,@accountName,@tzmoney,@moneyacountid,@moneyacountname,@tztime,@endtime,@remarks,@jdid,@Wellid,@yearid,@yearName)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@accountNumber", System.Data.DbType.String, accountnumber);
            db.AddInParameter(cmd, "@accountName", System.Data.DbType.String, accountName);
            db.AddInParameter(cmd, "@tzmoney", System.Data.DbType.String, tzmoney);
            db.AddInParameter(cmd, "@moneyacountid", System.Data.DbType.Int32, moneyAmountID);
            db.AddInParameter(cmd, "@moneyacountname", System.Data.DbType.String, moneyAmountName);
            db.AddInParameter(cmd, "@tztime", System.Data.DbType.String, tzTime);
            db.AddInParameter(cmd, "@endtime", System.Data.DbType.String, endTime);
            db.AddInParameter(cmd, "@remarks", System.Data.DbType.String, remarks);
            db.AddInParameter(cmd, "@jdid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@Wellid", System.Data.DbType.Int32, wellid);
            db.AddInParameter(cmd, "@yearid", System.Data.DbType.Int32, yearid);
            db.AddInParameter(cmd, "@yearName", System.Data.DbType.String, yearName);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


      











    }
}