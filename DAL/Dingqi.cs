﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class Dingqi
    {
        Database db;

        public Dingqi()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public Dingqi(bool isService)
        {
            db = Tool.DatabaseHelper_Service.CreateDatabase();
        }

        #region 增加

        public int Add(int uid, decimal jine,  string danhao, int cunqukuan, int canBaofei, int hasQukuan, int autoQukuan, int qukuanType, int oid, int daoqi_handler, string zhouqi, decimal lilv, decimal budaoqi, decimal lixi, string addTime, string cunkuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark, DbTransaction tran)
        {
            //string sql = "insert into Dingqi(uid,jine,danhao,cunqukuan,canBaofei,hasQukuan,autoQukuan,qukuanType,oid,daoqi_handler,zhouqi,lilv,budaoqi,lixi,addTime,cunkuanTime,daoqiTime,jbid,add_remark,delTime,delid,del_remark) values (@uid,@jine,@danhao,@cunqukuan,@canBaofei,@hasQukuan,@autoQukuan,@qukuanType,@oid,@daoqi_handler,@zhouqi,@lilv,@budaoqi,@lixi,@addTime,@cunkuanTime,@daoqiTime,@jbid,@add_remark,@delTime,@delid,@del_remark);select last_insert_rowid()";
            string sql = "insert into Dingqi(uid,jine,yuanjine,danhao,cunqukuan,canBaofei,hasQukuan,autoQukuan,qukuanType,oid,daoqi_handler,zhouqi,lilv,budaoqi,lixi,addTime,cunkuanTime,daoqiTime,jbid,add_remark,delTime,delid,del_remark) values (@uid,@jine,@yuanjine,@danhao,@cunqukuan,@canBaofei,@hasQukuan,@autoQukuan,@qukuanType,@oid,@daoqi_handler,@zhouqi,@lilv,@budaoqi,@lixi,@addTime,@cunkuanTime,@daoqiTime,@jbid,@add_remark,@delTime,@delid,@del_remark)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@yuanjine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@danhao", System.Data.DbType.String, danhao);
            db.AddInParameter(cmd, "@cunqukuan", System.Data.DbType.Int32, cunqukuan);
            db.AddInParameter(cmd, "@canBaofei", System.Data.DbType.Int32, canBaofei);
            db.AddInParameter(cmd, "@hasQukuan", System.Data.DbType.Int32, hasQukuan);
            db.AddInParameter(cmd, "@autoQukuan", System.Data.DbType.Int32, autoQukuan);
            db.AddInParameter(cmd, "@qukuanType", System.Data.DbType.Int32, qukuanType);
            db.AddInParameter(cmd, "@oid", System.Data.DbType.Int32, oid);
            db.AddInParameter(cmd, "@daoqi_handler", System.Data.DbType.Int32, daoqi_handler);
            db.AddInParameter(cmd, "@zhouqi", System.Data.DbType.String, zhouqi);
            db.AddInParameter(cmd, "@lilv", System.Data.DbType.Decimal, lilv);
            db.AddInParameter(cmd, "@budaoqi", System.Data.DbType.Decimal, budaoqi);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@cunkuanTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@daoqiTime", System.Data.DbType.String, daoqiTime);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, delTime);
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);

            try
            {
                //  return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        public int Update(int id, int uid, decimal jine, string danhao, int cunqukuan, int canBaofei, int hasQukuan, int autoQukuan, int oid, int daoqi_handler, string zhouqi, decimal lilv, decimal budaoqi, decimal lixi, string addTime, string cunkuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark)
        {
            string sql = "update Dingqi set uid=@uid,jine=@jine,danhao=@danhao,cunqukuan=@cunqukuan,canBaofei=@canBaofei,hasQukuan=@hasQukuan,autoQukuan=@autoQukuan,oid=@oid,daoqi_handler=@daoqi_handler,zhouqi=@zhouqi,lilv=@lilv,budaoqi=@budaoqi,lixi=@lixi,addTime=@addTime,cunkuanTime=@cunkuanTime,daoqiTime=@daoqiTime,jbid=@jbid,add_remark=@add_remark,delTime=@delTime,delid=@delid,del_remark=@del_remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@danhao", System.Data.DbType.String, danhao);
            db.AddInParameter(cmd, "@cunqukuan", System.Data.DbType.Int32, cunqukuan);
            db.AddInParameter(cmd, "@canBaofei", System.Data.DbType.Int32, canBaofei);
            db.AddInParameter(cmd, "@hasQukuan", System.Data.DbType.Int32, hasQukuan);
            db.AddInParameter(cmd, "@autoQukuan", System.Data.DbType.Int32, autoQukuan);
            db.AddInParameter(cmd, "@oid", System.Data.DbType.Int32, oid);
            db.AddInParameter(cmd, "@daoqi_handler", System.Data.DbType.Int32, daoqi_handler);
            db.AddInParameter(cmd, "@zhouqi", System.Data.DbType.String, zhouqi);
            db.AddInParameter(cmd, "@lilv", System.Data.DbType.Decimal, lilv);
            db.AddInParameter(cmd, "@budaoqi", System.Data.DbType.Decimal, budaoqi);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@cunkuanTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@daoqiTime", System.Data.DbType.String, daoqiTime);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, delTime);
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 存款（未取过款）报废或已取款单据报废
        /// </summary>
        /// <param name="id"></param>
        /// <param name="delid"></param>
        /// <param name="del_remark"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public int Update_Baofei(int id, int delid, string del_remark, DbTransaction tran)
        {
            string sql = "update Dingqi set delTime=@delTime,delid=@delid,del_remark=@del_remark where id=@id and canBaofei=1";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Cunkuan_Rollback(int id, DbTransaction tran)
        {
            string sql = "update Dingqi set hasQukuan=0,autoQukuan=0,qukuanType=0,oid=0,lixi=0 where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }


        public int Update_Qukuan(int id, int hasQukuan, int autoQukuan, int qukuanType, int oid, decimal lixi, string add_remark_plus,  DbTransaction tran)
        {
            string sql = "update Dingqi set  hasQukuan=@hasQukuan,autoQukuan=@autoQukuan,qukuanType=@qukuanType,oid=@oid,lixi=@lixi,add_remark=@add_remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@hasQukuan", System.Data.DbType.Int32, hasQukuan);
            db.AddInParameter(cmd, "@autoQukuan", System.Data.DbType.Int32, autoQukuan);
            db.AddInParameter(cmd, "@qukuanType", System.Data.DbType.Int32, qukuanType);
            db.AddInParameter(cmd, "@oid", System.Data.DbType.Int32, oid);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark_plus);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Qukuan(int id, decimal lixi,  decimal jine, DbTransaction tran)
        {
            string sql = "update Dingqi set  jine=jine-@jine,lixi=lixi+@lixi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }


        #endregion

        #region 查询

        public Model.Dingqi GetModel(int id)
        {
            string sql = "select * from V_Dingqi_Info where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Dingqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Dingqi>(reader);
            }
            return model;
        }

        public Model.Dingqi GetModel(int id, int uid)
        {
            string sql = "select * from V_Dingqi_Info where id=@id and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.Dingqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Dingqi>(reader);
            }
            return model;
        }

        public List<Model.Dingqi> GetListModel()
        {
            string sql = "select * from Dingqi";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Dingqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
            }
            return list;
        }

        public List<Model.Dingqi> GetListByWhere(string where)
        {
            string sql = "select * from Dingqi where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Dingqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
            }
            return list;
        }

        public int GetRecordCount(string where)
        {
            string sql = "select count(0) from Dingqi where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
            }
            catch
            {
                return 0;
            }
        }

        public List<Model.Dingqi> GetList_Pager(int pageindex, int pagesize, string where)
        {
            //string sql = "select * from Dingqi where " + where + " limit @pagesize offset @pagesize*@pageindex";
            string sql = "select * from Dingqi where 1=1  " + where + " order by id desc offset  (@pageindex - 1) * @pagesize  rows fetch next @pagesize rows only";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@pageindex", System.Data.DbType.Int32, pageindex);
            db.AddInParameter(cmd, "@pagesize", System.Data.DbType.Int32, pagesize);

            List<Model.Dingqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
            }
            return list;
        }

        /// <summary>
        /// 获取指定用户id已审核的定期账户余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public decimal GetYue(int uid)
        {
            string sql = "select sum(jine) from Dingqi where uid=@uid and cunqukuan=1 and hasQukuan=0 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            try
            {
                return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 获取系统中所有的定期存款总金额
        /// </summary>
        /// <returns></returns>
        public decimal GetSysYue()
        {
            string sql = "select sum(jine) from Dingqi where cunqukuan=1 and hasQukuan=0 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 获取系统中已经付款的总利息
        /// </summary>
        /// <returns></returns>
        public decimal GetSysLixi_Payed()
        {
            string sql = "select sum(lixi) from Dingqi where cunqukuan=1 and hasQukuan=1 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 获取系统中到期的定期取款单据
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_Qukuan(DateTime dtNow, DbTransaction tran)
        {
            string sql = "select * from Dingqi where jine!=0 and cunqukuan=1 and hasQukuan=0 and delid=0 and daoqiTime<='" + dtNow.ToString("yyyy-MM-dd") + "'";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Dingqi> list;
            if (tran == null)
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
                }
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                using (var reader = db.ExecuteReader(cmd, tran))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
                }
            }
            return list;
        }

        ///// <summary>
        ///// 获取用户取款单和利息信息
        ///// </summary>
        ///// <param name="qukuanType"></param>
        ///// <returns></returns>
        //public List<Model.Dingqi> GetList_Qukuan_Lixi(Config.Enums.Dingqi_Qukuan qukuanType)
        //{
        //    string sql = "select uid,lixi from Dingqi where lixi!=0 and cunqukuan=2 and delid=0 and qukuanType=@qukuanType";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);
        //    db.AddInParameter(cmd, "@qukuanType", System.Data.DbType.Int32, qukuanType);

        //    List<Model.Dingqi> list;
        //    using (var reader = db.ExecuteReader(cmd))
        //    {
        //        list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
        //    }
        //    return list;
        //}

        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_YifuLixi()
        {
            //定期本息取款单据的总利息lixi，定期利息结转到活期的单据（本金取款&本金转定期，利息转活期），定期本息结转到活期的单据，定期本利息转定期账户
            //V_Dingqi_Lixi，条件：1、定期存款单 2、系统自动取款 3、到期处理方式为：本金和利息转定期账户，返回字段为uid，lixi（原单据生成的利息），hasQukuan（转存的订单是否被取款） 、、qukuanType=" + (int)Config.Enums.Dingqi_Qukuan.本息取款 + " and
            string sql = "select uid,lixi from Dingqi where lixi!=0 and cunqukuan=2 and  delid=0 union all select uid,yuanjine-jine as lixi from Huoqi where cunqukuan=1 and delid=0 and laiyuan=" + (int)Config.Enums.Huoqi_Laiyuan.定期利息存入 + " union all select uid, case WHEN yuanjine-jine>=yuanjine-benjin then yuanjine-benjin else yuanjine-jine end as lixi from Huoqi where cunqukuan=1 and delid=0 and laiyuan=" + (int)Config.Enums.Huoqi_Laiyuan.定期本息存入 + " union all select uid,lixi from V_Dingqi_Lixi where hasQukuan=1";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Dingqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
            }
            return list;
        }

        /// <summary>
        /// 获取用户存款单和利息信息 
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_WeifuLixi()
        {
            //V_Dingqi_Lixi，条件：1、定期存款单 2、系统自动取款 3、到期处理方式为：本金和利息转定期账户，返回字段为uid，lixi，hasQukuan（转存的订单是否被取款）
            string sql = "select uid,lixi from V_Dingqi_Lixi where hasQukuan=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Dingqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Dingqi>(reader);
            }
            return list;
        }
        #endregion

        #region  删除

        public int Delete(int id, DbTransaction tran)
        {
            string sql = "delete from Dingqi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}