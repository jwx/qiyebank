﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// 获取datatable，多用于excel导出
    /// </summary>
    /// </summary>
    public class DataTableHelper
    {
        public System.Data.DataTable GetDataTableByWhere(string view, string filter, string where)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select " + filter + " from " + view + " where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch
            {
                return null;
            }
        }
    }

    public class ModelHelper
    {
        public T GetModelById<T>(string view, int id, DbTransaction tran)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from @view where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@view", System.Data.DbType.String, view);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            T model = Activator.CreateInstance<T>();
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<T>(reader);
            }
            return model;
        }

        //public T GetModelById<T>(string view, int id, DbTransaction tran)
        //{
        //    Database db =Tool.DatabaseHelper.CreateDatabase();
        //    string sql = "select * from @view where id=@id";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);
        //    db.AddInParameter(cmd, "@view", System.Data.DbType.String, view);
        //    db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

        //    cmd.Transaction = tran;
        //    cmd.Connection = tran.Connection;
        //    T model = Activator.CreateInstance<T>();
        //    using (var reader = db.ExecuteReader(cmd, tran))
        //    {
        //        model = Tool.DataReaderToModel.ReaderToModel<T>(reader);
        //    }
        //    return model;
        //}

        public T GetModelByWhere<T>(string view, string where)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from @view where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@view", System.Data.DbType.String, view);
            db.AddInParameter(cmd, "@where", System.Data.DbType.Int32, where);

            T model = Activator.CreateInstance<T>();
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<T>(reader);
            }
            return model;
        }

        public List<T> GetListModelById<T>(string view, int id)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from @view where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@view", System.Data.DbType.String, view);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            List<T> model = Activator.CreateInstance<List<T>>();
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }
            return model;
        }

        public List<T> GetListModelByWhere<T>(string view, string where)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from @view where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@view", System.Data.DbType.String, view);
            db.AddInParameter(cmd, "@where", System.Data.DbType.Int32, where);

            List<T> list = Activator.CreateInstance<List<T>>();
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }
            return list;
        }
    }

    public class Reflect
    {
        public List<string> GetFilter(string model)
        {
            return Model.Reflect.GetFilter(model);
        }
    }

    public class Notification
    {
        public int Add(string title, string content, int isEnable, int sort, int jbid, DbTransaction tran)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            //string sql = "insert into Notification (title,content,addTime,isEnable,sort,jbid) values (@title,@content,getdate(),@isEnable,@sort,@jbid);select @@identity";
            string sql = "insert into Notification (title,content,addTime,isEnable,sort,jbid) values (@title,@content,getdate(),@isEnable,@sort,@jbid)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@title", System.Data.DbType.String, title);
            db.AddInParameter(cmd, "@content", System.Data.DbType.String, content);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd));
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran));
                // return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string title, string content, int isEnable, int sort, int jbid, DbTransaction tran)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "update Notification set title=@title,content=@content,addTime=getdate(),isEnable=@isEnable,sort=@sort,jbid=@jbid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@title", System.Data.DbType.String, title);
            db.AddInParameter(cmd, "@content", System.Data.DbType.String, content);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Notification GetModel(int id)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from Notification where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Notification model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Notification>(reader);
            }
            return model;
        }

        public int Delete(int id)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "delete from Notification where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }

    public class SubNotification
    {
        public int Add(int nid, int uid, DbTransaction tran)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "insert into SubNotification (nid,uid,isRead) values (@nid,@uid,0)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@nid", System.Data.DbType.Int32, nid);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@isRead", System.Data.DbType.Int32, 0);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
               // return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Read(int nid, int uid)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "update SubNotification set isRead=1,readTime=getdate() where nid=@nid and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@nid", System.Data.DbType.Int32, nid);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public List<Model.SubNotification> GetListByWhere(string where)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from V_SubNotification_Info where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.SubNotification> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.SubNotification>(reader);
            }
            return list;
        }

        public int GetCount_NotRead(int uid)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "select count(0) from V_SubNotification_Info where uid=@uid and isRead=0 and isEnable=1";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd));
            }
            catch
            {
                return 0;
            }
        }

        public int Delete(int nid, DbTransaction tran)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            string sql = "delete from SubNotification where nid=@nid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@nid", System.Data.DbType.Int32, nid);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return -1;
            }
        }
    }
}
