﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class UserLogs
    {
        Database db;

        public UserLogs()
        {
            db =Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(int uid, int jbid, string remark)
        {
            string sql = "insert into UserLogs (uid,jbid,remark,addTime) values (@uid,@jbid,@remark,@addTime)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "addTime", System.Data.DbType.DateTime, DateTime.Now);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }
}
