﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class ServiceLog
    {
        Database db;

        public ServiceLog(bool isService)
        {
            db = Tool.DatabaseHelper_Service.CreateDatabase();
        }

        public int Add(DateTime addTime, int isBackup, int huoqiJiexi, int dingqiZhuancun, DbTransaction tran)
        {
            //string sql = "insert into ServiceLog (addTime,isBackup,huoqiJiexi,dingqiZhuancun) values (@addTime,@isBackup,@huoqiJiexi,@dingqiZhuancun);select last_insert_rowid()";
            string sql = "insert into ServiceLog (addTime,isBackup,huoqiJiexi,dingqiZhuancun) values (@addTime,@isBackup,@huoqiJiexi,@dingqiZhuancun)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@isBackup", System.Data.DbType.Int32, isBackup);
            db.AddInParameter(cmd, "@huoqiJiexi", System.Data.DbType.Int32, huoqiJiexi);
            db.AddInParameter(cmd, "@dingqiZhuancun", System.Data.DbType.Int32, dingqiZhuancun);

            try
            {
             //   return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string strFilter, int value, DbTransaction tran)
        {
            string sql = "update ServiceLog set " + strFilter + "=" + value + " where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public Model.ServiceLog GetLastRecord()
        {
            string sql = "select top 1 * from ServiceLog order by id desc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.ServiceLog model = null;
            try
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    model = Tool.DataReaderToModel.ReaderToModel<Model.ServiceLog>(reader);
                }
            }
            catch { }
            return model;
        }
    }
}
