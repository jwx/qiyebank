﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class InvestmenInfo
    {

        Database db;

        public InvestmenInfo()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public Model.InvestmenInfo getModelById(int id)
        {
            string sql = "select * from InvestmenInfo where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.InvestmenInfo model;
            using (var read = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.InvestmenInfo>(read);
            }
            return model;
        }


        public int Add(string month, int everymonthDay, decimal thisMonethMoney, int isinto, int isoperation)
        {
            string sql = "insert into InvestmenInfo(Month,EveryMonthDay,thisMonthMoney,isInto,isOperation)values(@month,@everymonthday,@thismonthmoney,@isinto,@isoperation)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@month", System.Data.DbType.String, month);
            db.AddInParameter(cmd, "@everymonthday", System.Data.DbType.Int32, everymonthDay);
            db.AddInParameter(cmd, "@thismonthmoney", System.Data.DbType.Decimal, thisMonethMoney);
            db.AddInParameter(cmd, "@isinto", System.Data.DbType.Int32, isinto);
            db.AddInParameter(cmd, "@isoperation", System.Data.DbType.Int32, isoperation);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int Update(int id,string month, int everymonthDay, decimal thisMonethMoney, int isinto, int isoperation)
        {
            string sql = "update InvestmenInfo set Month=@Month,EveryMonthDay=@EveryMonthDay,thisMonthMoney=@thisMonthMoney,isInto=@isInto,isOperation=@isOperation";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@Month", System.Data.DbType.String, month);
            db.AddInParameter(cmd, "@EveryMonthDay", System.Data.DbType.Int32, everymonthDay);
            db.AddInParameter(cmd, "@thisMonthMoney", System.Data.DbType.Decimal, thisMonethMoney);
            db.AddInParameter(cmd, "@isInto", System.Data.DbType.Int32, isinto);
            db.AddInParameter(cmd, "@isOperation", System.Data.DbType.Int32, isoperation);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }







    }
}
