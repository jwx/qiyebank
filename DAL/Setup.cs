﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Setup
    {
        Database db;

        public Setup()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public Setup(bool isService)
        {
            db = Tool.DatabaseHelper_Service.CreateDatabase();
        }

        public int Update_Setup(int useShouquanma, int backupTime, int hq_byDay, int hq_autoCalc, decimal hq_daylilv, int days, int hq_houcunxianqu, int dq_byDay, int dq_daoqi)
        {
            string sql = "update Setup set isSaved=1,useShouquanma=@useShouquanma,backupTime=@backupTime,hq_byDay=@hq_byDay,hq_autoCalc=@hq_autoCalc,hq_daylilv=@hq_daylilv,days=@days,hq_houcunxianqu=@hq_houcunxianqu,dq_byDay=@dq_byDay,dq_daoqi=@dq_daoqi";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@useShouquanma", System.Data.DbType.Int32, useShouquanma);
            db.AddInParameter(cmd, "@backupTime", System.Data.DbType.Int32, backupTime);
            db.AddInParameter(cmd, "@hq_byDay", System.Data.DbType.Int32, hq_byDay);
            db.AddInParameter(cmd, "@hq_autoCalc", System.Data.DbType.Int32, hq_autoCalc);
            db.AddInParameter(cmd, "@hq_daylilv", System.Data.DbType.Decimal, hq_daylilv);
            db.AddInParameter(cmd, "@days", System.Data.DbType.Int32, days);
            db.AddInParameter(cmd, "@hq_houcunxianqu", System.Data.DbType.Int32, hq_houcunxianqu);
            db.AddInParameter(cmd, "@dq_byDay", System.Data.DbType.Int32, dq_byDay);
            db.AddInParameter(cmd, "@dq_daoqi", System.Data.DbType.Int32, dq_daoqi);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_HuoqiLilv(string hq_lilv)
        {
            string sql = "update Setup set hq_lilv=@hq_lilv";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@hq_lilv", System.Data.DbType.String, hq_lilv);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_DingqiLilv(string dq_lilv, int dq_zc, decimal dq_zclilv)
        {
            string sql = "update Setup set dq_lilv=@dq_lilv,dq_zc=@dq_zc,dq_zclilv=@dq_zclilv";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@dq_lilv", System.Data.DbType.String, dq_lilv);
            db.AddInParameter(cmd, "@dq_zc", System.Data.DbType.Int32, dq_zc);
            db.AddInParameter(cmd, "@dq_zclilv", System.Data.DbType.Decimal, dq_zclilv);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        public int Update_DaikuanLilv(string dk_lilv)
        {
            string sql = "update Setup set dk_lilv=@dk_lilv";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@dk_lilv", System.Data.DbType.String, dk_lilv);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Setup Get_Setup()
        {
            string sql = "select top 1* from Setup ";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.Setup model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Setup>(reader);
            }
            return model;
        }

        public Model.Setup Get_HuoqiLilv()
        {
            string sql = "select top 1hq_byDay,hq_lilv,hq_daylilv,days from Setup ";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.Setup model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Setup>(reader);
            }
            return model;
        }

        public Model.Setup Get_DingqiLilv()
        {
            string sql = "select top 1 dq_byDay,dq_lilv,dq_zc,dq_zclilv from Setup";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.Setup model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Setup>(reader);
            }
            return model;
        }
        public Model.Setup Get_DaikuanLilv()
        {
            string sql = "select top 1 dk_byDay,dk_lilv from Setup";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.Setup model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Setup>(reader);
            }
            return model;
        }

        public void InitSystem_Table(string table)
        {
            string sql = "delete from " + table;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            db.ExecuteNonQuery(cmd);
        }

        public int InitSystem_Setup()
        {
            string sql = "update Setup set isSaved=0,useShouquanma=1,backupTime=0,hq_byDay=0,hq_autoCalc=0,hq_daylilv=0,days=0,hq_houcunxianqu=0,hq_lilv='',dq_byDay=0,dq_daoqi=0,dq_lilv='',dq_zc=0,dq_zclilv=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }

    //public class Setup_Service
    //{
    //    Database db;

    //    public Setup_Service(string SqlitePath)
    //    {
    //        db = new Tool.DatabaseHelper().CreateDatabase_Sqlite(SqlitePath);
    //    }

    //    public Model.Setup Get_Setup()
    //    {
    //        string sql = "select * from Setup limit 1";
    //        DbCommand cmd = db.GetSqlStringCommand(sql);

    //        Model.Setup model;
    //        using (var reader = db.ExecuteReader(cmd))
    //        {
    //            model = Tool.DataReaderToModel.ReaderToModel<Model.Setup>(reader);
    //        }
    //        return model;
    //    }
    //}
}
