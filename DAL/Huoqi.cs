﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class Huoqi
    {
        Database db;

        public Huoqi()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public Huoqi(bool isService)
        {
            db = isService ? Tool.DatabaseHelper_Service.CreateDatabase() : Tool.DatabaseHelper.CreateDatabase();
        }

        #region 增加

        public int Add(int uid, decimal jine, decimal yuanjine, string danhao, int cunqukuan, int canBaofei, int laiyuan, int isAutoCalc, string lilv, decimal zonglixi, string addTime, string cunkuanTime, int jbid, string add_remark, string delTime, int delid, string del_remark, string del_json, int oid_qk, decimal benjin, DbTransaction tran)
        {
            //string sql = "insert into Huoqi(uid,jine,yuanjine,danhao,cunqukuan,canBaofei,laiyuan,isAutoCalc,lilv,zonglixi,addTime,cunkuanTime,jxTime,jbid,add_remark,delTime,delid,del_remark,del_json,oid_qk,benjin) values (@uid,@jine,@yuanjine,@danhao,@cunqukuan,@canBaofei,@laiyuan,@isAutoCalc,@lilv,@zonglixi,@addTime,@cunkuanTime,@jxTime,@jbid,@add_remark,@delTime,@delid,@del_remark,@del_json,@oid_qk,@benjin);select last_insert_rowid()";
            string sql = "insert into Huoqi(uid,jine,yuanjine,danhao,cunqukuan,canBaofei,laiyuan,isAutoCalc,lilv,zonglixi,addTime,cunkuanTime,jxTime,jbid,add_remark,delTime,delid,del_remark,del_json,oid_qk,benjin,ExamineId) values (@uid,@jine,@yuanjine,@danhao,@cunqukuan,@canBaofei,@laiyuan,@isAutoCalc,@lilv,@zonglixi,@addTime,@cunkuanTime,@jxTime,@jbid,@add_remark,@delTime,@delid,@del_remark,@del_json,@oid_qk,@benjin,@ExamineId);SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@yuanjine", System.Data.DbType.Decimal, yuanjine);
            db.AddInParameter(cmd, "@danhao", System.Data.DbType.String, danhao);
            db.AddInParameter(cmd, "@cunqukuan", System.Data.DbType.Int32, cunqukuan);
            db.AddInParameter(cmd, "@canBaofei", System.Data.DbType.Int32, canBaofei);
            db.AddInParameter(cmd, "@laiyuan", System.Data.DbType.Int32, laiyuan);
            db.AddInParameter(cmd, "@isAutoCalc", System.Data.DbType.Int32, isAutoCalc);
            db.AddInParameter(cmd, "@lilv", System.Data.DbType.String, lilv);
            db.AddInParameter(cmd, "@zonglixi", System.Data.DbType.Decimal, zonglixi);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@cunkuanTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@jxTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, delTime);
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
            db.AddInParameter(cmd, "@del_json", System.Data.DbType.String, del_json);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);
            db.AddInParameter(cmd, "@benjin", System.Data.DbType.Decimal, benjin);
            db.AddInParameter(cmd, "@ExamineId", System.Data.DbType.Int32, -1);
            try
            {
                // return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        public int Add(int uid, decimal jine, decimal yuanjine, string danhao, int cunqukuan, int canBaofei, int laiyuan, int isAutoCalc, string lilv, decimal zonglixi, string addTime, string cunkuanTime, int jbid, string add_remark, string delTime, int delid, string del_remark, string del_json, int oid_qk, decimal benjin, int ExamineId, string ExPath, string ExRemark, string qyQK_json, int ExStaut, DbTransaction tran)
        {
            //string sql = "insert into Huoqi(uid,jine,yuanjine,danhao,cunqukuan,canBaofei,laiyuan,isAutoCalc,lilv,zonglixi,addTime,cunkuanTime,jxTime,jbid,add_remark,delTime,delid,del_remark,del_json,oid_qk,benjin) values (@uid,@jine,@yuanjine,@danhao,@cunqukuan,@canBaofei,@laiyuan,@isAutoCalc,@lilv,@zonglixi,@addTime,@cunkuanTime,@jxTime,@jbid,@add_remark,@delTime,@delid,@del_remark,@del_json,@oid_qk,@benjin);select last_insert_rowid()";
            string sql = "insert into Huoqi(uid,jine,yuanjine,danhao,cunqukuan,canBaofei,laiyuan,isAutoCalc,lilv,zonglixi,addTime,cunkuanTime,jxTime,jbid,add_remark,delTime,delid,del_remark,del_json,oid_qk,benjin,ExamineId,ExPath,ExRemark,qyQK_json,ExStaut) values (@uid,@jine,@yuanjine,@danhao,@cunqukuan,@canBaofei,@laiyuan,@isAutoCalc,@lilv,@zonglixi,@addTime,@cunkuanTime,@jxTime,@jbid,@add_remark,@delTime,@delid,@del_remark,@del_json,@oid_qk,@benjin,@ExamineId,@ExPath,@ExRemark,@qyQK_json,@ExStaut)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@yuanjine", System.Data.DbType.Decimal, yuanjine);
            db.AddInParameter(cmd, "@danhao", System.Data.DbType.String, danhao);
            db.AddInParameter(cmd, "@cunqukuan", System.Data.DbType.Int32, cunqukuan);
            db.AddInParameter(cmd, "@canBaofei", System.Data.DbType.Int32, canBaofei);
            db.AddInParameter(cmd, "@laiyuan", System.Data.DbType.Int32, laiyuan);
            db.AddInParameter(cmd, "@isAutoCalc", System.Data.DbType.Int32, isAutoCalc);
            db.AddInParameter(cmd, "@lilv", System.Data.DbType.String, lilv);
            db.AddInParameter(cmd, "@zonglixi", System.Data.DbType.Decimal, zonglixi);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@cunkuanTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@jxTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, delTime);
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
            db.AddInParameter(cmd, "@del_json", System.Data.DbType.String, del_json);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);
            db.AddInParameter(cmd, "@benjin", System.Data.DbType.Decimal, benjin);
            db.AddInParameter(cmd, "@ExamineId", System.Data.DbType.Int32, ExamineId);
            db.AddInParameter(cmd, "@ExPath", System.Data.DbType.String, ExPath);
            db.AddInParameter(cmd, "@ExRemark", System.Data.DbType.String, ExRemark);
            db.AddInParameter(cmd, "@qyQK_json", System.Data.DbType.String, qyQK_json);
            db.AddInParameter(cmd, "@ExStaut", System.Data.DbType.Int32, ExStaut);
            try
            {
                // return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        /// <summary>
        /// 报废
        /// </summary>
        /// <param name="id"></param>
        /// <param name="delid"></param>
        /// <param name="del_remark"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public int Update_Baofei(int id, int delid, string del_remark, DbTransaction tran)
        {
            string sql = "update Huoqi set delTime=@delTime,delid=@delid,del_remark=@del_remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_BaofeiByOid_Qk(int oid_qk, int laiyuan, int delid, string del_remark, DbTransaction tran)
        {
            string sql = "update Huoqi set delTime=@delTime,delid=@delid,del_remark=@del_remark where oid_qk=@oid_qk";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@delTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@delid", System.Data.DbType.Int32, delid);
            db.AddInParameter(cmd, "@del_remark", System.Data.DbType.String, del_remark);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Baofei_JineBack(int id, decimal jine, DbTransaction tran)
        {
            string sql = "update Huoqi set jine=jine+@jine where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            //db.AddInParameter(cmd, "@zonglixi", System.Data.DbType.Decimal, zonglixi);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Qukuan(int id, decimal jine, decimal lixi, DbTransaction tran)
        {
            string sql = "update Huoqi set jine=jine-@jine,zonglixi=zonglixi+@lixi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 溢家  取款  无利息，利息03-20 06-20 09-20 12-20自动结算
        /// </summary>
        /// <param name="id"></param>
        /// <param name="jine"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public int Update_Qukuan(int id, decimal jine, DbTransaction tran)
        {
            string sql = "update Huoqi set jine=jine-@jine where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            //  db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }


        public int Update_qukuanStatus(int id, int status, string remark, int ExId, DbTransaction tran)
        {
            string sql = "update Huoqi set ExamineId=@ExamineId,ExRemark=@ExRemark,ExStaut=@ExStaut where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@ExRemark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@ExamineId", System.Data.DbType.Int32, ExId);
            db.AddInParameter(cmd, "@ExStaut", System.Data.DbType.Int32, status);
            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_LiXi(int id, decimal lixi, DbTransaction tran)
        {
            string sql = "update Huoqi set zonglixi=zonglixi+@lixi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 自动结算活期单据，设置单据不能报废
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lilv"></param>
        /// <param name="jxTime"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public int Update_QukuanAuto_UpdtJxTime(int id, string lilv, decimal lixi, string jxTime, DbTransaction tran)
        {
            string sql = "update Huoqi set jine=0,canBaofei=0,lilv=@lilv,zonglixi=zonglixi+@lixi,jxTime=@jxTime where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@lilv", System.Data.DbType.String, lilv);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, lixi);
            db.AddInParameter(cmd, "@jxTime", System.Data.DbType.String, jxTime);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        ///// <summary>
        ///// 批量结息，取出所有存款，重新存入一张存款单据中
        ///// </summary>
        ///// <param name="id"></param> 
        ///// <param name="jxTime"></param>
        ///// <param name="tran"></param>
        ///// <returns></returns>
        //public int Update_JiexiAuto(int id, DateTime jxTime, DbTransaction tran)
        //{
        //    string sql = "update Huoqi set jine-0=0,canBaofei=0,jxTime=@jxTime,add_remark=add_remark+@add_remark where id=@id";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);
        //    db.AddInParameter(cmd, "@jxTime", System.Data.DbType.String, jxTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //    db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, "_批量结息");
        //    db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

        //    try
        //    {
        //        if (tran == null)
        //            return db.ExecuteNonQuery(cmd);
        //        cmd.Transaction = tran;
        //        cmd.Connection = tran.Connection;
        //        return db.ExecuteNonQuery(cmd, tran);
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}
        #endregion

        #region 查询

        public Model.Huoqi GetModel(int id)
        {
            string sql = "select * from V_Huoqi_Info where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Huoqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
            }
            return model;
        }

        public Model.Huoqi GetModel(int id, int uid)
        {
            string sql = "select * from V_Huoqi_Info where id=@id and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.Huoqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
            }
            return model;
        }

        public Model.Huoqi GetModelByWhere(int uid, string where)
        {
            string sql = "select * from V_Huoqi_Info where uid=@uid and " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.Huoqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
            }
            return model;
        }

        public Model.Huoqi GetModelByuid(int uid)
        {
            string sql = "select * from Huoqi where id=@uid and jine!=0 and cunqukuan=1 and delid=0  and canBaofei=1";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.Huoqi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
            }
            return model;
        }

        /// <summary>
        /// 获取活期利息数据，定期取款报废时调用
        /// </summary>
        /// <param name="oid_dqqk">定期取款单id</param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public Model.Huoqi GetModel_LixiByDingqi(int oid_dqqk, DbTransaction tran)
        {
            string sql = "select * from Huoqi where laiyuan=2 and oid_qk=@oid_qk";//laiyuan:定期利息存入
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_dqqk);

            Model.Huoqi model;
            if (tran == null)
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
                }
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                using (var reader = db.ExecuteReader(cmd, tran))
                {
                    model = Tool.DataReaderToModel.ReaderToModel<Model.Huoqi>(reader);
                }
            }
            return model;
        }

        public List<Model.Huoqi> GetListModel()
        {
            string sql = "select * from Huoqi";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Huoqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
            }
            return list;
        }

        public List<Model.Huoqi> GetListByWhere(string where)
        {
            string sql = "select * from Huoqi where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Huoqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
            }
            return list;
        }

        public int GetRecordCount(string where)
        {
            string sql = "select count(0) from Huoqi where " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
            }
            catch
            {
                return 0;
            }
        }

        public List<Model.Huoqi> GetList_Pager(int pageindex, int pagesize, string where)
        {
            //string sql = "select * from Huoqi where " + where + " limit @pagesize offset @pagesize*@pageindex";
            string sql = "select * from Huoqi where  1=1 " + where + " order by id desc offset  (@pageindex - 1) * @pagesize  rows fetch next @pagesize rows only";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@pageindex", System.Data.DbType.Int32, pageindex);
            db.AddInParameter(cmd, "@pagesize", System.Data.DbType.Int32, pagesize);

            List<Model.Huoqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
            }
            return list;
        }

        /// <summary>
        /// 获取指定用户id已审核的活期账户余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public decimal GetYue(int uid, DbTransaction tran)
        {
            string sql = "select sum(jine) from Huoqi where uid=@uid and jine!=0 and cunqukuan=1 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            try
            {
                if (tran == null)
                    return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd, tran)), 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                return 0;
            }
        }








        /// <summary>
        /// 获取当前系统中的活期存款总金额
        /// </summary>
        /// <returns></returns>
        public decimal GetSysYue()
        {
            string sql = "select sum(jine) from Huoqi where jine!=0 and cunqukuan=1 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                return 0;
            }
        }

        ///// <summary>
        ///// 获取当前系统中的活期已付利息总金额
        ///// </summary>
        ///// <returns></returns>
        //public decimal GetSysLixi_Payed()
        //{
        //    string sql = "select sum(zonglixi) from Huoqi where zonglixi!=0 and cunqukuan=1 and delid=0";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);

        //    try
        //    {
        //        return Math.Round(Falcon.Function.ToDecimal(db.ExecuteScalar(cmd)), 2, MidpointRounding.AwayFromZero);
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}

        /// <summary>
        /// 获取用户取款列表
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="hq_houcunxianqu"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_Qukuan(int uid, int hq_houcunxianqu, DbTransaction tran)
        {
            string sql = "select * from Huoqi where uid=@uid and jine!=0 and cunqukuan=1 and delid=0 order by id " + (hq_houcunxianqu == 0 ? "asc" : "desc");
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            List<Model.Huoqi> list;
            if (tran == null)
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
                }
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                using (var reader = db.ExecuteReader(cmd, tran))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取系统中所有有效的活期存款单
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_Qukuan(DbTransaction tran)
        {
            string sql = "select * from Huoqi where jine!=0 and cunqukuan=1 and delid=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Huoqi> list;
            if (tran == null)
            {
                using (var reader = db.ExecuteReader(cmd))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
                }
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                using (var reader = db.ExecuteReader(cmd, tran))
                {
                    list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
                }
            }
            return list;
        }


        ///// <summary>
        ///// 获取用户取款单和利息信息
        ///// </summary>
        ///// <param name="laiyuan"></param>
        ///// <returns></returns>
        //public List<Model.Huoqi> GetList_Qukuan_Lixi(Config.Enums.Huoqi_Laiyuan laiyuan)
        //{
        //    string sql = "select uid,zonglixi from Huoqi where zonglixi!=0 and cunqukuan=2 and delid=0 and laiyuan=@laiyuan";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);
        //    db.AddInParameter(cmd, "@laiyuan", System.Data.DbType.Int32, laiyuan);

        //    List<Model.Huoqi> list;
        //    using (var reader = db.ExecuteReader(cmd))
        //    {
        //        list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
        //    }
        //    return list;
        //}

        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_YifuLixi()
        {
            //活期本息取款单据的总利息zonglixi，活期结转单据的原金额-剩余金额,yuanjine-jine
            string sql = "select uid,jine,yuanjine,zonglixi,laiyuan from Huoqi where zonglixi!=0 and cunqukuan=2 and delid=0 and laiyuan=" + ((int)Config.Enums.Huoqi_Laiyuan.本息取款) + " union all select uid,jine,yuanjine,zonglixi,laiyuan from Huoqi where cunqukuan=1 and delid=0 and laiyuan=" + (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入 + "";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Huoqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
            }
            return list;
        }

        ///// <summary>
        ///// 获取用户存款单和利息信息--零钱已结转
        ///// </summary>
        ///// <param name="laiyuan"></param>
        ///// <returns></returns>
        //public List<Model.Huoqi> GetList_WeifuLixi()
        //{
        //    ////活期存款单来源为活期利息存入||定期利息存入||定期本息存入（需要去除本金）三者的剩余金额，活期结转单据的原金额-剩余金额,yuanjine-jine
        //    //string sql = "select uid,jine from Huoqi where cunqukuan=1 and delid=0 and laiyuan in (" + ((int)Config.Enums.Huoqi_Laiyuan.零钱利息存入) + "," + ((int)Config.Enums.Huoqi_Laiyuan.定期利息存入) + ") union all select uid,case when jine-benjin>=0 then jine-benjin else 0  from Huoqi where cunqukuan=1 and delid=0 and laiyuan=" + ((int)Config.Enums.Huoqi_Laiyuan.定期本息存入) + "";
        //    //活期存款单来源为活期利息存入（活期本金取款和系统自动结算的活期利息）+预估
        //    string sql = "select uid,jine from Huoqi where cunqukuan=1 and delid=0 and laiyuan=" + ((int)Config.Enums.Huoqi_Laiyuan.零钱利息存入);
        //    DbCommand cmd = db.GetSqlStringCommand(sql);

        //    List<Model.Huoqi> list;
        //    using (var reader = db.ExecuteReader(cmd))
        //    {
        //        list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
        //    }
        //    return list;
        //}


        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_WeifuLixi()
        {
            //活期存款单来源为活期利息存入||定期利息存入||定期本息存入（需要去除本金）三者的剩余金额，活期结转单据的原金额-剩余金额,yuanjine-jine
            //select uid, jine, laiyuan from Huoqi where cunqukuan = 1 and jine-0 != 0 and delid = 0 and laiyuan in (1, 2) union all select uid, laiyuan,case when jine-benjin >= 0 then jine-benjin else 0 end as jine from Huoqi where cunqukuan = 1 and jine-0 != 0 and delid = 0 and laiyuan = 3
            string sql = "select uid,jine,laiyuan from Huoqi where cunqukuan=1 and jine!=0 and delid=0 and laiyuan in (" + ((int)Config.Enums.Huoqi_Laiyuan.零钱利息存入) + "," + ((int)Config.Enums.Huoqi_Laiyuan.定期利息存入) + ") union all select uid,laiyuan,case when jine-benjin>=0 then jine-benjin else 0 end as jine from Huoqi where cunqukuan=1 and jine!=0 and delid=0 and laiyuan=" + ((int)Config.Enums.Huoqi_Laiyuan.定期本息存入) + "";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Huoqi> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Huoqi>(reader);
            }
            return list;
        }
        #endregion

        #region  删除

        public int Delete(int id, DbTransaction tran)
        {
            string sql = "delete from Huoqi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}