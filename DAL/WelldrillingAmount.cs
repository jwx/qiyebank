﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class WelldrillingAmount
    {

        Database db;

        public WelldrillingAmount()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }


        public Model.WelldrillingAmount GetModelByID(int id)
        {
            string sql = "select * from WelldrillingAmount where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.WelldrillingAmount model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.WelldrillingAmount>(reader);
            }
            return model;
        }

        public int Add(string money, DbTransaction tran)
        {
            string sql = "insert into WelldrillingAmount(money) values (@money)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@money", System.Data.DbType.String, money);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update(int id, string money)
        {
            string sql = "update WelldrillingAmount set  money=@money  where  id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@money", System.Data.DbType.String, money);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Delete(int id)
        {
            string sql = "delete from  WelldrillingAmount  where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }


        public List<Model.WelldrillingAmount> getList()
        {
            string sql = "select * from WelldrillingAmount";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            List<Model.WelldrillingAmount> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.WelldrillingAmount>(reader);
                return list;
            }
        }


    }
}
