﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class HuoqiIn
    {
        Database db;

        public HuoqiIn()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public int Add(int uid, decimal jine, string zhifubao, string realname, string mobile, string model_setup, string addTime, string cunkuanTime, string add_remark)
        {
            string sql = "insert into HuoqiIn (uid,jine,zhifubao,realname,mobile,model_setup,addTime,cunkuanTime,add_remark,ckid,ck_remark,ckTime,oid) values (@uid,@jine,@zhifubao,@realname,@mobile,@model_setup,@addTime,@cunkuanTime,@add_remark,@ckid,@ck_remark,@ckTime,@oid)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@zhifubao", System.Data.DbType.String, zhifubao);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@model_setup", System.Data.DbType.String, model_setup);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);
            db.AddInParameter(cmd, "@cunkuanTime", System.Data.DbType.String, cunkuanTime);
            db.AddInParameter(cmd, "@add_remark", System.Data.DbType.String, add_remark);
            db.AddInParameter(cmd, "@ckid", System.Data.DbType.Int32, 0);
            db.AddInParameter(cmd, "@ck_remark", System.Data.DbType.String, "");
            db.AddInParameter(cmd, "@ckTime", System.Data.DbType.String, Config.Config.str_defTime);
            db.AddInParameter(cmd, "@oid", System.Data.DbType.Int32, 0);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update_Check(int id, int ckid, string ck_remark, int oid, DbTransaction tran)
        {
            string sql = "update HuoqiIn set ckid=@ckid,ck_remark=@ck_remark,ckTime=@ckTime,oid=@oid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@ckid", System.Data.DbType.Int32, ckid);
            db.AddInParameter(cmd, "@ck_remark", System.Data.DbType.String, ck_remark);
            db.AddInParameter(cmd, "@oid", System.Data.DbType.Int32, oid);
            db.AddInParameter(cmd, "@ckTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public Model.HuoqiIn GetModel(int id)
        {
            string sql = "select * from V_HuoqiIn_Info where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.HuoqiIn model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.HuoqiIn>(reader);
            }
            return model;
        }

        public Model.HuoqiIn GetModel(int id, int uid)
        {
            string sql = "select * from V_HuoqiIn_Info where id=@id and uid=@uid";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);

            Model.HuoqiIn model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.HuoqiIn>(reader);
            }
            return model;
        }
    }
}
