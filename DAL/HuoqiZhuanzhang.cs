﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class HuoqiZhuanzhang
    {
        Database db;

        public HuoqiZhuanzhang()
        {
            db = Tool.DatabaseHelper.CreateDatabase();
        }

        public HuoqiZhuanzhang(bool isService)
        {
            db = Tool.DatabaseHelper_Service.CreateDatabase();
        }

        public int Add(int uid, decimal jine, decimal lixi, int oid_qk, int to_uid, int oid_ck, string remark, string addTime, DbTransaction tran)
        {
            //string sql = "insert into HuoqiZhuanzhang (uid,jine,lixi,oid_qk,to_uid,oid_ck,remark,addTime) values (@uid,@jine,@lixi,@oid_qk,@to_uid,@oid_ck,@remark,@addTime);select last_insert_rowid()";
            string sql = "insert into HuoqiZhuanzhang (uid,jine,lixi,oid_qk,to_uid,oid_ck,remark,addTime) values (@uid,@jine,@lixi,@oid_qk,@to_uid,@oid_ck,@remark,@addTime)SELECT @@IDENTITY";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@jine", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@lixi", System.Data.DbType.Decimal, jine);
            db.AddInParameter(cmd, "@oid_qk", System.Data.DbType.Int32, oid_qk);
            db.AddInParameter(cmd, "@to_uid", System.Data.DbType.Int32, to_uid);
            db.AddInParameter(cmd, "@oid_ck", System.Data.DbType.Int32, oid_ck);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, addTime);

            try
            {
               // return db.ExecuteNonQuery(cmd);
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }

        public Model.HuoqiZhuanzhang GetModel(int id)
        {
            string sql = "select * from V_HuoqiZhuanzhang_Info where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.HuoqiZhuanzhang model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.HuoqiZhuanzhang>(reader);
            }
            return model;
        }
    }
}
