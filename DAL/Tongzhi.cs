﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Tongzhi
    {
        public int Add(string title, string content, int isEnable, int sort, int jbid, DbTransaction tran)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "insert into Tongzhi (title,content,addTime,isEnable,sort,jbid) values (@title,@content,@addTime,@isEnable,@sort,@jbid)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@title", System.Data.DbType.String, title);
            db.AddInParameter(cmd, "@content", System.Data.DbType.String, content);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
             //   return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string title, string content, int isEnable, int sort, int jbid, DbTransaction tran)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "update Tongzhi set title=@title,content=@content,isEnable=@isEnable,sort=@sort,jbid=@jbid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@title", System.Data.DbType.String, title);
            db.AddInParameter(cmd, "@content", System.Data.DbType.String, content);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Tongzhi GetModel(int id)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "select * from Tongzhi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Tongzhi model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Tongzhi>(reader);
            }
            return model;
        }

        public int Delete(int id)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            string sql = "delete from Tongzhi where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }
}
