USE [jingyi_aliyun_ajax]
GO
/****** Object:  StoredProcedure [dbo].[DistinctQuery]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[DistinctQuery]

(
@tableName NVARCHAR(200),      ----要显示的表或多个表的连接
@fieldNames NVARCHAR(200)='*', ----要显示的字段列表
@pageSize INT = 10,            ----每页显示的记录个数
@page INT = 10,                ----要显示那一页的记录
@pageCount INT = 1 output,     ----查询结果分页后的总页数
@counts INT = 1 output,        ----查询到的总记录数
@fieldSort NVARCHAR(200)= null,----排序字段列表或条件
@sort BIT = 1,                 ----排序方法，0为升序，1为降序--程序传参如：' SortA Asc,SortB Desc,SortC ')
@condition NVARCHAR(200)= null,----查询条件,不需WHERE
@keyID NVARCHAR(100),          ----主表的主键
@distinct BIT = 0              ----是否添加查询字段的 DISTINCT 默认0不添加/1添加
)

AS
  
SET NOCOUNT ON 
Declare @SELECT NVARCHAR(1000)    ----存放动态生成的SQL语句 
Declare @strCounts NVARCHAR(1000) ----存放取得查询结果总数的查询语句 
Declare @strID  NVARCHAR(1000)    ----存放取得查询开头或结尾ID的查询语句  
Declare @sortTypeA NVARCHAR(10)   ----数据排序规则A 
Declare @SortTypeB NVARCHAR(10)   ----数据排序规则B 
Declare @distSelect NVARCHAR(50)  ----对含有DISTINCT的查询进行SQL构造 
Declare @distCounts NVARCHAR(50)  ----对含有DISTINCT的总数查询进行SQL构造 
DECLARE @SortfieldA NVARCHAR(50)  ----对含有是否还有排序字段时的排序方式组合A 
DECLARE @SortfieldB NVARCHAR(50)  ----对含有是否还有排序字段时的排序方式组合B
 
IF @distinct = 0 
    BEGIN 
        SET @distSelect = 'SELECT ' 
        SET @distCounts = ' COUNT(*)' 
    END 
ELSE 
    BEGIN 
        SET @distSelect = 'SELECT distinct ' 
       SET @distCounts = ' COUNT(DISTINCT '+@keyID+')' 
    END 
IF @sort=0 
    BEGIN 
        SET @SortTypeB=' ASC '
        SET @sortTypeA=' DESC ' 
    END 
ELSE 
    BEGIN 
        SET @SortTypeB=' DESC ' 
        SET @sortTypeA=' ASC ' 
    END 
IF @fieldSort IS NOT NULL AND @fieldSort<>'' --排序字段不为空时 
    BEGIN 
        SET @SortfieldB=' order by '+ @fieldSort +' '+ @SortTypeB 
        SET @SortfieldA=' order by '+ @fieldSort +' '+ @SortTypeA 
    END 
ELSE 
    BEGIN 
        SET @SortfieldB='' 
        SET @SortfieldA='' 
    END 
 

--------生成查询语句--------

--此处@strCounts为取得查询结果数量的语句

IF @condition is null or @condition=''     --没有设置显示条件
    BEGIN 
        SET @SELECT =  @fieldNames + ' FROM ' + @tableName 
        SET @strCounts = @distSelect+' @counts='+@distCounts+' FROM '+@tableName 
        SET @strID = ' FROM ' + @tableName 
    END 
ELSE 
    BEGIN 
        SET @SELECT = + @fieldNames + 'FROM ' + @tableName + ' WHERE  ' + @condition 
        SET @strCounts = @distSelect+' @counts='+@distCounts+' FROM '+@tableName + ' WHERE ' + @condition 
        SET @strID = ' FROM ' + @tableName + ' WHERE  ' + @condition 
    END
	 

----取得查询结果总数量-----

exec sp_executesql @strCounts,N'@counts INT out ',@counts out 
DECLARE @tmpCounts INT  
IF @counts = 0
    SET @tmpCounts = 1 
ELSE 
    SET @tmpCounts = @counts 

    --取得分页总数

    SET @pageCount=(@tmpCounts+@pageSize-1)/@pageSize 

    --/**当前页大于总页数 取最后一页**/

    IF @page>@pageCount 
        SET @page=@pageCount 

   --/*-----数据分页2分处理-------*/

    DECLARE @pageIndex INT --总数/页大小 
    DECLARE @lastcount INT --总数%页大小
	  
    SET @pageIndex = @tmpCounts/@pageSize
    SET @lastcount = @tmpCounts%@pageSize

    IF @lastcount > 0 
        SET @pageIndex = @pageIndex + 1 
    ELSE
        SET @lastcount = @pageSize
		 
    --显示分页

    IF @condition is null or @condition=''     --没有设置显示条件 
    BEGIN 
        IF @pageIndex<2 or @page<=@pageIndex / 2 + @pageIndex % 2   --前半部分数据处理 
            BEGIN 
                SET @strCounts=@distSelect+' TOP '+ CAST(@pageSize as VARCHAR(4))+' '+ @fieldNames+' FROM '+@tableName + ' WHERE '+@keyID+' not in('+ @distSelect+' TOP '+ CAST(@pageSize*(@page-1) as Varchar(20)) +' '+ @keyID +' FROM '+@tableName + @SortfieldB +')' + @SortfieldB 
            END 
        ELSE 
            BEGIN 
            SET @page = @pageIndex-@page+1 --后半部分数据处理 
                IF @page <= 1 --最后一页数据显示 
                    SET @strCounts=@distSelect+' * FROM ('+@distSelect+' TOP '+ CAST(@lastcount as VARCHAR(4))+' '+ @fieldNames+' FROM '+@tableName + @SortfieldA+') AS TempTB '+@SortfieldB 
                ELSE                
                    SET @strCounts=@distSelect+' * FROM ('+@distSelect+' TOP '+ CAST(@pageSize as VARCHAR(4))+' '+ @fieldNames+' FROM '+@tableName + ' WHERE '+@keyID+' not in('+ @distSelect+' TOP '+ CAST(@pageSize*(@page-2)+@lastcount as Varchar(20)) +' '+ @keyID +' FROM '+@tableName + @SortfieldA+')' + @SortfieldA+') AS TempTB '+@SortfieldB
            END 
    END
	 

    ELSE --有查询条件 
    BEGIN 
        IF @pageIndex<2 or @page<=@pageIndex / 2 + @pageIndex % 2   --前半部分数据处理 
        BEGIN 
                SET @strCounts=@distSelect+' TOP '+ CAST(@pageSize as VARCHAR(4))+' '+ @fieldNames +' FROM  '+@tableName + ' WHERE '+@keyID+' not in('+ @distSelect+' TOP '+ CAST(@pageSize*(@page-1) as Varchar(20)) +' '+ @keyID +' FROM '+@tableName + ' Where ' + @condition + @SortfieldB+')'+' AND ' + @condition + @SortfieldB            
        END 
        ELSE 
        BEGIN 
            SET @page = @pageIndex-@page+1 --后半部分数据处理 
            IF @page <= 1 --最后一页数据显示 
                    SET @strCounts=@distSelect+' * FROM ('+@distSelect+' TOP '+ CAST(@lastcount as VARCHAR(4))+' '+ @fieldNames+' FROM '+@tableName + ' WHERE  '+ @condition +@SortfieldA+') AS TempTB '+@SortfieldB 
            ELSE 
                    SET @strCounts=@distSelect+' * FROM ('+@distSelect+' TOP '+ CAST(@pageSize as VARCHAR(4))+' '+ @fieldNames+' FROM '+@tableName + ' WHERE '+@keyID+' not in('+ @distSelect+' TOP '+ CAST(@pageSize*(@page-2)+@lastcount as Varchar(20)) +' ' + @keyID +' FROM '+@tableName +' WHERE  '+ @condition +@SortfieldA+')' +' AND '+ @condition +@SortfieldA+') AS TempTB ' + @SortfieldB
        END    
    END 

------返回查询结果-----

exec sp_executesql @strCounts
SET NOCOUNT OFF




GO
/****** Object:  StoredProcedure [dbo].[Query]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Query]
    @TableName VARCHAR(200),     --表名          
    @FieldList VARCHAR(2000),    --显示列名，如果是全部字段则为*          
    @PrimaryKey VARCHAR(100),    --单一主键或唯一值键          
    @Where VARCHAR(2000),        --查询条件 不含'where'字符，如id>10 and len(userid)>9          
    @Order VARCHAR(1000),        --排序 不含'order by'字符，如id asc,userid desc，必须指定asc或desc          
    --注意当@SortType=3时生效，记住一定要在最后加上主键，否则会让你比较郁闷          
    @SortType INT,               --排序规则 1:主键正序asc 2:主键倒序desc 3:多列排序方法          
    @RecorderCount INT,          --记录总数 0:会返回总记录          
    @PageSize INT,               --每页输出的记录数          
    @PageIndex INT,              --当前页数          
    @TotalCount INT OUTPUT ,     --记返回总记录          
    @TotalPageCount INT OUTPUT   --返回总页数          
AS          
SET NOCOUNT ON          
    IF ISNULL(@TotalCount,'') = '' SET @TotalCount = 0          
    SET @Order = RTRIM(LTRIM(@Order))          
    SET @PrimaryKey = RTRIM(LTRIM(@PrimaryKey))          
    SET @FieldList = REPLACE(RTRIM(LTRIM(@FieldList)),' ','')          
    WHILE CHARINDEX(', ',@Order) > 0 or CHARINDEX(' ,',@Order) > 0          
        BEGIN          
            SET @Order = REPLACE(@Order,', ',',')          
            SET @Order = REPLACE(@Order,' ,',',')          
        END          
    IF ISNULL(@TableName,'') = '' or ISNULL(@FieldList,'') = ''          
            or ISNULL(@PrimaryKey,'') = ''          
            or @SortType < 1 or @SortType >3          
            or @RecorderCount  < 0 or @PageSize < 0 or @PageIndex < 0          
        BEGIN          
            PRINT('ERR_00')          
            RETURN          
        END          
    IF @SortType = 3          
        BEGIN          
            IF (UPPER(RIGHT(@Order,4))!=' ASC' AND UPPER(RIGHT(@Order,5))!=' DESC')          
                BEGIN PRINT('ERR_02') RETURN END          
        END          
    DECLARE @new_where1 VARCHAR(1000)          
    DECLARE @new_where2 VARCHAR(1000)          
    DECLARE @new_order1 VARCHAR(1000)          
    DECLARE @new_order2 VARCHAR(1000)          
    DECLARE @new_order3 VARCHAR(1000)          
    DECLARE @Sql VARCHAR(8000)          
    DECLARE @SqlCount NVARCHAR(4000)          
    IF ISNULL(@where,'') = ''          
        BEGIN          
            SET @new_where1 = ' '          
            SET @new_where2 = ' Where  '          
        END          
    ELSE          
        BEGIN          
            SET @new_where1 = ' Where ' + @where          
            SET @new_where2 = ' Where ' + @where + ' AND '          
        END          
    IF ISNULL(@order,'') = '' or @SortType = 1  or @SortType = 2          
        BEGIN          
            IF @SortType = 1          
                BEGIN          
                    SET @new_order1 = ' orDER BY ' + @PrimaryKey + ' ASC'          
                    SET @new_order2 = ' orDER BY ' + @PrimaryKey + ' DESC'          
                END          
            IF @SortType = 2          
                BEGIN          
                    SET @new_order1 = ' orDER BY ' + @PrimaryKey + ' DESC'          
                    SET @new_order2 = ' orDER BY ' + @PrimaryKey + ' ASC'          
                END          
        END          
    ELSE          
        BEGIN          
            SET @new_order1 = ' orDER BY ' + @Order          
        END          
    
    IF @SortType = 3 AND  CHARINDEX(','+@PrimaryKey+' ',','+@Order)>0          
    BEGIN          
        SET @new_order1 = ' orDER BY ' + @Order          
        SET @new_order2 = @Order + ','          
        SET @new_order2 = REPLACE(REPLACE(@new_order2,'ASC,','{ASC},'),'DESC,','{DESC},')          
        SET @new_order2 = REPLACE(REPLACE(@new_order2,'{ASC},','DESC,'),'{DESC},','ASC,')          
        SET @new_order2 = ' orDER BY ' + SUBSTRING(@new_order2,1,LEN(@new_order2)-1)          
        IF @FieldList <> '*'          
            BEGIN          
                SET @new_order3 = REPLACE(REPLACE(@Order + ',','ASC,',','),'DESC,',',')          
                SET @FieldList = ',' + @FieldList          
                WHILE CHARINDEX(',',@new_order3)>0          
                    BEGIN          
                        IF CHARINDEX(SUBSTRING(','+@new_order3,1,CHARINDEX(',',@new_order3)),','+@FieldList+',')>0          
                            BEGIN          
                                SET @FieldList =          
                                @FieldList + ',' + SUBSTRING(@new_order3,1,CHARINDEX(',',@new_order3))          
                            END          
                        SET @new_order3 =          
                        SUBSTRING(@new_order3,CHARINDEX(',',@new_order3)+1,LEN(@new_order3))          
                    END          
                SET @FieldList = SUBSTRING(@FieldList,2,LEN(@FieldList))          
            END          
        END     
         
    SET @SqlCount = 'Select @TotalCount=COUNT(*),@TotalPageCount=CEILING((COUNT(*)+0.0)/'          
    + CAST(@PageSize AS VARCHAR)+') FROM (Select * FROM ' + @TableName + @new_where1+') AS T'          
    IF @RecorderCount  = 0          
        BEGIN          
            EXEC SP_EXECUTESQL @SqlCount,N'@TotalCount INT OUTPUT,@TotalPageCount INT OUTPUT',          
            @TotalCount OUTPUT,@TotalPageCount OUTPUT          
        END          
    ELSE          
        BEGIN          
            Select @TotalCount = @RecorderCount        
        END          
    IF @PageIndex > CEILING((@TotalCount+0.0)/@PageSize)          
        BEGIN          
            SET @PageIndex =  CEILING((@TotalCount+0.0)/@PageSize)          
        END          
    IF @PageIndex = 1 or @PageIndex >= CEILING((@TotalCount+0.0)/@PageSize)          
        BEGIN          
            IF @PageIndex = 1 --返回第一页数据          
                BEGIN          
                    SET @Sql = 'Select * FROM (Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM '          
                    + @TableName + @new_where1 + @new_order1 +') AS TMP ' + @new_order1    
                END          
            IF @PageIndex >= CEILING((@TotalCount+0.0)/@PageSize)  --返回最后一页数据          
                BEGIN          
                    SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ('          
                    + 'Select TOP ' + STR(ABS(@PageSize*@PageIndex-@TotalCount-@PageSize))          
                    + ' ' + @FieldList + ' FROM '          
                    + @TableName + @new_where1 + @new_order2 + ' ) AS TMP '          
                    + @new_order1          
                END          
        END          
    ELSE      
            
        BEGIN          
        IF @SortType = 1  --仅主键正序排序          
            BEGIN          
                IF @PageIndex <= CEILING((@TotalCount+0.0)/@PageSize)/2  --正向检索          
                    BEGIN          
                        SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM '          
                        + @TableName + @new_where2 + @PrimaryKey + ' > '          
                        + '(Select MAX(' + @PrimaryKey + ') FROM (Select TOP '          
                        + STR(@PageSize*(@PageIndex-1)) + ' ' + @PrimaryKey          
                        + ' FROM ' + @TableName          
                        + @new_where1 + @new_order1 +' ) AS TMP) '+ @new_order1          
                    END          
                ELSE  --反向检索          
                    BEGIN          
                        SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ('          
                        + 'Select TOP ' + STR(@PageSize) + ' '          
                        + @FieldList + ' FROM '          
                        + @TableName + @new_where2 + @PrimaryKey + ' < '          
                        + '(Select MIN(' + @PrimaryKey + ') FROM (Select TOP '         
                        + STR(@TotalCount-@PageSize*@PageIndex) + ' ' + @PrimaryKey          
                        + ' FROM ' + @TableName          
                        + @new_where1 + @new_order2 +' ) AS TMP) '+ @new_order2          
                        + ' ) AS TMP ' + @new_order1          
                    END          
            END          
        IF @SortType = 2  --仅主键反序排序          
            BEGIN          
                IF @PageIndex <= CEILING((@TotalCount+0.0)/@PageSize)/2  --正向检索          
                    BEGIN          
                        SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM '          
                        + @TableName + @new_where2 + @PrimaryKey + ' < '          
                        + '(Select MIN(' + @PrimaryKey + ') FROM (Select TOP '          
                        + STR(@PageSize*(@PageIndex-1)) + ' ' + @PrimaryKey          
                        +' FROM '+ @TableName          
                        + @new_where1 + @new_order1 + ') AS TMP) '+ @new_order1          
                    END          
                ELSE  --反向检索          
                    BEGIN          
                        SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ('          
                        + 'Select TOP ' + STR(@PageSize) + ' '          
                        + @FieldList + ' FROM '          
                        + @TableName + @new_where2 + @PrimaryKey + ' > '          
                        + '(Select MAX(' + @PrimaryKey + ') FROM (Select TOP '          
                        + STR(@TotalCount-@PageSize*@PageIndex) + ' ' + @PrimaryKey          
                        + ' FROM ' + @TableName          
                        + @new_where1 + @new_order2 +' ) AS TMP) '+ @new_order2          
                        + ' ) AS TMP ' + @new_order1          
                    END          
            END          
        IF @SortType = 3  --多列排序，必须包含主键，且放置最后，否则不处理          
            BEGIN          
                IF CHARINDEX(',' + @PrimaryKey + ' ',',' + @Order) = 0          
                    BEGIN PRINT('ERR_02') RETURN END          
                    IF @PageIndex <= CEILING((@TotalCount+0.0)/@PageSize)/2  --正向检索          
                        BEGIN          
                            SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ( '          
                            + 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ( '          
                            + ' Select TOP ' + STR(@PageSize*@PageIndex) + ' ' + @FieldList          
                            + ' FROM ' + @TableName + @new_where1 + @new_order1 + ' ) AS TMP '          
                            + @new_order2 + ' ) AS TMP ' + @new_order1          
                        END          
                    ELSE  --反向检索          
                        BEGIN          
                            SET @Sql = 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ( '          
                            + 'Select TOP ' + STR(@PageSize) + ' ' + @FieldList + ' FROM ( '          
                            + ' Select TOP ' + STR(@TotalCount-@PageSize *@PageIndex+@PageSize) + ' ' + @FieldList          
                            + ' FROM ' + @TableName + @new_where1 + @new_order2 + ' ) AS TMP '          
                            + @new_order1 + ' ) AS TMP ' + @new_order1          
                        END          
            END          
        END          
    PRINT(@SQL)          
    EXEC(@Sql)





GO
/****** Object:  Table [dbo].[Notification]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[content] [nvarchar](max) NULL,
	[addTime] [datetime] NULL,
	[isEnable] [int] NULL,
	[sort] [int] NULL,
	[jbid] [int] NULL,
 CONSTRAINT [PK_Notice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubNotification]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubNotification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nid] [int] NULL,
	[uid] [int] NULL,
	[isRead] [int] NULL,
	[readTime] [datetime] NULL,
 CONSTRAINT [PK_SubNotification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_Depart]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_Depart](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[pid] [int] NULL,
	[sort] [int] NULL,
	[remark] [nvarchar](100) NULL,
 CONSTRAINT [PK_Z_Depart] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_Forms]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_Forms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[url] [nvarchar](300) NULL,
	[pid] [int] NULL,
	[power] [nvarchar](500) NULL,
	[sort] [int] NULL,
	[isMenu] [int] NULL,
	[isEnable] [int] NULL,
	[remark] [nvarchar](100) NULL,
	[model] [nvarchar](30) NULL,
	[defFilter] [nvarchar](max) NULL,
	[disFilter] [nvarchar](max) NULL,
 CONSTRAINT [PK_Z_Forms] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_LoginLogs]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_LoginLogs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uid] [int] NULL,
	[username] [nvarchar](50) NULL,
	[realname] [nvarchar](50) NULL,
	[ip] [nvarchar](50) NULL,
	[addTime] [datetime] NULL,
 CONSTRAINT [PK_Z_LoginLogs_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_Role]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[power] [nvarchar](max) NULL,
	[isEnable] [int] NULL,
	[sort] [int] NULL,
	[remark] [nvarchar](100) NULL,
 CONSTRAINT [PK_Z_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_Settings]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_Settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[logo] [nvarchar](300) NULL,
	[image] [nvarchar](300) NULL,
	[mlogo] [nvarchar](300) NULL,
	[smtp] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[pswd] [nvarchar](50) NULL,
	[keywords] [nvarchar](300) NULL,
	[description] [nvarchar](max) NULL,
	[cpuinfo] [nvarchar](500) NULL,
 CONSTRAINT [PK_Z_Settings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Z_Users]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Z_Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[userpswd] [nvarchar](500) NULL,
	[realname] [nvarchar](30) NULL,
	[mobile] [nvarchar](30) NULL,
	[email] [nvarchar](50) NULL,
	[count] [int] NULL,
	[depart] [int] NULL,
	[role] [int] NULL,
	[isEnable] [int] NULL,
	[isDptMgr] [int] NULL,
	[addTime] [datetime] NULL,
	[remark] [nvarchar](100) NULL,
	[jbid] [int] NULL,
	[isDel] [int] NULL,
 CONSTRAINT [PK_Z_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[V_Notification_Users]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Notification_Users]
AS
SELECT   dbo.Notification.id, dbo.Notification.title, dbo.Notification.[content], dbo.Notification.addTime, dbo.Notification.isEnable, 
                dbo.Notification.jbid, dbo.Notification.sort, dbo.Z_Users.realname AS jbname
FROM      dbo.Notification INNER JOIN
                dbo.Z_Users ON dbo.Notification.jbid = dbo.Z_Users.id



GO
/****** Object:  View [dbo].[V_SubNotification_Info]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_SubNotification_Info]
AS
SELECT   dbo.SubNotification.nid, dbo.SubNotification.uid, dbo.SubNotification.isRead, dbo.SubNotification.readTime, 
                Z_Users_1.realname, dbo.Notification.title, dbo.Notification.[content], dbo.Notification.addTime, 
                dbo.Notification.isEnable, dbo.Notification.sort, dbo.Notification.jbid, dbo.SubNotification.id, 
                dbo.Z_Users.realname AS jbname
FROM      dbo.SubNotification INNER JOIN
                dbo.Notification ON dbo.SubNotification.nid = dbo.Notification.id INNER JOIN
                dbo.Z_Users ON dbo.Notification.jbid = dbo.Z_Users.id LEFT OUTER JOIN
                dbo.Z_Users AS Z_Users_1 ON dbo.SubNotification.uid = Z_Users_1.id



GO
/****** Object:  View [dbo].[V_Users_Depart_Role]    Script Date: 2016/8/4 12:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_Users_Depart_Role]
AS
SELECT   dbo.Z_Users.id, dbo.Z_Users.username, dbo.Z_Users.userpswd, dbo.Z_Users.realname, dbo.Z_Users.mobile, 
                dbo.Z_Users.email, dbo.Z_Users.count, dbo.Z_Users.depart, dbo.Z_Users.role, dbo.Z_Users.isEnable, 
                dbo.Z_Users.isDptMgr, dbo.Z_Users.addTime, dbo.Z_Users.remark, dbo.Z_Users.jbid, dbo.Z_Users.isDel, 
                dbo.Z_Depart.name AS departname, dbo.Z_Role.name AS rolename, Z_Users_1.realname AS jbname
FROM      dbo.Z_Users INNER JOIN
                dbo.Z_Role ON dbo.Z_Users.role = dbo.Z_Role.id INNER JOIN
                dbo.Z_Depart ON dbo.Z_Users.depart = dbo.Z_Depart.id INNER JOIN
                dbo.Z_Users AS Z_Users_1 ON dbo.Z_Users.jbid = Z_Users_1.id



GO
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([id], [title], [content], [addTime], [isEnable], [sort], [jbid]) VALUES (1, N'通知公告测试', N'<p>
	系统通知公告测试内容如下：
</p>
<p>
	<br />
</p>
<p>
	<br />
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	原标题：南<a href="http://travel.ifeng.com/theme/island/list_0/0.shtml" target="_blank">海岛</a>礁面临的最大难题，解决了！
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	在我们为中国南海造岛、永暑礁试飞成功而欢欣鼓舞的时候，其实在南海还有一个非常严重的问题，就是缺水。虽然常有淡水运输过去，但也会因为天气原因，不能及时送达，造成一系列问题。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	不过，好消息来了！最近广东研发的一款系统将使缺水问题成为历史。以下为《南华早报》特别为此刊登的一篇报道：
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	在中国控制的大部分南海岛屿中，淡水资源十分匮乏。全能靠一艘艘小船将一桶桶的水送上小岛。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	中国官方媒体曾报道，一些士兵几个月都不能洗澡，有时候因为天气原因，淡水没有及时送达，他们只能靠雨水支撑。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	为了加强对南海这些岛屿的控制，中国原本想在这里建造酒店和开发<a href="http://travel.ifeng.com/" target="_blank">旅游</a>景点，但因为淡水资源短缺的问题不得不搁置。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	中国政府也一直对这个问题予以重视，去年4月彭博社的一项报告称，中国计划在2020年前，每天生产300万吨的淡化海水，大约是现在产量的四倍。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	最近，这个问题似乎得到了解决。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	近日，一款由广州先进所研发的柴油发电机组缸套冷却水废热驱动的海水淡化示范系统成功调试出水。这意味着，在不久的将来，中国这项突破性的海水淡化技术，不仅可以在南海的偏远岛屿上生产出淡水，还能够在这里迅速发展出适宜居住的区域。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	据了解，这套设备不需要消耗额外的能源，和传统的淡化海水技术相比，降低了成本。一台1000kW的柴油发电机产出的废热，每天可以生产60吨淡水，对300个居民或一个营的士兵来说，完全足够。
</p>
<p style="text-indent:28px;font-size:14px;color:#2B2B2B;font-family:simsun, arial, helvetica, clean, sans-serif;background-color:#FFFFFF;">
	通常，使用热法技术来淡化海水需要消耗大量的能量，一个小岛屿是很难满足的。传统的办法是通过加热海水，使之沸腾汽化，再把蒸汽冷凝成淡水。
</p>
<p>
	<br />
</p>', CAST(0x0000A59100B5A8D9 AS DateTime), 1, 100, 2)
INSERT [dbo].[Notification] ([id], [title], [content], [addTime], [isEnable], [sort], [jbid]) VALUES (2, N'系统使用培训通知', N'<p>
	暂定于本周六在会议室举行系统使用培训通知。
</p>
<p>
	望准时与会。
</p>', CAST(0x0000A591009360B8 AS DateTime), 1, 100, 2)
INSERT [dbo].[Notification] ([id], [title], [content], [addTime], [isEnable], [sort], [jbid]) VALUES (3, N'元旦放假通知', N'<p>
	&nbsp; &nbsp; 元旦放假三天。
</p>', CAST(0x0000A5950115ED5F AS DateTime), 0, 100, 2)
INSERT [dbo].[Notification] ([id], [title], [content], [addTime], [isEnable], [sort], [jbid]) VALUES (4, N'测试', N'订单', CAST(0x0000A59100B9B1BE AS DateTime), 1, 100, 2)
INSERT [dbo].[Notification] ([id], [title], [content], [addTime], [isEnable], [sort], [jbid]) VALUES (5, N'test again', N'&nbsp; &nbsp; test test test', CAST(0x0000A5910094C72A AS DateTime), 1, 100, 2)
SET IDENTITY_INSERT [dbo].[Notification] OFF
SET IDENTITY_INSERT [dbo].[SubNotification] ON 

INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (6, 2, 2, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (7, 2, 4, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (8, 2, 3, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (15, 5, 2, 1, CAST(0x0000A59B009E5443 AS DateTime))
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (16, 5, 3, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (17, 1, 2, 1, CAST(0x0000A59B00A1C667 AS DateTime))
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (19, 4, 4, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (22, 3, 2, 0, NULL)
INSERT [dbo].[SubNotification] ([id], [nid], [uid], [isRead], [readTime]) VALUES (23, 3, 3, 0, NULL)
SET IDENTITY_INSERT [dbo].[SubNotification] OFF
SET IDENTITY_INSERT [dbo].[Z_Depart] ON 

INSERT [dbo].[Z_Depart] ([id], [name], [pid], [sort], [remark]) VALUES (1, N'总公司', 0, 100, N'')
SET IDENTITY_INSERT [dbo].[Z_Depart] OFF
SET IDENTITY_INSERT [dbo].[Z_Forms] ON 

INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (1, N'功能设置', N'', 0, N'', 500, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (2, N'菜单导航', N'power/forms/list.aspx', 1, N'添加,修改,删除', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (3, N'系统设置', N'settings/settings.aspx', 1, N'保存', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (4, N'部门管理', N'power/depart/list.aspx', 1, N'添加,修改,删除', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (5, N'角色管理', N'power/role/list.aspx', 1, N'添加,修改,查询,高级查询,删除', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (6, N'系统人员', N'power/users/list.aspx', 1, N'添加,修改,重置密码,查询,高级查询,删除,批量删除', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (7, N'权限分配', N'power/setpower.aspx', 1, N'保存', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (8, N'个人中心', N'settings/individual.aspx', 1, N'保存', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (9, N'修改密码', N'settings/uppswd.aspx', 1, N'保存', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (10, N'登录日志', N'power/logs.aspx', 1, N'查询,高级查询,导出,删除,批量删除', 100, 1, 1, N'', NULL, NULL, NULL)
INSERT [dbo].[Z_Forms] ([id], [name], [url], [pid], [power], [sort], [isMenu], [isEnable], [remark], [model], [defFilter], [disFilter]) VALUES (11, N'通知公告', N'settings/notification.aspx', 1, N'添加,修改,查询,高级查询,删除', 100, 1, 1, N'', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Z_Forms] OFF
SET IDENTITY_INSERT [dbo].[Z_Role] ON 

INSERT [dbo].[Z_Role] ([id], [name], [power], [isEnable], [sort], [remark]) VALUES (1, N'管理员', N'[{"Fid":1,"Url":"","Power":""},{"Fid":2,"Url":"power/forms/list.aspx","Power":"添加,修改,删除"},{"Fid":3,"Url":"settings/settings.aspx","Power":"保存"},{"Fid":4,"Url":"power/depart/list.aspx","Power":"添加,修改,删除"},{"Fid":5,"Url":"power/role/list.aspx","Power":"添加,修改,查询,高级查询,删除"},{"Fid":12,"Url":"","Power":"添加,修改,删除,查询"},{"Fid":6,"Url":"power/users/list.aspx","Power":"添加,修改,重置密码,查询,高级查询,删除,批量删除"},{"Fid":7,"Url":"power/setpower.aspx","Power":"保存"},{"Fid":8,"Url":"settings/individual.aspx","Power":"保存"},{"Fid":9,"Url":"settings/uppswd.aspx","Power":"保存"},{"Fid":10,"Url":"power/logs.aspx","Power":"查询,高级查询,导出,删除,批量删除"},{"Fid":11,"Url":"settings/notification.aspx","Power":"添加,修改,查询,高级查询,删除"}]', 1, 100, N'')
INSERT [dbo].[Z_Role] ([id], [name], [power], [isEnable], [sort], [remark]) VALUES (2, N'普通用户', N'[{"Fid":8,"Url":"settings/individual.aspx","Power":"保存"},{"Fid":9,"Url":"settings/uppswd.aspx","Power":"保存"},{"Fid":1,"Url":"","Power":""}]', 1, 100, N'')
INSERT [dbo].[Z_Role] ([id], [name], [power], [isEnable], [sort], [remark]) VALUES (3, N'普通管理员', N'[{"Fid":3,"Url":"settings/settings.aspx","Power":"保存"},{"Fid":10,"Url":"power/logs.aspx","Power":"查询,高级查询,导出,删除,批量删除"},{"Fid":1,"Url":"","Power":""}]', 1, 100, N'')
SET IDENTITY_INSERT [dbo].[Z_Role] OFF
SET IDENTITY_INSERT [dbo].[Z_Settings] ON 

INSERT [dbo].[Z_Settings] ([id], [name], [logo], [image], [mlogo], [smtp], [email], [pswd], [keywords], [description], [cpuinfo]) VALUES (1, N'精益软件管理系统', N'/images/logo_jingyi.png', N'/images/logo_login.jpg', N'/images/logo_jingyi.png', N'smtp.163.com', N'jingyisoftware@163.com', N'Falcon196!@#', N'', N'', N'')
SET IDENTITY_INSERT [dbo].[Z_Settings] OFF
SET IDENTITY_INSERT [dbo].[Z_Users] ON 

INSERT [dbo].[Z_Users] ([id], [username], [userpswd], [realname], [mobile], [email], [count], [depart], [role], [isEnable], [isDptMgr], [addTime], [remark], [jbid], [isDel]) VALUES (1, N'jingyisoft', N'89D1E569A4DAA34ADDC100B79961B398', N'超级管理员', N'18315711855', N'706994177@qq.com', 0, 0, 0, 1, 1, CAST(0x0000A3F400BBF0CE AS DateTime), N'', 2, 0)
INSERT [dbo].[Z_Users] ([id], [username], [userpswd], [realname], [mobile], [email], [count], [depart], [role], [isEnable], [isDptMgr], [addTime], [remark], [jbid], [isDel]) VALUES (2, N'admin', N'86971A6F928A4CEB', N'系统管理员', N'18315711855', N'706994177@qq.com', 0, 1, 1, 1, 1, CAST(0x0000A3F400CE6106 AS DateTime), N'', 2, 0)
INSERT [dbo].[Z_Users] ([id], [username], [userpswd], [realname], [mobile], [email], [count], [depart], [role], [isEnable], [isDptMgr], [addTime], [remark], [jbid], [isDel]) VALUES (3, N'admin1', N'D5FC5B5624A2DF25', N'测试账号', N'13513513515', N'', 0, 2, 2, 1, 1, CAST(0x0000A5880106C074 AS DateTime), N'123', 1, 1)
INSERT [dbo].[Z_Users] ([id], [username], [userpswd], [realname], [mobile], [email], [count], [depart], [role], [isEnable], [isDptMgr], [addTime], [remark], [jbid], [isDel]) VALUES (4, N'admin2', N'D5FC5B5624A2DF25', N'test', N'', N'', 0, 1, 1, 1, 0, CAST(0x0000A59000C4F518 AS DateTime), N'', 2, 1)
SET IDENTITY_INSERT [dbo].[Z_Users] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Notification"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 332
               Right = 180
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Z_Users"
            Begin Extent = 
               Top = 6
               Left = 218
               Bottom = 333
               Right = 364
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Notification_Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Notification_Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SubNotification"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 270
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Notification"
            Begin Extent = 
               Top = 1
               Left = 437
               Bottom = 249
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Z_Users_1"
            Begin Extent = 
               Top = 6
               Left = 220
               Bottom = 326
               Right = 366
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Z_Users"
            Begin Extent = 
               Top = 6
               Left = 617
               Bottom = 291
               Right = 763
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_SubNotification_Info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_SubNotification_Info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Z_Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 333
               Right = 184
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Z_Role"
            Begin Extent = 
               Top = 0
               Left = 427
               Bottom = 327
               Right = 571
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Z_Depart"
            Begin Extent = 
               Top = 6
               Left = 222
               Bottom = 333
               Right = 364
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Z_Users_1"
            Begin Extent = 
               Top = 6
               Left = 609
               Bottom = 325
               Right = 755
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Users_Depart_Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Users_Depart_Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_Users_Depart_Role'
GO
