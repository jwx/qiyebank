﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace BLL
{
    public class Huoqi
    {
        #region 增加

        public int Add(int uid, decimal jine, decimal yuanjine, string danhao, int cunqukuan, int canBaofei, int laiyuan, int isAutoCalc, string lilv, decimal zonglixi, string addTime, string cunkuanTime, int jbid, string add_remark, string delTime, int delid, string del_remark, string del_json, int oid_qk, decimal benjin, bool isService = false, DbTransaction tran = null)
        {
            return new DAL.Huoqi(isService).Add(uid, jine, yuanjine, danhao, cunqukuan, canBaofei, laiyuan, isAutoCalc, lilv, zonglixi, addTime, cunkuanTime, jbid, add_remark, delTime, delid, del_remark, del_json, oid_qk, benjin, tran);
        }


        public bool Update_LiXi(int id, decimal lixi, DbTransaction tran)
        {
            return new DAL.Huoqi().Update_LiXi(id, lixi, tran) != 0;
        }

        public bool Import(System.Data.DataTable dt, int jbid, Model.Setup modelSetup, ref string msg)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                try
                {
                    List<Model.UserInfo> listDb_UserInfo = new DAL.UserInfo().GetListUidAndUname(tran);
                    if (listDb_UserInfo == null || listDb_UserInfo.Count == 0)
                    {
                        tran.Rollback();
                        msg = "没有读取到用户数据";
                        return false;
                    }

                    bool flag = true;
                    int i = 0;
                    foreach (System.Data.DataRow one in dt.Rows)
                    {
                        i++;
                        if (i == 1) continue;//跳过示例数据
                        string username = one["账号"].ToString().Trim();
                        if (!string.IsNullOrEmpty(username))
                        {
                            int uid = (from m in listDb_UserInfo where m.username == username select m.id).FirstOrDefault();
                            if (uid == 0)
                            {
                                msg = msg + username + ",";
                                flag = false;
                            }

                            if (flag)
                            {
                                decimal cunkuan = Math.Round(Falcon.Function.ToDecimal(one["存款金额"].ToString().Trim()), 2);

                                //if (new DAL.Huoqi().Add(uid, cunkuan, cunkuan, DateTime.Now.ToString("yyyyMMddHHmmss" + i + new Random().Next(0, 100).ToString().PadLeft('0')), 1, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, dtNow, Falcon.Function.ToDateTime(one["存款日期"].ToString().Trim()).ToString("yyyy-MM-dd"), jbid, "存款批量导入", modelSetup.hqck_autoCk == 1 ? dtNow : Config.Config.str_defTime, modelSetup.hqck_autoCk == 1 ? -1 : 0, modelSetup.hqck_autoCk == 1 ? "自动审核" : "", Config.Config.str_defTime, 0, "", "", "", 0, tran) == 0)
                                if (new DAL.Huoqi().Add(uid, cunkuan, cunkuan, new Tool.OrderNo().GetOrderNo(), 1, 1, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, Falcon.Function.ToDateTime(one["存款日期"].ToString().Trim()).ToString("yyyy-MM-dd"), jbid, "存款批量导入", Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
                                {
                                    msg = "操作失败";
                                    tran.Rollback();
                                    return false;
                                }
                            }
                        }
                    }
                    if (flag)
                    {
                        tran.Commit();
                        return true;
                    }
                    else
                    {
                        msg = "操作失败！不存在下列账号：" + msg;
                        tran.Rollback();
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    tran.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// 人工本金取款
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_Benjin(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), new Tool.OrderNo().GetOrderNo(), 2, 1, (int)Config.Enums.Huoqi_Laiyuan.本金取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);

                    if (oid_qk != 0)
                    {
                        ////存入利息,利息单据不能报废
                        //if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, jbid, "零钱利息_系统自动存款", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                        //{
                        //    msg = "利息存款失败！";
                        //    tran.Rollback();
                        //    return 0;
                        //}

                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, one.lixi, tran) == 0)
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }

                            ////记录利息
                            //if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                            //{
                            //    msg = "利息保存失败！";
                            //    tran.Rollback();
                            //    return 0;
                            //}
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }
        /// <summary>
        /// 个人取款 ----溢家
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_GBenjin(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), new Tool.OrderNo().GetOrderNo(), 2, 1, (int)Config.Enums.Huoqi_Laiyuan.本金取款, modelSetup.hq_autoCalc, modelSetup.hq_lilv, modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);

                    if (oid_qk != 0)
                    {
                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }

        /// <summary>
        /// 企业取款审核同意
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="remark"></param>
        /// <param name="ExId"></param>
        /// <returns></returns>
        public int Update_qukuanStatus(int id, int status, string remark, int ExId, ref string msg, Model.HuoqiQuKuan modelQukuan)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                int res = new DAL.Huoqi().Update_qukuanStatus(id, status, remark, ExId, tran);  //更改 状态、审核备注，审核人id
                if (res > 0)
                {
                    foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                    {
                        //取款
                        if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0) //修改零钱金额
                        {
                            msg = "取款失败！";
                            tran.Rollback();
                            return 0;
                        }
                    }
                    tran.Commit();
                    return res;
                }
                else
                {
                    msg = "审核失败！";
                    tran.Rollback();
                    return 0;
                }
            }
        }
        /// <summary>
        /// 企业取款审核拒绝
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="remark"></param>
        /// <param name="ExId"></param>
        /// <returns></returns>
        public int Update_qukuanStatus(int id, int status, string remark, int ExId)
        {
            return new DAL.Huoqi().Update_qukuanStatus(id, status, remark, ExId, null);
        }
        /// <summary>
        /// 企业账户取本金
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <param name="imgpath"></param>
        /// <returns></returns>
        public int Qukuan_Benjin(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg, string imgpath)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), new Tool.OrderNo().GetOrderNo(), 2, 1, (int)Config.Enums.Huoqi_Laiyuan.本金取款, modelSetup.hq_autoCalc, modelSetup.hq_lilv, modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, 0, imgpath, add_remark, json_Qukuan, (int)Config.Enums.Huoqi_SHstatus.未审核, tran);

                    if (oid_qk != 0)
                    {
                        //审核后执行
                        //foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        //{
                        //    //取款
                        //    if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, one.lixi, tran) == 0)
                        //    {
                        //        msg = "取款失败！";
                        //        tran.Rollback();
                        //        return 0;
                        //    }
                        //}
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }
        /// <summary>
        /// 手机端本金取款_转账
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_Benjin_Zhanzhang(int uid, string name, int to_uid, string to_uname, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, DateTime dtNow, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = dtNow.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    add_remark += "_付款人：" + name + "，收款人：" + to_uname + "，类型：转账";

                    decimal jine = (modelQukuan.benjinlixi - modelQukuan.lixi_sum);
                    //添加取款单，不可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -jine, -jine, new Tool.OrderNo().GetOrderNo(), 2, 0, (int)Config.Enums.Huoqi_Laiyuan.本金取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);

                    if (oid_qk != 0)
                    {
                        //存入利息,利息单据不能报废
                        if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, jbid, "零钱利息_系统自动存款", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                        {
                            msg = "利息存款失败！";
                            tran.Rollback();
                            return 0;
                        }

                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }

                            //记录利息
                            if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                            {
                                msg = "利息保存失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }

                        int oid_ck = new DAL.Huoqi().Add(to_uid, jine, jine, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", "", 0, 0, tran);
                        if (oid_ck != 0)
                        {
                            //转账
                            int oid_zz = new DAL.HuoqiZhuanzhang().Add(uid, jine, modelQukuan.lixi_sum, oid_qk, to_uid, oid_ck, add_remark, strDtNow, tran);
                            if (oid_zz != 0)
                            {
                                //付款人推送消息
                                if (!string.IsNullOrEmpty(Config.Config.AppName))
                                {
                                    Dictionary<string, string> dict = new Dictionary<string, string>();
                                    dict.Add("page", "huoqizhuanzhanginfo");
                                    dict.Add("id", oid_zz.ToString());
                                    dict.Add("uid", uid.ToString());
                                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + uid }, "零钱转账付款通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "转账给" + to_uname + jine + "元", dict, ref msg);

                                    //收款人消息推送
                                    dict = new Dictionary<string, string>();
                                    dict.Add("page", "huoqizhuanzhanginfo");
                                    dict.Add("id", oid_zz.ToString());
                                    dict.Add("uid", to_uid.ToString());
                                    Tool.JPushHelper.PushMessage(new string[] { "uid_" + to_uid }, "零钱转账收款通知", "您于" + DateTime.Now.ToString("yyyy年MM月dd日") + "收到" + name + "转账" + jine + "元", dict, ref msg);
                                }
                                tran.Commit();
                                return oid_qk;
                            }
                            else
                            {
                                msg = "转账数据添加失败！";
                                return 0;
                            }
                        }
                        else
                        {
                            msg = "存款单添加失败！";
                            return 0;
                        }

                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }

        /// <summary>
        /// 企业取款本息
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <param name="imgpath"></param>
        /// <returns></returns>
        public int Qukuan_Benxi(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg, string imgpath)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");

                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -modelQukuan.benjinlixi, -modelQukuan.benjinlixi, new Tool.OrderNo().GetOrderNo(), 2, 1, (int)Config.Enums.Huoqi_Laiyuan.本息取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, 0, imgpath, add_remark, json_Qukuan, (int)Config.Enums.Huoqi_SHstatus.未审核, tran);
                    if (oid_qk != 0)
                    {
                        ////存入利息
                        //if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, dtNow, jbid, "系统自动存款", modelSetup.hqck_autoCk == 1 ? dtNow : Config.Config.str_defTime, modelSetup.hqck_autoCk == 1 ? -1 : 0, modelSetup.hqck_autoCk == 1 ? 1 : 0, modelSetup.hqck_autoCk == 1 ? "自动审核" : "", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                        //{
                        //    msg = "利息存款失败！";
                        //    tran.Rollback();
                        //    return false;
                        //}

                        //foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        //{
                        //    //取款
                        //    if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, one.lixi, tran) == 0)//每月自动结算时，利息为0
                        //    {
                        //        msg = "取款失败！";
                        //        tran.Rollback();
                        //        return 0;
                        //    }

                        //    //记录利息
                        //    if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                        //    {
                        //        msg = "利息保存失败！";
                        //        tran.Rollback();
                        //        return 0;
                        //    }
                        //}
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }
        /// <summary>
        /// 人工本息取款
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_Benxi(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");

                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -modelQukuan.benjinlixi, -modelQukuan.benjinlixi, new Tool.OrderNo().GetOrderNo(), 2, 1, (int)Config.Enums.Huoqi_Laiyuan.本息取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);
                    if (oid_qk != 0)
                    {
                        ////存入利息
                        //if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, dtNow, jbid, "系统自动存款", modelSetup.hqck_autoCk == 1 ? dtNow : Config.Config.str_defTime, modelSetup.hqck_autoCk == 1 ? -1 : 0, modelSetup.hqck_autoCk == 1 ? 1 : 0, modelSetup.hqck_autoCk == 1 ? "自动审核" : "", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                        //{
                        //    msg = "利息存款失败！";
                        //    tran.Rollback();
                        //    return false;
                        //}

                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)//每月自动结算时，利息为0
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }

                            //记录利息
                            if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                            {
                                msg = "利息保存失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }

        public int Qukuan_Benxi_Service(int uid, Model.HuoqiQuKuan modelQukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg, DbTransaction tran)
        {
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                string json_Qukuan = Tool.JsonHelper.JsonSerializer<Model.HuoqiQuKuan>(modelQukuan);

                //添加取款单，不可以报废
                int oid_qk = new DAL.Huoqi(true).Add(uid, -modelQukuan.benjinlixi, -modelQukuan.benjinlixi, new Tool.OrderNo().GetOrderNo(), 2, 0, (int)Config.Enums.Huoqi_Laiyuan.本息取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);
                if (oid_qk != 0)
                {
                    ////存入利息
                    //if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, dtNow, jbid, "系统自动存款", modelSetup.hqck_autoCk == 1 ? dtNow : Config.Config.str_defTime, modelSetup.hqck_autoCk == 1 ? -1 : 0, modelSetup.hqck_autoCk == 1 ? 1 : 0, modelSetup.hqck_autoCk == 1 ? "自动审核" : "", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                    //{
                    //    msg = "利息存款失败！";
                    //    tran.Rollback();
                    //    return false;
                    //}

                    foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                    {
                        //取款
                        if (new DAL.Huoqi(true).Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)//每月自动结算时，利息为0
                        {
                            msg = "取款失败！";
                            //tran.Rollback();
                            return 0;
                        }

                        //记录利息
                        if (new DAL.HuoqiLixi(true).Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                        {
                            msg = "利息保存失败！";
                            //tran.Rollback();
                            return 0;
                        }
                    }
                    //tran.Commit();
                    return oid_qk;
                }
                msg = "取款单添加失败！";
                //tran.Rollback();
                return 0;
            }
            msg = "操作失败！";
            return 0;
        }

        /// <summary>
        /// 活期本息提现
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="json_Qukuan"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_BenxiTixian(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");

                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，不可以报废
                    int oid_qk = new DAL.Huoqi().Add(uid, -modelQukuan.benjinlixi, -modelQukuan.benjinlixi, new Tool.OrderNo().GetOrderNo(), 2, 0, (int)Config.Enums.Huoqi_Laiyuan.本息取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);
                    if (oid_qk != 0)
                    {
                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)//每月自动结算时，利息为0
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }

                            //记录利息
                            if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                            {
                                msg = "利息保存失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }

                        if (new DAL.Apply().Add(uid, modelQukuan.benjinlixi, modelQukuan.benjinlixi - modelQukuan.lixi_sum, modelQukuan.lixi_sum, strDtNow, add_remark, json_Qukuan, tran) == 0)
                        {
                            msg = "提现申请添加失败！";
                            tran.Rollback();
                            return 0;
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }

        public int Qukuan_Zhuancun(int uid, string json_Qukuan, Model.Setup modelSetup, int jbid, string add_remark, int dqjs, string zhouqi, decimal lilv, decimal budaoqi, string daoqiTime, string dingqiRemark, ref string msg)
        {
            Model.HuoqiQuKuan modelQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(json_Qukuan);
            if (modelQukuan != null)
            {
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，不可报废
                    string danhao = new Tool.OrderNo().GetOrderNo();
                    int oid_qk = new DAL.Huoqi().Add(uid, -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), -(modelQukuan.benjinlixi - modelQukuan.lixi_sum), danhao, 2, 0, (int)Config.Enums.Huoqi_Laiyuan.本金取款, modelSetup.hq_autoCalc, "", modelQukuan.lixi_sum, strDtNow, strDt, jbid, "转定期_" + add_remark, Config.Config.str_defTime, 0, "", json_Qukuan, 0, 0, tran);

                    if (oid_qk != 0)
                    {
                        //存入利息,利息单据不能报废
                        if (new DAL.Huoqi().Add(uid, modelQukuan.lixi_sum, modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, jbid, "零钱利息_本金转定期_利息存零钱账户", Config.Config.str_defTime, 0, "", "", oid_qk, modelQukuan.benjinlixi - modelQukuan.lixi_sum, tran) == 0)
                        {
                            msg = "利息存款失败！";
                            tran.Rollback();
                            return 0;
                        }
                        foreach (Model.HuoqiLixi one in modelQukuan.list_lixi)
                        {
                            //取款
                            if (new DAL.Huoqi().Update_Qukuan(one.oid_ck, one.qukuan, tran) == 0)
                            {
                                msg = "取款失败！";
                                tran.Rollback();
                                return 0;
                            }

                            //记录利息
                            if (new DAL.HuoqiLixi().Add(one.oid_ck, oid_qk, one.qukuan, one.lixi, tran) == 0)
                            {
                                msg = "利息保存失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }

                        //if(new BLL.Dingqi().Add(uid,modelQukuan.benjinlixi-modelQukuan.lixi_sum,new Tool.OrderNo().GetOrderNo(),1,0,1,
                        if (new DAL.Dingqi().Add(uid, modelQukuan.benjinlixi - modelQukuan.lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, 0, 0, 0, 0, dqjs, zhouqi, lilv, budaoqi, 0, strDtNow, strDt, daoqiTime, jbid, "活期转定期，活期取款单号:" + danhao + "," + dingqiRemark, Config.Config.str_defTime, 0, "", tran) != 0)
                        {
                            tran.Commit();
                            return oid_qk;
                        }
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "操作失败！";
            return 0;
        }

        ///// <summary>
        ///// 活期自动取款，自动结算
        ///// </summary>
        ///// <param name="listHuoqi"></param>
        ///// <param name="dtNow"></param>
        ///// <param name="modelSetup"></param>
        ///// <param name="tran"></param>
        ///// <param name="msg"></param>
        ///// <returns></returns>
        //public bool Qukuan_Auto(List<Model.Huoqi> listHuoqi, DateTime dtNow, Model.Setup modelSetup, DbTransaction tran, ref string msg)
        //{
        //    string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");

        //    List<int> listUid = (from m in listHuoqi select m.uid).Distinct().ToList();
        //    foreach (int uid in listUid)
        //    {
        //        decimal lixi_sum = 0;
        //        List<Model.Huoqi> listHuoqi_Uid = (from m in listHuoqi where m.uid == uid select m).ToList();
        //        foreach (Model.Huoqi one in listHuoqi_Uid)
        //        {
        //            decimal lixi = new BLL.Huoqi().GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.cunkuanTime), modelSetup.hq_byDay);
        //            lixi_sum += lixi;
        //            if (new DAL.Huoqi().Update_QukuanAuto(one.id, modelSetup.hq_lilv, dtNow, tran) != 1)//自动计算时，活期订单自动执行最新的活期利率
        //            {
        //                msg = "存款单id：" + one.id + "状态修改失败！";
        //                return false;
        //            }
        //        }
        //        //利息不能报废
        //        if (new DAL.Huoqi().Add(uid, lixi_sum, lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDtNow, strDtNow, -1, "零钱利息_系统自动结算", Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
        //        {
        //            msg = "用户id：" + uid + "利息存入失败！";
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        /// <summary>
        /// 活期自动（每月指定日期）结算利息
        /// </summary>
        /// <param name="isService"></param>
        /// <param name="listHuoqi"></param>
        /// <param name="dtNow"></param>
        /// <param name="modelSetup"></param>
        /// <param name="tran"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool ServiceAutoLixi(bool isService, List<Model.Huoqi> listHuoqi, DateTime dtNow, Model.Setup modelSetup, DbTransaction tran, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");

            List<int> listUid = (from m in listHuoqi select m.uid).Distinct().ToList();
            foreach (int uid in listUid)
            {
                decimal lixi_sum = 0;
                List<Model.Huoqi> listHuoqi_Uid = (from m in listHuoqi where m.uid == uid select m).ToList();
                foreach (Model.Huoqi one in listHuoqi_Uid)
                {
                    decimal lixi = GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                    lixi_sum += lixi;
                    if (new DAL.Huoqi(true).Update_QukuanAuto_UpdtJxTime(one.id, modelSetup.hq_lilv, lixi, strDt, tran) != 1)//自动计算时，活期订单自动执行最新的活期利率
                    {
                        msg = "存款单id：" + one.id + "状态修改失败！";
                        return false;
                    }
                }
                //存入利息，利息不能报废
                if (new DAL.Huoqi(true).Add(uid, lixi_sum, lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, -1, "零钱利息_系统自动结算", Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
                {
                    msg = "用户id：" + uid + "利息存入失败！";
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 活期利息每日计算
        /// </summary>
        /// <param name="isService"></param>
        /// <param name="listHuoqi"></param>
        /// <param name="dtNow"></param>
        /// <param name="modelSetup"></param>
        /// <param name="tran"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool ServiceAutoLixi_Everyday(bool isService, List<Model.Huoqi> listHuoqi, DateTime dtNow, Model.Setup modelSetup, DbTransaction tran, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");

            List<int> listUid = (from m in listHuoqi select m.uid).Distinct().ToList();
            foreach (int uid in listUid)
            {
                List<Model.Huoqi> listHuoqi_Uid = (from m in listHuoqi where m.uid == uid && Falcon.Function.ToDateTime(m.jxTime).ToString("yyyy-MM-dd") != strDt select m).ToList();//获取今天之前的所有存款单据 
                decimal lixi_sum = 0;
                if (listHuoqi_Uid != null && listHuoqi_Uid.Count != 0)
                {
                    foreach (Model.Huoqi one in listHuoqi_Uid)
                    {
                        decimal lixi = Math.Round(one.jine * modelSetup.hq_daylilv / 36500, 2, MidpointRounding.AwayFromZero);
                        lixi_sum += lixi;
                        if (new DAL.Huoqi(true).Update_QukuanAuto_UpdtJxTime(one.id, modelSetup.hq_lilv, lixi, strDt, tran) != 1)//自动计算时，活期订单自动执行最新的活期利率。 modelSetup.hq_lilv为空
                        {
                            msg = "存款单id：" + one.id + "状态修改失败！";
                            return false;
                        }
                    }
                    //利息不能报废
                    if (new DAL.Huoqi(true).Add(uid, lixi_sum, lixi_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, modelSetup.hq_autoCalc, modelSetup.hq_daylilv.ToString(), 0, strDtNow, strDt, -1, "零钱利息_系统自动结算_每日结算", Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
                    {
                        msg = "用户id：" + uid + "利息存入失败！";
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 人工手动结息
        /// </summary>
        /// <param name="listHuoqi"></param>
        /// <param name="dtNow"></param>
        /// <param name="modelSetup"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool ManualAutoLixi(List<Model.Huoqi> listHuoqi, DateTime dtNow, Model.Setup modelSetup, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");

            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                List<int> listUid = (from m in listHuoqi select m.uid).Distinct().ToList();
                foreach (int uid in listUid)
                {
                    decimal jine_sum = 0;
                    List<Model.Huoqi> listHuoqi_Uid = (from m in listHuoqi where m.uid == uid select m).ToList();
                    foreach (Model.Huoqi one in listHuoqi_Uid)
                    {
                        decimal lixi = GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        jine_sum += one.jine + lixi;
                        if (new DAL.Huoqi().Update_QukuanAuto_UpdtJxTime(one.id, modelSetup.hq_lilv, lixi, strDt, tran) != 1)//自动计算时，活期订单自动执行最新的活期利率
                        {
                            msg = "存款单id：" + one.id + "状态修改失败！";
                            return false;
                        }
                    }
                    //新存款不能报废
                    if (new DAL.Huoqi().Add(uid, jine_sum, jine_sum, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDtNow, -1, "零钱批量结息", Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
                    {
                        msg = "用户id：" + uid + "批量结息存入失败！";
                        return false;
                    }
                }
                tran.Commit();
                return true;
            }
        }

        #endregion

        #region 修改

        //public bool Update(int id, int uid, decimal jine, decimal yuanjine, string danhao, int cunqukuan, int laiyuan, int isAutoCalc, string lilv, decimal zonglixi, string addTime, string cunkuanTime, int jbid, string add_remark, string ckTime, int ckid, int ckPass, string ck_remark, string delTime, int delid, string del_remark, string del_json, int oid_qk, decimal benjin)
        //{
        //    return new DAL.Huoqi().Update(id, uid, jine, yuanjine, danhao, cunqukuan, laiyuan, isAutoCalc, lilv, zonglixi, addTime, cunkuanTime, jbid, add_remark, ckTime, ckid, ckPass, ck_remark, delTime, delid, del_remark, del_json, oid_qk, benjin) == 1;
        //}

        /// <summary>
        /// 报废
        /// </summary>
        /// <param name="id"></param>
        /// <param name="delid"></param>
        /// <param name="del_remark"></param>
        /// <returns></returns>
        public bool Update_CunkuanBaofei(int id, int delid, string del_remark)
        {
            return new DAL.Huoqi().Update_Baofei(id, delid, del_remark, null) == 1;
        }

        public bool Update_QukuanBaofei(int id, int delid, string del_remark, ref string msg)
        {
            Model.Huoqi model = new DAL.Huoqi().GetModel(id);
            if (model != null && model.id != 0)
            {
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();
                    if (new DAL.Huoqi().Update_Baofei(model.id, delid, del_remark, tran) == 1)
                    {
                        //bool isBenjinQukuan = model.laiyuan == (int)Config.Enums.Huoqi_Laiyuan.本金取款;
                        //bool flag = isBenjinQukuan ? new DAL.Huoqi().Update_BaofeiByOid_Qk(id, (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入, delid, del_remark, tran) != 0 : true;//本金取款，报废利息
                        //if (flag)
                        //{
                        if (!string.IsNullOrEmpty(model.del_json))
                        {
                            Model.HuoqiQuKuan modelHuoqiQukuan = Tool.JsonHelper.JsonDeserialize<Model.HuoqiQuKuan>(model.del_json);
                            //if (listLixi != null && listLixi.Count != 0)
                            //{ 
                            foreach (Model.HuoqiLixi one in modelHuoqiQukuan.list_lixi)
                            {
                                if (new DAL.Huoqi().Update_Baofei_JineBack(one.oid_ck, one.qukuan, tran) == 0)
                                {
                                    msg = "存款数据回滚失败！";
                                    tran.Rollback();
                                    return false;
                                }
                            }
                            tran.Commit();
                            return true;
                        }
                        else
                        {
                            msg = "取款明细读取失败！";
                            tran.Rollback();
                            return false;
                        }
                        //}
                        //msg = "取款明细读取失败！";
                        //tran.Rollback();
                        //return false;
                        //}
                        //msg = "利息存款单报废失败！";
                        //tran.Rollback();
                        //return false;
                    }
                    msg = "取款单报废失败！";
                    tran.Rollback();
                    return false;
                }
            }
            msg = "取款单读取失败！";
            return false;
        }

        #endregion

        #region 查询

        public Model.Huoqi GetModel(int id)
        {
            return new DAL.Huoqi().GetModel(id);
        }

        public Model.Huoqi GetModel(int id, int uid)
        {
            return new DAL.Huoqi().GetModel(id, uid);
        }

        public List<Model.Huoqi> GetListModel()
        {
            return new DAL.Huoqi().GetListModel();
        }

        public List<Model.Huoqi> GetListByWhere(string where)
        {
            return new DAL.Huoqi().GetListByWhere(where);
        }

        public int GetRecordCount(string where)
        {
            return new DAL.Huoqi().GetRecordCount(where);
        }

        public List<Model.Huoqi> GetList_Pager(int pageindex, int pagesize, string where)
        {
            return new DAL.Huoqi().GetList_Pager(pageindex, pagesize, where);
        }

        /// <summary>
        /// 获取指定用户id已审核的活期账户余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public decimal GetYue(int uid)
        {
            return new DAL.Huoqi().GetYue(uid, null);
        }

        /// <summary>
        /// 获取用户取款列表
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="hq_houcunxianqu"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_Qukuan(int uid, int hq_houcunxianqu, bool isService = false, DbTransaction tran = null)
        {
            return new DAL.Huoqi(isService).GetList_Qukuan(uid, hq_houcunxianqu, tran);
        }

        /// <summary>
        /// 获取系统中所有有效的活期存款单
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_Qukuan(DbTransaction tran)
        {
            return new DAL.Huoqi().GetList_Qukuan(tran);
        }

        /// <summary>
        /// 获取系统中所有有效的活期存款单
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_Qukuan(bool isService, DbTransaction tran)
        {
            return new DAL.Huoqi(isService).GetList_Qukuan(tran);
        }

        /// <summary>
        /// 获取当前系统中的活期存款总金额
        /// </summary>
        /// <returns></returns>
        public decimal GetSysYue()
        {
            return new DAL.Huoqi().GetSysYue();
        }

        ///// <summary>
        ///// 获取当前系统中的活期已付利息总金额
        ///// </summary>
        ///// <returns></returns>
        //public decimal GetSysLixi_Payed()
        //{
        //    return new DAL.Huoqi().GetSysLixi_Payed();
        //}

        ///// <summary>
        ///// 获取用户取款单和利息信息
        ///// </summary>
        ///// <param name="laiyuan"></param>
        ///// <returns></returns>
        //public List<Model.Huoqi> GetList_Qukuan_Lixi(Config.Enums.Huoqi_Laiyuan laiyuan)
        //{
        //    return new DAL.Huoqi().GetList_Qukuan_Lixi(laiyuan);
        //}

        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_YifuLixi()
        {
            return new DAL.Huoqi().GetList_YifuLixi();
        }

        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Huoqi> GetList_WeifuLixi()
        {
            return new DAL.Huoqi().GetList_WeifuLixi();
        }

        /// <summary>
        /// 获取单据产生的活期利息
        /// </summary>
        /// <param name="qukuan"></param>
        /// <param name="cunkuanLilv"></param>
        /// <param name="jxTime"></param>
        /// <param name="byDay"></param>
        /// <returns></returns>
        public decimal GetLixiByOrder(decimal qukuan, string cunkuanLilv, DateTime jxTime, int byDay, DateTime dtNow)
        {
            List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(cunkuanLilv);
            decimal lixi = 0;

            int lilvJishu;//年利率基数
            int cunkuanJishu;

            if (byDay != 0)//按天
            {
                lilvJishu = 365;
                cunkuanJishu = (dtNow - jxTime).Days;
            }
            else//按月
            {
                lilvJishu = 12;
                cunkuanJishu = new Tool.TimeHelper().GetFullMonth(dtNow, jxTime);
            }

            if (listLilvRange != null)
            {
                foreach (Model.LilvRange one in listLilvRange)
                {
                    if (cunkuanJishu > one.min && (one.max == 0 || cunkuanJishu <= one.max))
                    {
                        lixi = Math.Round((qukuan * one.lilv / 100 * cunkuanJishu / lilvJishu), 2, MidpointRounding.AwayFromZero);
                        break;
                    }
                }
            }
            return lixi;
        }

        public decimal GetLixiByOrder_Yesterday(int uid, DateTime dtNow)
        {
            Model.Huoqi model = new DAL.Huoqi().GetModelByWhere(uid, "cunqukuan=1 and delid=0 and laiyuan=" + (int)Config.Enums.Huoqi_Laiyuan.零钱利息存入 + " and cunkuanTime='" + dtNow.AddDays(-1).ToString("yyyy-MM-dd") + "' and add_remark='零钱利息_系统自动结算_每日结算'");
            return model == null || model.id == 0 ? 0 : model.yuanjine;
        }

        public decimal GetLixiByOrder_Yesterday(decimal qukuan, string cunkuanLilv, DateTime jxTime, int byDay, DateTime dtNow)
        {
            List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(cunkuanLilv);
            decimal lixi = 0;

            int lilvJishu;//年利率基数
            int cunkuanJishu;

            if (byDay != 0)//按天
            {
                lilvJishu = 365;
                cunkuanJishu = (dtNow - jxTime).Days;
            }
            else//按月
            {
                lilvJishu = 12;
                cunkuanJishu = new Tool.TimeHelper().GetFullMonth(dtNow, jxTime);
            }

            if (listLilvRange != null)
            {
                foreach (Model.LilvRange one in listLilvRange)
                {
                    if (cunkuanJishu > one.min && (one.max == 0 || cunkuanJishu <= one.max))
                    {
                        lixi = Math.Round((qukuan * one.lilv / 100 / lilvJishu), 2, MidpointRounding.AwayFromZero);
                        break;
                    }
                }
            }
            return lixi;
        }

        public string GetLixi(int uid, decimal qukuan, string strSetup, bool isService = false, DbTransaction tran = null)
        {
            Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(strSetup);
            List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(uid, modelSetup.hq_houcunxianqu, isService, tran);

            Model.HuoqiQuKuan modelQukuan = new Model.HuoqiQuKuan();
            decimal sum_lixi = 0;
            if (listHuoqi != null && listHuoqi.Count != 0)
            {
                //List<Model.HuoqiQuKuan> listQukuan = new List<Model.HuoqiQuKuan>();//在取款单中定义本次取款的json数据，供报废使用
                bool isAutoLixi = modelSetup.hq_autoCalc != 0;
                decimal zongqukuan = qukuan;
                List<Model.HuoqiLixi> listLixi = new List<Model.HuoqiLixi>();
                DateTime dtNow = DateTime.Now;
                foreach (Model.Huoqi one in listHuoqi)
                {
                    Model.HuoqiQuKuan m_qk = new Model.HuoqiQuKuan();
                    //Model.HuoqiLixi m_lixi = new Model.HuoqiLixi();
                    if (one.jine >= qukuan)
                    {
                        //m_qk.id = one.id;
                        //m_qk.sy = one.jine - qukuan;
                        //m_qk.qc = qukuan;
                        //listQukuan.Add(m_qk);
                        //还需要核算每笔存款的利息
                        decimal lixi = isAutoLixi ? 0 : GetLixiByOrder(qukuan, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = qukuan });//目前只知道存款单id，利息，本次取款，不知道取款单id
                        break;
                    }
                    else
                    {
                        qukuan = qukuan - one.jine;
                        //m_qk.id = one.id;
                        //m_qk.sy = 0;
                        //m_qk.qc = one.jine;
                        //还需要核算每笔存款的利息 
                        decimal lixi = isAutoLixi ? 0 : GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = one.jine });//目前只知道存款单id，利息，本次取款，不知道取款单id
                    }
                    //listQukuan.Add(m_qk);
                }
                modelQukuan.benjinlixi = zongqukuan + sum_lixi;
                modelQukuan.lixi_sum = sum_lixi;
                modelQukuan.list_lixi = listLixi;
                //modelQukuan.list_qukuan = listQukuan;
                return Tool.JsonHelper.JsonSerializer<Model.HuoqiQuKuan>(modelQukuan);
            }
            return null;
        }

        public Model.HuoqiQuKuan GetLixi(int uid, decimal qukuan, Model.Setup modelSetup, bool isService = false, DbTransaction tran = null)
        {
            List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(uid, modelSetup.hq_houcunxianqu, isService, tran);

            Model.HuoqiQuKuan modelQukuan = new Model.HuoqiQuKuan();
            decimal sum_lixi = 0;
            if (listHuoqi != null && listHuoqi.Count != 0)
            {
                //List<Model.HuoqiQuKuan> listQukuan = new List<Model.HuoqiQuKuan>();//在取款单中定义本次取款的json数据，供报废使用
                bool isAutoLixi = modelSetup.hq_autoCalc != 0;
                decimal zongqukuan = qukuan;
                List<Model.HuoqiLixi> listLixi = new List<Model.HuoqiLixi>();
                DateTime dtNow = DateTime.Now;
                foreach (Model.Huoqi one in listHuoqi)
                {
                    Model.HuoqiQuKuan m_qk = new Model.HuoqiQuKuan();
                    //Model.HuoqiLixi m_lixi = new Model.HuoqiLixi();
                    if (one.jine >= qukuan)
                    {
                        //m_qk.id = one.id;
                        //m_qk.sy = one.jine - qukuan;
                        //m_qk.qc = qukuan;
                        //listQukuan.Add(m_qk);
                        //还需要核算每笔存款的利息
                        decimal lixi = isAutoLixi ? 0 : GetLixiByOrder(qukuan, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = qukuan });//目前只知道存款单id，利息，本次取款，不知道取款单id
                        break;
                    }
                    else
                    {
                        qukuan = qukuan - one.jine;
                        //m_qk.id = one.id;
                        //m_qk.sy = 0;
                        //m_qk.qc = one.jine;
                        //还需要核算每笔存款的利息 
                        decimal lixi = isAutoLixi ? 0 : GetLixiByOrder(one.jine, one.lilv, Falcon.Function.ToDateTime(one.jxTime), modelSetup.hq_byDay, dtNow);
                        sum_lixi += lixi;
                        listLixi.Add(new Model.HuoqiLixi() { lixi = lixi, oid_ck = one.id, qukuan = one.jine });//目前只知道存款单id，利息，本次取款，不知道取款单id
                    }
                    //listQukuan.Add(m_qk);
                }
                modelQukuan.benjinlixi = zongqukuan + sum_lixi;
                modelQukuan.lixi_sum = sum_lixi;
                modelQukuan.list_lixi = listLixi;
                //modelQukuan.list_qukuan = listQukuan;
                return modelQukuan;
            }
            return null;
        }
        #endregion

        #region  删除

        public int Delete(int id, out string ex)
        {
            if (new DAL.Huoqi().Delete(id, null) == 1)
            {
                ex = "操作成功！";
                return 1;
            }
            else
            {
                ex = "操作失败！";
                return 0;
            }
        }

        public bool Delete(params int[] arrDelIds)
        {
            if (arrDelIds.Length == 1)
            {
                return new DAL.Huoqi().Delete(arrDelIds[0], null) == 1;
            }
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                bool flag = true;
                foreach (int one in arrDelIds)
                {
                    if (new DAL.Huoqi().Delete(one, tran) != 1)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }
        #endregion





        public Model.Huoqi GetModelByuid(int uid)
        {
            return new DAL.Huoqi().GetModelByuid(uid);
        }


    }
}