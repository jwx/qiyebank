﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class SetupPrint
    {
        public bool Add(Config.Enums.Print_Type type, string sysCode, string jsCode)
        {
            return new DAL.SetupPrint().Update(type, sysCode, jsCode) != 0;
        }

        public bool Update(Config.Enums.Print_Type type, string defCode, string jsCode)
        {
            return new DAL.SetupPrint().Update(type, defCode, jsCode) != 0;
        }

        public bool Update_Reset(Config.Enums.Print_Type type, string sysCode, string jsCode)
        {
            return new DAL.SetupPrint().Update_Reset(type, sysCode, jsCode) != 0;
        }

        public Model.SetupPrint GetModel(Config.Enums.Print_Type type)
        {
            return new DAL.SetupPrint().GetModel(type);
        }
    }
}
