﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Daikuan
    {
        public Model.Daikuan GetModelByID(int id)
        {
            return new DAL.Daikuan().GetModelByID(id);
        }

        public int Add(int uid, decimal jine, string danhao, int daihuankuan, int canBaofei, int huankuanType, string zhouqi, decimal lilv, decimal lixi, decimal benxi, decimal yihuan, string addTime, string daikuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark)
        {
            return new DAL.Daikuan().Add(uid, jine, danhao, daihuankuan, canBaofei, huankuanType, zhouqi, lilv, lixi, benxi, yihuan, addTime, daikuanTime, daoqiTime, jbid, add_remark, delTime, delid, del_remark, null);
        }


        public int Update(int id, decimal yihuan)
        {
            int isFinish = new DAL.DaikuanPayback().GetWeihuanCount(id, null) == 0 ? 1 : 0;
            return new DAL.Daikuan().Update(id, yihuan, isFinish, null);
        }

        public bool DaikuanPayback_Auto(bool isService, List<Model.DaikuanPayback> listDaikuan, DateTime dtNow, Model.Setup modelSetup, DbTransaction tran, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");
            List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);

            foreach (Model.DaikuanPayback one in listDaikuan)
            {
                Model.HuoqiQuKuan modelQukuan = new BLL.Huoqi().GetLixi(one.uid, one.jine, modelSetup, true, tran);
                if (modelQukuan.benjinlixi >= one.jine)//本金+利息，大于应还款金额
                {
                    int oid_qk = new BLL.Huoqi().Qukuan_Benxi_Service(one.uid, modelQukuan, modelSetup, -1, "本息取款_自动还贷款", ref msg, tran);
                    if (oid_qk == 0)
                    {
                        msg = "活期取款单生成失败，用户id:" + one.id + "，详细信息" + Tool.JsonHelper.JsonSerializer(one);
                        return false;
                    }
                    else
                    {
                        decimal huoqi_left = modelQukuan.benjinlixi - one.jine;//扣除贷款后，多余的钱，再次存入零钱账户
                        if (huoqi_left > 0)
                        {
                            //新增存款，不能报废
                            int oid = new DAL.Huoqi(isService).Add(one.uid, huoqi_left, huoqi_left, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow.ToString("yyyy-MM-dd HH:mm:ss"), dtNow.ToString("yyyy-MM-dd"), -1, "自动还贷_零钱存入。取款单id:" + oid_qk, Config.Config.str_defTime, 0, "", "", 0, 0, tran);
                            if (oid == 0)
                            {
                                msg = "活期取款后再次存款失败，用户id:" + one.id + "，详细信息" + Tool.JsonHelper.JsonSerializer(one);
                                return false;
                            }
                        }
                    }
                    //更新还贷信息
                    if (!new BLL.DaikuanPayback().Huandai(one.id, one.dkId, one.jine, dtNow, 1, -1, "零钱还款", oid_qk, ref msg, isService, tran))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        //贷款报废

        public int DaikuanBF(int id, int uid, string remark)
        {
            int res = new DAL.Daikuan().DaikuanBF(id, uid, remark,null);
            return res;
        }


        public Model.Daikuan_Mobile GetDaikuan(int uid, DbTransaction tran = null)
        {
            return new DAL.Daikuan().GetDaikuan(uid, tran);
        }











    }
}
