﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class ServiceLog
    {
        private bool _sqlitePath=true;
  
        public int Add(DateTime addTime, int isBackup, int huoqiJiexi, int dingqiZhuancun, DbTransaction tran)
        {
            return new DAL.ServiceLog(_sqlitePath).Add(addTime, isBackup, huoqiJiexi, dingqiZhuancun, tran);
        }

        public bool Update(int id, string strFilter, int value, DbTransaction tran)
        {
            return new DAL.ServiceLog(_sqlitePath).Update(id, strFilter, value, tran) == 1;
        }

        public Model.ServiceLog GetLastRecord()
        {
            return new DAL.ServiceLog(_sqlitePath).GetLastRecord();
        }
    }
}
