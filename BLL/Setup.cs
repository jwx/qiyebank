﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Setup
    {
        public bool Update_Setup(int useShouquanma, int backupTime, int hq_byDay, int hq_autoCalc, decimal hq_daylilv, int days, int hq_houcunxianqu, int dq_byDay, int dq_daoqi)
        {
            return new DAL.Setup().Update_Setup(useShouquanma, backupTime, hq_byDay, hq_autoCalc, hq_daylilv, days, hq_houcunxianqu, dq_byDay, dq_daoqi) != 0;
        }

        public bool Update_HuoqiLilv(string hq_lilv)
        {
            return new DAL.Setup().Update_HuoqiLilv(hq_lilv) != 0;
        }

        public bool Update_DingqiLilv(string dq_lilv, int dq_zc, decimal dq_zclilv)
        {
            return new DAL.Setup().Update_DingqiLilv(dq_lilv, dq_zc, dq_zclilv) != 0;
        }
        public bool Update_DaikuanLilv(string dq_lilv)
        {
            return new DAL.Setup().Update_DaikuanLilv(dq_lilv) != 0;
        }

        public Model.Setup Get_Setup()
        {
            return new DAL.Setup().Get_Setup();
        }

        public Model.Setup Get_Setup(bool isService)
        {
            return new DAL.Setup(isService).Get_Setup();
        }

        public Model.Setup Get_HuoqiLilv()
        {
            return new DAL.Setup().Get_HuoqiLilv();
        }

        public Model.Setup Get_DingqiLilv()
        {
            return new DAL.Setup().Get_DingqiLilv();
        }
        public Model.Setup Get_DaikuanLilv()
        {
            return new DAL.Setup().Get_DaikuanLilv();
        }

        public void InitSystem_Table(string[] arrayTable)
        {
            foreach (string one in arrayTable)
            {
                new DAL.Setup().InitSystem_Table(one);
            }
        }

        public bool InitSystem_Setup()
        {
            return new DAL.Setup().InitSystem_Setup() != 0;
        }
    }
}
