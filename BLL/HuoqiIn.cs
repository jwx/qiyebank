﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class HuoqiIn
    {
        public int Add(int uid, decimal jine, string zhifubao, string realname, string mobile, string modelSetup, string addTime, string cunkuanTime, string add_remark)
        {
            int id = new DAL.HuoqiIn().Add(uid, jine, zhifubao, realname, mobile, modelSetup, addTime, cunkuanTime, add_remark);
            if (id != 0)
            {
                new DAL.UserInfo().Update_Mobile(uid, mobile);
            }
            return id;
        }

        public bool Update_CheckPass(int id, int ckid, string ck_remark, ref string msg)
        {
            Model.HuoqiIn model = new DAL.HuoqiIn().GetModel(id);
            if (model != null && model.id != 0)
            {
                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd");
                Model.Setup modelSetup = Tool.JsonHelper.JsonDeserialize<Model.Setup>(model.model_setup);
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    int oid = new DAL.Huoqi().Add(model.uid, model.jine, model.jine, new Tool.OrderNo().GetOrderNo(), 1, 1, (int)Config.Enums.Huoqi_Laiyuan.零钱存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, model.cunkuanTime, ckid, "手机转入", Config.Config.str_defTime, 0, "", "", 0, 0, tran);
                    if (oid != 0)
                    {
                        if (new DAL.HuoqiIn().Update_Check(id, ckid, ck_remark, oid, tran) != 0)
                        {
                            tran.Commit();
                            return true;
                        }
                        else
                        {
                            msg = "数据审核失败！";
                            tran.Rollback();
                            return false;
                        }
                    }
                    else
                    {
                        msg = "零钱存款失败！";
                        tran.Rollback();
                        return false;
                    }
                }
            }
            msg = "读取转入数据失败！";
            return false;
        }

        public bool Update_CheckNotPass(int id, int ckid, string ck_remark)
        {
            return new DAL.HuoqiIn().Update_Check(id, ckid, ck_remark, -1, null) == 1;
        }

        public Model.HuoqiIn GetModel(int id)
        {
            return new DAL.HuoqiIn().GetModel(id);
        }

        public Model.HuoqiIn GetModel(int id, int uid)
        {
            return new DAL.HuoqiIn().GetModel(id, uid);
        }
    }
}
