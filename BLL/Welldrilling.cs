﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
   public class Welldrilling
    {

        public Model.Welldrilling GetModelByID(int id)
        {
            return new DAL.Welldrilling().GetModelByID(id);
        }
        public List<Model.Welldrilling> GetWModelByID(int id)
        {
            return new DAL.Welldrilling().GetWModelByID(id);
        }

        public bool Add(string year, int personalincometax, int Managementexpense, int other, int everydayton, int loss,int aid)
        {
            return new DAL.Welldrilling().Add(year, personalincometax, Managementexpense, other, everydayton, loss,aid,null) != 0;
        }

        public bool Update(int id,string year, int personalincometax, int Managementexpense, int other, int everydayton, int loss,int aid)
        {
            return new DAL.Welldrilling().Update(id,year, personalincometax, Managementexpense, other, everydayton, loss,aid) != 0;
        }


        public string Delete(string id)
        {
            if (new DAL.Welldrilling().Delete(Falcon.Function.ToInt(id)) == 1)
            {
                return "1";
            }
            return "操作失败！";
        }



    }
}
