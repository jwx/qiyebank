﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace BLL
{
    public class UserInfo
    {
        #region 增加

        public bool Add(string username, string loginpswd, string userpswd, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string addTime, string remark, string photo, string avatar, int jbid,int UserOrGS)
        {
            return new DAL.UserInfo().Add(username, loginpswd, userpswd, realname, idcard, email, gender, tel, mobile, address, bankcard, bank_uname, bankname, zhifubao, addTime, remark, photo, avatar, jbid, UserOrGS, null) != 0;
        }
        public bool Add(string username, string loginpswd, string userpswd, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string addTime, string remark, string photo, string avatar, int jbid,int gslxid,string gslxname,string fddbr,string zczb,string clsj,string yyqx,string jyfw,int dwlxid,string dwlxname, int UserOrGS)
        {
            return new DAL.UserInfo().Add(username, loginpswd, userpswd, realname, idcard, email, gender, tel, mobile, address, bankcard, bank_uname, bankname, zhifubao, addTime, remark, photo, avatar, jbid,null,gslxid, gslxname, fddbr, zczb, clsj, yyqx, jyfw, dwlxid, dwlxname, UserOrGS) != 0;
        }

        public bool Import(System.Data.DataTable dt, int jbid, ref string msg)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                try
                {
                    List<string> listDb_UserName = new DAL.UserInfo().GetListUserName(tran);
                    listDb_UserName = listDb_UserName == null ? new List<string>() : listDb_UserName;

                    int i = 0;
                    foreach (System.Data.DataRow one in dt.Rows)
                    {
                        i++;
                        if (i == 1) continue;//跳过示例数据
                        string username = one["账号"].ToString().Trim();
                        if (!string.IsNullOrEmpty(username))
                        {
                            if (listDb_UserName.Contains(username))
                            {
                                msg = "系统中已存在账号：" + username;
                                tran.Rollback();
                                return false;
                            }
                            listDb_UserName.Add(username);

                            int gender = -1;
                            string xingbie = one["性别"].ToString().Trim();
                            if (xingbie == "男")
                            {
                                gender = 1;
                            }
                            else if (xingbie == "女")
                            {
                                gender = 0;
                            }

                            if (new DAL.UserInfo().Add(username, one["登录密码"].ToString().Trim(), one["支付密码"].ToString().Trim(), one["户名"].ToString().Trim(), one["身份证号"].ToString().Trim(), one["电子邮箱"].ToString().Trim(), gender, one["座机号码"].ToString().Trim(), one["手机号码"].ToString().Trim(), one["居住地址"].ToString().Trim(), one["银行卡号"].ToString().Trim(), one["开户人姓名"].ToString().Trim(), one["开户行"].ToString().Trim(), one["支付宝账号"].ToString().Trim(), dtNow, one["备注"].ToString().Trim(), "", "", jbid,  tran) == 0)
                            {
                                msg = "操作失败";
                                tran.Rollback();
                                return false;
                            }
                        }
                    }
                    tran.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    tran.Rollback();
                    return false;
                }
            }
        }

        #endregion

        #region 修改

        public bool Update(int id, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string remark, string photo, int jbid)
        {
            return new DAL.UserInfo().Update(id, realname, idcard, email, gender, tel, mobile, address, bankcard, bank_uname, bankname, zhifubao, remark, photo, jbid) == 1;
        }

        public bool Update(int id, string realname, string idcard, string email, int gender, string tel, string mobile, string address, string bankcard, string bank_uname, string bankname, string zhifubao, string remark, string photo, int jbid,  int gslxid, string gslxname,  string fddbr, string zczb, string clsj, string yyqx, string jyfw, int dwlxid, string dwlxname)
        {
            return new DAL.UserInfo().Update(id, realname, idcard, email, gender, tel, mobile, address, bankcard, bank_uname, bankname, zhifubao, remark, photo, jbid, gslxid, gslxname, fddbr, zczb, clsj, yyqx, jyfw, dwlxid, dwlxname) == 1;
        }

        /// <summary>
        /// 密码修改
        /// </summary>
        /// <param name="id"></param>
        /// <param name="oldpswd"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public bool UpdatePswd_Login(int id, string oldpswd, string userpswd)
        {
            Model.UserInfo model = new DAL.UserInfo().GetModel(id);
            if (model != null && model.id != 0)
            {
                if (Falcon.Function.Encrypt(oldpswd) == model.loginpswd)
                {
                    return new DAL.UserInfo().UpdatePswd_Login(id, userpswd) == 1;
                }
                return false;
            }
            return false;
        }

        /// <summary>
        /// 修改支付密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="oldpswd"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public bool UpdatePswd_Pay(int id, string oldpswd, string userpswd, ref string msg)
        {
            Model.UserInfo model = new DAL.UserInfo().GetModel(id);
            if (model != null && model.id != 0)
            {
                if (Falcon.Function.Encrypt(oldpswd) == model.userpswd)
                {
                    return new DAL.UserInfo().UpdatePswd_Pay(id, userpswd) == 1;
                }
                msg = "原始支付密码有误！";
                return false;
            }
            msg = "用户不存在！";
            return false;
        }

        /// <summary>
        /// 密码挂失
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public bool UpdatePswd_Login(int id, string userpswd)
        {
            return new DAL.UserInfo().UpdatePswd_Login(id, userpswd) == 1;
        }

        /// <summary>
        /// 支付挂失
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public bool UpdatePswd_Pay(int id, string userpswd)
        {
            return new DAL.UserInfo().UpdatePswd_Pay(id, userpswd) == 1;
        }

        /// <summary>
        /// 账号挂失
        /// </summary>
        /// <param name="id"></param>
        /// <param name="oldusername"></param>
        /// <param name="username"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        public bool Update_GusShi(int id, string oldusername, string username)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                if (new DAL.UserInfo().Update_GuaShi(id, username, tran))
                {
                    tran.Commit();
                    return true;
                }
                tran.Rollback();
                return false;
            }

            //ex = new DAL.UserInfo().LogOff(id, username, out ex);//更改用户信息表
            //if (string.IsNullOrEmpty(ex))
            //{
            //    ex = new DAL.Dingqi().LogOff(oldusername, username, out ex);//更改定期存款表
            //    if (string.IsNullOrEmpty(ex))
            //    {
            //        return ex = new DAL.Huoqi().LogOff(oldusername, username, out ex);//更改活期存款表
            //    }
            //    return ex;
            //}
            //return ex;
            //}
            //return ex = "操作失败，请重新尝试！";
        }

        public int UpdateBaseInfo(int uid, string email, string mobile, string address)
        {
            return new DAL.UserInfo().UpdateBaseInfo(uid, email, mobile, address);
        }

        public bool Update_Avatar(int uid, string avatar)
        {
            return new DAL.UserInfo().Update_Avatar(uid, avatar) == 1;
        }

        #endregion

        #region 查询

        public Model.UserInfo GetModel(int id)
        {
            return new DAL.UserInfo().GetModel(id);
        }

        public Model.UserInfo GetModel(string username)
        {
            return new DAL.UserInfo().GetModel(username);
        }

        public Model.UserInfo GetModel(string filter, int id)
        {
            return new DAL.UserInfo().GetModel(filter, id);
        }

        public Model.UserInfo GetModel(string filter, string username)
        {
            return new DAL.UserInfo().GetModel(filter, username);
        }

        public List<Model.UserInfo> GetListModel()
        {
            return new DAL.UserInfo().GetListModel();
        }

        public List<Model.UserInfo> GetListByWhere(string where)
        {
            return new DAL.UserInfo().GetListByWhere(where);
        }

        public System.Data.DataTable GetListDtByWhere(string where)
        {
            return new DAL.UserInfo().GetListDtByWhere(where);
        }

        public int GetRecordCount(string where)
        {
            return new DAL.UserInfo().GetRecordCount(where);
        }

        public List<Model.UserInfo> GetList_Pager(int pageindex, int pagesize, string where)
        {
            return new DAL.UserInfo().GetList_Pager(pageindex, pagesize, where);
        }

        public int isExist_UserName(int id, string username)
        {
            return new DAL.UserInfo().isExist_UserName(id, username);
        }

        /// <summary>
        /// 手机端登录接口
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Model.UserInfo Login(string username)
        {
            return new DAL.UserInfo().Login(username);
        }
        #endregion

        #region  删除

        public int Delete(int id, out string ex)
        {
            if (new DAL.UserInfo().Delete(id, null) == 1)
            {
                ex = "操作成功！";
                return 1;
            }
            else
            {
                ex = "操作失败！";
                return 0;
            }
        }

        public bool Delete(params int[] arrDelIds)
        {
            if (arrDelIds.Length == 1)
            {
                return new DAL.UserInfo().Delete(arrDelIds[0], null) == 1;
            }
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                bool flag = true;
                foreach (int one in arrDelIds)
                {
                    if (new DAL.UserInfo().Delete(one, tran) != 1)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }
        #endregion
    }
}