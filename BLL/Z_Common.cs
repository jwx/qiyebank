﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 获取datatable，多用于excel导出
    /// </summary>
    public class DataTableHelper
    {
        public System.Data.DataTable GetDataTableByWhere(string view, string filter, string where)
        {
            return new DAL.DataTableHelper().GetDataTableByWhere(view, filter, where);
        }
    }

    public class Notification
    {
        public bool Add(string title, string content, int isEnable, int sort, int jbid, string strUids)
        {
            if (string.IsNullOrEmpty(strUids)) return new DAL.Notification().Add(title, content, isEnable, sort, jbid, null) != 0;
            Database db =Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                int nid = new DAL.Notification().Add(title, content, isEnable, sort, jbid, tran);
                if (nid != 0)
                {
                    int[] arrayUids = Array.ConvertAll<string, int>(strUids.Split(','), m => Falcon.Function.ToInt(m));
                    foreach (int one in arrayUids)
                    {
                        if (new DAL.SubNotification().Add(nid, one, tran) == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                    tran.Commit();
                    return true;
                }
                tran.Rollback();
                return false;
            }

        }

        public bool Update(int id, string title, string content, int isEnable, int sort, int jbid, string strUids)
        {
            Database db =Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                if (new DAL.Notification().Update(id, title, content, isEnable, sort, jbid, tran) == 1 && new DAL.SubNotification().Delete(id, tran) != -1)
                {
                    if (string.IsNullOrEmpty(strUids))
                    {
                        tran.Commit();
                        return true;
                    }
                    else
                    {
                        int[] arrayUids = Array.ConvertAll<string, int>(strUids.Split(','), m => Falcon.Function.ToInt(m));
                        foreach (int one in arrayUids)
                        {
                            if (new DAL.SubNotification().Add(id, one, tran) == 0)
                            {
                                tran.Rollback();
                                return false;
                            }
                        }
                        tran.Commit();
                        return true;
                    }
                }
                tran.Rollback();
                return false;
            }
        }

        public Model.Notification GetModel(int id)
        {
            return new DAL.Notification().GetModel(id);
        }

        public string Delete(int id)
        {
            if (new DAL.Notification().Delete(id) == 1)
            {
                return "1";
            }
            return "操作失败！";
        }
    }

    public class SubNotification
    {
        public int Add(int nid, int uid)
        {
            return new DAL.SubNotification().Add(nid, uid, null);
        }

        public int Update_Read(int nid, int uid)
        {
            return new DAL.SubNotification().Update_Read(nid, uid);
        }

        public List<Model.SubNotification> GetListByWhere(string where)
        {
            return new DAL.SubNotification().GetListByWhere(where);
        }

        public int GetCount_NotRead(int uid)
        {
            return new DAL.SubNotification().GetCount_NotRead(uid);
        }

        public string Delete(int nid)
        {
            if (new DAL.SubNotification().Delete(nid, null) == 1)
            {
                return "1";
            }
            return "操作失败！";
        }
    }
}
