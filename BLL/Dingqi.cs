﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace BLL
{
    public class Dingqi
    {
        #region 增加

        public int Add(int uid, decimal jine, string danhao, int cunqukuan, int canBaofei, int hasQukuan, int autoQukuan, int qukuanType, int oid, int daoqi_handler, string zhouqi, decimal lilv, decimal budaoqi, decimal lixi, string addTime, string cunkuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark)
        {
            return new DAL.Dingqi().Add(uid, jine, danhao, cunqukuan, canBaofei, hasQukuan, autoQukuan, qukuanType, oid, daoqi_handler, zhouqi, lilv, budaoqi, lixi, addTime, cunkuanTime, daoqiTime, jbid, add_remark, delTime, delid, del_remark, null);
        }

        /// <summary>
        /// 人工本金取款
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="uid"></param>
        /// <param name="lixi"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_Benjin(int oid, int uid, decimal jine, decimal lixi, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.Dingqi model = new DAL.Dingqi().GetModel(oid, uid);
            if (model != null && model.id != 0)
            {
                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Dingqi().Add(uid, -jine, new Tool.OrderNo().GetOrderNo(), 2, 1, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本金取款, oid, 0, model.zhouqi, model.lilv, model.budaoqi, lixi, dtNow, strDt, model.daoqiTime, jbid, add_remark, Config.Config.str_defTime, 0, "", tran);
                    if (oid_qk != 0)
                    {
                        //判断取款金额是否大于存款金额
                        if (model.jine == jine)
                        {
                            //存款单设为已取款
                            if (new DAL.Dingqi().Update_Qukuan(oid, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本金取款, oid_qk, lixi, "_本金取款", tran) != 1)
                            {
                                msg = "定期取款失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        else //没有全部取完
                        {
                            if (new DAL.Dingqi().Update_Qukuan(oid, lixi, jine, tran) != 1)
                            {
                                msg = "定期取款失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        if (lixi != 0)
                        {
                            //添加利息到活期账户，不能报废
                            if (new DAL.Huoqi().Add(uid, lixi, lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.定期利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, dtNow, jbid, "定期利息_系统自动存款", Config.Config.str_defTime, 0, "", "", oid_qk, model.jine, tran) == 0)
                            {
                                msg = "利息存款失败！";
                                tran.Rollback();
                                return 0;
                            }
                            tran.Commit();
                            return oid_qk;
                        }
                        else
                        {
                            tran.Commit();
                            return oid_qk;
                        }
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "读取存款单据失败！";
            return 0;
        }

        /// <summary>
        /// 人工本息取款
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="uid"></param>
        /// <param name="lixi"></param>
        /// <param name="modelSetup"></param>
        /// <param name="jbid"></param>
        /// <param name="add_remark"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int Qukuan_Benxi(int oid, int uid, decimal jine, decimal lixi, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.Dingqi model = new DAL.Dingqi().GetModel(oid, uid);
            if (model != null && model.id != 0)
            {
                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    //添加取款单，可以报废
                    int oid_qk = new DAL.Dingqi().Add(uid, -(jine), new Tool.OrderNo().GetOrderNo(), 2, 1, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本息取款, oid, 0, model.zhouqi, model.lilv, model.budaoqi, lixi, dtNow, strDt, model.daoqiTime, jbid, add_remark, Config.Config.str_defTime, 0, "", tran);
                    if (oid_qk != 0)
                    {
                        //判断取款金额是否大于存款金额
                        if (model.jine == jine)
                        {
                            //存款单设为已取款
                            if (new DAL.Dingqi().Update_Qukuan(oid, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本息取款, oid_qk, lixi, "_本息取款", tran) != 1)
                            {
                                msg = "定期取款失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        else
                        {
                            if (new DAL.Dingqi().Update_Qukuan(oid, lixi,  jine, tran) != 1)
                            {
                                msg = "定期取款失败！";
                                tran.Rollback();
                                return 0;
                            }
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "读取存款单据失败！";
            return 0;
        }

        public int Qukuan_ToHuoqi(int oid, int uid, decimal lixi, Model.Setup modelSetup, int jbid, string add_remark, ref string msg)
        {
            Model.Dingqi model = new DAL.Dingqi().GetModel(oid, uid);
            if (model != null && model.id != 0)
            {
                string dtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                Database db = Tool.DatabaseHelper.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    string danhao = new Tool.OrderNo().GetOrderNo();
                    decimal benxi = model.jine + lixi;
                    //添加取款单，可以报废
                    int oid_qk = new DAL.Dingqi().Add(uid, -benxi, danhao, 2, 1, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本息取款, oid, 0, model.zhouqi, model.lilv, model.budaoqi, lixi, dtNow, strDt, model.daoqiTime, jbid, add_remark, Config.Config.str_defTime, 0, "", tran);
                    if (oid_qk != 0)
                    {
                        string huoqidanhao = new Tool.OrderNo().GetOrderNo();
                        if (new DAL.Huoqi().Add(uid, benxi, benxi, huoqidanhao, 1, 0, (int)Config.Enums.Huoqi_Laiyuan.定期本息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, dtNow, strDt, -1, "定期转零钱_定期本息存入,定期单号：" + danhao, Config.Config.str_defTime, 0, "", "", oid_qk, 0, tran) == 0)
                        {
                            msg = "活期存款失败！";
                            tran.Rollback();
                            return 0;
                        }
                        //存款单设为已取款
                        if (new DAL.Dingqi().Update_Qukuan(oid, 1, (int)Config.Enums.Dingqi_AutoQukuan.人工取款, (int)Config.Enums.Dingqi_Qukuan.本息取款, oid_qk, lixi, "_本息取款，本金和利息转零钱账户，活期单号：" + huoqidanhao, tran) != 1)
                        {
                            msg = "定期取款失败！";
                            tran.Rollback();
                            return 0;
                        }
                        tran.Commit();
                        return oid_qk;
                    }
                    msg = "取款单添加失败！";
                    tran.Rollback();
                    return 0;
                }
            }
            msg = "读取存款单据失败！";
            return 0;
        }

        public bool Zhuancun_Auto(bool isService, List<Model.Dingqi> listDingqi, DateTime dtNow, Model.Setup modelSetup, DbTransaction tran, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");
            List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);

            foreach (Model.Dingqi one in listDingqi)
            {
                decimal lixi = GetLixi(modelSetup, one, dtNow);
                //取款时结算本金和利息 = 0,
                //本金转定期_利息转零钱 = 1,
                //本金和利息转定期账户 = 2,
                //本金和利息转零钱账户 = 3
                if (one.daoqi_handler == 1)
                {
                    //添加定期本金存款单，原定期存款单改为已取款，利息存入活期账户
                    int zc_oid = new DAL.Dingqi(isService).Add(one.uid, one.jine, new Tool.OrderNo().GetOrderNo(), 1, 0, 0, 0, 0, 0, one.daoqi_handler, "自动转存" + "_" + modelSetup.dq_zc + listLilvRange[0].unit, modelSetup.dq_zclilv, listLilvRange[0].budaoqi, 0, strDtNow, strDt, modelSetup.dq_byDay == 1 ? dtNow.AddDays(modelSetup.dq_zc + 1).ToString("yyyy-MM-dd") : dtNow.AddMonths(modelSetup.dq_zc).AddDays(1).ToString("yyyy-MM-dd"), -1, "本金转定期_利息转零钱_系统自动操作", Config.Config.str_defTime, 0, "", tran);
                    if (zc_oid == 0 || new DAL.Dingqi(isService).Update_Qukuan(one.id, 1, (int)Config.Enums.Dingqi_AutoQukuan.系统取款, (int)Config.Enums.Dingqi_Qukuan.本金取款, zc_oid, lixi, "_本金转定期_利息转零钱", tran) == 0 || new DAL.Huoqi(isService).Add(one.uid, lixi, lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.定期利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, -1, "零钱利息_系统自动转存定期利息", Config.Config.str_defTime, 0, "", "", one.id, one.jine, tran) == 0)
                    {
                        msg = "定期单据id：" + one.id + "本金转定期_利息转零钱操作失败！";
                        //tran.Rollback();
                        return false;
                    }
                }
                else if (one.daoqi_handler == 2)
                {
                    //添加定期本息存款单，原定期存款单改为已取款
                    int zc_oid = new DAL.Dingqi(isService).Add(one.uid, one.jine + lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, 0, 0, 0, 0, one.daoqi_handler, "自动转存" + "_" + modelSetup.dq_zc + listLilvRange[0].unit, modelSetup.dq_zclilv, listLilvRange[0].budaoqi, 0, strDtNow, strDt, modelSetup.dq_byDay == 1 ? dtNow.AddDays(modelSetup.dq_zc + 1).ToString("yyyy-MM-dd") : dtNow.AddMonths(modelSetup.dq_zc).AddDays(1).ToString("yyyy-MM-dd"), -1, "本金和利息转定期账户_系统自动操作", Config.Config.str_defTime, 0, "", tran);
                    if (zc_oid == 0 || new DAL.Dingqi(isService).Update_Qukuan(one.id, 1, (int)Config.Enums.Dingqi_AutoQukuan.系统取款, (int)Config.Enums.Dingqi_Qukuan.本金取款, zc_oid, lixi, "_本金和利息转定期账户", tran) == 0)
                    {
                        msg = "定期单据id：" + one.id + "本金和利息转定期账户操作失败！";
                        //tran.Rollback();
                        return false;
                    }
                }
                else if (one.daoqi_handler == 3)
                {
                    //原定期存款单改为已取款，添加活期本息存款单
                    if (new DAL.Dingqi(isService).Update_Qukuan(one.id, 1, (int)Config.Enums.Dingqi_AutoQukuan.系统取款, (int)Config.Enums.Dingqi_Qukuan.本金取款, one.id, lixi, "_本金和利息转零钱账户", tran) == 0 || new DAL.Huoqi(isService).Add(one.uid, one.jine + lixi, one.jine + lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, (int)Config.Enums.Huoqi_Laiyuan.定期利息存入, modelSetup.hq_autoCalc, modelSetup.hq_lilv, 0, strDtNow, strDt, -1, "零钱利息_系统自动转存定期本息", Config.Config.str_defTime, 0, "", "", one.id, one.jine, tran) == 0)
                    {
                        msg = "定期单据id：" + one.id + "本金和利息转零钱账户操作失败！";
                        //tran.Rollback();
                        return false;
                    }
                }
            }
            //tran.Commit();
            return true;
        }

        public bool Qukuan_Jiexi_Auto(List<Model.Dingqi> listDingqi, DateTime dtNow, Model.Setup modelSetup, ref string msg)
        {
            string strDtNow = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            string strDt = DateTime.Now.ToString("yyyy-MM-dd");
            List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.dq_lilv);

            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                foreach (Model.Dingqi one in listDingqi)
                {
                    decimal lixi = GetLixi(modelSetup, one, dtNow);

                    int qk_oid = new DAL.Dingqi().Add(one.uid, one.jine + lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, 0, 0, 0, 0, one.daoqi_handler, "自动转存_" + modelSetup.dq_zc + listLilvRange[0].unit, modelSetup.dq_zclilv, listLilvRange[0].budaoqi, 0, strDtNow, strDt, modelSetup.dq_byDay == 1 ? dtNow.AddDays(modelSetup.dq_zc + 1).ToString("yyyy-MM-dd") : dtNow.AddMonths(modelSetup.dq_zc).AddDays(1).ToString("yyyy-MM-dd"), -1, "_定期批量结息", Config.Config.str_defTime, 0, "", tran);

                    if (qk_oid == 0 || new DAL.Dingqi().Update_Qukuan(one.id, 1, 1, (int)Config.Enums.Dingqi_Qukuan.本息取款, qk_oid, lixi, "_定期批量结息", tran) == 0)
                    {
                        msg = "定期单据id：" + one.id + "批量结息！";
                        tran.Rollback();
                        return false;
                    }
                }
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 获取定期订单的实际利息
        /// </summary>
        /// <param name="modelSetup"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public decimal GetLixi(Model.Setup modelSetup, Model.Dingqi model, DateTime dtNow)
        {
            int lilvJishu = modelSetup.dq_byDay == 1 ? 365 : 12;//年利率基数，按天还是按月   
            DateTime cunkuanTime = Falcon.Function.ToDateTime(model.cunkuanTime);//存款周期
            decimal lilv = (dtNow - Falcon.Function.ToDateTime(model.daoqiTime)).Days >= 0 ? model.lilv : model.budaoqi;//正常利率和不到期利率 

            int cunkuanZhouqi = modelSetup.dq_byDay == 1 ? (dtNow - cunkuanTime).Days : new Tool.TimeHelper().GetFullMonth(dtNow, cunkuanTime);//实际存款天数或月数
            return Math.Round(model.jine * cunkuanZhouqi * lilv / 100 / lilvJishu, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// 获取定期订单的实际利息
        /// </summary>
        /// <param name="modelSetup"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public decimal GetLixi_Yesterday(Model.Setup modelSetup, Model.Dingqi model, DateTime dtNow)
        {
            int lilvJishu = modelSetup.dq_byDay == 1 ? 365 : 12;//年利率基数，按天还是按月   
            DateTime cunkuanTime = Falcon.Function.ToDateTime(model.cunkuanTime);//存款周期
            decimal lilv = (dtNow - Falcon.Function.ToDateTime(model.daoqiTime)).Days >= 0 ? model.lilv : model.budaoqi;//正常利率和不到期利率 

            int cunkuanZhouqi = modelSetup.dq_byDay == 1 ? (dtNow - cunkuanTime).Days : new Tool.TimeHelper().GetFullMonth(dtNow, cunkuanTime);//实际存款天数或月数
            return Math.Round(model.jine * lilv / 100 / lilvJishu, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// 获取定期订单的预估利息
        /// </summary>
        /// <param name="modelSetup"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public decimal GetLixi_Yugu(int byDay, Model.Dingqi model)
        {
            int lilvJishu = byDay == 1 ? 365 : 12;//年利率基数，按天还是按月  
            DateTime dtNow = DateTime.Now;
            DateTime cunkuanTime = Falcon.Function.ToDateTime(model.cunkuanTime);//存款周期
            decimal lilv = model.lilv;// (dtNow - Falcon.Function.ToDateTime(model.daoqiTime)).Days >= 0 ? model.lilv : model.budaoqi;//正常利率和不到期利率 

            int cunkuanZhouqi = byDay == 1 ? (dtNow - cunkuanTime).Days : new Tool.TimeHelper().GetFullMonth(dtNow, cunkuanTime);//实际存款天数或月数
            return Math.Round(model.jine * cunkuanZhouqi * lilv / 100 / lilvJishu, 2, MidpointRounding.AwayFromZero);
        }


        #endregion

        #region 修改

        public bool Update(int id, int uid, decimal jine, string danhao, int cunqukuan, int canBaofei, int hasQukuan, int autoQukuan, int oid, int daoqi_handler, string zhouqi, decimal lilv, decimal budaoqi, decimal lixi, string addTime, string cunkuanTime, string daoqiTime, int jbid, string add_remark, string delTime, int delid, string del_remark)
        {
            return new DAL.Dingqi().Update(id, uid, jine, danhao, cunqukuan, canBaofei, hasQukuan, autoQukuan, oid, daoqi_handler, zhouqi, lilv, budaoqi, lixi, addTime, cunkuanTime, daoqiTime, jbid, add_remark, delTime, delid, del_remark) == 1;
        }

        /// <summary>
        /// 报废
        /// </summary>
        /// <param name="id"></param>
        /// <param name="delid"></param>
        /// <param name="del_remark"></param>
        /// <returns></returns>
        public bool Update_Baofei(int id, int delid, string del_remark)
        {
            return new DAL.Dingqi().Update_Baofei(id, delid, del_remark, null) == 1;
        }

        public bool Update_QukuanBaofei(int ckoid, int qkoid, int jbid, string del_remark, Model.Setup modelSetup, ref string msg)
        {
            Model.Dingqi model_Cunkuan = new BLL.Dingqi().GetModel(ckoid);
            Model.Dingqi model_Qukuan = new BLL.Dingqi().GetModel(qkoid);
            if (model_Cunkuan != null && model_Cunkuan.id != 0)
            {
                if (model_Qukuan != null && model_Qukuan.id != 0)
                {
                    Database db = Tool.DatabaseHelper.CreateDatabase();
                    using (var conn = db.CreateConnection())
                    {
                        conn.Open();
                        DbTransaction tran = conn.BeginTransaction();

                        if (model_Cunkuan.qukuanType == (int)Config.Enums.Dingqi_Qukuan.本金取款)//本金取款，需要到活期存款表中报废利息
                        {
                            Model.Huoqi model_Lixi = new DAL.Huoqi().GetModel_LixiByDingqi(qkoid, tran);
                            if (model_Lixi == null || model_Lixi.id == 0)
                            {
                                msg = "定期利息读取失败！";
                                tran.Rollback();
                                return false;
                            }
                            if (model_Lixi.jine == model_Lixi.yuanjine)//利息没有被取过款
                            {
                                if (new DAL.Huoqi().Update_Baofei(model_Lixi.id, jbid, "定期利息系统报废_" + del_remark, tran) != 1)//报废定期转到活期的利息
                                {
                                    msg = "定期利息报废失败！";
                                    tran.Rollback();
                                    return false;
                                }
                            }
                            else//如果利息已被取款（或部分取款）
                            {
                                //decimal userCurMoney = new DAL.Huoqi().GetYue(model_Lixi.uid, tran);//获取用户活期账户剩余金额
                                List<Model.Huoqi> listHuoqi = new DAL.Huoqi().GetList_Qukuan(model_Lixi.uid, modelSetup.hq_houcunxianqu, tran);
                                listHuoqi = listHuoqi == null ? new List<Model.Huoqi>() : listHuoqi;
                                decimal userCurMoney = (from m in listHuoqi select m.jine).Sum();
                                decimal lixijine = model_Lixi.yuanjine;//当前利息金额
                                if (userCurMoney >= lixijine)//如果用户当前账户余额大于等于当前利息金额
                                {
                                    foreach (Model.Huoqi one in listHuoqi)
                                    {
                                        if (one.jine >= lixijine)
                                        {
                                            if (new DAL.Huoqi().Update_Qukuan(one.id, lixijine, 0, tran) != 1)
                                            {
                                                msg = "定期利息回滚失败！";
                                                tran.Rollback();
                                                return false;
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            lixijine = lixijine - one.jine;
                                            if (new DAL.Huoqi().Update_Qukuan(one.id, one.jine, 0, tran) != 1)
                                            {
                                                msg = "定期利息回滚失败！";
                                                tran.Rollback();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    msg = "当前单据无法报废，零钱账户余额不足！";
                                    tran.Rollback();
                                    return false;
                                }
                                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                string strDt = DateTime.Now.ToString("yyyy-MM-dd");
                                //添加取款单，取款单不能报废
                                if (new DAL.Huoqi().Add(model_Lixi.uid, -lixijine, 0, new Tool.OrderNo().GetOrderNo(), 2, 0, (int)Config.Enums.Huoqi_Laiyuan.本息取款, modelSetup.hq_autoCalc, "", 0, strDtNow, strDt, jbid, del_remark, Config.Config.str_defTime, 0, "", "", 0, 0, tran) == 0)
                                {
                                    msg = "取款单添加失败！";
                                    tran.Rollback();
                                    return false;
                                }
                            }
                        }

                        if (new DAL.Dingqi().Update_Baofei(qkoid, jbid, del_remark, tran) != 1)
                        {
                            msg = "定期取款单报废失败！";
                            tran.Rollback();
                            return false;
                        }
                        if (new DAL.Dingqi().Update_Cunkuan_Rollback(ckoid, tran) != 1)
                        {
                            msg = "定期存款单回退失败！";
                            tran.Rollback();
                            return false;
                        }
                        tran.Commit();
                        return true;
                    }
                }
                msg = "定期取款单读取失败！";
                return false;
            }
            msg = "定期存款单读取失败！";
            return false;
        }

        #endregion

        #region 查询

        public Model.Dingqi GetModel(int id)
        {
            return new DAL.Dingqi().GetModel(id);
        }

        public Model.Dingqi GetModel(int id, int uid)
        {
            return new DAL.Dingqi().GetModel(id, uid);
        }

        public List<Model.Dingqi> GetListModel()
        {
            return new DAL.Dingqi().GetListModel();
        }

        public List<Model.Dingqi> GetListByWhere(string where)
        {
            return new DAL.Dingqi().GetListByWhere(where);
        }

        public List<Model.Dingqi> GetList_Qukuan()
        {
            return new DAL.Dingqi().GetListByWhere("cunqukuan=1 and hasQukuan=0 and delid=0");
        }

        public List<Model.Dingqi> GetList_Qukuan(int uid)
        {
            return new DAL.Dingqi().GetListByWhere("cunqukuan=1 and hasQukuan=0 and delid=0 and uid=" + uid);
        }

        public int GetRecordCount(string where)
        {
            return new DAL.Dingqi().GetRecordCount(where);
        }

        public List<Model.Dingqi> GetList_Pager(int pageindex, int pagesize, string where)
        {
            return new DAL.Dingqi().GetList_Pager(pageindex, pagesize, where);
        }

        /// <summary>
        /// 获取指定用户id已审核的定期账户余额
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public decimal GetYue(int uid)
        {
            return new DAL.Dingqi().GetYue(uid);
        }

        /// <summary>
        /// 获取系统中所有的定期存款总金额
        /// </summary>
        /// <returns></returns>
        public decimal GetSysYue()
        {
            return new DAL.Dingqi().GetSysYue();
        }

        /// <summary>
        /// 获取系统中已经付款的总利息
        /// </summary>
        /// <returns></returns>
        public decimal GetSysLixi_Payed()
        {
            return new DAL.Dingqi().GetSysLixi_Payed();
        }

        /// <summary>
        /// 获取系统中到期的定期取款单据
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_Qukuan(DateTime dtNow, DbTransaction tran)
        {
            return new DAL.Dingqi().GetList_Qukuan(dtNow, tran);
        }

        /// <summary>
        /// 获取系统中到期的定期取款单据
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_Qukuan(bool isService, DateTime dtNow, DbTransaction tran)
        {
            return new DAL.Dingqi(isService).GetList_Qukuan(dtNow, tran);
        }

        /// <summary>
        /// 获取用户存款单和利息信息--零钱已结转
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_YifuLixi()
        {
            return new DAL.Dingqi().GetList_YifuLixi();
        }

        /// <summary>
        /// 获取用户存款单和利息信息 
        /// </summary>
        /// <param name="laiyuan"></param>
        /// <returns></returns>
        public List<Model.Dingqi> GetList_WeifuLixi()
        {
            return new DAL.Dingqi().GetList_WeifuLixi();
        }

        #endregion

        #region  删除

        public int Delete(int id, out string ex)
        {
            if (new DAL.Dingqi().Delete(id, null) == 1)
            {
                ex = "操作成功！";
                return 1;
            }
            else
            {
                ex = "操作失败！";
                return 0;
            }
        }

        public bool Delete(params int[] arrDelIds)
        {
            if (arrDelIds.Length == 1)
            {
                return new DAL.Dingqi().Delete(arrDelIds[0], null) == 1;
            }
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                bool flag = true;
                foreach (int one in arrDelIds)
                {
                    if (new DAL.Dingqi().Delete(one, tran) != 1)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }
        #endregion
    }
}