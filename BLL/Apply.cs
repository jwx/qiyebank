﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Apply
    {
        public bool Update_Fukuan(int id, int isFukuan, int jbid, string fk_remark, string fk_time)
        {
            return new DAL.Apply().Update(id, isFukuan, jbid, fk_remark, fk_time, null) == 1;
        }

        public int Update_Fukuan_Piliang(List<int> listIds, int isFukuan, int jbid)
        {
            Database db = Tool.DatabaseHelper.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                string strDtNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                foreach (int one in listIds)
                {
                    if (new DAL.Apply().Update(one, isFukuan, jbid, "批量操作", strDtNow, tran) == 0)
                    {
                        tran.Rollback();
                        return 0;
                    }
                }
                tran.Commit();
                return 1;
            }
        }

        public string CreatePihao()
        {
            string pihao = new Tool.OrderNo().GetOrderNo();
            return new DAL.Apply().CreatePihao(pihao) == 0 ? "" : pihao;
        }

        public Model.Apply GetModel(int id)
        {
            return new DAL.Apply().GetModel(id);
        }

        public Model.Apply GetModel(int id, int uid)
        {
            return new DAL.Apply().GetModel(id, uid);
        }

        public List<Model.Apply> GetListByWhere(string where)
        {
            return new DAL.Apply().GetListByWhere(where);
        }

        public System.Data.DataTable GetDataTableByWhere(string where)
        {
            return new DAL.Apply().GetDataTableByWhere(where);
        }
    }
}
