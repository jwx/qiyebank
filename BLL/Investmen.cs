﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
  public  class Investmen
    {
   
        public Model.Investmen GetModelByID(int id)
        {
            return new DAL.Investmen().GetModelByID(id);
        }

        public bool Add(string accountnumber, string accountName, decimal tzmoney, int moneyAmountID, string moneyAmountName, string tzTime, string endTime, string remarks, int jbid,int wellid, int yearid, string yearName)
        {
            return new DAL.Investmen().Add(accountnumber,accountName,tzmoney,moneyAmountID,moneyAmountName,tzTime,endTime,remarks,jbid,null,wellid,yearid,yearName)!=0;
        }

    }
}
