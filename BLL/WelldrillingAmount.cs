﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
   public class WelldrillingAmount
    {
        public Model.WelldrillingAmount GetModelByID(int id)
        {
            return new DAL.WelldrillingAmount().GetModelByID(id);
        }


        public bool Add(string  money)
        {
            return new DAL.WelldrillingAmount().Add(money, null)!=0;
        }

        public bool Update(int id, string money)
        {
            return new DAL.WelldrillingAmount().Update(id,money) != 0;
        }


        public string Delete(string id)
        {
            if (new DAL.WelldrillingAmount().Delete(Falcon.Function.ToInt(id)) == 1)
            {
                return "1";
            }
            return "操作失败！";
        }


        public List<Model.WelldrillingAmount> getList()
        {

            return new DAL.WelldrillingAmount().getList();
        }

    }
}
