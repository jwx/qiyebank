﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
   public class InvestmenInfo
    {

        public Model.InvestmenInfo getModelById(int id) {
            return new DAL.InvestmenInfo().getModelById(id);
        }


        public bool Add(string month, int everymonthDay, decimal thisMonethMoney, int isinto, int isoperation)
        {
            return new DAL.InvestmenInfo().Add(month, everymonthDay, thisMonethMoney, isinto, isoperation) != 0;
        }

        public bool Update(int id, string month, int everymonthDay, decimal thisMonethMoney, int isinto, int isoperation)
        {

            return new DAL.InvestmenInfo().Update(id,month, everymonthDay, thisMonethMoney, isinto, isoperation) != 0;
        }


    }
}
