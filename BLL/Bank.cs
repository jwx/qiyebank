﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Bank
    {
        public bool Add(string name, string startNo, int sort, int jbid)
        {
            return new DAL.Bank().Add(name, startNo, sort, jbid) == 1;
        }

        public bool Update(int id, string name, string startNo, int sort, int jbid)
        {
            return new DAL.Bank().Update(id, name, startNo, sort, jbid) == 1;
        }

        public Model.Bank GetModel(int id)
        {
            return new DAL.Bank().GetModel(id);
        }

        public List<Model.Bank> GetList()
        {
            return new DAL.Bank().GetList();
        }

        public string Delete(string id)
        {
            if (new DAL.Bank().Delete(Falcon.Function.ToInt(id)) == 1)
            {
                return "1";
            }
            return "操作失败！";
        }
    }
}
