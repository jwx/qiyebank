﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class DaikuanPayback
    {
        public Model.DaikuanPayback GetModelByID(int id)
        {
            return new DAL.DaikuanPayback().GetModelByID(id);
        }

        public List<Model.DaikuanPayback> getListModel(int id)
        {
            return new DAL.DaikuanPayback().getListModel(id);
        }


        public int Add(int dkId, decimal jine, string huankuanDate, string huankuanTime, int huankuanType, int jbid, string remark)
        {
            return new DAL.DaikuanPayback().Add(dkId, jine, huankuanDate, huankuanTime, huankuanType, jbid, remark, null);
        }

        public int Update(int id, string huankuanTime, int huankuanType, int jbid, string remark)
        {
            //return new DAL.DaikuanPayback().Update(id, dkId, jine, huankuanDate, huankuanTime, huankuanType, jbid, remark, null);
            return new DAL.DaikuanPayback().Update(id, huankuanTime, huankuanType, jbid, remark, null);
        }


        public List<Model.DaikuanPayback> GetList_NotPay(bool isService, DateTime dtNow)
        {
            return new DAL.DaikuanPayback(isService).GetList_NotPay(dtNow);
        }

        public List<Model.DaikuanPayback> GetList_Yuqi(int uid)
        {
            return new DAL.DaikuanPayback().GetList_Yuqi(uid);
        }

        public bool Huandai(int id, int dkId, decimal jine, DateTime dtNow, int huankuanType, int jbid, string remark, int hqId, ref string msg, bool isService = false, DbTransaction tran = null)
        {
            if (new DAL.DaikuanPayback(isService).Payback(id, dtNow.ToString("yyyy-MM-dd HH:mm:ss"), huankuanType, jbid, remark, hqId, tran) != 0)
            {
                int isFinish = new DAL.DaikuanPayback().GetWeihuanCount(dkId, tran) == 0 ? 1 : 0;
                if (new DAL.Daikuan(isService).Update(dkId, jine, isFinish, tran) != 0)
                {
                    return true;
                }
                else
                {
                    msg = "贷款主表更新失败！";
                    return false;
                }
            }
            msg = "贷款还款失败！";
            return false;
        }


        //public int Delete(int dkid)
        //{
        //    return new DAL.DaikuanPayback().Delete(dkid);
        //}
        public List<Model.DaikuanPayback> GetList(int dkId)
        {
            return new DAL.DaikuanPayback().GetList(dkId);
        }

    }
}
