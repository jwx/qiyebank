﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Tongzhi
    {
        public bool Add(string title, string content, int isEnable, int sort, int jbid)
        {
            return new DAL.Tongzhi().Add(title, content, isEnable, sort, jbid, null) != 0;
        }

        public bool Update(int id, string title, string content, int isEnable, int sort, int jbid)
        {
            return new DAL.Tongzhi().Update(id, title, content, isEnable, sort, jbid, null) == 1;
        }

        public Model.Tongzhi GetModel(int id)
        {
            return new DAL.Tongzhi().GetModel(id);
        }

        public int Delete(int id)
        {
            return new DAL.Tongzhi().Delete(id);
        }
    }
}
