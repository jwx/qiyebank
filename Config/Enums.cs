﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Config
{
    public class Enums
    {
        public Dictionary<string, int> Dic_Daikuan_Type()
        {
            return new Dictionary<string, int>()
            {
                { "按月付利息,到期还本金",0},
                { "按月付利息,分期还本金",1},
                { "每月付本息",2 },
                { "到期付本息",3 }
            };
        }

        public enum Huoqi_Laiyuan
        {
            零钱存入 = 0,
            零钱利息存入 = 1,
            定期利息存入 = 2,
            定期本息存入 = 3,

            本金取款 = 100,
            本息取款 = 101
        }

        public enum Huoqi_SHstatus {
            未审核=0,
            已审核=1,
            已拒绝=2
        }
        public enum Dingqi_Qukuan
        {
            //未取款 = 0,
            本金取款 = 1,
            本息取款 = 2
        }

        public enum Dingqi_AutoQukuan
        {
            未取款 = 0,
            系统取款 = 1,
            人工取款 = 2
        }

        public enum Dingqi_DaoqiHandler
        {
            取款时结算本金和利息 = 0,
            本金转定期_利息转零钱 = 1,
            本金和利息转定期账户 = 2,
            本金和利息转零钱账户 = 3
        }

        public enum CheckStatus
        {
            审核未通过 = -1,
            等待审核 = 0,
            审核通过 = 1
        }

        public enum Gender
        {
            保密 = -1,
            女 = 0,
            男 = 1
        }

        public enum Print_Type
        {
            活期存款 = 1,
            活期取款 = 2,
            定期存款 = 3,
            定期取款 = 4
        }
    }
}
