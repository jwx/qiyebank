﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace JiziService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: 在此处添加代码以启动服务。 
            Thread thread = new Thread(new ThreadStart(this.InitTimer));
            thread.IsBackground = true;
            thread.Start();
        }

        private string dbPath = Application.StartupPath.Replace("Debug", "App_Data\\jingyi.dll");

        /// <summary>
        /// 数据库备份方法
        /// </summary>
        private void DataBaseAutoBackup(string remark)
        {
            System.IO.File.Copy(dbPath, dbPath.Replace("App_Data", "database").Replace("jingyi.dll", "Bank" + DateTime.Now.ToString("yyyyMMddHHmmss") + (string.IsNullOrEmpty(remark) ? "" : remark) + ".db"));
        }

        private void InitTimer()
        {
            timer_Tick(null, null);
            System.Timers.Timer timer = new System.Timers.Timer(60 * 60000);//不能使用窗体的 Timer，他只能在窗体中使用，服务中无法使用，请使用 System.Timers.Timer类
            timer.Elapsed += timer_Tick;//到时间的时候执行事件
            timer.AutoReset = true;//设置是执行一次（false）还是一直执行(true)
            timer.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件 
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            //writeLogs(dbPath);
            try
            {
                Model.Setup modelSetup = new BLL.Setup().Get_Setup(true);
                if (modelSetup == null || modelSetup.isSaved == 0 || string.IsNullOrEmpty(modelSetup.dq_lilv)) return;//条件不符合，无法执行service
                if (modelSetup.hq_byDay != 2 && string.IsNullOrEmpty(modelSetup.hq_lilv)) return;//条件不符合，无法执行service

                DateTime dtNow = DateTime.Now;
                //if (modelSetup.backupTime == dtNow.Hour)
                //{
                //    DataBaseAutoBackup("到时自动备份");
                //}

                Model.ServiceLog modelServiceLog = new BLL.ServiceLog().GetLastRecord();//获取最后执行的记录 

                if (modelServiceLog != null && modelServiceLog.id != 0)//读取到了日志记录
                {
                    if (dtNow.ToString("yyyy-MM-dd") != modelServiceLog.addTime.ToString("yyyy-MM-dd"))//如果日志不是今天的
                    {
                        modelServiceLog = Execute_AddLog(dtNow);//新增今天的记录，返回今天的日志信息
                    }
                }
                else//没有读取到日志记录
                {
                    modelServiceLog = Execute_AddLog(dtNow);//新增今天的记录，返回今天的日志信息
                }

                if (modelServiceLog != null && modelServiceLog.id != 0)//今天的日志信息
                {
                    if (dtNow.ToString("yyyy-MM-dd") == modelServiceLog.addTime.ToString("yyyy-MM-dd"))//如果日志是今天的，ps：正常情况肯定为true
                    {
                        if (modelServiceLog.huoqiJiexi != 0 && modelServiceLog.dingqiZhuancun != 0 && modelServiceLog.daikuanHuankuan != 0)//如果今天的数据全部执行成功
                        {
                            return;
                        }

                        //if (modelServiceLog.isBackup == 0)//如果数据库备份不成功，执行备份数据库功能
                        //{
                        //    //Execute_Backup(modelServiceLog.id);
                        //}
                        if (modelServiceLog.huoqiJiexi == 0)
                        {
                            Execute_Huoqi(modelServiceLog.id, dtNow, modelSetup);
                        }
                        if (modelServiceLog.dingqiZhuancun == 0)
                        {
                            Execute_Dingqi(modelServiceLog.id, dtNow, modelSetup);
                        }
                        if (modelServiceLog.daikuanHuankuan == 0)
                        {
                            Execute_Daikuan(modelServiceLog.id, dtNow, modelSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                writeLogs(ex.ToString());
            }
        }

        protected override void OnStop()
        {

        }

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="dtNow"></param>
        /// <returns></returns>
        private Model.ServiceLog Execute_AddLog(DateTime dtNow)
        {
            int id = new BLL.ServiceLog().Add(dtNow, 0, 0, 0, null);
            if (id != 0)
            {
                return new Model.ServiceLog() { id = id, addTime = dtNow, isBackup = 0, huoqiJiexi = 0, dingqiZhuancun = 0 };
            }
            return null;
        }

        /// <summary>
        /// 执行数据库备份
        /// </summary>
        /// <param name="logId"></param>
        private void Execute_Backup(int logId)
        {
            return;
            int isBackup = 2;
            try
            {
                System.IO.File.Copy(dbPath, dbPath.Replace("App_Data\\jingyi.dll", "database\\bank" + DateTime.Now.ToString("yyyyMMddHHmmss") + "服务操作前备份.db"));
            }
            catch
            {
                isBackup = 0;
            }

            if (!new BLL.ServiceLog().Update(logId, "isBackup", isBackup, null))
            {
                writeLogs("数据库备份失败！");
            }
        }

        /// <summary>
        /// 检查活期
        /// </summary>
        /// <param name="logId"></param>
        /// <param name="dtNow"></param>
        /// <param name="modelSetup"></param>
        private void Execute_Huoqi(int logId, DateTime dtNow, Model.Setup modelSetup)
        {
            #region 溢家银行
            DateTime nowTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));  //当前时间
            string strDtNow = dtNow.ToString("MM-dd");
            if (strDtNow == "03-20" || strDtNow == "06-20" || strDtNow == "09-20" || strDtNow == "12-20")
            {
                //系统中所有的活期单据，单据的余额总和，存款时间为当前时间-上一季度时间（注意），最终每一笔利息=每一笔存款额*年利率/365*存款天数
                //    1000 本金 10利息   2000本金 20利息  3000本金 30利息  
                //    添加一条数据   60利息   更改本身总利息
                List<Model.Huoqi> listHuoqi_All = new BLL.Huoqi().GetList_Qukuan(true, null);//获取系统中所有的存款单
                if (listHuoqi_All != null && listHuoqi_All.Count != 0)
                {
                    List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(modelSetup.hq_lilv);
                    List<int> listUid = (from m in listHuoqi_All select m.uid).Distinct().ToList(); 

                    Database db = Tool.DatabaseHelper_Service.CreateDatabase();
                    using (var conn = db.CreateConnection())
                    {
                        bool res = true;
                        conn.Open();
                        DbTransaction tran = conn.BeginTransaction();

                        foreach (int uid in listUid)
                        {
                            decimal zonglixi = 0;
                            //当前人员的所有活期存款列表
                            List<Model.Huoqi> listHuoqi = (from m in listHuoqi_All where m.uid == uid select m).ToList();
                            for (int i = 0; i < listHuoqi.Count; i++)
                            {
                                decimal lixi = 0;
                                //List<Model.LilvRange> listLilvRange = Tool.JsonHelper.JsonDeserialize<List<Model.LilvRange>>(listHuoqi[i].lilv);
                                DateTime cunkuanTime = DateTime.Parse(listHuoqi[i].cunkuanTime); //获取存款时间
                                System.TimeSpan countTime = nowTime - cunkuanTime; //结算利息之后，第二次结算之间存款的时间
                                string strNowTime = DateTime.Now.Month.ToString();//当前月份

                                switch (strNowTime) //月份
                                {
                                    case "3":
                                        System.TimeSpan sumTime0 = nowTime - DateTime.Parse(DateTime.Now.AddYears(-1).ToString("yyyy") + "-12-20"); //得到去年至今年03-20 的天数
                                        if (countTime.TotalDays < sumTime0.TotalDays) //存款时间在两期结算之间
                                        {
                                            //不满一个季度
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(countTime.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        else
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(sumTime0.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        break;
                                    case "6":
                                        System.TimeSpan sumTime = nowTime - DateTime.Parse(DateTime.Now.Year + "-03-20"); //03-20至-06-20 的天数
                                        if (countTime.TotalDays < sumTime.TotalDays) //存款时间在两期结算之间
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(countTime.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        else
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(sumTime.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        break;
                                    case "9":
                                        System.TimeSpan sumTime1 = nowTime - DateTime.Parse(DateTime.Now.Year + "-06-20"); //-06-20 至09-20 的天数
                                        if (countTime.TotalDays < sumTime1.TotalDays) //存款时间在两期结算之间
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(countTime.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        else
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(sumTime1.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        break;
                                    case "12":
                                        System.TimeSpan sumTime2 = nowTime - DateTime.Parse(DateTime.Now.Year + "-09-20"); //09-20至12-20 天数
                                        if (countTime.TotalDays < sumTime2.TotalDays) //存款时间在两期结算之间
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(countTime.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        else
                                        {
                                            res = Settlement(listLilvRange, listHuoqi[i].jine, decimal.Parse(sumTime2.TotalDays.ToString()), listHuoqi[i].id, listHuoqi[i].uid, tran, listHuoqi[i].lilv, ref lixi);
                                        }
                                        break;
                                }
                                zonglixi += lixi;

                                if (res == false)
                                {
                                    writeLogs("活期：人员id：" + uid + "，单据id：" + listHuoqi[i].id + "结息失败！");
                                    break;
                                }
                            }

                            //当前人员所有的活期单据已经全部结息
                            if (res == false)
                            {
                                break;
                            }
                            else
                            {
                                //当前人员的总利息整体存入
                                res = new BLL.Huoqi().Add(uid, zonglixi, zonglixi, new Tool.OrderNo().GetOrderNo(), 1, 0, 1, 0, modelSetup.hq_lilv, 0M, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd"), -1, "零钱利息_系统自动存款", "", 0, "", "", 0, 0, true, tran) != 0;
                                if (res == false)
                                {
                                    writeLogs("活期：人员id：" + uid + "，利息整体结息失败！");
                                    break;
                                }
                            }
                        }

                        if (res)
                        {
                            tran.Commit();
                        }
                        else
                        {
                            tran.Rollback();
                        }
                    }
                }
            }
            #endregion

            #region 原本业务
            /*
            if (modelSetup.hq_autoCalc != 0 && (modelSetup.days == dtNow.Day || modelSetup.hq_byDay == 2))//活期自动结算，且为结算天数，或者每天结算
            {
                List<Model.Huoqi> listHuoqi = new BLL.Huoqi().GetList_Qukuan(true, null);//获取系统中所有的存款单
                if (listHuoqi != null && listHuoqi.Count != 0)
                {
                    Database db = Tool.DatabaseHelper_Service.CreateDatabase();
                    using (var conn = db.CreateConnection())
                    {
                        conn.Open();
                        DbTransaction tran = conn.BeginTransaction();

                        string msg = string.Empty;
                        if (modelSetup.hq_byDay == 1)//按天计算
                        {
                            if (!new BLL.Huoqi().ServiceAutoLixi(true, listHuoqi, dtNow, modelSetup, tran, ref msg))//结息是否成功
                            {
                                tran.Rollback();
                                writeLogs("活期：" + msg);
                                return;
                            }
                            if (new BLL.ServiceLog().Update(logId, "huoqiJiexi", 2, tran))//设置结息成功
                            {
                                tran.Commit();
                                return;
                            }
                            tran.Rollback();
                            writeLogs("活期结息失败！");
                        }
                        else//每天计算
                        {
                            if (!new BLL.Huoqi().ServiceAutoLixi_Everyday(true, listHuoqi, dtNow, modelSetup, tran, ref msg))
                            {
                                tran.Rollback();
                                writeLogs("活期：" + msg);
                                return;
                            }
                            if (new BLL.ServiceLog().Update(logId, "huoqiJiexi", 2, tran))//设置结息成功
                            {
                                tran.Commit();
                                return;
                            }
                            tran.Rollback();
                            writeLogs("活期结息失败！");
                        }
                    }
                }
            }
            else//非结算天数
            {
                new BLL.ServiceLog().Update(logId, "huoqiJiexi", 1, null);
            }
            */
            #endregion
        }

        private bool Settlement(List<Model.LilvRange> listLilvRange, decimal jine, decimal days, int id, int uid, DbTransaction tran, string lilv, ref decimal lixi) //结算
        {
            lixi = 0;
            if (listLilvRange.Count > 0)
            {
                foreach (Model.LilvRange one in listLilvRange)
                {
                    lixi = decimal.Round(jine * (one.lilv / 100) / 365 * days, 2); //得到利息
                }
            }
            //int res = new BLL.Huoqi().Add(uid, lixi, lixi, new Tool.OrderNo().GetOrderNo(), 1, 0, 1, 0, lilv, 0, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), -1, "零钱利息_系统自动存款", null, 0, null, null, 0, 0);
            //if (res > 0)
            //{
            if (!new BLL.Huoqi().Update_LiXi(id, lixi, tran))
            {
                //tran.Rollback();
                writeLogs("活期：结算更改利息失败");
                return false;
            }
            else
            {
                return true;
                //tran.Commit();
            }
            //}
            //else
            //{
            //    //tran.Rollback();
            //    writeLogs("活期：结算利息失败");
            //    return false;
            //}

        }

        private void Execute_Dingqi(int logId, DateTime dtNow, Model.Setup modelSetup)
        {
            List<Model.Dingqi> listDingqi = new BLL.Dingqi().GetList_Qukuan(true, dtNow, null);//获取系统中所有的存款单
            if (listDingqi != null && listDingqi.Count != 0)
            {
                Database db = Tool.DatabaseHelper_Service.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    string msg = string.Empty;
                    if (!new BLL.Dingqi().Zhuancun_Auto(true, listDingqi, dtNow, modelSetup, tran, ref msg))//结息是否成功
                    {
                        tran.Rollback();
                        writeLogs("定期：" + msg);
                        return;
                    }
                    if (new BLL.ServiceLog().Update(logId, "dingqiZhuancun", 2, tran))//设置结息成功
                    {
                        tran.Commit();
                        return;
                    }
                    tran.Rollback();
                    writeLogs("定期转存失败！");
                }
            }
            else
            {
                new BLL.ServiceLog().Update(logId, "dingqiZhuancun", 1, null);
            }
        }

        private void Execute_Daikuan(int logId, DateTime dtNow, Model.Setup modelSetup)
        {
            List<Model.DaikuanPayback> listDaikuan = new BLL.DaikuanPayback().GetList_NotPay(true, dtNow);//获取系统中所有的需要还款的数据
            if (listDaikuan != null && listDaikuan.Count != 0)
            {
                Database db = Tool.DatabaseHelper_Service.CreateDatabase();
                using (var conn = db.CreateConnection())
                {
                    conn.Open();
                    DbTransaction tran = conn.BeginTransaction();

                    string msg = string.Empty;
                    if (!new BLL.Daikuan().DaikuanPayback_Auto(true, listDaikuan, dtNow, modelSetup, tran, ref msg))//结息是否成功
                    {
                        tran.Rollback();
                        writeLogs("贷款还款日志：" + msg);
                        return;
                    }
                    if (new BLL.ServiceLog().Update(logId, "daikuanHuankuan", 2, tran))//设置结息成功
                    {
                        tran.Commit();
                        return;
                    }
                }
            }
            else
            {
                new BLL.ServiceLog().Update(logId, "daikuanHuankuan", 1, null);
            }
        }

        public void writeLogs(string logs)
        {
            try
            {
                //debug==================================================
                //StreamWriter dout = new StreamWriter(@"c:\" + System.DateTime.Now.ToString("yyyMMddHHmmss") + ".txt");
                StreamWriter dout = new StreamWriter(Application.StartupPath + "\\_Logs.txt", true);
                dout.Write(System.DateTime.Now.ToString("yyy-MM-dd HH:mm:ss") + "：" + logs + "\r\n");
                //debug==================================================
                dout.Close();
            }
            catch (Exception ex)
            {
                StreamWriter dout = new StreamWriter(Application.StartupPath + "\\_Logs.txt", true);
                dout.Write("异常时间：" + ex.Message);
                dout.Close();
            }
        }
    }
}
